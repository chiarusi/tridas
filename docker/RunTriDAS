#!/bin/sh -e

tmp_dir=/tmp/tridas

if [ ! -d "${tmp_dir}" ]; then
    mkdir -p ${tmp_dir}
elif [ "$1" = "--create-dir" ]; then
    mv ${tmp_dir} ${tmp_dir}~
    mkdir -p ${tmp_dir}
fi

if [ ! -d "${tmp_dir}" ]; then
  exit 1
fi
  
log_dir=${tmp_dir}/log
shared_dir=${tmp_dir}/shared

mkdir -p ${log_dir} ${shared_dir}

cp fcm.ini.template 6pmt_acquisition.dat ${shared_dir}

docker-compose build
docker-compose up -d
docker-compose ps

ip_address() {
    local cont_
    cont_=$(docker-compose ps -q $1)
    if [ -n "${cont_}" ]; then
	net_=$(docker inspect -f {{.HostConfig.NetworkMode}} ${cont_})
	[ -n ${net_} ] && docker inspect -f {{.NetworkSettings.Networks.${net_}.IPAddress}} ${cont_}
    fi
}

fcm_ip=$(ip_address fcm)
hm_ip=$(ip_address hm)
tcpu_ip=$(ip_address tcpu)
em_ip=$(ip_address em)
tsv_ip=$(ip_address tsv)

should_exit=0

if [ -z ${fcm_ip} ]; then
    >&2 echo "FCM not running, exiting..."
    should_exit=1
fi
if [ -z ${hm_ip} ]; then
    >&2 echo "HM not running, exiting..."
    should_exit=1
fi
if [ -z ${tcpu_ip} ]; then
    >&2 echo "TCPU not running, exiting..."
    should_exit=1
fi
if [ -z ${em_ip} ]; then
    >&2 echo "EM not running, exiting..."
    should_exit=1
fi
if [ -z ${tsv_ip} ]; then
    >&2 echo "TSV not running, exiting..."
    should_exit=1
fi

if [ ${should_exit} -eq 1 ]; then
    docker-compose stop
    docker-compose rm -v
    exit 1
fi

cat configuration.json.template | jq \
".FCM_ENDPOINTS[0].DATA_HOST = \"${fcm_ip}\""\
"| .HM.HOSTS[0].CTRL_HOST = \"${hm_ip}\""\
"| .TCPU.HOSTS[0].CTRL_HOST = \"${tcpu_ip}\""\
"| .TCPU.HOSTS[0].DATA_HOST = \"${tcpu_ip}\""\
"| .TSV.CTRL_HOST = \"${tsv_ip}\""\
"| .EM.CTRL_HOST = \"${em_ip}\""\
"| .EM.DATA_HOST = \"${em_ip}\"" > ${shared_dir}/configuration.json

echo "Configuration file is ${shared_dir}/configuration.json"
