# HowTo run the whole TriDAS in containers

This is a preliminary set of instructions to run the whole TriDAS in a set of Docker containers
on a single machine.

The instruction should be applied from the source root directory after TriDAS has been
successfully packaged.

```
# mkdir -p ../tridas_docker
# rm -rf ../tridas_docker/*
# cp -a DAQ/TSC/docker/* ../tridas_docker
# cp build/TriDAS-0.1.1-Linux.tar.gz ../tridas_docker/tridas.tgz
# cp datacards/6pmt_acquisition.dat ../tridas_docker
# cd ../tridas_docker/
# docker-compose build
# ./RunTriDAS
```
