#ifndef TRIDAS_PACKAGES_DATA_CONSISTENCY_HPP
#define TRIDAS_PACKAGES_DATA_CONSISTENCY_HPP

#include <cassert>
#include "f_dataformat_p3.hpp"
#include <cstdio>

namespace tridas {
namespace test {

#define check_buffer(buffer, size) do {                                           \
  char const* pos = buffer;                                                       \
  int count = 0;                                                                  \
  while (pos < buffer + size) {                                                   \
    phase3::DataFrameHeader const*const head = phase3::dataframeheader_cast(pos); \
    /* here are the dfh invariants (at least for simulated data) */               \
    if (phase3::compressed(*head) || ! phase3::testDFHSync(*head)) {              \
      std::fprintf(stderr, "frame count: %d ", count);                            \
      assert(! phase3::compressed(*head));                                        \
      assert(phase3::testDFHSync(*head));                                         \
    }                                                                             \
    pos += sizeof(*head) + phase3::getDFHPayloadSize(*head);                      \
    ++count;                                                                      \
  }                                                                               \
} while (0)

} // ns test
} // ns tridas

#endif // TRIDAS_PACKAGES_DATA_CONSISTENCY_HPP
