#include "data_consistency.hpp"
#include <fstream>
#include <iostream>

using namespace tridas;

int main(int argc, char* argv[]) {
  assert(argc == 2 && "You must specify a valid file name as the first command line argument.");

  std::ifstream data(argv[1]);

  std::size_t const buffer_size = 4096;
  char buffer[buffer_size];
  memset(buffer, 255, buffer_size);

  data.read(buffer, buffer_size);

  check_buffer(buffer, buffer_size);

  std::cout << "First check passed. The next one must fail at the 3th frame.\n";

  {
    phase3::DataFrameHeader* tmphead = phase3::dataframeheader_cast(buffer);

    tmphead = phase3::dataframeheader_cast(((char*) tmphead) + sizeof(*tmphead) + getDFHPayloadSize(*tmphead));
    tmphead = phase3::dataframeheader_cast(((char*) tmphead) + sizeof(*tmphead) + getDFHPayloadSize(*tmphead));

    tmphead->SyncBytes = 7;

  }

  check_buffer(buffer, buffer_size);
}
