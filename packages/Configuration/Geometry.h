#ifndef PACKAGES_CONFIGURATION_GEOMETRY_H
#define PACKAGES_CONFIGURATION_GEOMETRY_H

#include <vector>
#include <string>

struct Calibration
{
  size_t time_offset;
  float pedestal;
  float threshold;
  Calibration(size_t time_offset, float pedestal, float threshold) :
      time_offset(time_offset), pedestal(pedestal), threshold(threshold)
  {
  }
};

struct Position
{
  float x;
  float y;
  float z;
  float cx;
  float cy;
  float cz;
  Position(float x, float y, float z, float cx, float cy, float cz) :
      x(x), y(y), z(z), cx(cx), cy(cy), cz(cz)
  {
  }
};

struct Geometry
{
  size_t pmts_per_floor;
  size_t floors_per_tower;
  size_t towers;
  std::vector<Calibration> calibrations;
  std::vector<Position> positions;
};

#endif
