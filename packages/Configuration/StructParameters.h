#ifndef PACKAGES_CONFIGURATION_STRUCTPARAMETERS_H
#define PACKAGES_CONFIGURATION_STRUCTPARAMETERS_H

#include <vector>
#include <string>

struct StructParameters
{
  boost::chrono::milliseconds delta_timeslice;
  size_t pmt_buffer_size;
  int sts_ready_timeout;
  int tts_ready_timeout;
  size_t sts_in_memory;
};

#endif
