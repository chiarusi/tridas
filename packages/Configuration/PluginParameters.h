#ifndef PACKAGES_CONFIGURATION_PLUGINPARAMETERS_H
#define PACKAGES_CONFIGURATION_PLUGINPARAMETERS_H

#include <string>

#include "Configuration.h"

struct PluginParameters{
  std::string label;
  int id;
  std::string name;
  Configuration parameters;
};

#endif
