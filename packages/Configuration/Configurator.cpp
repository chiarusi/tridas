#include "Configurator.h"

#include <fstream>
#include <sstream>
#include <exception>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/chrono.hpp>

#include "Geometry.h"
#include "HMParameters.h"
#include "TCPUParameters.h"
#include "TriggerParameters.h"
#include "PluginParameters.h"
#include "StructParameters.h"
#include "TSVParameters.h"
#include "Configuration.h"
#include "Endpoint.h"
#include "MonitorParameters.h"

Geometry get_geometry_parameters(Configuration const& configuration) {
  Geometry geometry;
  geometry.pmts_per_floor = configuration.get<size_t>("DETECTOR_GEOMETRY.PMTS");
  geometry.floors_per_tower = configuration.get<size_t>(
      "DETECTOR_GEOMETRY.FLOORS");
  geometry.towers = configuration.get<size_t>("DETECTOR_GEOMETRY.TOWERS");

  for (size_t t = 0; t < geometry.towers; ++t) {
    Configuration pt_tower = configuration.get_child(
        "TOWER_" + boost::lexical_cast<std::string>(t));
    for (size_t f = 1; f <= geometry.floors_per_tower; ++f) {
      Configuration pt_floor = pt_tower.get_child(
          "FLOOR_" + boost::lexical_cast<std::string>(f));
      for (size_t p = 0; p < geometry.pmts_per_floor; p++) {
        Configuration pt_pmt = pt_floor.get_child(
            "PMT_" + boost::lexical_cast<std::string>(p));
        geometry.calibrations.push_back(
            Calibration(pt_pmt.get<size_t>("TIME_OFFSET"),
                        pt_pmt.get<float>("PEDESTAL"),
                        pt_pmt.get<float>("THRESHOLD")));
        geometry.positions.push_back(
            Position(pt_pmt.get<float>("X"), pt_pmt.get<float>("Y"),
                     pt_pmt.get<float>("Z"), pt_pmt.get<float>("CX"),
                     pt_pmt.get<float>("CY"), pt_pmt.get<float>("CZ")));
      }
    }
  }
  return geometry;
}

HMParameters get_hm_parameters(Configuration const& configuration) {
  HMParameters hm;
  Configuration const& hm_hosts = configuration.get_child("HM.HOSTS");
  hm.count = 0;
  for (Configuration::const_iterator it = hm_hosts.begin(), e = hm_hosts.end();
      it != e; ++it) {
    hm.count += it->second.get<size_t>("N_INSTANCES");
  }
  assert(hm.count != 0);
  return hm;
}

std::vector<Endpoint> get_hm_control_endpoints(
    Configuration const& configuration) {
  std::vector<Endpoint> endpoints;
  size_t base_port = configuration.get<size_t>("HM.BASE_CTRL_PORT");
  Configuration const& hosts = configuration.get_child("HM.HOSTS");
  for (Configuration::const_iterator it = hosts.begin(), e = hosts.end();
      it != e; ++it) {
    size_t const n_instances = it->second.get<size_t>("N_INSTANCES");
    for (size_t i = 0; i != n_instances; ++i) {
      endpoints.push_back(
          Endpoint(it->second.get<std::string>("CTRL_HOST"),
                   boost::lexical_cast<std::string>(base_port + i)));
    }
  }
  return endpoints;
}

TCPUParameters get_tcpu_parameters(Configuration const& configuration) {
  TCPUParameters tcpu;
  Configuration const& tcpu_hosts = configuration.get_child("TCPU.HOSTS");
  tcpu.count = 0;
  for (Configuration::const_iterator it = tcpu_hosts.begin(), e =
      tcpu_hosts.end(); it != e; ++it) {
    tcpu.count += it->second.get<size_t>("N_INSTANCES");
  }
  assert(tcpu.count != 0);
  tcpu.plugins_directory = configuration.get<boost::filesystem::path>(
      "TCPU.PLUGINS_DIR");
  tcpu.parallel_tts = configuration.get<size_t>("TCPU.PARALLEL_TTS");
  return tcpu;
}

std::vector<Endpoint> get_tcpu_control_endpoints(
    Configuration const& configuration) {
  std::vector<Endpoint> endpoints;
  size_t base_port = configuration.get<size_t>("TCPU.BASE_CTRL_PORT");
  Configuration const& hosts = configuration.get_child("TCPU.HOSTS");
  for (Configuration::const_iterator it = hosts.begin(), e = hosts.end();
      it != e; ++it) {
    size_t const n_instances = it->second.get<size_t>("N_INSTANCES");
    for (size_t i = 0; i != n_instances; ++i) {
      endpoints.push_back(
          Endpoint(it->second.get<std::string>("CTRL_HOST"),
                   boost::lexical_cast<std::string>(base_port + i)));
    }
  }
  return endpoints;
}

std::vector<Endpoint> get_tcpu_data_endpoints(
    Configuration const& configuration) {
  std::vector<Endpoint> endpoints;
  size_t base_port = configuration.get<size_t>("TCPU.BASE_DATA_PORT");
  Configuration const& hosts = configuration.get_child("TCPU.HOSTS");
  for (Configuration::const_iterator it = hosts.begin(), e = hosts.end();
      it != e; ++it) {
    size_t const n_instances = it->second.get<size_t>("N_INSTANCES");
    for (size_t i = 0; i != n_instances; ++i) {
      endpoints.push_back(
          Endpoint(it->second.get<std::string>("DATA_HOST"),
                   boost::lexical_cast<std::string>(base_port + i)));
    }
  }
  return endpoints;
}

TSVParameters get_tsv_parameters(Configuration const& configuration)
{
  TSVParameters tsv;
  tsv.hms = get_hm_control_endpoints(configuration);
  tsv.tcpus = get_tcpu_control_endpoints(configuration);

  StructParameters const parameters = get_struct_parameters(configuration);

  // Get the start time in seconds (since 2014-1-1)
  int64_t start_time = configuration.get<int64_t>("START_TIME");
  assert(start_time > 0);
  // Multiply the start time in second per te number of ts in one second
  // in order to compute the first ts to acquire
  tsv.start_timeslice_id = start_time * 1000. / parameters.delta_timeslice.count();
  return tsv;
}

TriggerParameters get_trigger_parameters(Configuration const& configuration) {
  TriggerParameters trigger;
  Configuration const& conf_trigger = configuration.get_child(
      "TCPU.TRIGGER_PARAMETERS");
  trigger.l1_event_window_half_size = conf_trigger.get<fine_time>(
      "L1_EVENT_WINDOW_HALF_SIZE");
  trigger.l1_delta_time_sc = conf_trigger.get<fine_time>("L1_DELTA_TIME_SC");
  trigger.l1_delta_time_fc = conf_trigger.get<fine_time>("L1_DELTA_TIME_FC");
  trigger.l1_charge_threshold = conf_trigger.get<size_t>("L1_CHARGE_THRESHOLD");
  trigger.l1_flag_rt = conf_trigger.get<bool>("L1_FLAG_RT");
  trigger.l1_frequency_rt = conf_trigger.get<size_t>("L1_FREQUENCY_RT");
  trigger.l1_delta_time_rt = conf_trigger.get<fine_time>("L1_DELTA_TIME_RT");
  return trigger;
}

std::vector<PluginParameters> get_plugins_parameters(
  Configuration const& configuration) {
  std::vector<PluginParameters> plugins;
  Configuration const& conf = configuration.get_child("TCPU.PLUGINS");
  for (Configuration::const_iterator it = conf.begin(), e = conf.end();
      it != e; ++it) {
    PluginParameters p;
    p.label = it->first;
    p.id = it->second.get<int>("ID");
    p.name = it->second.get<std::string>("NAME");
    p.parameters = it->second.get_child("PARAMETERS");
    plugins.push_back(p);
  }
  return plugins;
}

StructParameters get_struct_parameters(Configuration const& configuration) {
  StructParameters params;
  params.delta_timeslice = configuration.get<boost::chrono::milliseconds>(
      "INTERNAL_SW_PARAMETERS.DELTA_TS");
  params.pmt_buffer_size = configuration.get<size_t>(
      "INTERNAL_SW_PARAMETERS.PMT_BUFFER_SIZE");
  params.sts_ready_timeout = configuration.get<int>(
      "INTERNAL_SW_PARAMETERS.STS_READY_TIMEOUT");
  params.tts_ready_timeout = configuration.get<int>(
      "INTERNAL_SW_PARAMETERS.TTS_READY_TIMEOUT");
  params.sts_in_memory = configuration.get<size_t>(
      "INTERNAL_SW_PARAMETERS.STS_IN_MEMORY");

  if (params.delta_timeslice <= boost::chrono::milliseconds::zero()) {
    throw std::runtime_error(
        "Configuration error: timeslice duration is not a positive number"
    );
  }

  return params;
}

MonitorParameters get_monitor_parameters(Configuration const& configuration)
{
  MonitorParameters monitor;
  monitor.hostname = configuration.get<std::string>("MONITOR.CTRL_HOST");
  monitor.port = configuration.get<int>("MONITOR.CTRL_PORT");
  monitor.time_interval = configuration.get<boost::chrono::seconds>("MONITOR.TIME_INTERVAL");
  return monitor;
}

std::vector<Endpoint> get_fcm_endpoints(Configuration const& configuration) {
  std::vector<Endpoint> endpoints;
  Configuration const& conf_fcm = configuration.get_child("FCM_ENDPOINTS");
  for (Configuration::const_iterator it = conf_fcm.begin(), e = conf_fcm.end();
      it != e; ++it) {
    endpoints.push_back(
        Endpoint(it->second.get<std::string>("DATA_HOST"),
                 it->second.get<std::string>("DATA_PORT")));
  }
  return endpoints;
}
