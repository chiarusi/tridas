#ifndef PACKAGES_CONFIGURATION_TSVPARAMETERS_H
#define PACKAGES_CONFIGURATION_TSVPARAMETERS_H

#include "Endpoint.h"
#include "tridas_dataformat.hpp"

struct TSVParameters
{
  std::vector<Endpoint> hms;
  std::vector<Endpoint> tcpus;
  TS_t start_timeslice_id;
};

#endif
