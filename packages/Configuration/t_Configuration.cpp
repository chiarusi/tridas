#include <cassert>
#include <fstream>
#include <utility>
#include <cstdlib>
#include <boost/thread.hpp>  // just to avoid linking errors (it's a mistery!)
#include <boost/detail/lightweight_test.hpp>

#include "Configuration.h"

template<typename T, typename U>
std::ostream& operator<<(std::ostream& os, std::pair<T, U> const& entry)
{
  return std::cout << entry.first << " = " << entry.second;
}

int main(int argc, char* argv[])
{
  assert(argc == 2 && "usage: t_Configuration <ini-file>");
  std::ifstream f(argv[1]);
  if (!f) {
    std::cerr << argv[1] << ": No such file or directory\n";
    return EXIT_FAILURE;
  }

  Configuration configuration = read_configuration(f);

  std::cout << "*** Configuration dump ***\n\n" << configuration << '\n';

  {  // ok, the entry is before any section
    std::string const key("KEY1");
    int const expected = 1;
    BOOST_TEST_EQ(configuration.get<int>(key), expected);
  }
  {
    std::string const key("HM.NPMTFLOOR");
    int const expected = 6;
    BOOST_TEST_EQ(configuration.get<int>(key), expected);
  }
  {  // missing section
    std::string const key("NPMTFLOOR");
    BOOST_TEST_THROWS(configuration.get<int>(key), Configuration_error)
  }
  {
    std::string const key("HM.DISPATCHER");
    std::string const expected("131.154.90.28");
    BOOST_TEST_EQ(configuration.get<std::string>(key), expected);
  }
  {  // case mismatch
    std::string const key("HM.A_DUMP_FILENAME");
    BOOST_TEST_THROWS(configuration.get<int>(key), Configuration_error)
  }
  {
    std::string const key("TCPU.tcpu00");
    std::string const expected("8777 localhost");
    BOOST_TEST_EQ(configuration.get<std::string>(key), expected);
  }
  {
    std::string const section("HM");
    size_t const expected(4);
    BOOST_TEST_EQ(configuration.get_child(section).size(), expected);
  }


  boost::report_errors();
}
