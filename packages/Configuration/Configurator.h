#ifndef PACKAGES_CONFIGURATION_CONFIGURATOR_H
#define PACKAGES_CONFIGURATION_CONFIGURATOR_H

#include <vector>

#include "Configuration.h"

class Geometry;
Geometry get_geometry_parameters(Configuration const& configuration);

class HMParameters;
HMParameters get_hm_parameters(Configuration const& configuration);

class TCPUParameters;
TCPUParameters get_tcpu_parameters(Configuration const& configuration);

class TSVParameters;
TSVParameters get_tsv_parameters(Configuration const& configuration);

class TriggerParameters;
TriggerParameters get_trigger_parameters(Configuration const& configuration);

class PluginParameters;
std::vector<PluginParameters> get_plugins_parameters(Configuration const& configuration);

class StructParameters;
StructParameters get_struct_parameters(Configuration const& configuration);

class MonitorParameters;
MonitorParameters get_monitor_parameters(Configuration const& configuration);

class Endpoint;
std::vector<Endpoint> get_hm_control_endpoints(Configuration const& configuration);
std::vector<Endpoint> get_tcpu_control_endpoints(Configuration const& configuration);
std::vector<Endpoint> get_tcpu_data_endpoints(Configuration const& configuration);
std::vector<Endpoint> get_fcm_endpoints(Configuration const& configuration);

#endif
