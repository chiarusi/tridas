#ifndef PACKAGES_CONFIGURATION_MONITORPARAMETERS_H
#define PACKAGES_CONFIGURATION_MONITORPARAMETERS_H

#include <string>
#include <boost/asio/high_resolution_timer.hpp>

struct MonitorParameters
{
  std::string hostname;
  int port;
  boost::asio::high_resolution_timer::duration time_interval;
};

#endif

