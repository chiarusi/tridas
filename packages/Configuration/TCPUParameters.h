#ifndef PACKAGES_CONFIGURATION_TCPUPARAMETERS_H
#define PACKAGES_CONFIGURATION_TCPUPARAMETERS_H

#include <vector>
#include <string>

#include <boost/filesystem.hpp>

struct TCPUParameters
{
  size_t count;
  boost::filesystem::path plugins_directory;
  size_t parallel_tts;
};

#endif
