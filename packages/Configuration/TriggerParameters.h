#ifndef PACKAGES_CONFIGURATION_TRIGGERPARAMETERS_H
#define PACKAGES_CONFIGURATION_TRIGGERPARAMETERS_H

#include <vector>
#include <string>
#include "tridas_dataformat.hpp"

struct TriggerParameters
{
  bool l1_flag_rt;
  fine_time l1_event_window_half_size;
  fine_time l1_delta_time_sc;
  fine_time l1_delta_time_fc;
  size_t l1_charge_threshold;
  size_t l1_frequency_rt;
  fine_time l1_delta_time_rt;
};

#endif
