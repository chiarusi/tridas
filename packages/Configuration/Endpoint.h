#ifndef PACKAGES_CONFIGURATION_ENDPOINT_H
#define PACKAGES_CONFIGURATION_ENDPOINT_H

#include <string>
#include <vector>

struct Endpoint {
  std::string hostname;
  std::string port;

  Endpoint(std::string const& hostname, std::string const& port)
      :
        hostname(hostname),
        port(port) {
  }
};

#endif
