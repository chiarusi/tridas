#ifndef PACKAGES_CONFIGURATION_CONFIGURATION_H
#define PACKAGES_CONFIGURATION_CONFIGURATION_H

#include <iosfwd>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <boost/filesystem.hpp>

typedef boost::property_tree::ptree Configuration;
typedef boost::property_tree::ptree_error Configuration_error;

Configuration read_configuration(boost::filesystem::path const& file);
Configuration read_configuration(std::istream& is);

Configuration read_ini_configuration(boost::filesystem::path const& file);
Configuration read_ini_configuration(std::istream& is);

std::ostream& operator<<(std::ostream& os, Configuration const& configuration);

#endif
