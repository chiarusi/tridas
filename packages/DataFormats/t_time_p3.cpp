#include "f_dataformat_p3.hpp"
#include <boost/detail/lightweight_test.hpp>

using namespace tridas;

int main()
{
  {
    phase3::DataFrameHeader dfh;

    for (unsigned gregorian = 0; gregorian < 367; ++gregorian) {
      phase3::setDFHDays(gregorian, dfh);
      BOOST_TEST_EQ(phase3::getDFHDays(dfh), gregorian);
    }
  }

  {
    phase3::DataFrameHeader dfh;

    for (unsigned year = 0; year < 100; ++year) {
      phase3::setDFHYears(year, dfh);
      BOOST_TEST_EQ(phase3::getDFHYears(dfh), year);
    }

    // Something that I know ;)
    phase3::setDFHYears(14, dfh);

    BOOST_TEST_EQ(dfh.Years, 20);
  }

  {
    phase3::DataFrameHeader dfh;

    phase3::fine_time const t(94176000000000000);
    setDFHFullTime(t, dfh);

    BOOST_TEST_EQ(getDFHFullTime(dfh), t);
  }

  boost::report_errors();
}
