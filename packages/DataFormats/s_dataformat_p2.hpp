#ifndef PACKAGES_DATAFORMATS_S_DATAFORMAT_P2_HPP
#define PACKAGES_DATAFORMATS_S_DATAFORMAT_P2_HPP

#include <cstdlib>

namespace phase2 {

// The following variables was previously #define

const unsigned int VERSION_DATAFORMAT = 2013042700;

const unsigned int NPMTTOWER_MAX = 64; // MAGIC NUMBER

const unsigned int MAX_PLUGINS_NUMBER  = 8;
const unsigned int MAX_TRIGGERS_NUMBER = 6;
const unsigned int L1TOTAL_ID = 0;

const unsigned int L1Q_ID  = 1;
const unsigned int L1SC_ID = 2;
const unsigned int L1FC_ID = 3;
const unsigned int L1RT_ID = 4;
const unsigned int L1SH_ID = 5;

//Frame Header definition
struct DataFrameHeader
{
  unsigned int T10ns :16;  // [10 ns] precise time from FEM counter
  unsigned int PMTID :4; // PMT ID (0-3)
  unsigned int EFCMID :5; // EFCM ID
  unsigned int TowerID :7; // TowerID

  unsigned int T500us :11;  // [500 us] 500us counter from GPS
  unsigned int Seconds :17; //  [sec] seconds from GPS
  unsigned int Days :4;  // [days] (days since the beginning of the year)%10

  unsigned int NDataSample :6; // n data samples
  unsigned int Dummy :1;
  unsigned int FifoFull :1; // 1(0) if fifo (not) full
  unsigned int Charge :21; // [adc] uncalibrated charge
  unsigned int ParityFlag :1; // to push the time resolution to 5ns: if  1  add + 5ns
  unsigned int FragFlag :1; // 0 : start-frame of the hit , 1 a fragment frame
  unsigned int ZipFlag :1; // compression flag

  DataFrameHeader() :
    T10ns(0), PMTID(0), EFCMID(0), TowerID(0), T500us(0),
    Seconds(0), Days(0), NDataSample(0), Dummy(0),
    FifoFull(0), Charge(0), ParityFlag(0), FragFlag(0), ZipFlag(0)
  {
  }
};


typedef unsigned int TS_t;
typedef size_t TcpuId;

// Sample D-Word
struct Samples
{
  unsigned int samp3 :8;
  unsigned int samp2 :8;
  unsigned int samp1 :8;
  unsigned int samp0 :8;
  Samples(unsigned int samp1, unsigned int samp0):
    samp3(samp1 >> 8), samp2(samp1), samp1(samp0 >> 8), samp0(samp0)
  {
  }
};

/*
struct TimeFormat
{
  unsigned int T500us :11;
  unsigned int Seconds :17;
  unsigned int Days :4;
};
*/

// =============================================
/*

 PT Header Generale
 /---- TimeSlice 1
 /-------- TriggeredEvent 1
 /------------DataFrame 1
 /------------DataFrame 2
 /------------DataFrame ...
 /------------DataFrame n
 /-------- TriggeredEvent 2
 /------------DataFrame 1
 /------------DataFrame 2
 /------------DataFrame ...
 /------------DataFrame n

 /---- TimeSlice 2
 /-------- TriggeredEvent 1
 /------------DataFrame 1
 /------------DataFrame 2
 /------------DataFrame ...
 /------------DataFrame n
 /-------- TriggeredEvent 2
 /------------DataFrame 1
 /------------DataFrame 2
 /------------DataFrame ...
 /------------DataFrame n

 ...



 */

struct s_PTGeneralHeaderInfo
{
  unsigned int VersionPTFile :32; // Tag of the PTGeneralHeaderInfo. It is the release tag of the PT dataformat (eg: 2013040701,yyymmddrr)
  unsigned int RunNumber :32; // the Run Number
  unsigned long long int StartTime5ns :64; // Start time in 5ns
  /*
  unsigned int StartYear :32; // the Year of the run-start time
  unsigned int StartWrap :32; //  the Wrap of the run-start time (who many "ten days" since the beginning of the year)
  unsigned int StartDays :32; // the Days info of the run-start time
  unsigned int StartSeconds :32; //  the Seconds of the run-start time
  unsigned int StartT500us :32;  //[500us]  the T500us of the run-start time
  unsigned int StartT10ns :32;   //[10ns]  the 10ns of the run-start time
  */
  unsigned int MaxFileSize :32; // the maximum  size for a pt-file
  unsigned int EffectiveFileSize :32; // the effective size of this very pt-file
  unsigned int FileNumber :32; // the File number
  unsigned int TotEventsInFile :32; // the total amount of Events in this file
  unsigned int NPMTperTower :32; // the total number of PMTs in the tower
  unsigned int TimeOffset[NPMTTOWER_MAX]; // [ns] Time offsets corrections
  unsigned int PedestalA[NPMTTOWER_MAX]; // *1000 and converted from float to int - pedestal to be subtracted to odd samples
  unsigned int PedestalB[NPMTTOWER_MAX]; // *1000 and converted from float to int - pedestal to be subtracted to even samples
  unsigned int QThreshold[NPMTTOWER_MAX]; // *1000 and converted from float to int - minimum requested (and calibrated) charge to be accepted ofr a trigger
};

struct s_WrittenTSHeaderInfo
{
  TS_t TS_ID :32; // ID number of the TimeSlice(it is univocal for the present Run)
  unsigned int NEvents :32; // Number of triggered events selected in the present TimeSlice
  size_t TS_size :32; // size of the whole TS (writtenTSHeaderInfo+events payload)
  int is_not_complete :32; // >0 if something went wrong and only a fraction of the selected events are stored
};



struct TEHeaderInfo
{ // this is filled e.g in the TTSBuild

  unsigned int EventTag :32; // Tag for HeaderE = 12081972
  unsigned int EventID :32; //ID - to be set by the EM
  unsigned int EventL :32; //total length of the TE (header included)
  unsigned int nHit :32; // n tot. hits

  unsigned long long int StartTime5ns :64; // Start time in 5ns

  //unsigned int Wraps :32; // wrap index

  /*
  unsigned int Days :32; // time info of the first seed
  unsigned int Seconds :32; // time info of the first seed
  unsigned int T500us :32; // time info of the first seed
  unsigned int T10ns :32; // time info of the first seed
  unsigned int TSCompleted :32; // flag for event belonging to an (un)completed (0)1 TS
  */

  unsigned int nseeds[MAX_TRIGGERS_NUMBER]; // array that store the number of seeds per trigger type
  unsigned int plugin_trigtype[MAX_PLUGINS_NUMBER]; // array - seed-found flags for each kind of plugin
  unsigned int plugin_nseeds[MAX_PLUGINS_NUMBER]; // array - nseeds for each kind of plugin

};

}

#endif
