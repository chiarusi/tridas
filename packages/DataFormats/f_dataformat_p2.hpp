#ifndef PACKAGES_DATAFORMATS_F_DATAFORMAT_P2_HPP
#define PACKAGES_DATAFORMATS_F_DATAFORMAT_P2_HPP

#include "s_dataformat_p2.hpp"

#include <boost/chrono.hpp>
#include <boost/cstdint.hpp>

#include <cstddef>

#include <ostream>

typedef boost::chrono::duration<boost::int_least64_t, boost::ratio<1, 2000> > T500usec;
typedef boost::chrono::duration<boost::int_least64_t, boost::ratio<1, 200000000> > T5nsec;

namespace phase2 {

typedef T500usec coarse_time;
typedef T5nsec    fine_time;

inline DataFrameHeader* dataframeheader_cast(unsigned char* pointer) {
  return static_cast<DataFrameHeader*>(
    static_cast<void*>(pointer));
}

inline const DataFrameHeader* dataframeheader_cast(const unsigned char* pointer) {
  return static_cast<const DataFrameHeader*>(
    static_cast<const void*>(pointer));
}

// TODO: Remove them after passing to unsigned char* in the whole TriDAS
inline DataFrameHeader* dataframeheader_cast(char* pointer) {
  return static_cast<DataFrameHeader*>(
    static_cast<void*>(pointer));
}

inline const DataFrameHeader* dataframeheader_cast(const char* pointer) {
  return static_cast<const DataFrameHeader*>(
    static_cast<const void*>(pointer));
}

inline fine_time getCurrentTime() {
  time_t t = time(0);
  tm* now = gmtime(&t);

  return boost::chrono::seconds(
      86400 * ((now->tm_yday + 1) % 10) + 
      now->tm_hour * 3600 +
      now->tm_min * 60 +
      now->tm_sec
      );
}

inline unsigned int getDFHCharge(const phase2::DataFrameHeader& header) {
  return header.Charge;
}

inline std::size_t getDFHPayloadSize(const phase2::DataFrameHeader& header) {
  return !header.ZipFlag
    ? header.NDataSample + header.NDataSample % 4
    : header.NDataSample * 2 + (header.NDataSample * 2) % 4;
}

inline std::size_t getDFHNSamples(const phase2::DataFrameHeader& header) {
  return header.NDataSample;
}

// Time in seconds since midnight

inline fine_time getDFHDayTime(const phase2::DataFrameHeader& header) {
  return boost::chrono::seconds(header.Seconds) +
    T500usec(header.T500us) +
    T5nsec(header.T10ns * 2 + header.ParityFlag);
}

// Time elapsed since last wrap

inline fine_time getDFHFullTime(const phase2::DataFrameHeader& header) {
  return boost::chrono::seconds(86400 * header.Days) +
    getDFHDayTime(header);
}

/*
void setDFHFullTime(const fine_time& time);

void setDFHYearTime(const fine_time& time);
*/

inline void setDFHDayTime(const fine_time& time, phase2::DataFrameHeader& header) {
  boost::chrono::seconds boost_secs = boost::chrono::duration_cast<boost::chrono::seconds>(time);
  header.Seconds = boost_secs.count();

  T500usec boost_t500us = boost::chrono::duration_cast<T500usec>(time - boost_secs);
  header.T500us = boost_t500us.count();

  // Take care that the time resolution is 10ns, not 5. 5 ns is
  // reachable using the parity flag, which is not currently updated.
  // Nevertheless this is not an serious issue .

  header.T10ns = (time - boost_secs - boost_t500us).count() / 2;
}


inline std::ostream& operator <<(std::ostream& stream, const phase2::DataFrameHeader& header) {
  // First DWORD: Sync and ID
  stream << "\n == Sync & ID  ==\n";
  stream << "\nTowerID:       "  << header.TowerID;
  stream << "\nEFCMID:        "  << header.EFCMID;
  stream << "\nPMTID:         "  << header.PMTID;

  // Second and third DWORD: Time
  stream << "\n\n ==   Time   ==\n";
  stream << "\nDays (% 10):   " << header.Days;
  stream << "\nSeconds:       " << header.Seconds;
  stream << "\nT500usec:      " << header.T500us;
  stream << "\nT10ns:         " << header.T10ns;

  stream << "\n\nTime in 10 nanoseconds unit: " << getDFHFullTime(header);

  // Forth DWORD: Hit info
  stream << "\n\n == Hit info ==\n";
  stream << "\nZipFlag:       "  << header.ZipFlag;
  stream << "\nFragFlag:      "  << header.FragFlag;
  stream << "\nCharge:        "  << header.Charge;
  stream << "\nFifoFull:      "  << header.FifoFull;
  stream << "\nParityFlag:    "  << header.ParityFlag;
  stream << "\nNDataSamples:  "  << header.NDataSample;

  stream << "\nPayload size:  "  << getDFHPayloadSize(header) << std::endl;

  return stream;
}

inline TS_t getTimesliceId(DataFrameHeader const& dfh, int timeslice_ms){
  assert(timeslice_ms > 0);
  boost::chrono::milliseconds const total_ms =
      boost::chrono::duration_cast<boost::chrono::milliseconds>(
        getDFHFullTime(dfh)
      );
  return total_ms.count() / timeslice_ms;
}

}

#endif
