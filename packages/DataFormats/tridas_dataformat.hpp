#ifndef PACKAGES_DATAFORMATS_TRIDAS_DATAFORMAT_HPP
#define PACKAGES_DATAFORMATS_TRIDAS_DATAFORMAT_HPP

#include <f_dataformat_p2.hpp>
#include <f_dataformat_p3.hpp>


/*
ATTENTION: The following line defines the dataformat used in the
TriDAS code. Change it if you want to switch from one to another.
*/

  using namespace tridas::phase3;

#endif
