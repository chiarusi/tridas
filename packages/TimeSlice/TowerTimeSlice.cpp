#include "TowerTimeSlice.h"

#include <string>
#include <memory.h>
#include <cassert>

using namespace std;

TowerTimeSlice::TowerTimeSlice(size_t nstsin)
    :
      ts_id_(0),
      sts_vector_(nstsin, 0) {
  Reset();
}

void TowerTimeSlice::Reset() {
  ts_id_ = 0;
  for (size_t i = 0; i < sts_vector_.size(); ++i) {
    sts_vector_[i] = 0;
  }
}

void TowerTimeSlice::AddSTS(SectorTimeSlice* sts) {
  size_t const sector = sts->GetHeader().sector_;
  assert(sector < sts_vector_.size());
  assert(
    sts_vector_[sector] == 0 && "Trying to add an already inserted sector.");
  sts_vector_[sector] = sts;
}

SectorTimeSlice* TowerTimeSlice::GetSTS(size_t index) {
  assert(index < sts_vector_.size());
  return sts_vector_[index];
}

char* TowerTimeSlice::DumpTTS(char* p_buf) {
  return DumpAllBuffers(p_buf);
}

char* TowerTimeSlice::DumpAllBuffers(char* p_buf) {
  for (size_t i = 0; i < sts_vector_.size(); ++i) {
    if (sts_vector_[i]) {
      p_buf = sts_vector_[i]->DumpAllBuffers(p_buf);
    }
  }
  return p_buf;
}

bool TowerTimeSlice::completed() const {
  for (size_t i = 0; i < sts_vector_.size(); ++i) {
    if (!sts_vector_[i]) {
      return false;
    }
  }
  return true;
}
