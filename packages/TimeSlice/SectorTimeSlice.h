#ifndef PACKAGES_TIMESLICE_SECTORTIMESLICE_H
#define PACKAGES_TIMESLICE_SECTORTIMESLICE_H

#include <vector>

#include "SimpleBuffer.h"
#include "tridas_dataformat.hpp"

size_t const NMAXPMT = 84; // 1 tower
//size_t const NMAXPMT = 672; // 8 towers

struct s_HeaderInfoSTS
{
  TS_t id_; // TimeSlice id
  size_t sector_; // Sector id
  size_t pmtin_;  // N.pmt per Sector

  bool pmtlist_[NMAXPMT]; // pmtlist_[i] = 1 (0) : filled buffer (not filled)
  size_t BufSize_[NMAXPMT]; // effective load for each buffer
  size_t nhit_[NMAXPMT]; // effective hit counter for each buffer
  size_t nframe_[NMAXPMT]; // total frame counter for each buffer
};

class SectorTimeSlice
{

private:
  s_HeaderInfoSTS header_;
  std::vector<SimpleBuffer*> db_;
  bool ConstructedWithBuffers_;

public:
  SectorTimeSlice (size_t); // simple constructor (SimpleBuffers to be added)
  SectorTimeSlice(size_t, size_t); // constructor which instatiates its own SimpleBuffers
  size_t GetNPMTFull() const;

  // setting and retrieving info
  TS_t GetTSID() const
  {
    return header_.id_;
  }
  void SetTSID(TS_t ID)
  {
    header_.id_ = ID;
  }

  void SetSectorID(size_t sect)
  {
    header_.sector_ = sect;
  }
  size_t GetSectorID() const
  {
    return header_.sector_;
  }

  size_t GetNBuffers() const
  {
    return header_.pmtin_;
  }

  s_HeaderInfoSTS const& GetHeader() const
  {
    return header_;
  }

  s_HeaderInfoSTS& GetHeader()
  {
    return header_;
  }
  void SetHeader(s_HeaderInfoSTS const& header)
  {
    header_ = header;
  }
  std::vector<SimpleBuffer*> GetBuffers() const
  {
    return db_;
  }

  SimpleBuffer const* GetBuffer(size_t i) const
  {
    assert(i < db_.size());
    return db_[i];
  }

  SimpleBuffer* GetBuffer(size_t i)
  {
    assert(i < db_.size());
    return db_[i];
  }

  void setHitInfo(size_t ipmt, size_t hit_count, size_t frame_count)
  {
    assert(ipmt < NMAXPMT);
    header_.nhit_[ipmt] = hit_count;
    header_.nframe_[ipmt] = frame_count;
  }

  bool AddPMTBuf(size_t, SimpleBuffer*); // To be used by Assemblatore
  void Reset(); // To be used by Liberatore, before to reinsert a message in MQ_STS_Free.

  char* DumpSTS(char*);
  char* DumpAllBuffers(char*);

  bool CheckEmptiness() const;

};

#endif
