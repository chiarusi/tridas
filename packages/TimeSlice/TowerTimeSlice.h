#ifndef PACKAGES_TIMESLICE_TOWERTIMESLICE_H
#define PACKAGES_TIMESLICE_TOWERTIMESLICE_H

#include <vector>

#include "SectorTimeSlice.h"
#include "tridas_dataformat.hpp"

class TowerTimeSlice {
  TS_t ts_id_;
  std::vector<SectorTimeSlice*> sts_vector_;

 public:
  explicit TowerTimeSlice(size_t nstsin);
  void Reset();
  void AddSTS(SectorTimeSlice* sts);
  void SetTSID(TS_t tsid) {
    ts_id_ = tsid;
  }
  TS_t GetTSID() const {
    return ts_id_;
  }
  size_t GetNSTSin() const {
    return sts_vector_.size();
  }
  SectorTimeSlice* GetSTS(size_t index);
  char* DumpTTS(char* p_buf);
  char* DumpAllBuffers(char* p_buf);
  bool completed() const;
};

#endif
