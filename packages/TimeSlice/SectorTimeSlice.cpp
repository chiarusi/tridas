#include "SectorTimeSlice.h"

#include <string>
#include <memory.h>
#include <cassert>

using namespace std;

SectorTimeSlice::SectorTimeSlice(size_t npmt)
{
  assert(npmt <= NMAXPMT);
  header_.pmtin_ = npmt;
  db_.resize(header_.pmtin_);
  ConstructedWithBuffers_ = false;
  Reset();

}

SectorTimeSlice::SectorTimeSlice(size_t npmt, size_t sizebuf)
{
  assert(npmt <= NMAXPMT);
  header_.pmtin_ = npmt;
  db_.resize(header_.pmtin_);
  for (size_t i = 0; i < header_.pmtin_; ++i) {
    db_[i] = new SimpleBuffer(sizebuf);
  }
  ConstructedWithBuffers_ = true;
  Reset();
}

size_t SectorTimeSlice::GetNPMTFull() const
{
  size_t pmt_count = 0;
  for (size_t i = 0; i < header_.pmtin_; ++i) {
    if (header_.pmtlist_[i]) {
      ++pmt_count;
    }
  }
  return pmt_count;
}

void SectorTimeSlice::Reset()
{
  header_.id_ = 0;

  for (size_t i = 0; i < header_.pmtin_; ++i) {
    header_.pmtlist_[i] = false;
    header_.BufSize_[i] = 0;
    header_.nhit_[i] = 0;
    header_.nframe_[i] = 0;
  }

  if (ConstructedWithBuffers_) {
    for (size_t i = 0; i < header_.pmtin_; ++i) {
      db_[i]->FreeBuf();
    }
  } else {
    for (size_t i = 0; i < header_.pmtin_; ++i) {
      db_[i] = 0;
    }
  }
}

bool SectorTimeSlice::AddPMTBuf(size_t ipmt, SimpleBuffer* sb)
{
  assert(ipmt < header_.pmtin_);

  if (header_.pmtlist_[ipmt]) {
    // trying to readd a pmt buffer
    return false;
  }

  header_.pmtlist_[ipmt] = true;
  db_[ipmt] = sb;
  header_.BufSize_[ipmt] = sb->GetLoad();
  return true;
}

char* SectorTimeSlice::DumpSTS(char* p_buf)
{
  memcpy(p_buf, &header_, sizeof(header_));
  p_buf += sizeof(header_);
  return DumpAllBuffers(p_buf);
}

char* SectorTimeSlice::DumpAllBuffers(char*p_buf)
{
  for (size_t i = 0; i < header_.pmtin_; ++i) {
    if (header_.pmtlist_[i]) {
      p_buf += db_[i]->DumpLoad(p_buf);
    }
  }
  return p_buf;
}

bool SectorTimeSlice::CheckEmptiness() const
{
  /// Test that all the elements of PMTBuff array are pointing to NULL
  for (std::vector<SimpleBuffer*>::const_iterator it = db_.begin();
      it != db_.end(); ++it) {
    if (*it) {
      // this is not empty
      return false;
    }
  }
  return true;
}
