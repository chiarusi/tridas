#include "producer_consumer.hpp"
#include <iostream>
#include <string>
#include <cstring>

int main()
{
  network::Context c;
  network::Producer producer(c);

  producer.tcpConnect("localhost", "95959");

  while (std::cin) {
    std::string msg;

    std::cout << "msg> ";
    if (std::getline(std::cin, msg)) {

      std::cout << "Sending: " << msg << '\n';

      std::size_t const sent_size = producer.timedSend(msg.c_str(), msg.size(), 500);
      if (sent_size != msg.size()) {
        std::cout << "not sent\n";
      } else {
        std::cout << "sent\n";
      }
    }
  }
  std::cout << std::endl;
}
