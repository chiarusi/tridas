#include "stream_receiver.hpp"
#include <iostream>
#include <string>
#include <log.hpp>

int main()
{
  tridas::Log::init("t_stream_receiver", tridas::Log::DEBUG);

  network::StreamReceiver sr;

  try {
    sr.tcpConnect("localhost", "5555");

    while (true) {
      std::size_t const buf_size = 512;
      char buffer[buf_size];
      std::size_t const size = sr.timedRecv(buffer, buf_size, 1000);

      if (size == 0) {
        std::cout << "No data\n";
      } else {
        std::cout << std::string(buffer, size) << '\n';
      }
    }
  } catch (std::exception const& e) {
    std::cout << e.what() << '\n';
  }
}
