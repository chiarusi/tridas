#ifndef PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_PRODUCER_CONSUMER_HPP
#define PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_PRODUCER_CONSUMER_HPP

#include <boost/shared_ptr.hpp>

#include "inter_objects_communication.hpp"

namespace network
{

class Producer : private Connection
{
public:
  Producer(Context& c) :
    Connection(c, ZMQ_PUSH)
  {
  }
  // Expose connection methods
  void tcpBind(std::string const& port)
  {
    Connection::tcpBind(port);
  }
  void tcpConnect(std::string const& address, std::string const& port)
  {
    Connection::tcpConnect(address, port);
  }
  // Implements send/recv methods
  std::size_t timedSend(void const* data, std::size_t size, long timeout)
  {
    zmq::pollitem_t item = { static_cast<void*>(socket_), 0, ZMQ_POLLOUT, 0 };
    zmq::poll(&item, 1, timeout); // timeout of 0 returns immediately
    if (item.revents & ZMQ_POLLOUT) {
      return socket_.send(data, size, ZMQ_DONTWAIT);
    }
    return 0;
  }
  std::size_t timedSendMore(void const* data, std::size_t size, long timeout)
  {
    zmq::pollitem_t item = { static_cast<void*>(socket_), 0, ZMQ_POLLOUT, 0 };
    zmq::poll(&item, 1, timeout); // timeout of 0 returns immediately
    if (item.revents & ZMQ_POLLOUT) {
      return socket_.send(data, size, ZMQ_DONTWAIT | ZMQ_SNDMORE);
    }
    return 0;
  }
};

typedef boost::shared_ptr<Producer> ProducerPtr;

class Consumer : private Connection
{
public:
  Consumer(Context& c) :
    Connection(c, ZMQ_PULL)
  {
  }
  // Expose connection methods
  void tcpConnect(std::string const& address, std::string const& port)
  {
    Connection::tcpConnect(address, port);
  }
  void tcpBind(std::string const& port)
  {
    Connection::tcpBind(port);
  }

  // Implements recv methods
  std::size_t timedRecv(void* data, std::size_t size, long timeout)
  {
    zmq::pollitem_t item = { static_cast<void*>(socket_), 0, ZMQ_POLLIN, 0 };
    zmq::poll(&item, 1, timeout); // timeout of 0 returns immediately
    if (item.revents & ZMQ_POLLIN) {
      return socket_.recv(data, size, ZMQ_DONTWAIT);
    }
    return 0;
  }

  bool timedRecv(zmq::message_t& msg, long timeout)
  {
    zmq::pollitem_t item = { static_cast<void*>(socket_), 0, ZMQ_POLLIN, 0 };
    zmq::poll(&item, 1, timeout); // timeout of 0 returns immediately
    if (item.revents & ZMQ_POLLIN) {
      return socket_.recv(&msg, ZMQ_DONTWAIT);
    }
    return false;
  }

  bool more()
  {
    int64_t more = 0;
    size_t more_size = sizeof(more);
    socket_.getsockopt(ZMQ_RCVMORE, &more, &more_size);
    return more;
  }
};

typedef boost::shared_ptr<Consumer> ConsumerPtr;

}

#endif
