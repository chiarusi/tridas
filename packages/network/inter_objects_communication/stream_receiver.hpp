#ifndef PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_STREAM_RECEIVER_HPP
#define PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_STREAM_RECEIVER_HPP

#include <boost/shared_ptr.hpp>

#include <boost/asio.hpp>
#include <boost/optional.hpp>
#include <boost/bind.hpp>
#include <log.hpp>

namespace network {

static
void set_result(boost::optional<boost::system::error_code>* a,
                boost::system::error_code const& error)
{
  a->reset(error);
}

static
void receive_callback(boost::optional<boost::system::error_code>* a,
                      boost::system::error_code const& error,
                      std::size_t bytes_transferred,
                      std::size_t* btrans_copy)
{
  a->reset(error);
  *btrans_copy = bytes_transferred;
}

inline
boost::asio::ip::tcp::endpoint make_endpoint(boost::asio::io_service& service,
                                             std::string const& address,
                                             std::string const& port)
{
  boost::asio::ip::tcp::resolver resolver(service);
  boost::asio::ip::tcp::resolver::query query(
      boost::asio::ip::tcp::v4(),
      address,
      port);
  return *resolver.resolve(query);
}

class StreamReceiver
{
  boost::asio::io_service m_service;
  boost::asio::ip::tcp::socket m_socket;
  boost::asio::ip::tcp::endpoint m_ep;
  bool m_connected;

  void connect(boost::system::error_code& ec)
  {
    m_socket.connect(m_ep, ec);

    if (!ec) {
      m_connected = true;
      boost::asio::ip::tcp::socket::receive_buffer_size option(8*1024*1024);
      m_socket.set_option(option);
    }
  }

 public:

  StreamReceiver()
    :
    m_socket(m_service, boost::asio::ip::tcp::v4()),
    m_connected(false)
  {}

  ~StreamReceiver()
  {
    if (m_socket.is_open()) {
      boost::system::error_code ec;
      m_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
      m_socket.close(ec);
    }
  }

  // Expose connection methods

  void tcpConnect(std::string const& address, std::string const& port)
  {
    m_ep = make_endpoint(
        m_service,
        address,
        port);

    boost::system::error_code ec;

    connect(ec);

    if (ec) {
      TRIDAS_LOG(ERROR) << "Error connecting to "
                        << m_ep
                        << ": "
                        << ec;
    } else {
      TRIDAS_LOG(INFO) << "Connection to "
                       << m_ep
                       << " succeeded";
    }
  }

  std::size_t timedRecv(void* data, std::size_t size, long timeout)
  {
    boost::system::error_code ec;
    return timedRecv(data, size, timeout, ec);
  }

  std::size_t timedRecv(
      void* data,
      std::size_t size,
      long timeout,
      boost::system::error_code& ec
  )
  {
    assert(m_ep.port() && "Connect the socket before trying to receive");

    if (!m_connected) {
      connect(ec);

      if (!ec) {
        TRIDAS_LOG(INFO) << "Connection to "
                         << m_ep
                         << " succeeded";
      }
    }

    if (m_connected) {
      boost::asio::deadline_timer timer(m_service);

      timer.expires_from_now(boost::posix_time::milliseconds(timeout));

      boost::optional<boost::system::error_code> timer_result;
      timer.async_wait(boost::bind(set_result, &timer_result, _1));

      std::size_t recv_size = 0;

      boost::optional<boost::system::error_code> read_result;
      m_socket.async_receive(
          boost::asio::buffer(data, size),
          boost::bind(
              receive_callback,
              &read_result,
              boost::asio::placeholders::error,
              boost::asio::placeholders::bytes_transferred,
              &recv_size));

      m_service.reset();
      while (m_service.run_one()) {
        if (read_result) {
          timer.cancel();
        } else if (timer_result) {
          m_socket.cancel();
        }
      }

      if (read_result.is_initialized()) {
        if (
             *read_result 
          && *read_result != boost::asio::error::operation_aborted
        ) {
          m_connected = false;
          TRIDAS_LOG(ERROR) << "Error on socket connected to "
                            << m_ep
                            << ": "
                            << *read_result;

          m_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
          m_socket.close(ec);
        } else {
          ec = *read_result;
        }
      } else {
        assert(!"Uninitialized timed receive result");
      }

      return recv_size;
    } else {
      return 0;
    }
  }
};

typedef boost::shared_ptr<StreamReceiver> StreamReceiverPtr;

} // ns network

#endif
