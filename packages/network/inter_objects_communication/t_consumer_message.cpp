#include "producer_consumer.hpp"
#include <iostream>
#include <string>
#include <cstring>

int main()
{
  network::Context c;
  network::Consumer consumer(c);

  consumer.tcpBind("95959");

  while (true) {
    zmq::message_t message;

    bool const recvd = consumer.timedRecv(message, 500);
    if (recvd) {
      std::cout << "msg: " << std::string(static_cast<char*>(message.data()), message.size()) << '\n';
      std::cout << "size: " << message.size() << '\n';
    } else {
      std::cout << "timeout\n";
    }
  }
}
