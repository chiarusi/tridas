#include "producer_consumer.hpp"
#include <iostream>
#include <string>
#include <cstring>

int main()
{
  network::Context c;
  network::Consumer consumer(c);

  consumer.tcpBind("95959");

  while (true) {
    std::size_t const buffer_size = 4096;
    char buffer[buffer_size];
    std::memset(buffer, '\0', buffer_size);

    std::size_t const read_size = consumer.timedRecv(buffer, buffer_size, 500);
    if (read_size) {
      std::cout << "msg: " << std::string(buffer, read_size) << '\n';
      std::cout << "size: " << read_size << '\n';
    } else {
      std::cout << "timeout\n";
    }
  }
}
