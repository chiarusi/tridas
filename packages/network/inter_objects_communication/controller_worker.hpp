#ifndef PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_CONTROLLER_WORKER_HPP
#define PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_CONTROLLER_WORKER_HPP

#include <boost/shared_ptr.hpp>

#include "inter_objects_communication.hpp"

namespace network
{

typedef size_t Command;

// Define commands
Command const NONE = 0;
Command const STOP = 1;

class Controller : private Connection
{
 public:
  Controller(Context& c)
  :
    Connection(c, ZMQ_PUB)
  {}

  // Expose connection methods
  void inprocConnect(std::string const& name)
  {
    Connection::inprocConnect(name);
  }

  // Implements send/recv methods
  size_t sendCmd(Command const command)
  {
    return socket_.send(&command, sizeof(command), ZMQ_DONTWAIT);
  }
};

typedef boost::shared_ptr<Controller> ControllerPtr;

class Worker : private Connection
{
  void subscribe()
  {
    socket_.setsockopt(ZMQ_SUBSCRIBE, "", 0);
  }

 public:

  Worker(Context& c) :
    Connection(c, ZMQ_SUB)
  {
    subscribe();
  }

  // Expose connection methods
  void inprocBind(std::string const& name)
  {
    Connection::inprocBind(name);
  }

  // Implements send/recv methods
  size_t recvCmd()
  {
    Command command = NONE;
    socket_.recv(&command, sizeof(command), ZMQ_DONTWAIT);
    return command;
  }
};

typedef boost::shared_ptr<Worker> WorkerPtr;

}

#endif
