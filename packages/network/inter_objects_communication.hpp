#ifndef PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_HPP
#define PACKAGES_NETWORK_INTER_OBJECTS_COMMUNICATION_HPP

#include <zmq.hpp>

namespace network
{

typedef zmq::context_t Context;

class Connection
{
 protected:
  zmq::socket_t socket_;

 protected:
  Connection(Context& c, int type)
  :
    socket_(c, type)
  {}

  // Connect methods
  void inprocConnect(std::string const& name)
  {
    std::string const endpoint("inproc://" + name);
    socket_.connect(endpoint.c_str());
  }
  void ipcConnect(std::string const& name)
  {
    std::string const endpoint("ipc:///tmp/ipc_" + name);
    socket_.connect(endpoint.c_str());
  }
  void tcpConnect(std::string const& address, std::string const& port)
  {
    std::string const endpoint("tcp://" + address + ":" + port);
    socket_.connect(endpoint.c_str());
  }
  // Bind methods
  void inprocBind(std::string const& name)
  {
    std::string const endpoint("inproc://" + name);
    socket_.bind(endpoint.c_str());
  }
  void ipcBind(std::string const& name)
  {
    std::string const endpoint("ipc:///tmp/ipc_" + name);
    socket_.bind(endpoint.c_str());
  }
  void tcpBind(std::string const& port)
  {
    std::string const endpoint("tcp://*:" + port);
    socket_.bind(endpoint.c_str());
  }
};

}

#endif
