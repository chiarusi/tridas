#ifndef PACKAGES_LOG_LOG_HPP
#define PACKAGES_LOG_LOG_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <syslog.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>

#define TRIDAS_LOG(level) \
  if (tridas::Log::level > tridas::Log::BaseLevel()) ; \
  else tridas::Log(tridas::Log::level).tmp_stream()

namespace tridas {

namespace detail {

inline std::string Now() {
  using namespace boost::posix_time;

  ptime t = second_clock::local_time();
  std::ostringstream oss;
  oss << t;
  return oss.str();
  // note: a call to to_iso_string() would require linking the date-time library
}

inline void log_to_syslog(
    std::istream& input_stream,
    boost::mutex& mx,
    int level,
    std::string const& level_str
)
{
  boost::mutex::scoped_lock l(mx);
  bool continuation = false;
  for (std::string s; getline(input_stream, s); ) {
    std::ostringstream os;
    os << level_str
       << (continuation ? "+ " : ": ")
       << s;
    syslog(level, "%s", os.str().c_str());
    continuation = true;
  }
}

inline void log_to_stream(
    std::ostream& output_stream,
    std::istream& input_stream,
    boost::mutex& mx,
    std::string const& ident,
    std::string const& level
)
{
  std::ostringstream os;
  std::string const now = Now();
  bool continuation = false;
  for (std::string s; getline(input_stream, s); ) {
    if (continuation) os << '\n';
    os << now << ' ' << ident << ": ";
    os << level
       << (continuation ? "+ " : ": ")
       << s;
    continuation = true;
  }
  boost::mutex::scoped_lock l(mx);
  output_stream << os.str().c_str() << std::endl;
}

}

class Log: boost::noncopyable
{
 public:
  enum Level { ERROR, WARNING, INFO, DEBUG };

 public:
  Log(Level level = INFO): m_level(level)
  {
  }
  ~Log() {
    StaticMembers& sm(static_members());
    assert(!sm.ident.empty() && "Did you forget to call Log::Init()?");
    if (sm.use_syslog) {
      detail::log_to_syslog(m_tmp_stream, sm.mx, ToSyslog(m_level), ToString(m_level));
    } else {
      detail::log_to_stream(*sm.log_stream, m_tmp_stream, sm.mx, sm.ident, ToString(m_level));
    }
  }
  std::ostream& tmp_stream() {
    return m_tmp_stream;
  };

 private:
  Level m_level;
  std::stringstream m_tmp_stream;

  struct StaticMembers: boost::noncopyable {
    std::string ident;
    bool use_syslog;
    boost::mutex mx;
    std::ostream* log_stream;
    Level base_level;
    StaticMembers()
        : use_syslog(false), log_stream(&std::clog), base_level(INFO)
    {
    }
  };

  static StaticMembers& static_members()
  {
    static StaticMembers sm;
    return sm;
  }

 public:

  static void init(
      std::string const& ident,
      Level base_level = INFO,
      std::ostream& log_stream = std::clog
  )
  {
    assert(!ident.empty() && "The ident string must be non empty");
    assert(base_level >= ERROR && base_level <= DEBUG && "Invalid logging level");
    assert(log_stream.good() && "The log stream is not in a good state");
    StaticMembers& sm(static_members());
    sm.ident = ident;
    sm.base_level = base_level;
    sm.log_stream = &log_stream;
  }
  static Level BaseLevel()
  {
    return static_members().base_level;
  }
  static void UseSyslog(int option = 0, int facility = LOG_LOCAL7)
  {
    StaticMembers& sm(static_members());
    sm.use_syslog = true;
    openlog(sm.ident.c_str(), option, facility);
  }
  static int ToSyslog(Level level) {
    static int const syslog_level[] = {
      LOG_ERR, LOG_WARNING, LOG_INFO, LOG_DEBUG
    };
    assert(level >= ERROR && level <= DEBUG);
    return syslog_level[level];
  }
  static std::string ToString(Level level) {
    static const char* const buffer[] = {"ERROR", "WARNING", "INFO", "DEBUG"};
    assert(level >= ERROR && level <= DEBUG);
    return buffer[level];
  }
  static Level FromString(const std::string& level) {
    if (level == "DEBUG") return DEBUG;
    if (level == "INFO") return INFO;
    if (level == "WARNING") return WARNING;
    if (level == "ERROR") return ERROR;

    assert(!"Unknown logging level");
  }
};

}

#endif
