#include "log.hpp"
#include <fstream>

using namespace tridas;

int main() {
  //  std::ofstream f("/tmp/tridas_log.txt");
  Log::init("t_log", Log::DEBUG);
  //  Log::UseSyslog();

  TRIDAS_LOG(DEBUG) << "This is a DEBUG message";

  TRIDAS_LOG(INFO) << "This is an INFO message";

  TRIDAS_LOG(WARNING) << "This is a WARNING message\non two lines";

  TRIDAS_LOG(ERROR) << "This is an ERROR message";
}
