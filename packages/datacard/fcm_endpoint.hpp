#ifndef TRIDAS_PACKAGES_DATACARD_FCM_ENDPOINT_HPP
#define TRIDAS_PACKAGES_DATACARD_FCM_ENDPOINT_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace detail {

class FcmEndpoint
{
  std::string m_host;
  int m_port;

 public:

  FcmEndpoint(std::string const& host, int port)
    :
    m_host(host),
    m_port(port)
  {}

  std::string host() const
  {
    return m_host;
  }

  void host(std::string const& h)
  {
    m_host = h;
  }

  int port() const
  {
    return m_port;
  }

  void port(int p)
  {
    m_port = p;
  }
};

inline
Configuration& operator &(Configuration& conf, FcmEndpoint const& fe)
{
  conf.put("DATA_HOST", fe.host());
  conf.put("DATA_PORT", fe.port());

  return conf;
}

inline
bool operator ==(FcmEndpoint const& first, FcmEndpoint const& second)
{
  return first.port() == second.port() && first.host() == second.host();
}

inline
bool operator !=(FcmEndpoint const& first, FcmEndpoint const& second)
{
  return ! (first == second);
}

inline
FcmEndpoint make_fcm_endpoint(Configuration const& conf)
{
  return FcmEndpoint(
      conf.get<std::string>("DATA_HOST"),
      conf.get<int>("DATA_PORT"));
}


} // ns detail
} // ns datacard
} // ns tridas
#endif
