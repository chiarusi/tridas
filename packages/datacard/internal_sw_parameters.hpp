#ifndef TRIDAS_PACKAGES_DATACARD_INTERNAL_SW_PARAMETERS_HPP
#define TRIDAS_PACKAGES_DATACARD_INTERNAL_SW_PARAMETERS_HPP

#include "Configuration.h"

namespace tridas {
namespace datacard {
namespace element {

class InternalSwParameters
{
  int m_delta_ts;
  int m_pmt_buffer_size;
  int m_sts_ready_timeout;
  int m_tts_ready_timeout;
  int m_sts_in_memory;

 public:

  InternalSwParameters(
      int delta_ts,
      int pmt_buffer_size,
      int sts_ready_timeout,
      int tts_ready_timeout,
      int sts_in_memory)
    :
    m_delta_ts(delta_ts),
    m_pmt_buffer_size(pmt_buffer_size),
    m_sts_ready_timeout(sts_ready_timeout),
    m_tts_ready_timeout(tts_ready_timeout),
    m_sts_in_memory(sts_in_memory)
  {}

  int deltaTs() const
  {
    return m_delta_ts;
  }

  void deltaTs(int delta_ts)
  {
    m_delta_ts = delta_ts;
  }

  int pmtBufferSize() const
  {
    return m_pmt_buffer_size;
  }

  void pmtBufferSize(int pmt_buffer_size)
  {
    m_pmt_buffer_size = pmt_buffer_size;
  }

  int stsReadyTimeout() const
  {
    return m_sts_ready_timeout;
  }

  void stsReadyTimeout(int sts_ready_timeout)
  {
    m_sts_ready_timeout = sts_ready_timeout;
  }

  int ttsReadyTimeout() const
  {
    return m_tts_ready_timeout;
  }

  void ttsReadyTimeout(int tts_ready_timeout)
  {
    m_tts_ready_timeout = tts_ready_timeout;
  }

  int stsInMemory() const
  {
    return m_sts_in_memory;
  }

  void stsInMemory(int sts_in_memory)
  {
    m_sts_in_memory = sts_in_memory;
  }
};

Configuration& operator &(Configuration& conf, InternalSwParameters const& isp)
{
  conf.put("INTERNAL_SW_PARAMETERS.DELTA_TS",   isp.deltaTs());
  conf.put("INTERNAL_SW_PARAMETERS.PMT_BUFFER_SIZE", isp.pmtBufferSize());
  conf.put("INTERNAL_SW_PARAMETERS.STS_READY_TIMEOUT", isp.stsReadyTimeout());
  conf.put("INTERNAL_SW_PARAMETERS.TTS_READY_TIMEOUT", isp.ttsReadyTimeout());
  conf.put("INTERNAL_SW_PARAMETERS.STS_IN_MEMORY", isp.stsInMemory());

  return conf;
}

inline
InternalSwParameters make_internal_sw_parameters(Configuration const& conf)
{
  return InternalSwParameters(
      conf.get<int>("INTERNAL_SW_PARAMETERS.DELTA_TS"),
      conf.get<int>("INTERNAL_SW_PARAMETERS.PMT_BUFFER_SIZE"),
      conf.get<int>("INTERNAL_SW_PARAMETERS.STS_READY_TIMEOUT"),
      conf.get<int>("INTERNAL_SW_PARAMETERS.TTS_READY_TIMEOUT"),
      conf.get<int>("INTERNAL_SW_PARAMETERS.STS_IN_MEMORY"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_INTERNAL_SW_PARAMETERS_HPP
