#ifndef TRIDAS_PACKAGES_DATACARD_FCM_ENDPOINTS_HPP
#define TRIDAS_PACKAGES_DATACARD_FCM_ENDPOINTS_HPP

#include "Configuration.h"
#include "fcm_endpoint.hpp"
#include <vector>

namespace tridas {
namespace datacard {
namespace element {

class FcmEndpoints
{
  std::vector<detail::FcmEndpoint> m_endpoints;

 public:

  std::size_t size() const
  {
    return m_endpoints.size();
  }

  detail::FcmEndpoint const& operator [](std::size_t n) const
  {
    return m_endpoints[n];
  }

  detail::FcmEndpoint& operator [](std::size_t n)
  {
    return m_endpoints[n];
  }

  void add_endpoint(detail::FcmEndpoint const& endpoint)
  {
    std::vector<detail::FcmEndpoint>::const_iterator const it
        = std::find(m_endpoints.begin(), m_endpoints.end(), endpoint);

    if (it == m_endpoints.end()) {
      m_endpoints.push_back(endpoint);
    }
  }

  void add_endpoint(std::string const& host, int port)
  {
    detail::FcmEndpoint const endpoint(host, port);

    add_endpoint(endpoint);
  }

  void erase_endpoint(std::string const& host, int port)
  {
    detail::FcmEndpoint const endpoint(host, port);

    std::vector<detail::FcmEndpoint>::iterator const it
        = std::find(m_endpoints.begin(), m_endpoints.end(), endpoint);

    if (it != m_endpoints.end()) {
      m_endpoints.erase(it);
    }
  }
};

Configuration& operator &(Configuration& conf, FcmEndpoints const& fes)
{
  Configuration array;
  for (unsigned int i = 0; i < fes.size(); ++i) {
    Configuration tmp;
    tmp & fes[i];
    array.push_back(std::make_pair("", tmp));
  }

  conf.put_child("FCM_ENDPOINTS", array);

  return conf;
}

FcmEndpoints make_fcm_endpoints(Configuration const& conf)
{
  FcmEndpoints fcmes;

  Configuration const& child = conf.get_child("FCM_ENDPOINTS");

  for (Configuration::const_iterator it = child.begin(); it != child.end(); ++it) {
    detail::FcmEndpoint const endpoint = detail::make_fcm_endpoint(it->second);

    fcmes.add_endpoint(endpoint);
  }
  return fcmes;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_FCM_ENDPOINTS_HPP
