#ifndef TRIDAS_PACKAGES_DATACARD_DETECTOR_HPP
#define TRIDAS_PACKAGES_DATACARD_DETECTOR_HPP

#include "Configuration.h"
#include <string>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <map>

namespace {

inline
int id(std::string const& key)
{
  std::size_t const pos = key.find('_');

  if (pos == std::string::npos) {
    throw std::runtime_error("Provided string is not a key.");
  }

  return boost::lexical_cast<int>(key.substr(pos + 1));
}

} // anonymous namespace

namespace tridas {
namespace datacard {
namespace element {

class PMT
{
  int m_id;
  float m_x;
  float m_y;
  float m_z;
  float m_cx;
  float m_cy;
  float m_cz;
  int m_time_offset;
  int m_pedestal;
  int m_threshold;

 public:

  PMT(
      int id,
      float x,
      float y,
      float z,
      float cx,
      float cy,
      float cz,
      int time_offset,
      int pedestal,
      int threshold)
    :
    m_id(id),
    m_x(x),
    m_y(y),
    m_z(z),
    m_cx(cx),
    m_cy(cy),
    m_cz(cz),
    m_time_offset(time_offset),
    m_pedestal(pedestal),
    m_threshold(threshold)
  {
    assert(id >= 0 && "Negative PMT ID provided.");
  }

  PMT()
    :
    m_id(0),
    m_x(0),
    m_y(0),
    m_z(0),
    m_cx(0),
    m_cy(0),
    m_cz(0),
    m_time_offset(0),
    m_pedestal(0),
    m_threshold(0)
  {}

  int id() const
  {
    return m_id;
  }

  float x() const
  {
    return m_x;
  }
  float y() const
  {
    return m_y;
  }
  float z() const
  {
    return m_z;
  }
  float cx() const
  {
    return m_cx;
  }
  float cy() const
  {
    return m_cy;
  }
  float cz() const
  {
    return m_cz;
  }
  int timeOffset() const
  {
    return m_time_offset;
  }
  int pedestal() const
  {
    return m_pedestal;
  }
  int threshold() const
  {
    return m_threshold;
  }

  void x (float x)
  {
    m_x = x;
  }
  void y (float y)
  {
    m_y = y;
  }
  void z (float z)
  {
    m_z = z;
  }
  void cx (float cx)
  {
    m_cx = cx;
  }
  void cy (float cy)
  {
    m_cy = cy;
  }
  void cz (float cz)
  {
    m_cz = cz;
  }
  void timeOffset (int time_offset)
  {
    m_time_offset = time_offset;
  }
  void pedestal (int pedestal)
  {
    m_pedestal = pedestal;
  }
  void threshold (int threshold)
  {
    m_threshold = threshold;
  }

};

inline
Configuration& operator &(Configuration& conf, PMT const& pmt)
{
  std::string const sect =
      "PMT_"
      + boost::lexical_cast<std::string>(pmt.id());

  conf.put(sect + ".X", pmt.x());
  conf.put(sect + ".Y", pmt.y());
  conf.put(sect + ".Z", pmt.z());
  conf.put(sect + ".CX", pmt.cx());
  conf.put(sect + ".CY", pmt.cy());
  conf.put(sect + ".CZ", pmt.cz());
  conf.put(sect + ".TIME_OFFSET", pmt.timeOffset());
  conf.put(sect + ".PEDESTAL", pmt.pedestal());
  conf.put(sect + ".THRESHOLD", pmt.threshold());

  return conf;
}

inline
bool operator ==(PMT const& first, PMT const& second)
{
  return first.id() == second.id();
}

inline
bool operator !=(PMT const& first, PMT const& second)
{
  return first.id() != second.id();
}

inline
PMT make_pmt(Configuration const& conf, std::string const& key)
{
  int const id = ::id(key);

  return PMT(
      id,
      conf.get<float>("X"),
      conf.get<float>("Y"),
      conf.get<float>("Z"),
      conf.get<float>("CX"),
      conf.get<float>("CY"),
      conf.get<float>("CZ"),
      conf.get<int>("TIME_OFFSET"),
      conf.get<int>("PEDESTAL"),
      conf.get<int>("THRESHOLD"));
}

class Floor
{
  int m_id;
  std::map<int, PMT> m_pmts;

 public:

  typedef std::map<int, PMT>::iterator iterator;
  typedef std::map<int, PMT>::const_iterator const_iterator;

  explicit
  Floor(int id)
    :
    m_id(id)
  {
    assert(id != 0 && "The floor id cannot be zero.");
    assert(id > 0 && "Negative floor ID provided.");
  }

  iterator begin()
  {
    return m_pmts.begin();
  }

  const_iterator begin() const
  {
    return m_pmts.begin();
  }

  iterator end()
  {
    return m_pmts.end();
  }

  const_iterator end() const
  {
    return m_pmts.end();
  }

  std::size_t size() const
  {
    return m_pmts.size();
  }

  PMT& operator [](int id)
  {
    std::map<int, PMT>::iterator const it = m_pmts.find(id);

    if (it == m_pmts.end()) {
      throw std::runtime_error("Requested element is not present.");
    }

    return it->second;
  }

  PMT const& operator [](int id) const
  {
    std::map<int, PMT>::const_iterator const it = m_pmts.find(id);

    if (it == m_pmts.end()) {
      throw std::runtime_error("Requested element is not present.");
    }

    return it->second;
  }

  void add_pmt(PMT const& pmt)
  {
    m_pmts[pmt.id()] = pmt;
  }

  void erase_pmt(PMT const& pmt)
  {
    m_pmts.erase(pmt.id());
  }

  int id() const
  {
    return m_id;
  }
};

inline
Configuration& operator &(Configuration& conf, Floor const& floor)
{
  std::string const sect =
      "FLOOR_"
      + boost::lexical_cast<std::string>(floor.id());

  Configuration temp;
  for (
      Floor::const_iterator it = floor.begin(), et = floor.end();
      it != et;
      ++it) {
    temp & it->second;
  }

  conf.put_child(sect, temp);

  return conf;
}

inline
bool operator ==(Floor const& first, Floor const& second)
{
  return first.id() == second.id();
}

inline
bool operator !=(Floor const& first, Floor const& second)
{
  return first.id() != second.id();
}

inline
Floor make_floor(Configuration const& conf, std::string const& key)
{
  int const id = ::id(key);

  Floor floor(id);

  for (
      Configuration::const_iterator it = conf.begin(), et = conf.end();
      it != et;
      ++it) {
    floor.add_pmt(make_pmt(it->second, it->first));
  }

  return floor;
}

class Tower
{
  int m_id;
  std::map<int, Floor> m_floors;

 public:

  typedef std::map<int, Floor>::iterator iterator;
  typedef std::map<int, Floor>::const_iterator const_iterator;

  explicit
  Tower(int id)
    :
    m_id(id)
  {
    assert(id >= 0 && "Negative tower ID provided.");
  }

  Tower()
    :
    m_id(0)
  {}

  iterator begin()
  {
    return m_floors.begin();
  }

  const_iterator begin() const
  {
    return m_floors.begin();
  }

  iterator end()
  {
    return m_floors.end();
  }

  const_iterator end() const
  {
    return m_floors.end();
  }

  std::size_t size() const
  {
    return m_floors.size();
  }

  Floor& operator [](int id)
  {
    std::map<int, Floor>::iterator const it = m_floors.find(id);

    if (it == m_floors.end()) {
      throw std::runtime_error("Requested floor is not present.");
    }

    return it->second;
  }

  Floor const& operator [](int id) const
  {
    std::map<int, Floor>::const_iterator const it = m_floors.find(id);

    if (it == m_floors.end()) {
      throw std::runtime_error("Requested floor is not present.");
    }

    return it->second;
  }

  std::pair<iterator, bool> add_floor(Floor const& floor)
  {
    return m_floors.insert(std::make_pair(floor.id(), floor));
  }

  void erase_floor(Floor const& floor)
  {
    m_floors.erase(floor.id());
  }

  int id() const
  {
    return m_id;
  }
};

inline
Configuration& operator &(Configuration& conf, Tower const& tower)
{
  std::string const sect =
      "TOWER_"
      + boost::lexical_cast<std::string>(tower.id());

  Configuration temp;
  for (
      Tower::const_iterator it = tower.begin(), et = tower.end();
      it != et;
      ++it) {
    temp & it->second;
  }

  conf.put_child(sect, temp);

  return conf;
}

inline
Tower make_tower(Configuration const& conf, int tower_id)
{
  std::string const sect =
      "TOWER_"
      + boost::lexical_cast<std::string>(tower_id);

  Configuration::const_assoc_iterator const it = conf.find(sect);

  if (it == conf.not_found()) {
    throw std::runtime_error("Requested tower not found.");
  }

  Configuration const& child = it->second;

  Tower tower(tower_id);

  for (
      Configuration::const_iterator it = child.begin(), et = child.end();
      it != et;
      ++it) {
    tower.add_floor(make_floor(it->second, it->first));
  }

  return tower;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_DETECTOR_HPP
