#ifndef TRIDAS_PACKAGES_DATACARD_TCPU_HPP
#define TRIDAS_PACKAGES_DATACARD_TCPU_HPP

#include "Configuration.h"
#include "tcpu_endpoints.hpp"
#include "trigger_parameters.hpp"
#include "plugins.hpp"
#include <string>

namespace tridas {
namespace datacard {
namespace element {

class TCPU
{
  std::string m_log_level;
  int m_base_ctrl_port;
  int m_base_data_port;
  std::string m_plugins_dir;
  int m_parallel_tts;
  TcpuEndpoints m_endpoints;
  TriggerParameters m_trigger_parameters;
  Plugins m_plugins;

 public:

  TCPU(
      std::string log_level,
      int base_ctrl_port,
      int base_data_port,
      std::string plugins_dir,
      int parallel_tts,
      TcpuEndpoints const& endpoints,
      TriggerParameters const& trigger_parameters,
      Plugins const& plug)
    :
    m_log_level(log_level),
    m_base_ctrl_port(base_ctrl_port),
    m_base_data_port(base_data_port),
    m_plugins_dir(plugins_dir),
    m_parallel_tts(parallel_tts),
    m_endpoints(endpoints),
    m_trigger_parameters(trigger_parameters),
    m_plugins(plug)
  {}

  std::string logLevel() const
  {
    return m_log_level;
  }

  void logLevel(std::string const& log_level)
  {
    m_log_level = log_level;
  }

  int baseCtrlPort() const
  {
    return m_base_ctrl_port;
  }

  void baseCtrlPort(int base_ctrl_port)
  {
    m_base_ctrl_port = base_ctrl_port;
  }

  int baseDataPort() const
  {
    return m_base_data_port;
  }

  void baseDataPort(int base_data_port)
  {
    m_base_data_port = base_data_port;
  }

  std::string pluginDir() const
  {
    return m_plugins_dir;
  }

  void pluginDir(std::string const& plugins_dir)
  {
    m_plugins_dir = plugins_dir;
  }

  int parallelTts() const
  {
    return m_parallel_tts;
  }

  void parallelTts(int parallel_tts)
  {
    m_parallel_tts = parallel_tts;
  }

  TcpuEndpoints& endpoints()
  {
    return m_endpoints;
  }

  TcpuEndpoints const& endpoints() const
  {
    return m_endpoints;
  }

  TriggerParameters& triggerParameters()
  {
    return m_trigger_parameters;
  }

  TriggerParameters const& triggerParameters() const
  {
    return m_trigger_parameters;
  }

  Plugins& plugins()
  {
    return m_plugins;
  }

  Plugins const& plugins() const
  {
    return m_plugins;
  }
};

inline
TCPU make_tcpu(Configuration const& conf)
{
  return TCPU(
      conf.get<std::string>("TCPU.LOG_LEVEL"),
      conf.get<int>("TCPU.BASE_CTRL_PORT"),
      conf.get<int>("TCPU.BASE_DATA_PORT"),
      conf.get<std::string>("TCPU.PLUGINS_DIR"),
      conf.get<int>("TCPU.PARALLEL_TTS"),
      make_tcpu_endpoints(conf.get_child("TCPU")),
      make_trigger_parameters(conf.get_child("TCPU")),
      make_plugins(conf.get_child("TCPU.PLUGINS")));
}

inline
Configuration& operator &(Configuration& conf, TCPU const& tcpu)
{
  conf.put("TCPU.LOG_LEVEL", tcpu.logLevel());
  conf.put("TCPU.BASE_CTRL_PORT", tcpu.baseCtrlPort());
  conf.put("TCPU.BASE_DATA_PORT", tcpu.baseDataPort());
  conf.put("TCPU.PLUGINS_DIR", tcpu.pluginDir());
  conf.put("TCPU.PARALLEL_TTS", tcpu.parallelTts());

  {
    Configuration array;

    array & tcpu.endpoints();

    conf.put_child("TCPU.HOSTS", array.get_child("HOSTS"));
  }

  {
    Configuration trig_param;

    trig_param & tcpu.triggerParameters();

    conf.put_child(
        "TCPU.TRIGGER_PARAMETERS",
        trig_param.get_child("TRIGGER_PARAMETERS"));
  }

  {
    Configuration plugins;

    conf.get_child("TCPU") & tcpu.plugins();

  }

  return conf;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TCPU_HPP
