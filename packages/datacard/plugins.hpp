#ifndef TRIDAS_PACKAGES_DATACARD_PLUGINS_HPP
#define TRIDAS_PACKAGES_DATACARD_PLUGINS_HPP

#include <vector>
#include <stdexcept>

#include "Configuration.h"
#include "plugin.hpp"

namespace tridas {
namespace datacard {
namespace element {

class Plugins
{
  std::vector<Plugin> m_plugins;

 public:

  void add(Plugin const& plug)
  {
    typedef std::vector<Plugin>::const_iterator citerator;
    for (
        citerator it = m_plugins.begin(), et = m_plugins.end();
        it != et;
        ++it) {
      if (it->id() == plug.id()) {

        throw std::runtime_error("Plugin id already present.");
      }
    }

    m_plugins.push_back(plug);
  }

  std::size_t size() const
  {
    return m_plugins.size();
  }

  Plugin const& operator [](std::size_t n) const
  {
    return m_plugins[n];
  }

  Plugin& operator [](std::size_t n)
  {
    return m_plugins[n];
  }
};

inline
Configuration& operator &(Configuration& conf, Plugins const& plugs)
{
  Configuration child;

  for (unsigned int i = 0; i < plugs.size(); ++i) {
    child & plugs[i];
  }

  conf.put_child("PLUGINS", child);

  return conf;
}

inline
Plugins make_plugins(Configuration const& conf)
{
  typedef Configuration::const_iterator citerator;

  Plugins plugs;

  for (citerator it = conf.begin(), et = conf.end(); it != et; ++it) {
    plugs.add(make_plugin(it->first, it->second));
  }

  return plugs;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_PLUGINS_HPP
