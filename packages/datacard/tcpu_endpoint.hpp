#ifndef TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINT_HPP
#define TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINT_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace detail {

class TcpuEndpoint
{
  std::string m_ctrl_host;
  std::string m_data_host;
  int m_n_instances;

 public:

  TcpuEndpoint(
      std::string const& ctrl_host,
      std::string const& data_host,
      int n_instances)
    :
    m_ctrl_host(ctrl_host),
    m_data_host(data_host),
    m_n_instances(n_instances)
  {}

  std::string ctrlHost() const
  {
    return m_ctrl_host;
  }

  void ctrlHost(std::string const& ctrl_host)
  {
    m_ctrl_host = ctrl_host;
  }

  std::string dataHost() const
  {
    return m_data_host;
  }

  void dataHost(std::string const& data_host)
  {
    m_data_host = data_host;
  }

  int instances() const
  {
    return m_n_instances;
  }

  void instances(int n)
  {
    m_n_instances = n;
  }
};

inline
Configuration& operator &(Configuration& conf, TcpuEndpoint const& te)
{
  conf.put("CTRL_HOST", te.ctrlHost());
  conf.put("DATA_HOST", te.dataHost());
  conf.put("N_INSTANCES", te.instances());

  return conf;
}

inline
bool operator ==(TcpuEndpoint const& first, TcpuEndpoint const& second)
{
  return first.ctrlHost() == second.ctrlHost()
      && first.dataHost() == second.dataHost();
}

inline
bool operator !=(TcpuEndpoint const& first, TcpuEndpoint const& second)
{
  return ! (first == second);
}

inline
TcpuEndpoint make_tcpu_endpoint(Configuration const& conf)
{
  return TcpuEndpoint(
      conf.get<std::string>("CTRL_HOST"),
      conf.get<std::string>("DATA_HOST"),
      conf.get<int>("N_INSTANCES"));
}

} // ns detail
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINT_HPP
