#ifndef TRIDAS_PACKAGES_DATACARD_MONITOR_HPP
#define TRIDAS_PACKAGES_DATACARD_MONITOR_HPP

#include "Configuration.h"
#include <vector>

namespace tridas {
namespace datacard {
namespace element {

class Monitor
{
  std::string m_ctrl_host;
  int m_ctrl_port;
  int m_time_interval;

 public:

  Monitor(std::string const& ctrl_host, int ctrl_port, int time_interval)
    :
    m_ctrl_host(ctrl_host),
    m_ctrl_port(ctrl_port),
    m_time_interval(time_interval)
  {}

  std::string ctrlHost() const
  {
    return m_ctrl_host;
  }

  void ctrlHost(std::string const& ctrl_host)
  {
    m_ctrl_host = ctrl_host;
  }

  int ctrlPort() const
  {
    return m_ctrl_port;
  }

  void ctrlPort(int ctrl_port)
  {
    m_ctrl_port = ctrl_port;
  }

  int timeInterval() const
  {
    return m_time_interval;
  }

  void timeInterval(int time_interval)
  {
    m_time_interval = time_interval;
  }
};

inline
Configuration& operator &(Configuration& conf, Monitor const& mon)
{
  conf.put("MONITOR.CTRL_HOST", mon.ctrlHost());
  conf.put("MONITOR.CTRL_PORT", mon.ctrlPort());
  conf.put("MONITOR.TIME_INTERVAL", mon.timeInterval());

  return conf;
}

inline
Monitor make_monitor(Configuration const& conf)
{
  return Monitor(
    conf.get<std::string>("MONITOR.CTRL_HOST"),
    conf.get<int>("MONITOR.CTRL_PORT"),
    conf.get<int>("MONITOR.TIME_INTERVAL"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_MONITOR_HPP
