#ifndef TRIDAS_PACKAGES_DATACARD_EM_HPP
#define TRIDAS_PACKAGES_DATACARD_EM_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace element {

class EM
{
  std::string m_ctrl_host;
  std::string m_data_host;
  int m_data_port;
  int m_network_threads;
  std::string m_log_level;
  bool m_log_to_syslog;
  int m_file_max_size;
  std::string m_pt_file_prefix;
  std::string m_pt_file_postfix;

 public:

  EM(
      std::string const& ctrl_host,
      std::string const& data_host,
      int data_port,
      int network_threads,
      std::string const& log_level,
      bool log_to_syslog,
      int file_max_size,
      std::string const& pt_file_prefix,
      std::string const& pt_file_postfix)
    :
    m_ctrl_host(ctrl_host),
    m_data_host(data_host),
    m_data_port(data_port),
    m_network_threads(network_threads),
    m_log_level(log_level),
    m_log_to_syslog(log_to_syslog),
    m_file_max_size(file_max_size),
    m_pt_file_prefix(pt_file_prefix),
    m_pt_file_postfix(pt_file_postfix)
  {}

  std::string ctrlHost() const
  {
    return m_ctrl_host;
  }

  void ctrlHost(std::string const& ctrl_host)
  {
    m_ctrl_host = ctrl_host;
  }

  std::string dataHost() const
  {
    return m_data_host;
  }

  void dataHost(std::string const& data_host)
  {
    m_data_host = data_host;
  }

  int dataPort() const
  {
    return m_data_port;
  }

  void dataPort(int data_port)
  {
    m_data_port = data_port;
  }

  int networkThreads() const
  {
    return m_network_threads;
  }

  void networkThreads(int network_threads)
  {
    m_network_threads = network_threads;
  }

  std::string logLevel() const
  {
    return m_log_level;
  }

  void logLevel(std::string const& log_level)
  {
    m_log_level = log_level;
  }

  bool logToSyslog() const
  {
    return m_log_to_syslog;
  }

  void logToSyslog(bool log_to_syslog)
  {
    m_log_to_syslog = log_to_syslog;
  }

  int fileMaxSize() const
  {
    return m_file_max_size;
  }

  void fileMaxSize(int file_max_size)
  {
    m_file_max_size = file_max_size;
  }

  std::string ptFilePrefix() const
  {
    return m_pt_file_prefix;
  }

  void ptFilePrefix(std::string const& pt_file_prefix)
  {
    m_pt_file_prefix = pt_file_prefix;
  }

  std::string ptFilePostfix() const
  {
    return m_pt_file_postfix;
  }

  void ptFilePostfix(std::string const& pt_file_postfix)
  {
    m_pt_file_postfix = pt_file_postfix;
  }
};

inline
EM make_em(Configuration const& conf)
{
  return EM(
      conf.get<std::string>("EM.CTRL_HOST"),
      conf.get<std::string>("EM.DATA_HOST"),
      conf.get<int>("EM.DATA_PORT"),
      conf.get<int>("EM.NETWORK_THREADS"),
      conf.get<std::string>("EM.LOG_LEVEL"),
      conf.get<bool>("EM.LOG_TO_SYSLOG"),
      conf.get<int>("EM.FILE_MAX_SIZE"),
      conf.get<std::string>("EM.PT_FILE_PREFIX"),
      conf.get<std::string>("EM.PT_FILE_POSTFIX"));
}

inline
Configuration& operator &(Configuration& conf, EM const& em)
{
  conf.put("EM.CTRL_HOST", em.ctrlHost());
  conf.put("EM.DATA_HOST", em.dataHost());
  conf.put("EM.DATA_PORT", em.dataPort());
  conf.put("EM.NETWORK_THREADS", em.networkThreads());
  conf.put("EM.LOG_LEVEL", em.logLevel());
  conf.put("EM.LOG_TO_SYSLOG", em.logToSyslog());
  conf.put("EM.FILE_MAX_SIZE", em.fileMaxSize());
  conf.put("EM.PT_FILE_PREFIX", em.ptFilePrefix());
  conf.put("EM.PT_FILE_POSTFIX", em.ptFilePostfix());

  return conf;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_EM_HPP
