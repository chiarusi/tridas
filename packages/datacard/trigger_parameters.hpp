#ifndef TRIDAS_PACKAGES_DATACARD_TRIGGER_PARAMETERS_HPP
#define TRIDAS_PACKAGES_DATACARD_TRIGGER_PARAMETERS_HPP

#include "Configuration.h"

namespace tridas {
namespace datacard {
namespace element {

class TriggerParameters
{
  int m_l1_event_window_half_size;
  int m_l1_delta_time_sc;
  int m_l1_delta_time_fc;
  int m_l1_charge_threshold;
  int m_l1_flag_rt;
  int m_l1_frequency_rt;
  int m_l1_delta_time_rt;

 public:

  TriggerParameters(
      int l1_event_window_half_size,
      int l1_delta_time_sc,
      int l1_delta_time_fc,
      int l1_charge_threshold,
      int l1_flag_rt,
      int l1_frequency_rt,
      int l1_delta_time_rt)
    :
    m_l1_event_window_half_size(l1_event_window_half_size),
    m_l1_delta_time_sc(l1_delta_time_sc),
    m_l1_delta_time_fc(l1_delta_time_fc),
    m_l1_charge_threshold(l1_charge_threshold),
    m_l1_flag_rt(l1_flag_rt),
    m_l1_frequency_rt(l1_frequency_rt),
    m_l1_delta_time_rt(l1_delta_time_rt)
  {}

  int l1EventWindowHalfSize()const
  {
    return m_l1_event_window_half_size;
  }

  int l1DeltaTimeSc() const
  {
    return m_l1_delta_time_sc;
  }

  int l1DeltaTimeFc() const
  {
    return m_l1_delta_time_fc;
  }

  int l1ChargeThreshold() const
  {
    return m_l1_charge_threshold;
  }

  int l1FlagRt() const
  {
    return m_l1_flag_rt;
  }

  int l1FrequencyRt() const
  {
    return m_l1_frequency_rt;
  }

  int l1DeltaTimeRt() const
  {
    return m_l1_delta_time_rt;
  }

  void l1EventWindowHalfSize (int l1_event_window_half_size)
  {
    m_l1_event_window_half_size = l1_event_window_half_size;
  }

  void l1DeltaTimeSc (int l1_delta_time_sc)
  {
    m_l1_delta_time_sc = l1_delta_time_sc;
  }

  void l1DeltaTimeFc (int l1_delta_time_fc)
  {
    m_l1_delta_time_fc = l1_delta_time_fc;
  }

  void l1ChargeThreshold (int l1_charge_threshold)
  {
    m_l1_charge_threshold = l1_charge_threshold;
  }

  void l1FlagRt (int l1_flag_rt)
  {
    m_l1_flag_rt = l1_flag_rt;
  }

  void l1FrequencyRt (int l1_frequency_rt)
  {
    m_l1_frequency_rt = l1_frequency_rt;
  }

  void l1DeltaTimeRt (int l1_delta_time_rt)
  {
    m_l1_delta_time_rt = l1_delta_time_rt;
  }

};

inline
Configuration& operator &(Configuration& conf, TriggerParameters const& tp)
{
  conf.put("TRIGGER_PARAMETERS.L1_EVENT_WINDOW_HALF_SIZE", tp.l1EventWindowHalfSize());
  conf.put("TRIGGER_PARAMETERS.L1_DELTA_TIME_SC", tp.l1DeltaTimeSc());
  conf.put("TRIGGER_PARAMETERS.L1_DELTA_TIME_FC", tp.l1DeltaTimeFc());
  conf.put("TRIGGER_PARAMETERS.L1_CHARGE_THRESHOLD", tp.l1ChargeThreshold());
  conf.put("TRIGGER_PARAMETERS.L1_FLAG_RT", tp.l1FlagRt());
  conf.put("TRIGGER_PARAMETERS.L1_FREQUENCY_RT", tp.l1FrequencyRt());
  conf.put("TRIGGER_PARAMETERS.L1_DELTA_TIME_RT", tp.l1DeltaTimeRt());

  return conf;
}

inline
TriggerParameters make_trigger_parameters(Configuration const& conf)
{
  return TriggerParameters(
      conf.get<int>("TRIGGER_PARAMETERS.L1_EVENT_WINDOW_HALF_SIZE"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_DELTA_TIME_SC"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_DELTA_TIME_FC"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_CHARGE_THRESHOLD"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_FLAG_RT"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_FREQUENCY_RT"),
      conf.get<int>("TRIGGER_PARAMETERS.L1_DELTA_TIME_RT"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TRIGGER_PARAMETERS_HPP
