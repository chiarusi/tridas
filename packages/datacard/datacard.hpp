#ifndef TRIDAS_PACKAGES_DATACARD_HPP
#define TRIDAS_PACKAGES_DATACARD_HPP

#include "detector_geometry.hpp"
#include "internal_sw_parameters.hpp"
#include "fcm_endpoints.hpp"
#include "hm.hpp"
#include "tcpu.hpp"
#include "em.hpp"
#include "tsv.hpp"
#include "tsc.hpp"
#include "monitor.hpp"
#include "detector.hpp"

#endif // TRIDAS_PACKAGES_DATACARD_HPP
