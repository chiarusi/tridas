#ifndef TRIDAS_PACKAGES_DATACARD_TSV_HPP
#define TRIDAS_PACKAGES_DATACARD_TSV_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace element {

class TSV
{
  std::string m_log_level;
  std::string m_ctrl_host;

 public:

  TSV(std::string const& log_level, std::string const& ctrl_host)
    :
    m_log_level(log_level),
    m_ctrl_host(ctrl_host)
  {}

  std::string logLevel() const
  {
    return m_log_level;
  }

  void logLevel(std::string const& log_level)
  {
    m_log_level = log_level;
  }

  std::string ctrlHost() const
  {
    return m_ctrl_host;
  }

  void ctrlHost(std::string const& ctrl_host)
  {
    m_ctrl_host = ctrl_host;
  }
};

inline
Configuration& operator &(Configuration& conf, TSV const& tsv)
{
  conf.put("TSV.LOG_LEVEL", tsv.logLevel());
  conf.put("TSV.CTRL_HOST", tsv.ctrlHost());

  return conf;
}

inline
TSV make_tsv(Configuration const& conf)
{
  return TSV(
    conf.get<std::string>("TSV.LOG_LEVEL"),
    conf.get<std::string>("TSV.CTRL_HOST"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TSV_HPP
