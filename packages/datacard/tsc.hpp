#ifndef TRIDAS_PACKAGES_DATACARD_TSC_HPP
#define TRIDAS_PACKAGES_DATACARD_TSC_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace element {

class TSC
{
  std::string m_datacard_shareddir;

 public:

  explicit
  TSC(std::string const& datacard_shareddir)
    :
    m_datacard_shareddir(datacard_shareddir)
  {}

  std::string datacardShareddir() const
  {
    return m_datacard_shareddir;
  }

  void datacardShareddir(std::string const& datacard_shareddir)
  {
    m_datacard_shareddir = datacard_shareddir;
  }
};

Configuration& operator &(Configuration& conf, TSC const& mon)
{
  conf.put("TSC.DATACARD_SHAREDDIR", mon.datacardShareddir());

  return conf;
}

TSC make_tsc(Configuration const& conf)
{
  return TSC(conf.get<std::string>("TSC.DATACARD_SHAREDDIR"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TSC_HPP
