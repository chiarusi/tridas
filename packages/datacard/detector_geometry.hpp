#ifndef TRIDAS_PACKAGES_DATACARD_DETECTOR_GEOMETRY_HPP
#define TRIDAS_PACKAGES_DATACARD_DETECTOR_GEOMETRY_HPP

#include "Configuration.h"

namespace tridas {
namespace datacard {
namespace element {

class DetectorGeometry
{
  int m_pmts;
  int m_floors;
  int m_towers;

 public:

  DetectorGeometry(int pmts_per_floor, int floors_per_tower, int towers)
    :
    m_pmts(pmts_per_floor),
    m_floors(floors_per_tower),
    m_towers(towers)
  {}

  int pmts() const
  {
    return m_pmts;
  }

  void pmts(int pmts_per_floor)
  {
    m_pmts = pmts_per_floor;
  }

  int floors() const
  {
    return m_floors;
  }

  void floors(int floors_per_tower)
  {
    m_floors = floors_per_tower;
  }

  int towers() const
  {
    return m_towers;
  }

  void towers(int towers)
  {
    m_towers = towers;
  }
};

inline
Configuration& operator &(Configuration& conf, DetectorGeometry const& dg)
{
  conf.put("DETECTOR_GEOMETRY.PMTS", dg.pmts());
  conf.put("DETECTOR_GEOMETRY.FLOORS", dg.floors());
  conf.put("DETECTOR_GEOMETRY.TOWERS", dg.towers());

  return conf;
}

inline
DetectorGeometry make_detector_geometry(Configuration const& conf)
{
  return DetectorGeometry(
      conf.get<int>("DETECTOR_GEOMETRY.PMTS"),
      conf.get<int>("DETECTOR_GEOMETRY.FLOORS"),
      conf.get<int>("DETECTOR_GEOMETRY.TOWERS"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_DETECTOR_GEOMETRY_HPP
