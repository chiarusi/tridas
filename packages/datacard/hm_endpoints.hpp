#ifndef TRIDAS_PACKAGES_DATACARD_HM_ENDPOINTS_HPP
#define TRIDAS_PACKAGES_DATACARD_HM_ENDPOINTS_HPP

#include "Configuration.h"
#include <vector>
#include "hm_endpoint.hpp"

namespace tridas {
namespace datacard {
namespace element {

class HmEndpoints
{
  std::vector<detail::HmEndpoint> m_endpoints;

 public:

  std::size_t size() const
  {
    return m_endpoints.size();
  }

  detail::HmEndpoint const& operator [](std::size_t n) const
  {
    return m_endpoints[n];
  }

  detail::HmEndpoint& operator [](std::size_t n)
  {
    return m_endpoints[n];
  }

  void add_endpoint(std::string const& host, int n_instances = 1)
  {
    assert(n_instances > 0);

    detail::HmEndpoint const endpoint(host, n_instances);

    add_endpoint(endpoint);
  }

  void add_endpoint(detail::HmEndpoint const& endpoint)
  {
    std::vector<detail::HmEndpoint>::iterator const it
        = std::find(m_endpoints.begin(), m_endpoints.end(), endpoint);

    if (it != m_endpoints.end()) {
      it->instances(it->instances() + endpoint.instances());
    } else {
      m_endpoints.push_back(endpoint);
    }
  }
};

inline
Configuration& operator &(Configuration& conf, HmEndpoints const& hms)
{
  Configuration array;
  for (unsigned int i = 0; i < hms.size(); ++i) {
    if (hms[i].instances() > 0) {
      Configuration tmp;
      tmp & hms[i];
      array.push_back(std::make_pair("", tmp));
    }
  }

  conf.put_child("HOSTS", array);

  return conf;
}

inline
HmEndpoints make_hm_endpoints(Configuration const& conf)
{
  HmEndpoints hmes;

  Configuration const& child = conf.get_child("HOSTS");

  for (Configuration::const_iterator it = child.begin(); it != child.end(); ++it) {
    detail::HmEndpoint const endpoint = detail::make_hm_endpoint(it->second);

    hmes.add_endpoint(endpoint);
  }
  return hmes;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_HM_ENDPOINTS_HPP
