#ifndef TRIDAS_PACKAGES_DATACARD_PLUGIN_HPP
#define TRIDAS_PACKAGES_DATACARD_PLUGIN_HPP

#include "Configuration.h"

namespace tridas {
namespace datacard {
namespace element {

class Plugin
{
  std::string m_name;
  std::string m_libname;
  int m_id;
  Configuration m_parameters;

 public:

  Plugin(
      std::string const& name,
      std::string const& libname,
      int id,
      Configuration const& parameters)
    :
    m_name(name),
    m_libname(libname),
    m_id(id),
    m_parameters(parameters)
  {}

  int id() const
  {
    return m_id;
  }

  std::string name() const
  {
    return m_name;
  }

  std::string libname() const
  {
    return m_libname;
  }

  Configuration parameters() const
  {
    return m_parameters;
  }
};

inline
Configuration& operator &(Configuration& conf, Plugin const& tp)
{
  conf.put(tp.name() + ".NAME", tp.libname());
  conf.put(tp.name() + ".ID", tp.id());
  conf.put_child(tp.name() + ".PARAMETERS", tp.parameters());

  return conf;
}

inline
Plugin make_plugin(std::string const& name, Configuration const& conf)
{
  return Plugin(
      name,
      conf.get<std::string>("NAME"),
      conf.get<int>("ID"),
      conf.get_child("PARAMETERS"));
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_PLUGIN_HPP
