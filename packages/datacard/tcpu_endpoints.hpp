#ifndef TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINTS_HPP
#define TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINTS_HPP

#include "Configuration.h"
#include <vector>
#include "tcpu_endpoint.hpp"

namespace tridas {
namespace datacard {
namespace element {

class TcpuEndpoints
{
  std::vector<detail::TcpuEndpoint> m_endpoints;

 public:

  std::size_t size() const
  {
    return m_endpoints.size();
  }

  detail::TcpuEndpoint const& operator [](std::size_t n) const
  {
    return m_endpoints[n];
  }

  detail::TcpuEndpoint& operator [](std::size_t n)
  {
    return m_endpoints[n];
  }

  void add_endpoint(
      std::string const& ctrl_host,
      std::string const& data_host,
      int n_instances = 1)
  {
    assert(n_instances > 0);

    detail::TcpuEndpoint const endpoint(ctrl_host, data_host, n_instances);

    add_endpoint(endpoint);
  }

  void add_endpoint(detail::TcpuEndpoint const& endpoint)
  {
    std::vector<detail::TcpuEndpoint>::iterator const it
        = std::find(m_endpoints.begin(), m_endpoints.end(), endpoint);

    if (it != m_endpoints.end()) {
      it->instances(it->instances() + endpoint.instances());
    } else {
      m_endpoints.push_back(endpoint);
    }
  }
};

inline
Configuration& operator &(Configuration& conf, TcpuEndpoints const& tcpus)
{
  Configuration array;
  for (unsigned int i = 0; i < tcpus.size(); ++i) {
    if (tcpus[i].instances() > 0) {
      Configuration tmp;
      tmp & tcpus[i];
      array.push_back(std::make_pair("", tmp));
    }
  }

  conf.put_child("HOSTS", array);

  return conf;
}

inline
TcpuEndpoints make_tcpu_endpoints(Configuration const& conf)
{
  TcpuEndpoints tcpues;

  Configuration const& child = conf.get_child("HOSTS");

  for (Configuration::const_iterator it = child.begin(); it != child.end(); ++it) {
    detail::TcpuEndpoint const endpoint = detail::make_tcpu_endpoint(it->second);

    tcpues.add_endpoint(endpoint);
  }
  return tcpues;
}

} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_TCPU_ENDPOINTS_HPP
