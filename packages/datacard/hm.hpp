#ifndef TRIDAS_PACKAGES_DATACARD_HM_HPP
#define TRIDAS_PACKAGES_DATACARD_HM_HPP

#include "Configuration.h"
#include "hm_endpoints.hpp"

namespace tridas {
namespace datacard {
namespace element {

class HM
{
  std::string m_log_level;
  int m_base_ctrl_port;
  HmEndpoints m_endpoints;

 public:

  HM()
    :
    m_log_level(""),
    m_base_ctrl_port(0)
  {}

  HM(
      std::string const& log_level,
      int base_ctrl_port,
      int dump_flag,
      std::string const& dump_filename_prefix,
      int dump_max_size,
      HmEndpoints const& endpoints)
    :
    m_log_level(log_level),
    m_base_ctrl_port(base_ctrl_port),
    m_endpoints(endpoints)
  {}

  std::string logLevel() const
  {
    return m_log_level;
  }

  void logLevel(std::string const& log_level)
  {
    m_log_level = log_level;
  }

  int baseCtrlPort() const
  {
    return m_base_ctrl_port;
  }

  void baseCtrlPort(int base_ctrl_port)
  {
    m_base_ctrl_port = base_ctrl_port;
  }

  HmEndpoints& endpoints()
  {
    return m_endpoints;
  }

  HmEndpoints const& endpoints() const
  {
    return m_endpoints;
  }
};

inline
Configuration& operator &(Configuration& conf, HM const& hm)
{
  conf.put("HM.LOG_LEVEL", hm.logLevel());
  conf.put("HM.BASE_CTRL_PORT", hm.baseCtrlPort());

  Configuration array;

  array & hm.endpoints();

  conf.put_child("HM.HOSTS", array.get_child("HOSTS"));

  return conf;
}

HM make_hm(Configuration const& conf)
{
  return HM(
      conf.get<std::string>("HM.LOG_LEVEL"),
      conf.get<int>("HM.BASE_CTRL_PORT"),
      conf.get<int>("HM.DUMP_FLAG"),
      conf.get<std::string>("HM.DUMP_FILENAME_PREFIX"),
      conf.get<int>("HM.DUMP_MAX_SIZE"),
      make_hm_endpoints(conf.get_child("HM")));
}


} // ns element
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_HM_HPP
