#include "datacard.hpp"
#include <boost/detail/lightweight_test.hpp>
#include <cassert>
#include <fstream>

int main(int argc, char* argv[])
{
  if (argc == 2) {
    // If the datacard is not valid this will fail
    std::ifstream dc_file(argv[1]);

    Configuration const conf = read_configuration(dc_file);

    using namespace tridas::datacard::element;

    DetectorGeometry const dg = make_detector_geometry(conf);
    InternalSwParameters const isp = make_internal_sw_parameters(conf);
    FcmEndpoints const fcme = make_fcm_endpoints(conf);
    HM const hm = make_hm(conf);
    TCPU const tcpu = make_tcpu(conf);
    EM const em = make_em(conf);
    TSV const tsv = make_tsv(conf);
    TSC const tsc = make_tsc(conf);
    Monitor const mon = make_monitor(conf);
    Tower towers[8];

    for (int i = 0; i < 8; ++i) {
      towers[i] = make_tower(conf, i);
    }
  }


  int const npmts = 6;
  int const nfloors = 14;
  int const ntowers = 8;

  Configuration datacard;

  {
    tridas::datacard::element::DetectorGeometry const geom(npmts, nfloors, ntowers);

    datacard & geom;
  }
  {
    tridas::datacard::element::DetectorGeometry const geom
        = tridas::datacard::element::make_detector_geometry(datacard);

    BOOST_TEST_EQ(geom.pmts(), npmts);
    BOOST_TEST_EQ(geom.floors(), nfloors);
    BOOST_TEST_EQ(geom.towers(), ntowers);
  }

  {
    tridas::datacard::element::InternalSwParameters const isp(
        200,
        1000000,
        5,
        30,
        300);

    datacard & isp;
  }
  {
    tridas::datacard::element::FcmEndpoints fcm;

    fcm.add_endpoint("lxbudda", 2000);
    fcm.add_endpoint("lxtom", 10000);

    datacard & fcm;
  }
  {
    tridas::datacard::element::HM hm;

    hm.endpoints().add_endpoint("lxbudda", 19);
    hm.endpoints().add_endpoint("lxtom", 15);

    datacard & hm;
  }
  {
    tridas::datacard::element::TcpuEndpoints endpoints;

    endpoints.add_endpoint("lxbudda1", "lxbudda2", 4);
    endpoints.add_endpoint("km3tridas9", "km3tridas5", 16);

    tridas::datacard::element::TriggerParameters trig_parameters(
        600,
        10,
        20,
        18,
        1,
        1000,
        1000);

    tridas::datacard::element::Plugins plugins;

    plugins.add(
        tridas::datacard::element::Plugin(
            "RANDOM",
            "TrigRandom",
            0,
            Configuration()));

    tridas::datacard::element::TCPU tcpu(
        "DEBUG",
        5000,
        5600,
        "./",
        3,
        endpoints,
        trig_parameters,
        plugins);

    datacard & tcpu;
  }
  {
    tridas::datacard::element::TSV const tsv("DEBUG", "localhost");

    datacard & tsv;
  }
  {
    tridas::datacard::element::EM em(
        "lxantares3",
        "km3tridas29",
        6969,
        3,
        "INFO",
        true,
        1000000000,
        "nemo_p4.5",
        ".dat");

    datacard & em;
  }
  {
    tridas::datacard::element::Monitor const mon("localhost", 9999, 2);

    datacard & mon;
  }
  {
    tridas::datacard::element::TSC const tsc("./");

    datacard & tsc;
  }
  {
    for (int itower = 0; itower < 8; ++itower) {
      tridas::datacard::element::Tower tower(itower);

      for (int ifloor = 1; ifloor < 15; ++ifloor) {
        tridas::datacard::element::Floor floor(ifloor);

        for (int ipmt = 0; ipmt < 6; ++ipmt) {
          floor.add_pmt(tridas::datacard::element::PMT(
              ipmt,
              rand() % 300,
              rand() % 300,
              rand() % 300,
              (1. * rand()) / RAND_MAX,
              (1. * rand()) / RAND_MAX,
              (1. * rand()) / RAND_MAX,
              rand() % 10,
              rand() % 5,
              rand() % 10));
        }

        tower.add_floor(floor);
      }
      datacard & tower;
    }
  }
  {
    tridas::datacard::element::Tower t5 = tridas::datacard::element::make_tower(datacard, 5);

    BOOST_TEST_EQ(t5[13][5].x(), datacard.get<float>("TOWER_5.FLOOR_13.PMT_5.X"));
    BOOST_TEST_THROWS(t5[17], std::runtime_error);
    BOOST_TEST_THROWS(t5[11][9], std::runtime_error);
  }

  // And now, after having written an entire datacard, lets read and parse it again.
  {
    using namespace tridas::datacard::element;

    DetectorGeometry const dg = make_detector_geometry(datacard);
    InternalSwParameters const isp = make_internal_sw_parameters(datacard);
    FcmEndpoints const fcme = make_fcm_endpoints(datacard);
    HM const hm = make_hm(datacard);
    TCPU const tcpu = make_tcpu(datacard);
    EM const em = make_em(datacard);
    TSV const tsv = make_tsv(datacard);
    TSC const tsc = make_tsc(datacard);
    Monitor const mon = make_monitor(datacard);
    Tower towers[8];

    for (int i = 0; i < 8; ++i) {
      towers[i] = make_tower(datacard, i);
    }

    Configuration temp;

    // the order is inmportant because I want to compare the two json strings.
    temp
        & dg
        & isp
        & fcme
        & hm
        & tcpu
        & tsv
        & em
        & mon
        & tsc;

    for (int i = 0; i < 8; ++i) {
      temp & towers[i];
    }

    std::ostringstream one, two;

    one << datacard;
    two << temp;

    BOOST_TEST_EQ(one.str(), two.str());
  }

  boost::report_errors();
}
