#ifndef TRIDAS_PACKAGES_DATACARD_HM_ENDPOINT_HPP
#define TRIDAS_PACKAGES_DATACARD_HM_ENDPOINT_HPP

#include "Configuration.h"
#include <string>

namespace tridas {
namespace datacard {
namespace detail {

class HmEndpoint
{
  std::string m_ctrl_host;
  int m_n_instances;

 public:

  HmEndpoint(std::string const& ctrl_host, int n_instances)
    :
    m_ctrl_host(ctrl_host),
    m_n_instances(n_instances)
  {}

  std::string ctrlHost() const
  {
    return m_ctrl_host;
  }

  void ctrlHost(std::string const& ctrl_host)
  {
    m_ctrl_host = ctrl_host;
  }

  int instances() const
  {
    return m_n_instances;
  }

  void instances(int n)
  {
    m_n_instances = n;
  }
};

inline
Configuration& operator &(Configuration& conf, HmEndpoint const& he)
{
  conf.put("CTRL_HOST", he.ctrlHost());
  conf.put("N_INSTANCES", he.instances());

  return conf;
}

inline
bool operator ==(HmEndpoint const& first, HmEndpoint const& second)
{
  return first.ctrlHost() == second.ctrlHost();
}

inline
bool operator !=(HmEndpoint const& first, HmEndpoint const& second)
{
  return ! (first == second);
}

inline
HmEndpoint make_hm_endpoint(Configuration const& conf)
{
  return HmEndpoint(
      conf.get<std::string>("CTRL_HOST"),
      conf.get<int>("N_INSTANCES"));
}

} // ns detail
} // ns datacard
} // ns tridas

#endif // TRIDAS_PACKAGES_DATACARD_HM_ENDPOINT_HPP
