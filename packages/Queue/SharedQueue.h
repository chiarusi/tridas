#ifndef PACKAGES_QUEUE_SHAREDQUEUE_H
#define PACKAGES_QUEUE_SHAREDQUEUE_H

#include <queue>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/core/noncopyable.hpp>

template<typename T>
class SharedQueue : private boost::noncopyable {
 private:
  std::queue<T> queue_;
  boost::mutex mutex_;
  boost::condition_variable cond_;

 public:

  void put(const T& msg) {
    boost::mutex::scoped_lock lock(mutex_);
    queue_.push(msg);
    cond_.notify_one();
  }

  bool get_no_wait(T& msg) {
    boost::mutex::scoped_lock lock(mutex_);
    if (queue_.empty()) {
      return false;
    }

    msg = queue_.front();
    queue_.pop();
    return true;
  }

  void get(T& msg) {
    boost::mutex::scoped_lock lock(mutex_);
    while (queue_.empty()) {
      cond_.wait(lock);
    }
    msg = queue_.front();
    queue_.pop();
  }

  template<typename Rep, typename Period>
  bool timed_get(T& msg, boost::chrono::duration<Rep, Period> const& timeout) {
    boost::mutex::scoped_lock lock(mutex_);
    boost::chrono::system_clock::time_point const tp =
        boost::chrono::system_clock::now() + timeout;
    while (queue_.empty()
        && cond_.wait_until(lock, tp) != boost::cv_status::timeout) {

    }
    if (!queue_.empty()) {
      msg = queue_.front();
      queue_.pop();
      return true;
    }
    return false;
  }
};

#endif

