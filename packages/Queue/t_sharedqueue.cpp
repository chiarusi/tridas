#include "SharedQueue.h"
#include <boost/detail/lightweight_test.hpp>
#include <boost/thread.hpp>
#include <vector>

std::vector<int> data;
SharedQueue<int> queue;

void produce()
{
   for (
        std::vector<int>::const_iterator it = data.begin(), et = data.end()
      ; it != et
      ; ++it) {

      queue.put(*it);
   }
}

int main()
{

  for (int i = 0; i < 1000; ++i) {
    data.push_back(i * i);
  }

  {
    boost::thread thread(produce);

    for (
        std::vector<int>::const_iterator it = data.begin(), et = data.end()
      ; it != et
      ; ++it) {

      int n = -1;
      queue.get(n);

      BOOST_TEST_EQ(n, *it);
    }

    thread.join();
  }

  {
    boost::thread thread(produce);

    for (
        std::vector<int>::const_iterator it = data.begin(), et = data.end()
      ; it != et
      ; ++it) {

      int n = -1;
      bool const ret = queue.timed_get(n, boost::chrono::seconds(2));

      BOOST_TEST_EQ(n, *it);
      BOOST_TEST_EQ(ret, true);
    }

    thread.join();
  }

  {
    boost::chrono::system_clock::time_point const start = boost::chrono::system_clock::now();
    for (
        std::vector<int>::const_iterator it = data.begin(), et = data.end()
      ; it != et
      ; ++it) {

      int n = -1;
      bool const ret = queue.timed_get(n, boost::chrono::milliseconds(1));

      BOOST_TEST_EQ(n, -1);
      BOOST_TEST_EQ(ret, false);
    }

    boost::chrono::system_clock::time_point const stop = boost::chrono::system_clock::now();

    boost::chrono::milliseconds const delay =
        boost::chrono::duration_cast<boost::chrono::milliseconds>(stop - start);

    boost::chrono::milliseconds const expected(data.size());

    bool const time_ok = delay >= expected && delay <= 2 * expected;

    BOOST_TEST_EQ(time_ok, true);
  }

  boost::report_errors();
}
