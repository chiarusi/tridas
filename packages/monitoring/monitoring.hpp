// Link with boost_system boost_chrono and pthread:
//   -lboost_system -lboost_chrono -pthread

#ifndef TRIDAS_PACKAGES_MONITORING_MONITORING_HPP
#define TRIDAS_PACKAGES_MONITORING_MONITORING_HPP

#include <string>
#include <cmath>

#include <boost/noncopyable.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>

#include <boost/lexical_cast.hpp>

#include <boost/chrono.hpp>

namespace tridas {

namespace monitoring {

typedef boost::chrono::time_point<boost::chrono::system_clock> TimePoint;
typedef boost::property_tree::ptree Representation;
// This is the common io_service for all monitoring purposes.
// It is possible to use in this way because its usage here is thread
// safe. See:
// http://www.boost.org/doc/libs/1_57_0/doc/html/boost_asio/reference/io_service.html

static boost::asio::io_service service;

// This class can be used to send a single double value to the ROyWeb
// server.
//
// Copyable
//
// Thread safety:
//   Distinct objects: safe
//   Shared objects: unsafe

class SimpleObservable
{
  std::size_t m_counter;
  double      m_value;
  TimePoint m_last_insert;
  Representation m_pt;

 public:

  SimpleObservable(std::string const& tag,
                   std::string const& unit,
                   std::string const& description)
    :
    m_counter(0),
    m_value(NAN)
  {
    m_pt.put("kind", "parameter");
    m_pt.put("type", tag);
    m_pt.put("unit", unit);
    m_pt.put("description", description);
  }

  double value() const
  {
    return m_value;
  }

  Representation const& representation() const
  {
    return m_pt;
  }

  SimpleObservable& put(double val)
  {
    m_value = val;
    m_pt.put("value", val);
    m_last_insert = boost::chrono::system_clock::now();
    ++m_counter;
    return *this;
  }

  std::size_t count() const
  {
    return m_counter;
  }

  TimePoint last_insert_time() const
  {
    return m_last_insert;
  }
};

// This class can be used to send the infinite average of a set of
// double values to the ROyWeb server.
//
// Copyable
//
// Thread safety:
//   Distinct objects: safe
//   Shared objects: unsafe

class AvgObservable
{
  Representation m_pt;
  TimePoint m_last_insert;
  boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean, boost::accumulators::tag::count> > m_accu;

 public:

  AvgObservable(std::string const& tag,
                std::string const& unit,
                std::string const& description)
  {
    m_pt.put("kind", "parameter");
    m_pt.put("type", tag);
    m_pt.put("unit", unit);
    m_pt.put("description", description);
  }

  double value() const
  {
    return boost::accumulators::mean(m_accu);
  }

  Representation const& representation() const
  {
    return m_pt;
  }

  AvgObservable& put(double val)
  {
    m_accu(val);
    m_pt.put("value", value());
    m_last_insert = boost::chrono::system_clock::now();
    return *this;
  }

  std::size_t count() const
  {
    return boost::accumulators::count(m_accu);
  }

  TimePoint last_insert_time() const
  {
    return m_last_insert;
  }
};

// This class can be used to send the running average of a set of N
// double values to the ROyWeb server.
//
// Copyable
//
// Thread safety:
//   Distinct objects: safe
//   Shared objects: unsafe

class RunningAvgObservable
{
  Representation m_pt;
  TimePoint m_last_insert;
  boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::rolling_mean, boost::accumulators::tag::count> > m_accu;

 public:

  RunningAvgObservable(std::string const& tag,
                       std::string const& unit,
                       std::string const& description,
                       unsigned int N)
    :
    m_accu(boost::accumulators::tag::rolling_window::window_size = N)
  {
    m_pt.put("kind", "parameter");
    m_pt.put("type", tag);
    m_pt.put("unit", unit);
    m_pt.put("description", description);
  }

  double value() const
  {
    return boost::accumulators::rolling_mean(m_accu);
  }

  Representation const& representation() const
  {
    return m_pt;
  }

  RunningAvgObservable& put(double val)
  {
    m_accu(val);
    m_pt.put("value", value());
    m_last_insert = boost::chrono::system_clock::now();
    return *this;
  }

  std::size_t count() const
  {
    return boost::accumulators::count(m_accu);
  }

  TimePoint last_insert_time() const
  {
    return m_last_insert;
  }
};

// This function determines if the observable has been initialized.
// Return value: true if initialized, false otherwise.
template<typename Observable>
inline bool initialized(Observable const& obs)
{
  return !isnan(obs.value());
}

// Destination object type.
typedef boost::asio::ip::udp::endpoint Destination;

// This free function resolves the host and create a Destination object.
inline Destination make_destination(std::string const& address,
                                    int port)
{
  boost::asio::ip::udp::resolver resolver(service);
  boost::asio::ip::udp::resolver::query query(
      boost::asio::ip::udp::v4(),
      address,
      boost::lexical_cast<std::string>(port));
  return *resolver.resolve(query);
}

// This function determines the time elapsed since last insertion of a
// value in the observable.
template<typename Duration, typename Observable>
Duration age(Observable const& obs)
{
  boost::chrono::system_clock::duration const d(
      boost::chrono::system_clock::now() - obs.last_insert_time());

  return boost::chrono::duration_cast<Duration>(d);
}

// This function determines if the last item inserted in the given
// observable has been put before a given duration from now.
template<typename Observable, typename Duration>
bool isOlderThan(Observable const& obs, Duration const& d)
{
  return boost::chrono::system_clock::now() - d > obs.last_insert_time();
}

// This function serialize the given observable according to the ROy
// format.
template<typename Observable>
std::string roySerialize(Observable const& obs)
{
  std::ostringstream buf;
  boost::property_tree::write_json(buf, obs.representation(), false);
  return buf.str();
}

// This function sends the observable to the specified destination
// through a UDP socket
template<typename Observable>
void sendToMonitoring(Destination const& destination,
                      Observable const& obs)
{
  boost::asio::ip::udp::socket sock(service);
  sock.open(boost::asio::ip::udp::udp::v4());
  sock.send_to(boost::asio::buffer(roySerialize(obs)), destination);
}

// This class handles the UDP socket so there is no need to recreate it
// every time. Can be used as an alternative to sendToMonitoring().
//
// Non-copyable
//
// Thread safety:
//   Distinct objects: safe
//   Shared objects: unsafe

class MonHandler : boost::noncopyable
{
  boost::asio::ip::udp::socket m_sock;

 public:

  MonHandler()
    :
    m_sock(service)
  {
    m_sock.open(boost::asio::ip::udp::udp::v4());
  }

  template<typename Observable>
  void send(Destination const& destination, Observable const& obs)
  {
    m_sock.send_to(boost::asio::buffer(roySerialize(obs)), destination);
  }
};

// This class perform a stream-like monitoring data sending.
//
// Non-copyable
//
// Thread safety:
//   Distinct objects: safe
//   Shared objects: unsafe

class MonStreamer : boost::noncopyable
{
  MonHandler m_handle;
  Destination m_destination;

 public:

  MonStreamer(Destination const& destination)
    :
    m_destination(destination)
  {}

  MonStreamer(std::string const& address, int port)
    :
    m_destination(make_destination(address, port))
  {}

  template<typename Observable>
  friend MonStreamer& operator <<(MonStreamer& stream, Observable const& obs)
  {
    stream.m_handle.send(stream.m_destination, obs);
    return stream;
  }
};

} // namespace monitoring

} // namespace tridas

#endif //TRIDAS_PACKAGES_MONITORING_MONITORING_HPP
