#ifndef PACKAGES_BUFFER_SIMPLEBUFFER_H
#define PACKAGES_BUFFER_SIMPLEBUFFER_H

#include <cassert>
#include <cstring>

#include <boost/noncopyable.hpp>

class SimpleBuffer: boost::noncopyable
{

private:
  char* const start_;
  char* current_;
  char* const end_;

public:
  typedef char* iterator;
  typedef char const* const_iterator;

  SimpleBuffer(size_t size);
  ~SimpleBuffer();
  bool CopyData(char const* input_buf, int buf_size);
  size_t DumpLoad(char* output_buf);
  bool BufferIsFull()
  {
    return CapacityLeft() == 0;
  }
  size_t GetSize() const
  {
    return end_ - start_;
  }
  size_t CapacityLeft() const
  {
    return end_ - current_;
  }
  size_t GetLoad() const
  {
    return current_ - start_;
  }
  const_iterator begin() const
  {
    return start_;
  }
  iterator begin()
  {
    return start_;
  }
  const_iterator end() const
  {
    return end_;
  }
  iterator end()
  {
    return end_;
  }
  void current(iterator p)
  {
    assert(start_ <= p);
    assert(p <= end_);
    current_ = p;
  }
  const_iterator current() const
  {
    return current_;
  }
  iterator current()
  {
    return current_;
  }
  void MemMove(const_iterator b, const_iterator e)
  {
    ssize_t const size = e - b;
    assert(size >= 0 && "Negative size.");
    std::memmove(start_, b, size);
    current_ = start_ + size;
  }
  void FreeBuf()
  {
    current_ = start_;
  }

  // Legacy methods

  size_t GetRemnant() const // legacy
  {
    return CapacityLeft();
  }
  void PutStartWrite(iterator p) // legacy
  {
    current(p);
  }
  iterator GetStartWrite() // legacy
  {
    return current();
  }
  const_iterator GetStartBuffer() const // legacy
  {
    return begin();
  }
  iterator GetStartBuffer() // legacy
  {
    return begin();
  }

};

#endif
