#include <iostream>

#include "SimpleBuffer.h"

using namespace std;

SimpleBuffer::SimpleBuffer(size_t size) :
    start_(new char[size]), current_(start_), end_(start_ + size)
{
}

SimpleBuffer::~SimpleBuffer()
{
    delete [] start_;
}

bool SimpleBuffer::CopyData(char const* input_buf, int buf_size)
{
  assert(buf_size > 0);
  assert(start_ <= current_);
  assert(current_ <= end_);

  if (current_ + buf_size <= end_) {
    memcpy(current_, input_buf, buf_size);
    current_ += buf_size;
    assert(start_ <= current_);
    assert(current_ <= end_);
    return true;
  }

  return false;
}

size_t SimpleBuffer::DumpLoad(char* output_buf)
{
  assert(start_ <= current_);
  memcpy(output_buf, start_, current_ - start_);
  return 0;
}

