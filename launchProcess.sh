#!/bin/bash
# Argument = -t type -d "datacard path" -i "id" -n "number of parallel process to spawn"


usage()
{
cat << EOF
usage: $0 options
example start: $0 -c START -t TCPU -d ./test.txt -i 0 -n 10 -v
example stop: $0 -c STOP
example status: $0 -c STATUS
This script launch a number of [HM|TCPU|EM|TSV] over a machine.

OPTIONS:
   -h      Show this message
   -c      Command Type [START|STOP|STATUS]
   -t      Datacard path [OPTIONAL default=$DATACARD]
   -t      Type of process to launch [HM|TCPU|EM|TSV]
   -i      initial id of the process from configuration of TSC
   -n      number of parallel process to spawn
   -v      Verbose
EOF
}

enableConsoleAndFileOutput() {
    #OUTPUT CONSOLE AND FILE
    exec > >(tee -a $LOGFILEPATH/launcher.log >&2)
}
enableOnlyFileOutput() {
    #OUTPUT ONLY FILE
    exec 2>&1 >>$LOGFILEPATH/launcher.log
}

log() {
    echo -e "$1"
}

function countProcess() {
    local count
    count=`ps h -C $1 | wc -l`
    log "count process -->$count"
    eval "$2='$count'"
}

function checkHM() {
    local ret
    countProcess "$RunHM" ret
    log "checkHM -$ret-"
    eval "$1='$ret'"
}
function checkTCPU() {
    local ret
    countProcess "$RunTCPU" ret
    log "checkTCPU -$ret-"
    eval "$1='$ret'"
}
function checkEM() {
    local ret
    countProcess "$RunEM" ret
    log "checkTCPU -$ret-"
    eval "$1='$ret'"
}
function checkTSV() {
    local ret
    countProcess "$RunTSV" ret
    log "checkTSV -$ret-"
    eval "$1='$ret'"
}

launchHM() {
    log "launchHM()->$INSTALL_PATH/$RunHM -d $DATACARD -i $1" 
     $INSTALL_PATH/$RunHM -d $DATACARD -i $1 >> $LOGFILEPATH/hm.$1.log  2>&1 & 
    echo $! >> $PIDFILE
}

launchTCPU() {
    log "launchTCPU()->$INSTALL_PATH/$RunTCPU -d $DATACARD -i $1" 
     $INSTALL_PATH/$RunTCPU -d $DATACARD -i $1 >> $LOGFILEPATH/tcpu.$1.log  2>&1 &
    echo $! >> $PIDFILE
}

launchEM() {
    log "launchEM()->$INSTALL_PATH/$RunEM -d $DATACARD -i $ID" 
     $INSTALL_PATH/$RunEM -d $DATACARD -i $1 >> $LOGFILEPATH/em.$1.log  2>&1 &
    echo $! >> $PIDFILE
}

launchTSV() {
    log "launchTSV()->$INSTALL_PATH/$RunTSV -d $DATACARD -i $1"
    $INSTALL_PATH/$RunTSV -d $DATACARD -i $1 >> $LOGFILEPATH/tsv.$1.log  2>&1 & 
    echo $! >> $PIDFILE
}

stopProcess() {
    log "start halting process"
    numline=`cat "$PIDFILE" | wc -l`
    
    if [ $numline -ne 0 ]
    then
        while read process
        do 
            echo "process to kill: $process"
            try=0
            log "ps h -p $process | wc -l"
            processExist=`ps h -p $process | wc -l`
            while [ $processExist -eq 1 ] 
            do
                if [ $try -lt 10 ]
                then
                    log "killing process"
                    kill $process
                else
                    log "killing 9 process"
                    kill -s 9 $process
                    sleep 2
                fi
                let try=$try+1
                
                processExist=`ps h -p $process | wc -l`
            done
            echo "$process killed";
        done < "$PIDFILE"
    else
        killall -q $RunTSV
        killall -q $RunEM
        killall -q $RunTCPU
        killall -q $RunHM
    fi
    
    > "$PIDFILE"
    # $PIDFILE.`date +"%Y-%m-%d_%H.%M"`
    echo "processes halted"
}

PIDFILE="/tmp/$0"
#clients
RunTSV="RunTSV"
RunEM="RunEM"
RunTCPU="RunTCPU"
RunHM="RunHM"

touch $PIDFILE

INSTALL_PATH="/yp/user/mfavaro/unix/tridas/build/DAQ/TSC"

TYPE=""
#TSC will move the datacard file into this directory!!!!!
#TO BE FIXED
DATACARD="/FIXED/PATH/filename-datacard"
ID=""
NUMBER=""
VERBOSE=0
COMMAND=""
#LOGFILEPATH="/var/log/tsc"
LOGFILEPATH="/tmp/tsc"

while getopts “vht:d:i:n:c:” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         t)
             TYPE=$OPTARG
             ;;
         d)
             DATACARD=$OPTARG
             ;;
         i)
             ID=$OPTARG
             ;;         
         n)
             NUMBER=$OPTARG
             ;;
         c)
            COMMAND=$OPTARG
            ;;
         v)
             VERBOSE=1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ ! -d "$LOGFILEPATH" ]
then
    echo "creating log dir $LOGFILEPATH"
    mkdir -p "$LOGFILEPATH"
fi

if [[ -z $NUMBER ]]
then
    NUMBER=1
fi

if [[ -z $COMMAND ]] || [ "$COMMAND" == "START" ]
then
    if  [[ -z $ID ]] || [[ -z $TYPE ]]
    then
        enableConsoleAndFileOutput
        usage
        exit 1
    fi
else if [ "$COMMAND" != "STOP" ] && [ "$COMMAND" != "STATUS" ]
    then
        enableConsoleAndFileOutput
        usage
        exit 1
    fi
fi

touch $LOGFILEPATH/launcher.log


# Redirect stdout ( > ) into a named pipe ( >() ) running "tee"
#exec > >(tee -a $LOGFILEPATH/launcher.log
if [ $VERBOSE -eq 1 ] 
    then
        enableConsoleAndFileOutput
    else
        enableOnlyFileOutput
fi
date
log "TYPE=$TYPE"
log "DATACARD=$DATACARD"
log "ID=$ID"
log "NUMBER=$NUMBER"
log "COMMAND=$COMMAND"
log "VERBOSE=$VERBOSE"

# Without this, only stdout would be captured - i.e. your
# log file would not contain any error messages.
# SEE answer by Adam Spiers, which keeps STDERR a seperate stream -
# I did not want to steal from him by simply adding his answer to mine.
let endID=$ID+$NUMBER-1

case $COMMAND in

    START)

        for i in $(seq $ID $endID)
        do
            log "launching $TYPE"
            case "$TYPE" in
                HM)
                    launchHM $i
                ;;
                TCPU)
                    launchTCPU $i
                    ;;
                EM)
                    launchEM $i
                    ;;
                TSV)
                    launchTSV $i
                    ;;
                *)
                    enableConsoleAndFileOutput
                    log "$TYPE is a INVALID PROGRAMM TYPE"
                    usage
                    exit 3
            esac
            #sleep 5
            log "end launching $i"
        done
        log "end start"
	enableConsoleAndFileOutput
	log "OK"
        exit 0
        ;;
    STOP)
        stopProcess
        exit 0
        ;;
    STATUS)
        returnVal=""
        prettyPrint=""
        case "$TYPE" in
            HM)
                checkHM HMCOUNT
                prettyPrint="there are $HMCOUNT HM processes"
                outputString="HM=$HMCOUNT"
                ;;
            TCPU)
                checkTCPU TCPUCOUNT
                prettyPrint="there are $TCPUCOUNT TCPU processes"
                outputString="TCPU=$TCPUCOUNT"
                ;;
            EM)
                checkEM EMCOUNT
                prettyPrint="there are $EMCOUNT EM processes"
                outputString="EM=$EMCOUNT"
                ;;
            TSV)
                checkTSV TSVCOUNT
                prettyPrint="there are $TSVCOUNT TSV processes"
                outputString="TSV=$TSVCOUNT"
                ;;
            *)
                checkHM HMCOUNT
                prettyPrint="there are $HMCOUNT HM processes"
                checkTCPU TCPUCOUNT
                prettyPrint=$prettyPrint"\nthere are $TCPUCOUNT TCPU processes"
                checkEM EMCOUNT
                prettyPrint=$prettyPrint"\nthere are $EMCOUNT EM processes"
                checkTSV TSVCOUNT
                prettyPrint=$prettyPrint"\nthere are $TSVCOUNT TSV processes\n\r"
                outputString="HM=$HMCOUNT,TCPU=$TCPUCOUNT,EM=$EMCOUNT,TSV=$TSVCOUNT"
        esac
        log "$prettyPrint"
        enableConsoleAndFileOutput
        log "$outputString"
        enableOnlyFileOutput
        log "STATUS end"
        exit 0
        ;;
    ?)
        usage
        exit 1
        ;;
esac

exit 2






