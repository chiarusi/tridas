#include "Spedizioniere.h"

#include <boost/chrono/duration.hpp>

#include "log.hpp"

namespace tridas {
namespace hm {


namespace {
long const tcpu_timeout_ms = 1;
long const tsv_timeout_ms = 1;
}

bool SendEmptySts(network::ProducerPtr socket, s_HeaderInfoSTS const& header) {
  size_t const sent = socket->timedSend(
    &header,
    sizeof(s_HeaderInfoSTS),
    tcpu_timeout_ms);
  if (sent != sizeof(s_HeaderInfoSTS)) {
    TRIDAS_LOG(WARNING)
      << "Spedizioniere::SendEmptySts - Sent "
      << sent
      << " instead of "
      << sizeof(s_HeaderInfoSTS)
      << " bytes for STS header";
    return false;
  }
  return true;
}

bool SendAllBuffers(network::ProducerPtr socket, SectorTimeSlice* sts) {
  s_HeaderInfoSTS const& header = sts->GetHeader();
  size_t const sts_size = sts->GetNPMTFull();
  size_t sent_buffers = 0;
  for (size_t i = 0; i < header.pmtin_; ++i) {
    if (header.pmtlist_[i]) {
      SimpleBuffer* db = sts->GetBuffer(i);
      size_t sent = 0;
      if (sent_buffers != sts_size - 1) {
        sent = socket->timedSendMore(
          db->GetStartBuffer(),
          header.BufSize_[i],
          tcpu_timeout_ms);
      } else {
        sent = socket->timedSend(
          db->GetStartBuffer(),
          header.BufSize_[i],
          tcpu_timeout_ms);
      }
      ++sent_buffers;
      if (sent != header.BufSize_[i]) {
        TRIDAS_LOG(WARNING)
          << "Spedizioniere::SendAllBuffers - Sent "
          << sent
          << " instead of "
          << header.BufSize_[i]
          << " bytes for pmt buffer "
          << i;
        return false;
      }
    }
  }
  return true;
}

bool SendToTCPU(network::ProducerPtr socket, SectorTimeSlice* sts) {
  //Start sending buffer info
  size_t const sent = socket->timedSendMore(
    &(sts->GetHeader()),
    sizeof(s_HeaderInfoSTS),
    tcpu_timeout_ms);
  if (sent != sizeof(s_HeaderInfoSTS)) {
    TRIDAS_LOG(WARNING)
      << "Spedizioniere::SendToTCPU - Sent "
      << sent
      << " instead of "
      << sizeof(s_HeaderInfoSTS)
      << " bytes for STS header";
    return false;
  }
  return SendAllBuffers(socket, sts);
}

Spedizioniere::Spedizioniere(
  SharedQueue<STSMessage>& MQ_STSReady,
  SharedQueue<SectorTimeSlice*>& MQ_STSFree,
  SharedQueue<SimpleBuffer*>& MQ_BufferFree,
  size_t sts_max_quantity,
  size_t sector_id)
  :
  MQ_STSReady_(MQ_STSReady),
  MQ_STSFree_(MQ_STSFree),
  MQ_BufferFree_(MQ_BufferFree),
  sts_max_quantity_(sts_max_quantity),
  sector_id_(sector_id) {
}

void Spedizioniere::updateSts(
  SectorTimeSlice* old_sts,
  SectorTimeSlice* new_sts) {
  s_HeaderInfoSTS const& old_header = old_sts->GetHeader();
  s_HeaderInfoSTS const& new_header = new_sts->GetHeader();
  assert(old_header.pmtin_ == new_header.pmtin_ && "Check sector size");
  assert(old_sts->GetTSID() == new_sts->GetTSID() && "Check TS id");
  for (size_t i = 0; i < new_header.pmtin_; ++i) {
    if (new_header.pmtlist_[i]) {
      bool const pmt_added = old_sts->AddPMTBuf(i, new_sts->GetBuffer(i));
      if (pmt_added) {
        old_sts->setHitInfo(i, new_header.nhit_[i], new_header.nframe_[i]);
      } else {
        TRIDAS_LOG(ERROR)
          << "Trying to update STS with TS "
          << new_sts->GetTSID()
          << " readding the existing PMT "
          << i;
        // recycle the new one
        new_sts->GetBuffer(i)->FreeBuf();
        MQ_BufferFree_.put(new_sts->GetBuffer(i));
      }
    }
  }
  new_sts->Reset();
  MQ_STSFree_.put(new_sts);
}

void Spedizioniere::addSts(STSMessage const& sts_msg) {
  if (sts_map_.find(sts_msg.timeslice_id) != sts_map_.end()) {
    // The sts already exists
    TRIDAS_LOG(WARNING)
      << "Updating existing STS with TS "
      << sts_msg.timeslice_id
      << " in STS buffer";
    updateSts(sts_map_[sts_msg.timeslice_id], sts_msg.pointer);
  } else if (sts_map_.size() >= sts_max_quantity_
    && sts_msg.timeslice_id < sts_map_.begin()->first) {
    // The sts is older than the oldest sts of the buffer, skip it
    TRIDAS_LOG(WARNING)
      << "Skipping too old STS with TS "
      << sts_msg.timeslice_id;
    s_HeaderInfoSTS const& header = sts_msg.pointer->GetHeader();
    std::vector<SimpleBuffer*> const& db = sts_msg.pointer->GetBuffers();
    for (size_t i = 0; i < header.pmtin_; ++i) {
      if (header.pmtlist_[i]) {
        db[i]->FreeBuf();
        MQ_BufferFree_.put(db[i]);
      }
    }
    sts_msg.pointer->Reset();
    MQ_STSFree_.put(sts_msg.pointer);
  } else {
    // Insert the new sts
    TRIDAS_LOG(DEBUG)
      << "Inserting new STS with TS "
      << sts_msg.timeslice_id
      << " to STS buffer";
    sts_map_[sts_msg.timeslice_id] = sts_msg.pointer;
  }

  // if there are too many sts then free oldest sts with related buffers
  if (sts_map_.size() > sts_max_quantity_) {
    std::map<TS_t, SectorTimeSlice*>::iterator it = sts_map_.begin();

    s_HeaderInfoSTS const& header = it->second->GetHeader();
    std::vector<SimpleBuffer*> const& db = it->second->GetBuffers();
    for (size_t i = 0; i < header.pmtin_; ++i) {
      if (header.pmtlist_[i]) {
        db[i]->FreeBuf();
        MQ_BufferFree_.put(db[i]);
      }
    }
    it->second->Reset();
    MQ_STSFree_.put(it->second);

    sts_map_.erase(it->first);
    TRIDAS_LOG(DEBUG)
      << "Removing oldest STS with TS "
      << it->first
      << " from STS buffer";
  }
}

void Spedizioniere::operator()(
  std::vector <Endpoint> const& tcpu_endpoints,
  std::string const& tsv_port,
  network::Context& context) {

  std::vector <network::ProducerPtr> clients;
  for (size_t i = 0; i < tcpu_endpoints.size(); ++i) {
    network::ProducerPtr p(new network::Producer(context));
    TRIDAS_LOG(DEBUG)
      << "Connecting to TCPU "
      << tcpu_endpoints[i].hostname
      << " on port "
      << tcpu_endpoints[i].port;
    p->tcpConnect(tcpu_endpoints[i].hostname, tcpu_endpoints[i].port);
    clients.push_back(p);
  }

  network::Consumer tsv_socket(context);
  TRIDAS_LOG(DEBUG)
    << "Opening socket on port "
    << tsv_port
    << " for TSV communications";
  tsv_socket.tcpBind(tsv_port);

  boost::chrono::system_clock::time_point start_time =
    boost::chrono::system_clock::now();

  TS_t last_ts_sent = 0;

  while (true) {

    // if there is an sts ready, fill the map with that sts
    STSMessage sts_msg;
    if (MQ_STSReady_.get_no_wait(sts_msg)) {
      addSts(sts_msg);
    }

    // if there is a new ts from the supervisor, get it
    TSMessage ts_msg;
    if (tsv_socket.timedRecv(&ts_msg, sizeof(ts_msg), tsv_timeout_ms)) {
      TRIDAS_LOG(DEBUG)
        << "Requested STS with TS "
        << ts_msg.timeslice_id
        << " for TCPU "
        << ts_msg.tcpu_id;
      ts_map_[ts_msg.timeslice_id] = ts_msg.tcpu_id;
    }

    // if there are both sts and ts ready
    if (sts_map_.size() && ts_map_.size()) {
      std::map<TS_t, TcpuId>::iterator it = ts_map_.begin();
      while (it != ts_map_.end()) {
        std::map<TS_t, SectorTimeSlice*>::iterator sts_it = sts_map_.find(
          it->first);
        // If there is the needed sts, send it
        if (sts_it != sts_map_.end()) {
          TRIDAS_LOG(DEBUG)
            << "Sending STS with TS "
            << it->first
            << " to TCPU "
            << it->second;
          bool const response = SendToTCPU(clients[it->second], sts_it->second);
          last_ts_sent = it->first;
          if (!response) {
            // In case of error there isn't an other resend (to discuss)
            TRIDAS_LOG(WARNING)
              << "Failed sending STS with TS "
              << it->first
              << " to TCPU "
              << it->second;
          }
          ts_map_.erase(it++);
        }
        // else if timeslice is older than the oldest sts, send empty sts
        else if (it->first < sts_map_.begin()->first) {
          TRIDAS_LOG(WARNING)
            << "Requested STS with TS "
            << it->first
            << " is too old, sending empty STS";
          s_HeaderInfoSTS sts_header;
          sts_header.id_ = it->first;
          sts_header.pmtin_ = 0;
          sts_header.sector_ = sector_id_;
          if (!SendEmptySts(clients[it->second], sts_header)) {
            TRIDAS_LOG(WARNING)
              << "Failed sending STS with TS "
              << it->first
              << " to TCPU "
              << it->second;
          }
          ts_map_.erase(it++);
        }
        // else if timeslice is younger than the youngest sts, stop the cycle
        else if (it->first > sts_map_.rbegin()->first) {
          it = ts_map_.end();
        } else {
          ++it;
        }
      }
    }

    if (sts_map_.size() && last_ts_sent) {
      boost::chrono::duration<double> duration_time =
        boost::chrono::system_clock::now() - start_time;

      if (duration_time.count() >= 1) {
        int const pending_sts = sts_map_.rbegin()->first - last_ts_sent;
        if (pending_sts > 0) {
          TRIDAS_LOG(WARNING)
            << "STSs waiting for a request: "
            << pending_sts;
        }
        start_time = boost::chrono::system_clock::now();
      }
    }

  }
}

}
}
