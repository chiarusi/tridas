#include <Smistatore.h>
#include <boost/detail/lightweight_test.hpp>
#include <boost/thread.hpp>
#include <vector>
#include "log.hpp"


using namespace tridas;
using namespace tridas::hm;

namespace tridas { namespace phase3 {

bool operator ==(
    tridas::phase3::DataFrameHeader const& first
  , tridas::phase3::DataFrameHeader const& second
)
{
  return
     first.PMTID        == second.PMTID
  && first.EFCMID       == second.EFCMID
  && first.TowerID      == second.TowerID
  && first.SyncBytes    == second.SyncBytes
  && first.T5ns         == second.T5ns
  && first.FrameCounter == second.FrameCounter
  && first.Seconds      == second.Seconds
  && first.Days         == second.Days
  && first.Years        == second.Years
  && first.NDataSamples == second.NDataSamples
  && first.unused       == second.unused
  && first.FifoFull     == second.FifoFull
  && first.Charge       == second.Charge
  && first.FragFlag     == second.FragFlag
  && first.ZipFlag      == second.ZipFlag;
}
}}

void write_frame(
    DataFrameHeader const& dfh
  , char const* const payload
  , std::size_t payload_size
  , boost::asio::ip::tcp::socket& client
)
{
  boost::system::error_code ec;
  {
    //write dfh
    size_t const write_ret_value = boost::asio::write(
        client,
        boost::asio::buffer(
            static_cast<char const*>(
                static_cast<void const*>(
                    &dfh
                )
            )
          , sizeof(dfh))
        , ec
    );

    BOOST_TEST_EQ(write_ret_value, sizeof(dfh));
    BOOST_TEST_EQ(ec, boost::system::error_code());
  }
  {
    //write payload
    size_t const write_ret_value = boost::asio::write(
        client,
        boost::asio::buffer(payload, payload_size),
        ec
    );

    BOOST_TEST_EQ(write_ret_value, payload_size);
    BOOST_TEST_EQ(ec, boost::system::error_code());
  }
}


int main()
{
  //PREPARING PHASE
  Log::init("t_smistatore", Log::FromString("DEBUG"));

  SharedQueue<SimpleBuffer*> MQ_BufferFree;
  SharedQueue<BufferMessage> MQ_BufferFull;

  std::vector<Endpoint> const fcm_endpoints(
      1
    , Endpoint("localhost", "8000")
  );

  size_t const sector_id = 0;
  size_t const floors_per_sector = 1;
  size_t const pmts_per_floor = 6;
  size_t const floors_per_tower = 1;
  boost::chrono::milliseconds const delta_timeslice(200);
  size_t const pmt_buffer_size = 1024 * 1024;

  char payload[18];
  std::memset(payload, 0xFF, sizeof(payload));

  MonitorParameters const monitor_parameters = {
      "localhost",
      9999,
      boost::chrono::seconds(1)
  };
  DataFrameHeader dfh, dfh_kick;

  setDFHFullTime(boost::chrono::seconds(1), dfh);
  dfh.SyncBytes = 21930;
  dfh.NDataSamples = sizeof(payload) / 2;
  dfh.EFCMID = 1;

  setDFHFullTime(boost::chrono::seconds(2), dfh_kick);
  dfh_kick.SyncBytes = 21930;
  dfh_kick.NDataSamples = sizeof(payload) / 2;
  dfh_kick.EFCMID = 1;

  boost::asio::io_service service;

  boost::asio::ip::tcp::acceptor server(
      service,
      boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 8000));


  { //Test 1: right dfh
    Smistatore sm(
        MQ_BufferFree,
        MQ_BufferFull,
        fcm_endpoints,
        sector_id,
        floors_per_sector,
        pmts_per_floor,
        floors_per_tower,
        delta_timeslice,
        pmt_buffer_size,
        monitor_parameters);

    // RUN TEST
    boost::thread runner(boost::ref(sm));

    boost::asio::ip::tcp::socket client(service);

    server.accept(client);

    TRIDAS_LOG(INFO) << "Client " << client.remote_endpoint().address().to_string()
                     << " accepted";

    boost::system::error_code ec;

    write_frame(dfh, payload, sizeof(payload), client);
    write_frame(dfh, payload, sizeof(payload), client);

    // second dataframe only for pushing out the previous one
    write_frame(dfh_kick, payload, sizeof(payload), client);

    {
      BufferMessage message;
      bool const ret = MQ_BufferFull.timed_get(message, boost::chrono::seconds(5));

      BOOST_TEST_EQ(ret, true);

      if (ret) {
        char const* first_dfh = message.pointer->begin();
        char const* second_dfh =
            message.pointer->begin()
          + getDFHPayloadSize(*dataframeheader_cast(first_dfh))
          + sizeof(DataFrameHeader);

        BOOST_TEST_EQ(*dataframeheader_cast(first_dfh), dfh);
        BOOST_TEST_EQ(*dataframeheader_cast(second_dfh), dfh);
      }
    }

    sm.stop();
    runner.join();

  }
  boost::report_errors();
}
