#include <string>
#include <csignal>
#include <cstdlib>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/ref.hpp>

#include "SimpleBuffer.h"
#include "SharedQueue.h"
#include "log.hpp"
#include "Configuration.h"
#include "Configurator.h"
#include "Smistatore.h"
#include "Spedizioniere.h"
#include "Assemblatore.h"
#include "Geometry.h"
#include "HMParameters.h"
#include "StructParameters.h"
#include "MonitorParameters.h"

#include "inter_objects_communication/producer_consumer.hpp"

#include "version.hpp"

namespace po = boost::program_options;
namespace trm = tridas::monitoring;

using namespace std;
using namespace tridas;
using namespace tridas::hm;

int main(int argc, char* argv[]) {
  tridas::Log::init("RunHM");

  po::options_description desc("Options");

  size_t sector_id;
  boost::filesystem::path cardfile;

  desc.add_options()("help,h", "Print help messages.")(
    "version,v",
    "print TriDAS version")(
    "id,i",
    po::value<size_t>(&sector_id)->required(),
    "HitManager sector id.")(
    "datacard,d",
    po::value<boost::filesystem::path>(&cardfile)->required(),
    "Datacard file.");

  try {
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    if (vm.count("help")) {
      std::cout << desc << endl;
      return EXIT_SUCCESS;
    }
    if (vm.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }
    po::notify(vm);

    if (!boost::filesystem::exists(cardfile)) {
      std::cerr << cardfile.string() << " does not exist\n";
      return EXIT_FAILURE;
    } else if (!boost::filesystem::is_regular_file(cardfile)) {
      std::cerr << cardfile.string() << " is not a regular file\n";
      return EXIT_FAILURE;
    }

    Configuration const configuration = read_configuration(cardfile);
    Geometry const geometry = get_geometry_parameters(configuration);
    HMParameters const hm_parameters = get_hm_parameters(configuration);
    StructParameters const struct_parameters = get_struct_parameters(
        configuration);
    MonitorParameters const monitor_parameters = get_monitor_parameters(
        configuration);

    // Configure log
    tridas::Log::init(
        "RunHM",
        tridas::Log::FromString(configuration.get<std::string>("HM.LOG_LEVEL")));
    if (configuration.get<bool>("HM.LOG_TO_SYSLOG")) {
      tridas::Log::UseSyslog();
    }

    // Check if sector_id is in the correct range
    if (sector_id >= hm_parameters.count) {
      TRIDAS_LOG(ERROR)
        << "Sector ID out of range: "
        << sector_id
        << " [0, "
        << hm_parameters.count
        << ")";
      return EXIT_FAILURE;
    }

    size_t const n_total_floors = geometry.floors_per_tower * geometry.towers;
    size_t const floors_per_sector = n_total_floors / hm_parameters.count;
    size_t const pmts_per_sector = floors_per_sector * geometry.pmts_per_floor;

    // Check if number of sectors are correct
    if (n_total_floors % hm_parameters.count) {
      TRIDAS_LOG(ERROR)
        << "Wrong total number of floors: "
        << n_total_floors
        << " is not a multiple of "
        << hm_parameters.count;
      return EXIT_FAILURE;
    }

    std::vector<Endpoint> const tcpu_endpoints = get_tcpu_data_endpoints(
        configuration);
    std::vector<Endpoint> const fcm_endpoints = get_fcm_endpoints(configuration);

    // Check if there are enough fcm endpoints
    if (fcm_endpoints.size() < n_total_floors) {
      TRIDAS_LOG(ERROR)
        << "There are "
        << n_total_floors
        << " floors but only "
        << fcm_endpoints.size()
        << " FCM endpoints";
      return EXIT_FAILURE;
    }

    // Get endpoints for this specific sector
    size_t const first_endpoint = floors_per_sector * sector_id;
    size_t const last_endpoint = floors_per_sector * (sector_id + 1) - 1;
    std::vector<Endpoint> sector_endpoints;
    for (size_t i = first_endpoint; i <= last_endpoint; ++i) {
      sector_endpoints.push_back(fcm_endpoints[i]);
    }

    sigset_t set;
    sigfillset(&set);  // mask all signals
    pthread_sigmask(SIG_SETMASK, &set, NULL);  // set mask

    // Message Queues
    std::string const hm_tag("HM_" + boost::lexical_cast<std::string>(sector_id));
    SharedQueue<SimpleBuffer*> MQ_BufferFree;
    SharedQueue<BufferMessage> MQ_BufferFull;
    SharedQueue<STSMessage> MQ_STSReady;
    SharedQueue<SectorTimeSlice*> MQ_STSFree;

    //Start FCM_th
    TRIDAS_LOG(DEBUG) << "RunHM::main - Starting FCM_th thread";
    Smistatore Receiver(
        MQ_BufferFree
      , MQ_BufferFull
      , sector_endpoints
      , sector_id
      , floors_per_sector
      , geometry.pmts_per_floor
      , geometry.floors_per_tower
      , struct_parameters.delta_timeslice
      , struct_parameters.pmt_buffer_size
      , monitor_parameters
    );

    boost::thread FCM_th_id(boost::ref(Receiver));

  //Start STS_th: a thread to build STS objects
    TRIDAS_LOG(DEBUG) << "RunHM::main - Starting STS_th thread";
    Assemblatore Wallee(
      MQ_BufferFull,
      MQ_STSFree,
      MQ_STSReady,
      sector_id,
      pmts_per_sector,
      monitor_parameters,
      boost::chrono::seconds(struct_parameters.sts_ready_timeout));
    boost::thread STS_th_id(Wallee);

    //Start TCPU_th: a thread to send STS objects to TCPU
    TRIDAS_LOG(DEBUG) << "RunHM::main - Starting TCPU_th thread";
    Spedizioniere Postino(MQ_STSReady, MQ_STSFree, MQ_BufferFree,
                          struct_parameters.sts_in_memory, sector_id);
    // Get ctrl_port of this hm
    std::string const supervisor_port =
        get_hm_control_endpoints(configuration)[sector_id].port;
    network::Context context;
    boost::thread TCPU_th_id(Postino, tcpu_endpoints, supervisor_port,
                             boost::ref(context));

    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGTERM);
    //sigdelset(&set, SIGCHLD);
    int sig_caught;
    sigwait(&set, &sig_caught);

    TRIDAS_LOG(WARNING) << "RunHM: Received signal " << sig_caught;

    // Send interrupt to all threads...
    Receiver.stop();
    STS_th_id.interrupt();
    TCPU_th_id.interrupt();

    FCM_th_id.join();
    STS_th_id.detach();
    TCPU_th_id.detach();
  } catch (const po::error& e) {
    std::cerr << e.what() << '\n' << desc << std::endl;
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    TRIDAS_LOG(ERROR) << e.what();
    return EXIT_FAILURE;
  } catch (...) {
    TRIDAS_LOG(ERROR) << "Unknown exception. Quitting...";
    return EXIT_FAILURE;
  }
}
