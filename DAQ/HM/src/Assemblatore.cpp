#include "Assemblatore.h"

#include <boost/lexical_cast.hpp>
#include <boost/chrono.hpp>

#include "log.hpp"

using namespace std;

namespace trm = tridas::monitoring;

namespace {
boost::chrono::milliseconds const queue_timeout_ms(1);
unsigned int const running_avg_window = 50;
}


namespace tridas {
namespace hm {


Assemblatore::Assemblatore(
  SharedQueue<BufferMessage>& MQ_BufferFull,
  SharedQueue<SectorTimeSlice*>& MQ_STSFree,
  SharedQueue<STSMessage>& MQ_STSReady,
  size_t sector_id,
  size_t pmts_per_sector,
  MonitorParameters const& monitor_parameters,
  boost::chrono::seconds const& sts_ready_timeout)
    :
      MQ_BufferFull_(MQ_BufferFull),
      MQ_STSFree_(MQ_STSFree),
      MQ_STSReady_(MQ_STSReady),
      sector_id_(sector_id),
      hits_v_(pmts_per_sector, 0),
      monitor_parameters_(monitor_parameters),
      sts_ready_timeout_(sts_ready_timeout) {

}

void Assemblatore::operator()() {

  trm::MonStreamer mon_streamer(
    trm::make_destination(
      monitor_parameters_.hostname,
      monitor_parameters_.port));

  std::string const sect_id_str = boost::lexical_cast<std::string>(sector_id_);
  trm::SimpleObservable pending_sts_observable(
    "pending_sts_hm_" + sect_id_str,
    "integer",
    "Number of pending sts");

  while (true) {

    // Get a message containing a filled buffer
    BufferMessage msg_buf;
    if (MQ_BufferFull_.timed_get(msg_buf, queue_timeout_ms)) {

      // Update or insert the upcoming pmt buffer
      if (sts_map_.find(msg_buf.timeslice_id) == sts_map_.end()) {
        assert(msg_buf.timeslice_id != 0 && "TS id can not be zero!");
        bool const new_sts_is_empty = addSts(msg_buf.timeslice_id);
        assert(new_sts_is_empty && "New STS needs to be empty");
      }
      updateSts(msg_buf);

      // Check if the updated sts is completed
      SectorTimeSlice* const sts = sts_map_[msg_buf.timeslice_id];
      s_HeaderInfoSTS const& sts_header = sts->GetHeader();
      bool sts_completed = true;
      for (size_t i = 0; i < sts_header.pmtin_ && sts_completed; ++i) {
        sts_completed = (sts_header.pmtlist_[i] == 1);
      }

      // if it is completed, send it to Spedizioniere
      if (sts_completed) {
        STSMessage msg;
        msg.timeslice_id = msg_buf.timeslice_id;
        msg.pointer = sts;
        MQ_STSReady_.put(msg);
        for (std::map<boost::chrono::system_clock::time_point, TS_t>::iterator it =
            ts_map_.begin(); it != ts_map_.end(); ++it) {
          if (it->second == msg_buf.timeslice_id) {
            sts_map_.erase(msg_buf.timeslice_id);
            ts_map_.erase(it);
            break;
          }
        }
      }
    }

    // If there are uncompleted sts ready to send, send them to Spedizioniere
    boost::chrono::system_clock::time_point const ready_time(
        boost::chrono::system_clock::now()
            - boost::chrono::system_clock::duration(sts_ready_timeout_));
    std::map<boost::chrono::system_clock::time_point, TS_t>::iterator const it =
        ts_map_.lower_bound(ready_time);
    for (int i = 0, sts_to_remove = std::distance(ts_map_.begin(), it);
        i != sts_to_remove; ++i) {
      STSMessage msg;
      msg.timeslice_id = ts_map_.begin()->second;
      msg.pointer = sts_map_[ts_map_.begin()->second];
      TRIDAS_LOG(WARNING)
        << "Timedout uncompleted STS with TS "
        << msg.timeslice_id;
      MQ_STSReady_.put(msg);
      sts_map_.erase(ts_map_.begin()->second);
      ts_map_.erase(ts_map_.begin());
    }

    // Send informations to monitor
    boost::chrono::duration<double> const duration_time =
        boost::chrono::system_clock::now() - pending_sts_observable.last_insert_time();
    if (duration_time >= monitor_parameters_.time_interval) {
      mon_streamer << pending_sts_observable.put(sts_map_.size());

      std::string const sect_id_str = boost::lexical_cast<std::string>(
          sector_id_);

      for (std::vector<size_t>::iterator it = hits_v_.begin(), end =
          hits_v_.end(); it != end; ++it) {

        trm::AvgObservable obs(
            "hits_rate_" + sect_id_str + '.'
                + boost::lexical_cast<std::string>(
                    std::distance(hits_v_.begin(), it)),
            "Hz",
            "Hits rate");

        obs.put(static_cast<double>(*it) / duration_time.count());
        mon_streamer << obs;

        *it = 0;
      }
    }
  }

}

bool Assemblatore::addSts(TS_t timeslice_id) {

  ts_map_[boost::chrono::system_clock::now()] = timeslice_id;

  // Get a pointer to a free sts
  SectorTimeSlice* sts;

  if (!MQ_STSFree_.get_no_wait(sts)) {
    sts = new SectorTimeSlice(hits_v_.size());
  }

  sts_map_[timeslice_id] = sts;

  // fill sts with preliminary informations
  sts->SetTSID(timeslice_id);
  sts->SetSectorID(sector_id_);

  // be sure that sts is empty
  if (!sts->CheckEmptiness()) {
    TRIDAS_LOG(ERROR) << "Trying to use not empty STS";
    return false;
  }

  return true;
}

bool Assemblatore::updateSts(BufferMessage const& msg_buf) {
  // Only if the PMT is really filled
  if (msg_buf.pointer->GetLoad()) {
    SectorTimeSlice* const sts = sts_map_[msg_buf.timeslice_id];
    bool const pmt_added = sts->AddPMTBuf(msg_buf.pmt_id, msg_buf.pointer);
    if (pmt_added) {
      sts->setHitInfo(msg_buf.pmt_id, msg_buf.hit_count, msg_buf.frame_count);
      hits_v_[msg_buf.pmt_id] += msg_buf.hit_count;
      return true;
    } else {
      TRIDAS_LOG(ERROR)
        << "Trying to readd a buffer to PMT "
        << msg_buf.pmt_id
        << " of TS "
        << msg_buf.timeslice_id;
    }
  } else {
    TRIDAS_LOG(ERROR)
      << "Trying to add an empty buffer to PMT "
      << msg_buf.pmt_id
      << " of TS "
      << msg_buf.timeslice_id;
  }

  // Delete the buffer if it isn't inserted
  delete msg_buf.pointer;

  return false;
}

}
}
