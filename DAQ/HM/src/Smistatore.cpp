#include "Smistatore.h"

#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/make_shared.hpp>

#include "inter_objects_communication/stream_receiver.hpp" // for make_endpoint
#include <log.hpp>

namespace tridas {
namespace hm {

namespace trm = tridas::monitoring;

namespace {
size_t const floor_offset = 1;
size_t const recv_buffer_size = 128 * 1024;
}

static std::size_t data_frame_boundary(
    std::vector<unsigned char> const& buffer,
    std::size_t max_to_read) {

  assert(
      buffer.size() >= max_to_read
          && "asked to read more bytes than the buffer size");

  std::size_t read = 0;
  bool searching = true;

  while (searching) {
    if (read + sizeof(DataFrameHeader) < max_to_read) {
      DataFrameHeader const& dfh = *dataframeheader_cast(
          &buffer.front() + read);
      std::size_t const size_of_payload = getDFHPayloadSize(dfh);

      if (read + sizeof(DataFrameHeader) + size_of_payload < max_to_read) {
        read += sizeof(dfh) + size_of_payload;
      } else {
        searching = false;
      }
    } else {
      searching = false;
    }
  }

  return read;
}

bool checkPMTId(DataFrameHeader const& dfh, size_t pmts_per_floor) {
  if (dfh.PMTID >= pmts_per_floor) {
    TRIDAS_LOG(ERROR) << "Smistatore::checkPMTId() - Receiving (PTMID) "
                      << dfh.PMTID
                      << " >=  (NPMTFLOOR) "
                      << pmts_per_floor;
    return false;
  }
  return true;
}

bool checkFloorRange(
    DataFrameHeader const& dfh,
    size_t sector_id,
    size_t floors_per_sector,
    size_t floors_per_tower) {
  size_t const lower = sector_id * floors_per_sector + floor_offset;
  size_t const upper = lower + floors_per_sector - 1;
  size_t const abs_id = dfh.EFCMID + dfh.TowerID * floors_per_tower;

  if (abs_id < lower || abs_id > upper) {
    TRIDAS_LOG(ERROR) << "Smistatore::checkFloorRange() - Receiving Floor "
                      << abs_id
                      << " outside expected range ["
                      << lower
                      << ","
                      << upper
                      << "]";
    return false;
  }
  return true;
}

// check multiple frames
bool Smistatore::checkFrames(int idx, std::size_t max_to_read) {

  std::vector<unsigned char> const& buffer = recv_buffers_[idx];
  std::vector<DataFrameHeader const*>& prev_dfh = prev_dfhs_[idx];

  std::size_t read = 0;

  bool ok = true;

  while (read < max_to_read && ok) {
    DataFrameHeader const& dfh = *dataframeheader_cast(&buffer.front() + read);
    // Check the frame fields
    ok &= checkFrame(dfh);
    read += sizeof(dfh) + getDFHPayloadSize(dfh);
    // If the frame is OK and it isn't a subsequent hit
    if (ok && !subsequent(dfh)) {
      // Check if the hits are ordered
      if (prev_dfh[dfh.PMTID]
          && getDFHFullTime(dfh) < getDFHFullTime(*prev_dfh[dfh.PMTID])) {
        TRIDAS_LOG(ERROR) << "Smistatore::checkFrames() - found temporally unordered hits:\n"
                          << "Previous hit:\n"
                          << *prev_dfh[dfh.PMTID]
                          << "\nCurrent hit:\n"
                          << dfh;
        ok = false;
      }
      prev_dfh[dfh.PMTID] = &dfh;
    }
  }

  return ok;
}

// check a single frame
bool Smistatore::checkFrame(DataFrameHeader const& dfh) const {
  // other tests on the dfh may be implemented here
  bool const sync = testDFHSync(dfh);

  if (!sync) {
    TRIDAS_LOG(ERROR) << "Smistatore::checkFrame() - wrong SyncBytes: "
                      << dfh.SyncBytes;
  }

  bool const pmtid = checkPMTId(dfh, pmts_per_floor_);

  bool const floor_range = checkFloorRange(
      dfh,
      sector_id_,
      floors_per_sector_,
      floors_per_tower_);

  size_t const dfh_size = sizeof(dfh) + getDFHPayloadSize(dfh);
  bool const size = dfh_size <= maxFrameSize();

  if (!size) {
    TRIDAS_LOG(ERROR) << "Smistatore::checkFrame() - wrong dataframe size: "
                      << dfh_size;
  }

  return sync && pmtid && floor_range && size;
}

std::string formatTimeForFilename(const boost::posix_time::ptime& time) {
  // Don't be worried about the new operator. The allocated memory will
  // be freed in the loc destructor.
  static std::locale loc(
      std::cout.getloc(),
      new boost::posix_time::time_facet("%Y%m%d_%H%M%S"));

  std::stringstream ss;
  ss.imbue(loc);
  ss << time;
  return ss.str();
}

std::string get_dump_suffix(DataFrameHeader const& dfh) {
  std::string suffix;
  // Add tower ID
  suffix.append(boost::lexical_cast<std::string>(dfh.TowerID));
  suffix.append("_");
  // Add floor ID
  suffix.append(boost::lexical_cast<std::string>(dfh.EFCMID));
  suffix.append("_");
  // Add date and time
  boost::posix_time::ptime const now(
      boost::posix_time::second_clock::local_time());
  suffix.append(formatTimeForFilename(now));
  return suffix;
}

Smistatore::Smistatore(
    SharedQueue<SimpleBuffer*>& MQ_BufferFree,
    SharedQueue<BufferMessage>& MQ_BufferFull,
    std::vector<Endpoint> const& fcm_endpoints,
    size_t sector_id,
    size_t floors_per_sector,
    size_t pmts_per_floor,
    size_t floors_per_tower,
    boost::chrono::milliseconds delta_timeslice,
    size_t pmt_buffer_size,
    MonitorParameters const& monitor_parameters)
    : MQ_BufferFree_(MQ_BufferFree),
      MQ_BufferFull_(MQ_BufferFull),
      fcm_endpoints_(fcm_endpoints),
      sockets_(fcm_endpoints_.size()),
      connect_timers_(fcm_endpoints_.size()),
      recv_buffers_(
          fcm_endpoints_.size(),
          std::vector<unsigned char>(recv_buffer_size)),
      prev_dfhs_(fcm_endpoints_.size()),
      sector_id_(sector_id),
      floors_per_sector_(floors_per_sector),
      pmts_per_floor_(pmts_per_floor),
      floors_per_tower_(floors_per_tower),
      delta_timeslice_(delta_timeslice),
      pmt_buffer_size_(pmt_buffer_size),
      monitor_time_interval_(
          (monitor_parameters.time_interval)),
      monitoring_timer_(service_, monitor_time_interval_),
      monitoring_server_(
          trm::make_destination(
              monitor_parameters.hostname,
              monitor_parameters.port)),
      throughput_(0) {
  // Activate monitoring
  if (monitor_time_interval_
      != boost::asio::high_resolution_timer::duration::zero()) {
    monitoring_timer_.expires_from_now(monitor_time_interval_);
    monitoring_timer_.async_wait(
        boost::bind(
            &Smistatore::monitoring_handler
          , this
          , boost::asio::placeholders::error
        )
    );
  } else {
    TRIDAS_LOG(WARNING) << "Throughput monitoring disabled!";
  }

  // Activate connection handling
  for (unsigned i = 0; i < fcm_endpoints_.size(); ++i) {
    connect_timers_[i].reset(new boost::asio::deadline_timer(service_));
    sockets_[i].reset(new boost::asio::ip::tcp::socket(service_));

    sockets_[i]->async_connect(
        network::make_endpoint(
            service_,
            fcm_endpoints_[i].hostname,
            fcm_endpoints_[i].port),
        boost::bind(
            &Smistatore::connect_handler,
            this,
            boost::asio::placeholders::error,
            i));
  }
}

size_t Smistatore::AbsPmtId(DataFrameHeader const& dfh) const {
  return dfh.PMTID
      + ((dfh.EFCMID - floor_offset + dfh.TowerID * floors_per_tower_)
          % floors_per_sector_) * pmts_per_floor_;
}

// add multiple data frames
void Smistatore::add(int idx, std::size_t size) {
  std::size_t read = 0;
  unsigned char const* const begin = &recv_buffers_[idx].front();

  while (read < size) {
    DataFrameHeader const& dfh = *dataframeheader_cast(begin + read);
    std::size_t const size_of_frame = sizeof(dfh) + getDFHPayloadSize(dfh);

    add(idx, begin + read, size_of_frame);

    read += size_of_frame;
  }

  DataFrameHeader const* const nil = 0;
  std::fill(prev_dfhs_[idx].begin(), prev_dfhs_[idx].end(), nil);
}

// add a single data frame
void Smistatore::add(int idx, unsigned char const* frame, std::size_t size) {
  DataFrameHeader const& dfh = *dataframeheader_cast(frame);

  std::size_t const ipmt = AbsPmtId(dfh);

  std::map<size_t, PMTInfo>::iterator it = PMTbuf_.find(ipmt);

  if (it == PMTbuf_.end()) {
    it = PMTbuf_.insert(std::make_pair(ipmt, PMTInfo(idx))).first;

    if (!MQ_BufferFree_.get_no_wait(it->second.buffer)) {
      it->second.buffer = new SimpleBuffer(pmt_buffer_size_);
    }
  }

  ++it->second.frames_counter;

  TS_t ts_id = 0;

  if (!dfh.FragFlag) {
    ts_id = getTimesliceId(dfh, delta_timeslice_);
    ++it->second.hits_counter;  // for each new hit and only for it !!!
  } else {
    ts_id = it->second.current_timeslice;
  }

  if (ts_id == 0) {
    TRIDAS_LOG(WARNING) << "Skipping first dataframe due to fragmentation";
    return;
  }

  if (it->second.stream_idx != idx) {
    int const pidx = it->second.stream_idx;

    TRIDAS_LOG(ERROR) << "Interference between input streams: data of PMT ("
                      << dfh.TowerID
                      << ", "
                      << dfh.EFCMID
                      << ", "
                      << dfh.PMTID
                      << ") coming from both "
                      << fcm_endpoints_[idx].hostname
                      << ':'
                      << fcm_endpoints_[idx].port
                      << " and "
                      << fcm_endpoints_[pidx].hostname
                      << ':'
                      << fcm_endpoints_[pidx].port;
    return;
  }

  // Kick off the old PMT buffer and get a new one for the new TimeSlice
  if (it->second.current_timeslice && ts_id != it->second.current_timeslice) {
    BufferMessage msg;
    msg.timeslice_id = it->second.current_timeslice;
    msg.pointer = it->second.buffer;
    msg.pmt_id = ipmt;
    msg.hit_count = it->second.hits_counter;
    msg.frame_count = it->second.frames_counter;

    // Find PMTBuffer time duration
    DataFrameHeader const& first_dfh = *dataframeheader_cast(
        msg.pointer->begin());
    DataFrameHeader const& last_dfh = dfh;
    msg.time_duration = getDFHFullTime(last_dfh) - getDFHFullTime(first_dfh);

    MQ_BufferFull_.put(msg);

    if (!MQ_BufferFree_.get_no_wait(it->second.buffer)) {
      it->second.buffer = new SimpleBuffer(pmt_buffer_size_);
    }
    it->second.hits_counter = 0;
    it->second.frames_counter = 0;
  }

  it->second.current_timeslice = ts_id;

  bool const copied = it->second.buffer->CopyData(
      static_cast<char const*>(static_cast<void const*>(frame)),
      size);

  if (!copied) {
    TRIDAS_LOG(ERROR) << "Cannot copy the dataframe";
  }
}

void Smistatore::operator()() {
  assert(!service_.stopped());

  service_.run();
}

void Smistatore::connect_handler(boost::system::error_code const& ec, int idx) {
  if (!ec) {
    TRIDAS_LOG(INFO) << "Connection to "
                     << fcm_endpoints_[idx].hostname
                     << ':'
                     << fcm_endpoints_[idx].port
                     << " succeded";

    prev_dfhs_[idx] = std::vector<DataFrameHeader const*>(pmts_per_floor_, 0);

    boost::asio::async_read(
        *sockets_[idx],
        boost::asio::buffer(recv_buffers_[idx]),
        boost::asio::transfer_at_least(sizeof(DataFrameHeader)),
        boost::bind(
            &Smistatore::read_buffer_handler,
            this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred,
            idx,
            0));
  } else if (ec == boost::asio::error::operation_aborted) {
    return;
  } else {
    connect_timers_[idx]->expires_from_now(boost::posix_time::seconds(1));

    connect_timers_[idx]->async_wait(
        boost::bind(
            &Smistatore::wait_handler,
            this,
            boost::asio::placeholders::error,
            idx));
  }
}

void Smistatore::read_buffer_handler(
    boost::system::error_code const& ec,
    std::size_t byte_transferred,
    int idx,
    std::size_t recv_buffer_offset) {

  size_t const max_to_read = recv_buffer_offset + byte_transferred;
  std::size_t complete_frames_size = data_frame_boundary(
      recv_buffers_[idx],
      max_to_read);

  if (!ec && checkFrames(idx, complete_frames_size)) {

    throughput_.store(throughput_.load() + byte_transferred);

    size_t const uncomplete_frame_size = max_to_read - complete_frames_size;

    add(idx, complete_frames_size);

    // move the uncompleted part to the beginning of the buffer
    std::memmove(
        &recv_buffers_[idx].front(),
        &recv_buffers_[idx].front() + complete_frames_size,
        uncomplete_frame_size);

    std::size_t const recv_buffer_request = recv_buffers_[idx].size()
        - uncomplete_frame_size;

    boost::asio::async_read(
        *sockets_[idx],
        boost::asio::buffer(
            &recv_buffers_[idx].front() + uncomplete_frame_size,
            recv_buffer_request),
        boost::asio::transfer_at_least(sizeof(DataFrameHeader)),
        boost::bind(
            &Smistatore::read_buffer_handler,
            this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred,
            idx,
            uncomplete_frame_size));
  } else if (ec == boost::asio::error::operation_aborted) {
    return;
  } else {
    TRIDAS_LOG(ERROR) << "Connection to "
                      << fcm_endpoints_[idx].hostname
                      << ':'
                      << fcm_endpoints_[idx].port
                      << " broken: "
                      << ec;

    sockets_[idx]->close();

    sockets_[idx]->async_connect(
        network::make_endpoint(
            service_,
            fcm_endpoints_[idx].hostname,
            fcm_endpoints_[idx].port),
        boost::bind(
            &Smistatore::connect_handler,
            this,
            boost::asio::placeholders::error,
            idx));
  }
}

void Smistatore::wait_handler(boost::system::error_code const& ec, int idx) {
  if (ec != boost::asio::error::operation_aborted) {
    sockets_[idx]->async_connect(
        network::make_endpoint(
            service_,
            fcm_endpoints_[idx].hostname,
            fcm_endpoints_[idx].port),
        boost::bind(
            &Smistatore::connect_handler,
            this,
            boost::asio::placeholders::error,
            idx));
  }
}

void Smistatore::monitoring_handler(boost::system::error_code const& ec) {
  if (!ec) {
    trm::SimpleObservable observable(
        "rx_hm_" + boost::lexical_cast<std::string>(sector_id_),
        "Byte/s",
        "Received throughput of hm "
            + boost::lexical_cast<std::string>(sector_id_));

    double const throughput = (1.0 * throughput_.load())
        / monitor_time_interval_.count();
    // here the throughput_ variable can be set in another thread,
    // but I'm confident that, if this happens, the error is small (~O(df))

    throughput_.store(0);

    TRIDAS_LOG(DEBUG) << "Incoming throughput from sector"
                      << sector_id_
                      << ": "
                      << throughput
                      << " B/s";

    observable.put(throughput);
    trm::sendToMonitoring(monitoring_server_, observable);

    monitoring_timer_.expires_from_now(monitor_time_interval_);
    monitoring_timer_.async_wait(
        boost::bind(
            &Smistatore::monitoring_handler,
            this,
            boost::asio::placeholders::error));
  }
}

void Smistatore::flush() {
  for (std::map<size_t, PMTInfo>::iterator it = PMTbuf_.begin(), et = PMTbuf_
      .end(); it != et; ++it) {
    if (it->second.current_timeslice) {
      DataFrameHeader const& dfh = *dataframeheader_cast(
          it->second.buffer->begin());

      BufferMessage msg;
      msg.timeslice_id = it->second.current_timeslice;
      msg.pointer = it->second.buffer;
      msg.pmt_id = AbsPmtId(dfh);
      msg.hit_count = it->second.hits_counter;
      msg.frame_count = it->second.frames_counter;

      MQ_BufferFull_.put(msg);

      it->second.current_timeslice = 0;
      it->second.buffer = 0;
    }
  }
}

}  // ns hm
}  // ns tridas
