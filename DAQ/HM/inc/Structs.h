#ifndef DAQ_HM_INC_STRUCTS_H
#define DAQ_HM_INC_STRUCTS_H

#include "SimpleBuffer.h"
#include "SectorTimeSlice.h"

#include <boost/asio/high_resolution_timer.hpp>

namespace tridas {
namespace hm {

struct TSMessage
{
  TS_t timeslice_id;
  TcpuId tcpu_id;
};


struct STSMessage
{
  TS_t timeslice_id; // init id=0, then DAQ start from 1
  SectorTimeSlice* pointer;
};

struct BufferMessage
{
  int pmt_id; // absolute PMT number starting from 0
  TS_t timeslice_id; // init id=0, then DAQ start from 1
  unsigned int hit_count;
  unsigned int frame_count;
  SimpleBuffer* pointer;
  boost::asio::high_resolution_timer::duration time_duration;
};

} // hm
} // tridas
#endif
