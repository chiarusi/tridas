#ifndef DAQ_HM_INC_ASSEMBLATORE_H
#define DAQ_HM_INC_ASSEMBLATORE_H

#include <map>
#include <boost/chrono.hpp>

#include "SectorTimeSlice.h"
#include "SharedQueue.h"
#include "monitoring.hpp"
#include "Structs.h"
#include "MonitorParameters.h"


namespace tridas {
namespace hm {

class Assemblatore {

 private:

  SharedQueue<BufferMessage>& MQ_BufferFull_;
  SharedQueue<SectorTimeSlice*>& MQ_STSFree_;
  SharedQueue<STSMessage>& MQ_STSReady_;
  std::map<boost::chrono::system_clock::time_point, TS_t> ts_map_;
  std::map<TS_t, SectorTimeSlice*> sts_map_;
  size_t const sector_id_;
  std::vector<size_t> hits_v_;
  MonitorParameters monitor_parameters_;
  boost::chrono::seconds sts_ready_timeout_;

 private:
  bool addSts(TS_t timeslice_id);
  bool updateSts(BufferMessage const& msg_buf);

 public:

  Assemblatore(
    SharedQueue<BufferMessage>& MQ_BufferFull,
    SharedQueue<SectorTimeSlice*>& MQ_STSFree,
    SharedQueue<STSMessage>& MQ_STSReady,
    size_t sector_id,
    size_t pmts_per_sector,
    MonitorParameters const& monitor_parameters,
    boost::chrono::seconds const& sts_ready_timeout);

  void operator()();
};

} // hm
} // tridas

#endif
