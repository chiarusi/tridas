#ifndef DAQ_HM_INC_SMISTATORE_H
#define DAQ_HM_INC_SMISTATORE_H

#include <vector>
#include <map>

#include <boost/filesystem.hpp>
#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>

#include <boost/atomic.hpp>
#include <boost/chrono.hpp>

#include "Endpoint.h"
#include "tridas_dataformat.hpp"
#include "SharedQueue.h"
#include "SimpleBuffer.h"
#include "Structs.h"
#include "MonitorParameters.h"
#include "monitoring.hpp"


namespace tridas {
namespace hm {


struct PMTInfo {
  size_t current_timeslice;
  SimpleBuffer* buffer;
  unsigned int hits_counter;
  unsigned int frames_counter;
  int stream_idx;

  PMTInfo(int stream_index)
    :
    current_timeslice(0),
    buffer(NULL),
    hits_counter(0),
    frames_counter(0),
    stream_idx(stream_index)
  {}
};

class Smistatore
{
  SharedQueue<SimpleBuffer*>& MQ_BufferFree_;
  SharedQueue<BufferMessage>& MQ_BufferFull_;

  boost::asio::io_service service_;

  std::vector<Endpoint> const fcm_endpoints_;
  std::vector< boost::shared_ptr<boost::asio::ip::tcp::socket> > sockets_;
  std::vector< boost::shared_ptr<boost::asio::deadline_timer> > connect_timers_;
  std::vector< std::vector<unsigned char> > recv_buffers_;
  std::vector< std::vector<DataFrameHeader const*> > prev_dfhs_;

  size_t const sector_id_;
  size_t const floors_per_sector_;
  size_t const pmts_per_floor_;
  size_t const floors_per_tower_;
  boost::chrono::milliseconds const delta_timeslice_;
  size_t const pmt_buffer_size_;

  std::map<size_t, PMTInfo> PMTbuf_;

  boost::asio::high_resolution_timer::duration monitor_time_interval_;
  boost::asio::high_resolution_timer monitoring_timer_;
  tridas::monitoring::Destination monitoring_server_;
  boost::atomic<int> throughput_;

 private:

  size_t AbsPmtId(DataFrameHeader const& dfh) const;
  bool checkFrame(DataFrameHeader const& dfh) const;
  bool checkFrames(int idx, std::size_t max_to_read);

  void connect_handler(boost::system::error_code const& ec, int idx);

  void read_buffer_handler(
      boost::system::error_code const& ec
    , std::size_t byte_transferred
    , int idx
    , std::size_t recv_buffer_offset
  );

  void wait_handler(boost::system::error_code const& ec, int idx);

  void monitoring_handler(boost::system::error_code const& ec);

  void add(int idx, unsigned char const* frame, std::size_t size);
  void add(int idx, std::size_t size);

  void flush();

 public:
  Smistatore(
    SharedQueue<SimpleBuffer*>& MQ_BufferFree,
    SharedQueue<BufferMessage>& MQ_BufferFull,
    std::vector<Endpoint> const& fcm_endpoints,
    size_t sector_id,
    size_t floors_per_sector,
    size_t pmts_per_floor,
    size_t floors_per_tower,
    boost::chrono::milliseconds delta_timeslice,
    size_t pmt_buffer_size,
    MonitorParameters const& monitor_parameters);

  void operator()();

  void stop()
  {
    service_.stop();
    flush();
  }

  ~Smistatore()
  {
    flush();
  }
};

} // hm
} // tridas
#endif
