#ifndef DAQ_HM_INC_SPEDIZIONIERE_H
#define DAQ_HM_INC_SPEDIZIONIERE_H

#include <map>

#include "Endpoint.h"
#include "SharedQueue.h"
#include "SectorTimeSlice.h"
#include "Structs.h"

#include "inter_objects_communication/producer_consumer.hpp"

namespace tridas {
namespace hm {

class Spedizioniere {

 private:
  SharedQueue<STSMessage>& MQ_STSReady_;
  SharedQueue<SectorTimeSlice*>& MQ_STSFree_;
  SharedQueue<SimpleBuffer*>& MQ_BufferFree_;

  std::map<TS_t, SectorTimeSlice*> sts_map_;
  std::map<TS_t, TcpuId> ts_map_;

  size_t const sts_max_quantity_;
  size_t const sector_id_;

 private:
  void updateSts(SectorTimeSlice* old_sts, SectorTimeSlice* new_sts);
  void addSts(STSMessage const& sts_msg);

 public:
  Spedizioniere(
    SharedQueue<STSMessage>& MQ_STSReady,
    SharedQueue<SectorTimeSlice*>& MQ_STSFree,
    SharedQueue<SimpleBuffer*>& MQ_BufferFree,
    size_t sts_max_quantity,
    size_t sector_id);
  void operator()(
    std::vector<Endpoint> const& tcpu_endpoints,
    std::string const& tsv_port,
    network::Context& context);
};

} // hm
} // tridas
#endif
