
if (DOCUMENTATION_ONLY)

add_subdirectory(TSC)

else (DOCUMENTATION_ONLY)

# add all the directories in DAQ

add_subdirectory(EM)
add_subdirectory(FCM)
add_subdirectory(HM)
add_subdirectory(TCPU)
add_subdirectory(TSV)
add_subdirectory(TSC)

endif (DOCUMENTATION_ONLY)
