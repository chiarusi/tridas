#include "FileWriter.h"
#include "log.hpp"

namespace tridas {
namespace em {

void FileWriter::operator()()
{
  running_status_ = true;
  tridas::monitoring::SimpleObservable ntrev(
    "ntrev",
    "#",
    "Number of events in the current PTFile");

  tridas::monitoring::SimpleObservable ptdump(
    "ptdump",
    "bytes",
    "Size of the current PTFile");

  tridas::monitoring::SimpleObservable fileno(
    "ntrev",
    "#",
    "Current PTFile file number");

  int count = 0;

  bool more = false;

  while (running_status_) {
    do {
      // '!more' signals whether the current message is a
      // starting-ts message or not. The test is based on the fact that
      // the "ZMQmore" flag of the previously received message is zero
      // (last part of the multi-part message) or one (there is more to receive
      // for the current triggered timeslice).

      zmq::message_t message;
      if (input_data_->timedRecv(message, deltaTS_)) {

        ptfile_.write(message.data(), message.size(), !more);

        if (!more) {
          if (count++ % 50 == 0) {
            TRIDAS_LOG(INFO) << "FileWriter - " << count <<
            " timeslices written to file.";
            monitor_ << ntrev.put(ptfile_.numberOfEvents())
                     << ptdump.put(ptfile_.size())
                     << fileno.put(ptfile_.fileNumber());
          }
        }

        more = input_data_->more();
      }
    } while (more);
  }
}

}
}