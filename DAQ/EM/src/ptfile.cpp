#include "ptfile.hpp"
#include "log.hpp"
#include <iomanip>
#include <sstream>
#include <cassert>
#include <limits>

namespace tridas {
namespace em {


std::string pad_number(unsigned int n, char pad, int npad) {
  std::ostringstream oss;
  oss << std::setfill(pad) << std::setw(npad) << n;
  return oss.str();
}

boost::filesystem::path ptFileRunningFileName(
  boost::filesystem::path const& templ,
  unsigned int run_number,
  unsigned int file_number) {
  std::string copy = templ.string();

  std::string::size_type const run_number_pos = copy.find("%r");

  if (run_number_pos != std::string::npos) {
    copy.replace(run_number_pos, 2, 'R' + pad_number(run_number, '0', 8));
  }

  std::string::size_type const file_number_pos = copy.find("%n");

  if (file_number_pos != std::string::npos) {
    copy.replace(file_number_pos, 2, 'F' + pad_number(file_number, '0', 8));
  }

  return boost::filesystem::path(copy);
}

std::size_t cat(std::ostream& os, std::istream& is) {
  std::size_t const buffer_size = 4096;
  char buffer[buffer_size];

  std::size_t transferred_bytes = 0;

  while (is) {
    is.read(buffer, buffer_size);
    std::size_t read_bytes = is.gcount();
    os.write(buffer, read_bytes);
    transferred_bytes += read_bytes;
  }

  return transferred_bytes;
}

PtFile::PtFile(
  unsigned int run_number,
  unsigned int run_start_time,
  std::size_t max_file_size,
  boost::filesystem::path const& name_template,
  boost::filesystem::path const& datacard_file_name)
  :
  m_max_file_size(max_file_size),
  m_template(name_template),
  m_dc_fname(datacard_file_name) {
  m_header.run_number = run_number;
  m_header.file_number = 0;
  m_header.run_start_time = run_start_time;
  m_header.datacard_size = 0;
  m_header.number_of_events = 0;
  m_header.number_of_timeslices = 0;
  m_header.effective_file_size = 0;

  std::string::size_type const file_number_pos = name_template.string().find(
    "%n");

  if (file_number_pos == std::string::npos) {
    TRIDAS_LOG(WARNING) << "Placeholder for file number is missing.\
A single PT file will be written.";
    m_max_file_size = std::numeric_limits<std::size_t>::max();
  }

  openNewFile();
}

void PtFile::openNewFile() {
  boost::filesystem::path const filename = ptFileRunningFileName(
    m_template,
    m_header.run_number,
    m_header.file_number
  );

  m_file.open(filename.c_str());

  assert(m_file && "Error opening a new PTFile.");

  m_file.write(
    static_cast<const char*>(
      static_cast<const void*>(
        &m_header
      )
    ),
    sizeof(m_header)
  );

  std::ifstream datacard(m_dc_fname.c_str());

  m_header.datacard_size = cat(m_file, datacard);
}

std::size_t PtFile::write(
  void const* const data,
  std::size_t size,
  bool has_header) {
  if (has_header && m_header.number_of_timeslices%50 == 0) {
    TRIDAS_LOG(DEBUG) << "PtFile::write - file_max_size_: "
                      << m_max_file_size
                      << " written_size_: "
                      << m_header.effective_file_size;
  }

  if (has_header && (size + m_header.effective_file_size) > m_max_file_size) {
    finalise();

    m_header.number_of_events = 0;
    m_header.number_of_timeslices = 0;
    m_header.effective_file_size = 0;
    ++m_header.file_number;

    openNewFile();
  }

  m_file.write(static_cast<char const* const>(data), size);

  if (has_header) {
    TimeSliceHeader const* const whi =
      static_cast<TimeSliceHeader const* const>(data);

    ++m_header.number_of_timeslices;
    m_header.number_of_events += whi->NEvents;
  }

  m_header.effective_file_size += size;
  return size;
}

void PtFile::finalise() {
  m_header.effective_file_size = m_file.tellp();

  m_file.seekp(0);
  m_file.write(
    static_cast<const char*>(
      static_cast<const void*>(
        &m_header
      )
    ),
    sizeof(m_header)
  );

  m_file.close();
}

}
}