#include <string>
#include <stdexcept>
#include <cstdlib>
#include <boost/thread.hpp>
#include <boost/ref.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

// Local EM Includes
#include "FileWriter.h"

#include "monitoring.hpp"
#include "Configuration.h"
#include "Configurator.h"
#include "MonitorParameters.h"
#include "log.hpp"

#include "version.hpp"

using namespace tridas::em;


class configuration_error : public std::runtime_error
{
 public:

  explicit
  configuration_error(std::string const& what)
    :
    std::runtime_error(what)
  {}
};

static
inline
Configuration ReadDataCard(boost::filesystem::path const& cardfile)
{
  // This function performs the validity check of the relevant elements
  // read from the datacard (ranges etc...).
  Configuration const configuration = read_configuration(cardfile);

  try {
    int const delta_ts = configuration.get<int>(
        "INTERNAL_SW_PARAMETERS.DELTA_TS"
    );
    if (delta_ts <= 0) {
      throw configuration_error(
          "delta ts must be positive, got "
          + boost::lexical_cast<std::string>(delta_ts)
          + '.'
      );
    }

    int64_t const file_max_size = configuration.get<int64_t>(
        "EM.FILE_MAX_SIZE"
    );
    if (file_max_size < 100 * 1024 * 1024) {
      throw configuration_error(
          "Max file size cannot be less than 100 MB, got "
          + boost::lexical_cast<std::string>(file_max_size)
          + '.'
      );
    }

    std::string const ptfile_name_template = configuration.get<std::string>(
        "EM.PT_NAME_TEMPLATE"
    );

    int const run_number = configuration.get<int>("RUN_NUMBER");
    if (run_number <= 0) {
      throw configuration_error(
          "Run number must be positive, got "
          + boost::lexical_cast<std::string>(run_number)
          + '.'
      );
    }

    int const network_threads = configuration.get<int>("EM.NETWORK_THREADS");
    if (network_threads <= 0) {
      throw configuration_error(
          "Number of network threads must be positive, got "
          + boost::lexical_cast<std::string>(network_threads)
          + '.'
      );
    }

    int const data_port = configuration.get<int>("EM.DATA_PORT");
    if (data_port <= 1024 || data_port >= 65536) {
      throw configuration_error(
          "TCP data port must be in the range ]1024, 65536[, got "
          + boost::lexical_cast<std::string>(data_port)
          + '.'
      );
    }

  } catch (boost::property_tree::ptree_bad_path const& e) {
    throw configuration_error(
        std::string("Missing configuration parameter: ")
        + e.what()
    );
  }

  return configuration;
}

int main(int argc, char* argv[])
{
  tridas::Log::init("RunEM");

  boost::filesystem::path cardfile;
  boost::program_options::options_description desc("Options");

  desc.add_options()("help,h", "Print help messages.")(
    "version,v",
    "print TriDAS version")(
    "datacard,d",
    boost::program_options::value<boost::filesystem::path>(&cardfile)->required(),
    "Datacard file.");

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv)
          .options(desc)
          .run(),
        vm
    );

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return EXIT_SUCCESS;
    }

    if (vm.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }

    boost::program_options::notify(vm);

    if (!boost::filesystem::exists(cardfile)) {
      std::cerr << cardfile.string() << " does not exist\n";
      return EXIT_FAILURE;
    } else if (!boost::filesystem::is_regular_file(cardfile)) {
      std::cerr << cardfile.string() << " is not a regular file\n";
      return EXIT_FAILURE;
    }

    Configuration const config = ReadDataCard(cardfile);

    tridas::Log::init(
        "RunEM",
        tridas::Log::FromString(config.get<std::string>("EM.LOG_LEVEL")));

    if (config.get<bool>("EM.LOG_TO_SYSLOG")) {
      tridas::Log::UseSyslog();
    }

    sigset_t set;
    sigfillset(&set);

    pthread_sigmask(SIG_SETMASK, &set, 0); // set mask before going in
                                           // multi-thread

    network::Context context(config.get<int>("EM.NETWORK_THREADS"));
    network::Consumer socket(context);
    socket.tcpBind(config.get<std::string>("EM.DATA_PORT"));

    MonitorParameters const monitor_parameters = get_monitor_parameters(config);

    FileWriter file_writer(
        socket,
        config.get<int>("INTERNAL_SW_PARAMETERS.DELTA_TS"),
        monitor_parameters.hostname,
        monitor_parameters.port,
        config.get<unsigned int>("RUN_NUMBER"),
        config.get<unsigned int>("START_TIME"),
        config.get<size_t>("EM.FILE_MAX_SIZE"),
        cardfile,
        config.get<std::string>("EM.PT_NAME_TEMPLATE"));

    boost::thread FILE_th_id(boost::ref(file_writer));

    TRIDAS_LOG(DEBUG) << "FILE_th created...";

    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGQUIT);
    sigaddset(&set, SIGTERM);
    sigaddset(&set, SIGHUP);
    pthread_sigmask(SIG_SETMASK, &set, 0);

    while (1) {
      timespec const timeout = {1, 0};

      int const wait_value = sigtimedwait(&set, 0, &timeout);

      if (wait_value > 0) {
        break;
      }
    }

    // Send interrupt to all threads...

    file_writer.stop();
    FILE_th_id.join();
  } catch (boost::program_options::error const& e) {
    std::cerr << e.what() << '\n' << desc << '\n';
    return EXIT_FAILURE;
  } catch (configuration_error const& e) {
    TRIDAS_LOG(ERROR) << "Configuration error: " << e.what();
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    TRIDAS_LOG(ERROR) << e.what();
    return EXIT_FAILURE;
  } catch (...) {
    TRIDAS_LOG(ERROR) << "Unknown exception. Quitting...";
    return EXIT_FAILURE;
  }
}
