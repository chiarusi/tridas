#ifndef TRIDAS_DAQ_EM_INC_PTFILE_HPP
#define TRIDAS_DAQ_EM_INC_PTFILE_HPP

#include <fstream>
#include <string>
#include <boost/filesystem.hpp>
#include "tridas_dataformat.hpp"


namespace tridas {
namespace em {

class PtFile
{
  std::ofstream m_file;
  std::size_t m_max_file_size;
  PTHeader m_header;
  boost::filesystem::path m_template;
  boost::filesystem::path m_dc_fname;

  void openNewFile();

  void finalise();

 public:

  PtFile(
      unsigned int run_number,
      unsigned int run_start_time,
      std::size_t max_file_size,
      boost::filesystem::path const& name_template,
      boost::filesystem::path const& datacard_file_name);

  ~PtFile()
  {
    finalise();
  }

  int fileNumber() const
  {
    return m_header.file_number;
  }

  int numberOfEvents() const
  {
    return m_header.number_of_events;
  }

  std::size_t size() const
  {
    return m_header.effective_file_size;
  }

  std::size_t write(void const*const data, std::size_t size, bool has_header);
};


} // ns em
} // ns tridas

#endif // TRIDAS_DAQ_EM_INC_PTFILE_HPP
