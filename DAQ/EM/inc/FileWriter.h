#ifndef DAQ_EM_INC_FILEWRITER_H
#define DAQ_EM_INC_FILEWRITER_H

#include <string>
#include <boost/atomic.hpp>
#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>
#include "monitoring.hpp"
#include "ptfile.hpp"
#include "inter_objects_communication/producer_consumer.hpp"

namespace tridas {
namespace em {

class FileWriter : boost::noncopyable
{
  network::Consumer* input_data_;
  int deltaTS_;
  PtFile ptfile_;
  tridas::monitoring::MonStreamer monitor_;
  boost::atomic<bool> running_status_;

 public:

  FileWriter(
      network::Consumer& sock,
      int deltaTS,
      std::string const& monitoring_host,
      unsigned int monitoring_port,
      unsigned int run_number,
      unsigned int start_run_time,
      size_t file_max_size,
      boost::filesystem::path const& datacard_file_name,
      boost::filesystem::path const& PTfile_template)
      : input_data_(&sock),
        deltaTS_(deltaTS),
        ptfile_(
            run_number,
            start_run_time,
            file_max_size,
            PTfile_template,
            datacard_file_name),
        monitor_(monitoring_host, monitoring_port) {
  }

  void operator ()();

  inline
  void stop() {
    running_status_ = false;
  }
};


}
}
#endif // DAQ_EM_INC_FILEWRITER_H
