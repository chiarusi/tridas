#include "command_generator.hpp"
#include "Log/log.hpp"

namespace fs = boost::filesystem;

namespace tridas {
namespace tsc {

namespace {

class StartCommandGenerator
{
  std::string launcher_;
  fs::path configuration_file_;
  std::string pre_start_command_;
 public:
  StartCommandGenerator(
      std::string const& launcher
    , fs::path const& configuration_file
    , std::string const& pre_start_command
  )
      : launcher_(launcher)
      , configuration_file_(configuration_file)
      , pre_start_command_(pre_start_command)
  {}
  Command operator()(Node const& node) const
  {
    std::ostringstream os;
    os << launcher_
       << " -c START"
       << " -r " << ToString(node.role())
       << " -d " << configuration_file_
       << " -s";
    if (node.role() == Node::TCPU || node.role() == Node::HM) {
      os << " -i " << node.starting_id()
         << " -n " << node.n_instances();
    }
    if (!pre_start_command_.empty()) {
      // the use of double-quotes allows the command to do shell substitution
      // is this the right thing to do?
      os << " -p \"" << pre_start_command_ << '"';
    }
    return Command(os.str());
  }
};

class StopCommandGenerator
{
  std::string launcher_;
 public:
  explicit StopCommandGenerator(std::string const& launcher)
      : launcher_(launcher) {}
  Command operator()(Node const& node) const
  {
    std::ostringstream os;
    os << launcher_
       << " -c STOP"
       << " -r " << ToString(node.role())
       << " -s";
    return Command(os.str());
  }
};

class QueryCommandGenerator
{
  std::string launcher_;
 public:
  explicit QueryCommandGenerator(std::string const& launcher)
      : launcher_(launcher) {}
  Command operator()(Node const& node) const
  {
    std::ostringstream os;
    os << launcher_
       << " -c STATUS"
       << " -r " << ToString(node.role())
       << " -s";
    return Command(os.str());
  }
};

}

CommandGenerator::CommandGenerator(std::string const& launcher)
    : launcher_(launcher)
{
}

Commands
CommandGenerator::prepareStartCommands(
    Nodes const& nodes
  , fs::path const& configuration_file
  , std::string const& pre_start_command
) const
{
  Commands commands;
  std::transform(
      nodes.begin()
    , nodes.end()
    , std::back_inserter(commands)
    , StartCommandGenerator(launcher_, configuration_file, pre_start_command)
  );
  return commands;
}

Commands
CommandGenerator::prepareStopCommands(Nodes const& nodes) const
{
  Commands commands;
  std::transform(
      nodes.begin()
      , nodes.end()
      , std::back_inserter(commands)
      , StopCommandGenerator(launcher_)
  );
  return commands;
}

Commands
CommandGenerator::prepareQueryCommands(Nodes const& nodes) const
{
  Commands commands;
  std::transform(
      nodes.begin()
      , nodes.end()
      , std::back_inserter(commands)
      , QueryCommandGenerator(launcher_)
  );
  return commands;
}

}}
