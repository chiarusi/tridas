#include "sshshell_command_executor.hpp"
#include <algorithm>
#include <iterator>
#include <cassert>
#include "ssh_shell.hpp"
#include "Log/log.hpp"

namespace {

boost::asio::deadline_timer::duration_type const connect_timeout =
    boost::posix_time::seconds(3);

boost::asio::deadline_timer::duration_type const exec_timeout =
    boost::posix_time::seconds(3);

}

namespace fs = boost::filesystem;

namespace tridas {
namespace tsc {

SshShellCommandExecutor::SshShellCommandExecutor(
    boost::asio::io_service& ios
  , std::string const& user
  , fs::path const& ssh_dir
  , bool dry_run
)
    : ios_(ios), user_(user), ssh_dir_(ssh_dir), dry_run_(dry_run)
{}

CommandResult
SshShellCommandExecutor::operator()(
    Command const& command
    , Node const& node
) const
{
  Commands commands;
  commands.push_back(command);
  Nodes nodes;
  nodes.push_back(node);
  return *(*this)(commands, nodes).begin();
}

CommandResults
SshShellCommandExecutor::operator()(
    Commands const& commands
    , Nodes const& nodes
) const
{
  assert(commands.size() == nodes.size());

  ssh::Shells shells;
  for (Nodes::const_iterator node_it = nodes.begin(), nodes_end = nodes.end();
       node_it != nodes_end; ++node_it) {
    shells.push_back(
        ssh::Shell::create(ios_, ssh_dir_, user_, node_it->hostname())
    );
  }
  if (!dry_run_) {
    ssh::connect(shells, connect_timeout);
  }

  assert(commands.size() == shells.size());

  Commands::const_iterator command_it = commands.begin();
  Commands::const_iterator const command_end = commands.end();
  ssh::Shells::iterator shell_it = shells.begin();
  ssh::Shells::iterator const shell_end = shells.end();
  for ( ; command_it != command_end; ++command_it, ++shell_it) {
    ssh::ShellPtr shell = *shell_it;
    TRIDAS_LOG(INFO) << (dry_run_ ? "would execute" : "executing")
                     << " on " << shell->host() << ": " << *command_it;
    if (!dry_run_) {
      shell->async_exec(
          ssh::Command(command_it->command_line())
          , exec_timeout
      );
    }
  }
  if (!dry_run_) {
    ios_.reset(); ios_.run();
  }

  CommandResults results;
  
  for (ssh::Shells::iterator shell_it = shells.begin() , shell_end = shells.end();
       shell_it != shell_end; ++shell_it) {
    if (dry_run_) {
      Nodes::const_iterator node_it = nodes.begin();
      std::advance(node_it, std::distance(shells.begin(), shell_it));
      Node const& node = *node_it;
      results.push_back(
          CommandResult::make_successful_result(
              0
              , std::string(
                  ToString(node.role()) + ':'
                // there is no info here on what was the issued command
                // assume a start command and accept that the info for a stop
                // command be wrong
                  + boost::lexical_cast<std::string>(node.n_instances())
              )
          )
      );
    } else {
      ssh::ShellPtr shell = *shell_it;
      int const exit_status = shell->is_connected() ? EXIT_SUCCESS : EXIT_FAILURE;
      std::string const reply = shell->is_connected() ? shell->release_reply() : std::string("error");
      results.push_back(CommandResult::make_successful_result(exit_status, reply));
    }
  }

  disconnect(shells);

  return results;
}

}}
