#ifndef DAQ_TSC_COMMANDGENERATOR_H
#define DAQ_TSC_COMMANDGENERATOR_H

#include <string>
#include <boost/filesystem.hpp>
#include "node.hpp"
#include "command.hpp"

namespace tridas {
namespace tsc {

/// @brief The mechanism used to generate commands to be executed on the various
/// nodes belonging to the TriDAS farm
class CommandGenerator
{
 public:
  explicit CommandGenerator(std::string const& launcher);
  Commands prepareStartCommands(
      Nodes const& nodes
    , boost::filesystem::path const& configuration_file
    , std::string const& pre_start_command
  ) const;
  Commands prepareStopCommands(Nodes const& nodes) const;
  Commands prepareQueryCommands(Nodes const& nodes) const;

 private:
  std::string launcher_;
};

}}

#endif
