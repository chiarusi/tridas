#include "state_machine_utils.hpp"
#include "update_node.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <cassert>
#include <boost/algorithm/string.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "Log/log.hpp"
#include "Configuration/Configuration.h"
#include "command.hpp"
#include "command_executor.hpp"
#include "events.hpp"
#include "context.hpp"
#include "exception.hpp"

namespace fs = boost::filesystem;

namespace tridas {
namespace tsc {

NodeMap readNodeMap(Configuration const& configuration) {

  NodeMap result;

  result[Node::TCPU] = read_tcpu_nodes(configuration);
  result[Node::TSV] = read_tsv_nodes(configuration);
  result[Node::HM] = read_hm_nodes(configuration);
  result[Node::EM] = read_em_nodes(configuration);

  return result;
}


bool node_map_makes_sense(NodeMap const& nm)
{
  return !nm[Node::TCPU].empty()
      && !nm[Node::HM].empty()
      && nm[Node::TSV].size() == 1
      && nm[Node::EM].size() == 1;
}

bool startProcesses(
    Nodes& nodes
  , CommandGenerator const& generator
  , CommandExecutor const& execute
  , fs::path const& configuration_file
  , std::string const& pre_start_command
)
{
  Commands const commands = generator.prepareStartCommands(
      nodes, configuration_file, pre_start_command
  );
  CommandResults const command_results = execute(commands, nodes);

  return update_nodes(nodes, command_results);
}

bool stopProcesses(
    Nodes& nodes
  , CommandGenerator const& generator
  , CommandExecutor const& execute
)
{
  Commands commands = generator.prepareStopCommands(nodes);
  CommandResults command_results = execute(commands, nodes);

  return update_nodes(nodes, command_results);
}

bool queryProcesses(
    Nodes& nodes
  , CommandGenerator const& generator
  , CommandExecutor const& execute
)
{
  Commands commands = generator.prepareQueryCommands(nodes);
  CommandResults command_results = execute(commands, nodes);

  return update_nodes(nodes, command_results);
}

Configuration retrieve_configuration(std::string const& run_setup)
{
  // consider the run setup as a file name for the moment
  fs::path p(run_setup);
  boost::system::error_code ec;
  if (!p.empty()
      && p.is_absolute()
      && fs::is_regular_file(p, ec)
      && p.string().find(' ') == std::string::npos) {
    return read_configuration(run_setup);
  } else {
    throw Exception("invalid run setup \"" + run_setup + '"');
  }
}

void check_rw_dir(fs::path const& d)
{
  if (!fs::is_directory(d)) {  // is not a directory
    throw Exception("path isn't a directory");
  }
  fs::path p = fs::unique_path();
  {
    fs::ofstream tmp(d/p);
    if (!tmp) {  // cannot write
      throw Exception("path isn't writable");
    }
  }
  {
    fs::ifstream tmp(d/p);
    if (!tmp) {  // cannot read
      throw Exception("path isn't readable");
    }
  }
  fs::remove(d/p);
}

void datacard_shareddir_test(fs::path const& dir)
{
  if (dir.empty()) {
    throw Exception("path string empty");
  }
  if (!dir.is_absolute()) {
    throw Exception("path isn't absolute");
  }
  check_rw_dir(dir);
}

int retrieve_run_number(Configuration const& datacard)
{
  int result = 1;

  try {
    fs::path const dir = datacard.get<std::string>("TSC.DATACARD_SHAREDDIR");
    try {
      datacard_shareddir_test(dir);
    } catch (Exception const& e) {
      throw Exception(
          "invalid TSC.DATACARD_SHAREDDIR: " + std::string(e.what()));
    }

    fs::path const latest = dir/"latest";

    if (fs::exists(latest)) {
      if (fs::is_symlink(latest)) {
        fs::path const run_number_file = fs::canonical(latest);
        try {
          int const prev_run_number
              = boost::lexical_cast<int>(run_number_file.filename().string());
          if (prev_run_number > 0) {
            result = prev_run_number + 1;
          } else {
            TRIDAS_LOG(WARNING) << "the previous run number " << prev_run_number
                                << " is not a valid run number";
          }
        } catch (boost::bad_lexical_cast const& e) {
          TRIDAS_LOG(WARNING) << latest << " links to " << run_number_file
                              << " and " << run_number_file.filename()
                              << " is not a number";
        }
      } else {
        TRIDAS_LOG(WARNING) << latest << " is not a symbolic link as it should be";
      }
    } else {
      TRIDAS_LOG(WARNING) << latest << " does not exist";
    }
  } catch (Configuration_error const& e) {
    TRIDAS_LOG(WARNING) << "error reading TSC.DATACARD_SHAREDDIR from the datacard";
  }

  if (result == 1) {
    TRIDAS_LOG(WARNING) << "restarting run number counting from 1";
  }

  return result;
}

std::string basename(std::string const& filename)
{
  std::string::size_type p = filename.find_last_of('/');
  assert(p != filename.size() - 1);
  return filename.substr(p == std::string::npos ? 0 : p + 1);
}

fs::path
make_configuration_filename(
    Configuration const& datacard,
    std::string const& run_setup,
    int run_number
)
{
  assert(!run_setup.empty());
  assert(run_number > 0);

  try {
    fs::path const dir = datacard.get<std::string>("TSC.DATACARD_SHAREDDIR");
    try {
      datacard_shareddir_test(dir);
    } catch (Exception const& e) {
      throw Exception(
          "invalid TSC.DATACARD_SHAREDDIR: " + std::string(e.what()));
    }

    std::ostringstream filename_os;
    filename_os << "tridas_" << basename(run_setup) << '_'
                << std::setw(8) << std::setfill('0') << run_number
                << ".json";

    return fs::path(dir)/filename_os.str();

  } catch (Configuration_error const& e) {
    throw Exception("error reading TSC.DATACARD_SHAREDDIR from the datacard");
  }
}

void
dump_on_file(
    Configuration const& configuration,
    fs::path const& configuration_file
)
{
  // first write on a temporary file
  fs::path tmp(configuration_file);
  tmp += '_';
  fs::ofstream o(tmp);
  using ::operator<<; // operator<< for Configuration is in the global namespace
  o << configuration;
  if (o) {
    // success; rename the temp file to the real destination
    fs::rename(tmp, configuration_file);
  } else {
    throw Exception("failed writing to " + tmp.string());
  }
}

fs::path
make_run_number_filename(
    Configuration const& datacard
  , int run_number
)
{
  assert(run_number > 0);

  try {
    fs::path const dir = datacard.get<std::string>("TSC.DATACARD_SHAREDDIR");
    try {
      datacard_shareddir_test(dir);
    } catch (Exception const& e) {
      throw Exception(
          "invalid TSC.DATACARD_SHAREDDIR: " + std::string(e.what()));
    }

    std::ostringstream filename_os;
    filename_os << std::setfill('0') << std::setw(8) << run_number;

    return fs::path(dir)/filename_os.str();
 
  } catch (Configuration_error const& e) {
    throw Exception("error reading TSC.DATACARD_SHAREDDIR from the datacard");
  }  
}

void create_run_number_file(fs::path const& p)
{
  assert(p.is_absolute());
  assert(!fs::is_directory(p));

  fs::path const latest = p.parent_path()/"latest";

  fs::ofstream o(p);
  if (!o) {
    throw Exception("failed creating " + p.string());
  }
  if (fs::exists(latest)) {
    if (fs::is_symlink(latest)) {
      fs::remove(fs::canonical(latest));
    }
    fs::remove(latest);
  }
  fs::create_symlink(p.filename(), latest);
}

}}
