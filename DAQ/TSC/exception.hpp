#ifndef DAQ_TSC_EXCEPTION_HPP
#define DAQ_TSC_EXCEPTION_HPP

#include <string>
#include <stdexcept>

namespace tridas {
namespace tsc {

class Exception : public std::runtime_error
{
 public:
  explicit Exception(std::string const& error) : std::runtime_error(error) {}
};

}}

#endif
