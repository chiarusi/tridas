#include "local_command_executor.hpp"
#include <algorithm>
#include <iterator>
#include <cassert>
#include "Log/log.hpp"
#include "popen_utils.hpp"

namespace tridas {
namespace tsc {

LocalCommandExecutor::LocalCommandExecutor(bool dry_run)
    : dry_run_(dry_run)
{}

CommandResult LocalCommandExecutor::operator()(Command const& command, Node const& node) const
{
  TRIDAS_LOG(INFO) << (dry_run_ ? "would execute: " : "executing: ") << command;

  return dry_run_ ? CommandResult::make_successful_result(0, std::string()) : popen_execute(command);
}

CommandResults LocalCommandExecutor::operator()(Commands const& commands, Nodes const& nodes) const
{
  assert(commands.size() == nodes.size());
  CommandResults results;
  std::transform(commands.begin(), commands.end(), nodes.begin(), std::back_inserter(results), *this);
  return results;
}

}}
