#include "ssh_command_executor.hpp"
#include <algorithm>
#include <iterator>
#include <cassert>
#include "Log/log.hpp"
#include "popen_utils.hpp"

namespace tridas {
namespace tsc {

SSHCommandExecutor::SSHCommandExecutor(bool dry_run)
    : dry_run_(dry_run)
{}

CommandResult SSHCommandExecutor::operator()(Command const& command, Node const& node) const
{
  Command const ssh_command(std::string("ssh ") + node.hostname() + ' ' + command.command_line());

  TRIDAS_LOG(INFO) << (dry_run_ ? "would execute: " : "executing: ") << ssh_command;

  return dry_run_
      ? CommandResult::make_successful_result(
          0
          , std::string(
              ToString(node.role()) + ':'
              + boost::lexical_cast<std::string>(node.n_instances())
          )
      )
      : popen_execute(ssh_command);
}

CommandResults SSHCommandExecutor::operator()(Commands const& commands, Nodes const& nodes) const
{
  assert(commands.size() == nodes.size());
  CommandResults results;
  std::transform(commands.begin(), commands.end(), nodes.begin(), std::back_inserter(results), *this);
  return results;
}

}}
