#include "popen_utils.hpp"
#include <cstdio>
#include <cerrno>
#include <cstdlib>
#include <utility>

#include "command.hpp"
#include "command_result.hpp"

namespace {

class PopenFailed: public std::runtime_error
{
  int e_; // errno
 public:
  explicit PopenFailed(int e) : std::runtime_error(strerror(e)), e_(e) {}
  int error() const { return e_; }
};

struct PcloseFailed: std::runtime_error
{
  int e_; // errno
 public:
  explicit PcloseFailed(int e) : std::runtime_error(strerror(e)), e_(e) {}
  int error() const { return e_; }
};

std::pair<int, std::string> popen_execute_impl(std::string const& command_line)
{
  // call fflush()?
  FILE* in = popen(command_line.c_str(), "r");
  if (!in) {
    throw PopenFailed(errno);
  }
  // call setvbuf() to make the output line-buffered?

  std::string command_output;
  char buffer[512];
  while (fgets(buffer, sizeof(buffer), in)) {
    command_output += buffer;
  }

  int const r = pclose(in);
  if (r == -1) {
    throw PcloseFailed(errno);
  }

  int const exit_status = WEXITSTATUS(r);

  // what if the output is multi-line? should we do the newline check after
  // every fgets above?
  if (command_output[command_output.size() - 1] == '\n') {
    command_output.resize(command_output.size() - 1);
  }

  return std::make_pair(exit_status, command_output);
}

}

namespace tridas {
namespace tsc {

CommandResult popen_execute(Command const& command)
{
  try {

    std::pair<int, std::string> r = popen_execute_impl(command.command_line());

    return CommandResult::make_successful_result(r.first, r.second);

  } catch (PopenFailed const& e) {

    return CommandResult::make_internal_error_result(
        e.error(),
        std::string("popen: ") + e.what()
    );

  } catch (PcloseFailed const& e) {

    return CommandResult::make_internal_error_result(
        e.error(),
        std::string("pclose: ") + e.what()
    );

  }
}

}}
