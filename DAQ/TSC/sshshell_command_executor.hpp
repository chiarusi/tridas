#ifndef DAQ_TSC_SSHSHELLCOMMANDEXECUTOR_HPP
#define DAQ_TSC_SSHSHELLCOMMANDEXECUTOR_HPP

#include "command_executor.hpp"
#include <string>
#include <boost/filesystem.hpp>
#include <boost/asio/io_service.hpp>

namespace tridas {
namespace tsc {

class SshShellCommandExecutor: public CommandExecutor
{
  boost::asio::io_service& ios_;
  std::string user_;
  boost::filesystem::path ssh_dir_;
  bool dry_run_;

 public:
  SshShellCommandExecutor(
      boost::asio::io_service& ios
    , std::string const& user
    , boost::filesystem::path const& ssh_dir
    , bool dry_run = false
  );

  CommandResult operator()(Command const& command, Node const& node) const;
  CommandResults operator()(Commands const& commands, Nodes const& nodes) const;

  // define the virtual destructor to silence -Wnon-virtual-dtor
  virtual ~SshShellCommandExecutor() {}

};

}}

#endif
