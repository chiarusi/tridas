#ifndef TRIDAS_DAQ_TSC_REQUEST_HPP
#define TRIDAS_DAQ_TSC_REQUEST_HPP

#include <unistd.h>
#include <string>
#include <stdexcept>
#include <sstream>
#include <boost/variant.hpp>
#include "state_machine.hpp"
#include "node.hpp"
#include "exception.hpp"

namespace tridas {
namespace tsc {

struct InitRequest
{
  std::string run_setup_;
  explicit InitRequest(std::string const& run_setup): run_setup_(run_setup) {}
  static std::string const string;
};

struct StartRequest
{
  static std::string const string;
};

struct ConfigureRequest
{
  int run_number_;
  explicit ConfigureRequest(int run_number): run_number_(run_number) {}
  static std::string const string;
};

struct StopRequest
{
  static std::string const string;
};

struct ResetRequest
{
  static std::string const string;
};

struct SleepRequest
{
  int seconds_;
  explicit SleepRequest(int seconds): seconds_(seconds) {}
  static std::string const string;
};

struct StateRequest
{
  static std::string const string;
};

struct QueryRequest
{
  static std::string const string;
};

struct QuitRequest
{
  static std::string const string;
};

typedef boost::variant<
  InitRequest
  , StartRequest
  , ConfigureRequest
  , StopRequest
  , ResetRequest
  , SleepRequest
  , StateRequest
  , QueryRequest
  , QuitRequest
> Request;

class GenericResponse
{
  std::string sm_state_;
 public:
  GenericResponse(std::string const& state)
      : sm_state_(state)
  {}
  std::string sm_state() const { return sm_state_; }
};

class InitResponse
{
  StateMachineInfo sm_info_;
 public:
  InitResponse(StateMachineInfo const& info)
      : sm_info_(info)
  {}
  std::string sm_state() const { return sm_info_.sm_state; }
  std::string run_setup() const { return sm_info_.run_setup; }
};

class ConfigureResponse
{
  StateMachineInfo sm_info_;
 public:
  ConfigureResponse(StateMachineInfo const& info)
      : sm_info_(info)
  {}
  std::string sm_state() const { return sm_info_.sm_state; }
  StateMachineInfo const& info() const { return sm_info_; }
};

class StartResponse
{
  StateMachineInfo sm_info_;
 public:
  StartResponse(StateMachineInfo const& info)
      : sm_info_(info)
  {}
  std::string sm_state() const { return sm_info_.sm_state; }
  StateMachineInfo const& info() const { return sm_info_; }
};

class StateResponse
{
  StateMachineInfo tridas_info_;
 public:
  StateResponse(StateMachineInfo const& tridas_info)
      : tridas_info_(tridas_info)
  {}
  std::string sm_state() const { return tridas_info_.sm_state; }
  StateMachineInfo const& info() const { return tridas_info_; }
};

typedef boost::variant<
    GenericResponse
  , InitResponse
  , ConfigureResponse
  , StartResponse
  , StateResponse
> Response;

Request parse_request(std::string const& cmd_line);

class RequestDispatcher: public boost::static_visitor<Response>
{
  StateMachine& sm_;

 public:
  explicit RequestDispatcher(StateMachine& sm): sm_(sm) {}
  Response operator()(InitRequest const& cmd) const
  {
    sm_.init(cmd.run_setup_);
    return StateResponse(sm_.info());
  }
  Response operator()(StartRequest) const
  {
    sm_.start();
    return StateResponse(sm_.info());
  }
  Response operator()(ConfigureRequest) const
  {
    sm_.configure();
    return StateResponse(sm_.info());
  }
  Response operator()(StopRequest) const
  {
    sm_.stop();
    return StateResponse(sm_.info());
  }
  Response operator()(ResetRequest) const
  {
    sm_.reset();
    return StateResponse(sm_.info());
  }
  Response operator()(SleepRequest const& cmd) const
  {
    ::sleep(cmd.seconds_);
    return StateResponse(sm_.info());
  }
  Response operator()(StateRequest const& cmd) const
  {
    return StateResponse(sm_.info());
  }
  Response operator()(QueryRequest const& cmd) const
  {
    sm_.query();
    return StateResponse(sm_.info());
  }
  Response operator()(QuitRequest r) const
  {
    throw r;
  }
};

class ResponseMaker: public boost::static_visitor<std::string>
{
 public:
  std::string operator()(GenericResponse const& response) const
  {
    std::ostringstream os;
    os << "O " << response.sm_state();
    return os.str();
  }
  std::string operator()(InitResponse const& response) const
  {
    std::ostringstream os;

    os << "O " << response.sm_state()
       << " RUNSETUP:" << response.run_setup();

    return os.str();
  }
  std::string operator()(ConfigureResponse const& response) const
  {
    std::ostringstream os;

    os << "O " << response.sm_state()
       << " RUNDATACARD:" << response.info().run_datacard_file
       << " RUNNUMBER:" << response.info().run_number
       << " NODEMAP:" << response.info().node_map.size();

    return os.str();
  }
  std::string operator()(StartResponse const& response) const
  {
    std::ostringstream os;

    boost::chrono::seconds::rep const run_start_time(
        boost::chrono::duration_cast<boost::chrono::seconds>(
            response.info().run_start_time.time_since_epoch()
        ).count()
    );
    os << "O " << response.sm_state()
       << " RUNDATACARD:" << response.info().run_datacard_file
       << " RUNNUMBER:" << response.info().run_number
       << " STARTTIME:" << run_start_time
       << " NODEMAP:" << response.info().node_map.size();

    return os.str();
  }
  std::string operator()(StateResponse const& response) const
  {
    std::ostringstream os;

    os << "O " << response.info();

    return os.str();
  }
};

inline Response dispatch_request(
    RequestDispatcher const& dispatcher
  , Request const& request
)
{
  return boost::apply_visitor(dispatcher, request);
}

inline std::string make_response(
    ResponseMaker const& response_maker
  , Response const& response
)
{
  return boost::apply_visitor(response_maker, response);
}

inline std::string make_response(Exception const& e, std::string const& state)
{
  std::ostringstream os;
  os << "X " << state << " reason: " << e.what();
  return os.str();
}

}}

#endif
