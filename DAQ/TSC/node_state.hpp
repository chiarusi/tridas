#ifndef DAQ_TSC_NODE_STATE_HPP
#define DAQ_TSC_NODE_STATE_HPP

/// @file

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include "node.hpp"

namespace tridas {
namespace tsc {

/// @class NodeState
/// @brief The result of parsing the output of a command executed on a node
///
/// The state includes two values: the role and the number of running instances
/// of the process assigned to that node. When actual values are not available,
/// invalid values are assigned to the members of the triplet: @c
/// Node::INVALID_ROLE and @c Node::INVALID_INSTANCES respectively.
/// @see Node
struct NodeState
{
  Node::Role role;
  int n_running;
  NodeState()
      : role(Node::INVALID_ROLE)
      , n_running(Node::INVALID_INSTANCES)
  {}
  NodeState(Node::Role r, int running)
      : role(r), n_running(running)
  {
  }

  /// @class ParseError
  /// @brief %Exception raised when parse() fails on the output of a command
  struct ParseError {};

  /// @brief Parse the output of a command executed on a node
  ///
  /// The node state can be of the form:
  /// - Role:N
  ///   N instances of a process of type Role are running on the node
  /// - Role:
  ///   the process is of type Role but there is no configuration nor runtime
  ///   information available
  /// .
  /// The information about the node state is typically obtained running a
  /// START, STOP or STATE command.
  /// @param input the output of the command
  /// @throw ParseError in case of parsing failure
  static NodeState parse(std::string const& input)
  {
    NodeState result;

    // match:
    //   bol, followed by
    //   zero or more spaces, followed by
    //   one of HM, TCPU, TSV, EM, followed by
    //   a ':' character, possibly followed by
    //   a number
    //   zero or more spaces, followed by
    //   eol

    static boost::regex const re(
        "^\\s*(HM|TCPU|TSV|EM):(-?\\d+)?\\s*$"
    );

    boost::smatch what;
    if (boost::regex_match(input, what, re)) {

      // the following assert has nothing to do with the input, but only with the
      // structure of the regex itself
      assert(
          what.size() == 3
        && "the size of the regex match for the command result should be 3"
      );

      assert(what[1].matched && "if the regex matches, what[1] must match as well");

      result.role = FromString(what[1]);

      if (what[2].matched) {
        result.n_running = boost::lexical_cast<int>(what[2]);
      }
    } else {
      throw ParseError();
    }

    return result;
  }
};

inline
bool operator==(NodeState const& lhs, NodeState const& rhs)
{
  return lhs.role == rhs.role && lhs.n_running == rhs.n_running;
}

inline
bool operator!=(NodeState const& lhs, NodeState const& rhs)
{
  return !(lhs == rhs);
}

inline
std::ostream& operator<<(std::ostream& os, NodeState const& v)
{
  return os << '(' << v.role << ',' << v.n_running << ')';
}

}}

#endif
