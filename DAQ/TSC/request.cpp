#include "request.hpp"
#include <cassert>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

namespace tridas {
namespace tsc {

std::string const InitRequest::string("init");
std::string const StartRequest::string("start");
std::string const ConfigureRequest::string("configure");
std::string const StopRequest::string("stop");
std::string const ResetRequest::string("reset");
std::string const SleepRequest::string("sleep");
std::string const StateRequest::string("state");
std::string const QueryRequest::string("query");
std::string const QuitRequest::string("quit");

Request parse_request(std::string const& cmd_line)
{
  try {
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(" \t");
    tokenizer tok(cmd_line, sep);
    tokenizer::const_iterator token_it = tok.begin();
    if (token_it != tok.end()) {
      std::string const cmd = *token_it++;
      if (cmd == InitRequest::string && token_it != tok.end()) {
        return InitRequest(*token_it);
      }
      if (cmd == StartRequest::string)     return StartRequest();
      if (cmd == ConfigureRequest::string) {
        int run_number = -1;

        if (token_it != tok.end()) {
          run_number = boost::lexical_cast<int>(*token_it++);
        }

        // gcc gives an apparently false warning here:
        // ‘result’ may be used uninitialized in this function [-Wmaybe-uninitialized]
        // however it seems confused by something inside lexical_cast
        return ConfigureRequest(run_number >= 0 ? run_number : -1);
      }
      if (cmd == StopRequest::string)      return StopRequest();
      if (cmd == ResetRequest::string)     return ResetRequest();
      if (cmd == SleepRequest::string && token_it != tok.end()) {
        int seconds = boost::lexical_cast<int>(*token_it);
        return SleepRequest(seconds);
      }
      if (cmd == StateRequest::string)     return StateRequest();
      if (cmd == QueryRequest::string)     return QueryRequest();
      if (cmd == QuitRequest::string)     return QuitRequest();
      // there are no checks for subsequent tokens, which, in principle, would
      // make the command invalid
    }
  } catch (std::exception const&) {
  }

  throw Exception("invalid request (" + cmd_line + ')');
}

}}
