#ifndef DAQ_TSC_COMMAND_HPP
#define DAQ_TSC_COMMAND_HPP

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "Log/log.hpp"

namespace tridas {
namespace tsc {

class Command
{
 public:
  explicit Command(std::string const& command_line)
      : command_line_(command_line)
  {}
  std::string command_line() const
  {
    return command_line_;
  }

 private:
  std::string command_line_;
};

inline std::ostream& operator<<(std::ostream& os, Command const& command)
{
  return os << command.command_line();
}

class Commands: private std::vector<Command>
{
  typedef std::vector<Command> V;
 public:
  using V::value_type;
  using V::iterator;
  using V::const_iterator;
  using V::reference;
  using V::const_reference;
  using V::begin;
  using V::end;
  using V::empty;
  using V::size;
  using V::push_back;
};

}}

#endif
