#include "state_machine.hpp"
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/back/tools.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/chrono/system_clocks.hpp>
#include <boost/chrono/floor.hpp>
#include <time.h>
#include <exception>
#include "Log/log.hpp"
#include "context.hpp"
#include "state_machine_utils.hpp"
#include "events.hpp"
#include "exception.hpp"

/// @file

namespace msm = boost::msm;
namespace mpl = boost::mpl;
namespace chrono = boost::chrono;
namespace fs = boost::filesystem;

namespace tridas {
namespace tsc {

struct init_transition
{
  template<class Fsm, class Evt, class Source, class Target>
  bool operator()(Evt const& ev, Fsm& fsm, Source& src, Target& target) const
  {
    std::string run_setup = ev.run_setup();

    Configuration template_datacard = retrieve_configuration(run_setup);

    if (template_datacard.empty()) {
      std::string const error_message
          = "Empty datacard for run setup \"" + run_setup + '"';
      throw Exception(error_message);
    }

    NodeMap node_map = readNodeMap(template_datacard);
    if (!node_map_makes_sense(node_map)) {
      std::string const error_message = "Invalid node map";
      throw Exception(error_message);
    }

    // save the state in the target
    using std::swap;
    swap(target.run_setup, run_setup);
    swap(target.template_datacard, template_datacard);
    swap(target.node_map, node_map);

    return true;
  }
};

struct query_transition
{
  template<class Fsm, class Evt, class Source, class Target>
  bool operator()(Evt const& ev, Fsm& fsm, Source& src, Target& target) const
  {
    NodeMap& node_map = *fsm.node_map_p;
    Context& c = ev.context();
    Nodes& tcpus = node_map[Node::TCPU];
    Nodes& hms = node_map[Node::HM];
    Nodes& em  = node_map[Node::EM];
    Nodes& tsv = node_map[Node::TSV];

    bool const ok =
        queryProcesses(tcpus, c.command_generator, c.command_executor)
        && queryProcesses(hms, c.command_generator, c.command_executor)
        && queryProcesses(em, c.command_generator, c.command_executor)
        && queryProcesses(tsv, c.command_generator, c.command_executor);

    return true;
  }
};

struct configure_transition
{
  template<class Fsm, class Evt, class Source, class Target>
  bool operator()(Evt const& ev, Fsm& fsm, Source& src, Target& target) const
  {
    Configuration run_datacard = fsm.template_datacard;

    int const run_number = retrieve_run_number(run_datacard);
    assert(run_number > 0);

    TRIDAS_LOG(INFO) << "using run number " << run_number;

    run_datacard.put("RUN_NUMBER", run_number);

    // make datacard persistent

    fs::path run_datacard_file
        = make_configuration_filename(run_datacard, fsm.run_setup, run_number);
    assert(!run_datacard_file.empty());
    assert(run_datacard_file.is_absolute());

    if (fs::exists(run_datacard_file)) {
      throw Exception(run_datacard_file.string() + " already exists");
    }

    dump_on_file(run_datacard, run_datacard_file);

    TRIDAS_LOG(INFO) << "run datacard saved in " << run_datacard_file;

    // make run number persistent

    fs::path const run_number_file
        = make_run_number_filename(run_datacard, run_number);
    assert(!run_number_file.empty());
    assert(run_number_file.is_absolute());

    if (fs::exists(run_number_file)) {
      throw Exception(run_number_file.string() + " already exists");
    }

    create_run_number_file(run_number_file);

    TRIDAS_LOG(INFO) << "run number file " << run_number_file << " created";

    // start ControlHost, TCPUs and HMs in this order

    NodeMap& node_map = fsm.node_map;

    Context& c = ev.context();
    Nodes& tcpus = node_map[Node::TCPU];
    Nodes& hms = node_map[Node::HM];

    boost::asio::io_service& ios = ev.context().ios;
    
    std::string const pre_start_command
        = run_datacard.get("TSC.PRE_START_COMMAND", std::string());

    bool ok = startProcesses(
        tcpus,
        c.command_generator,
        c.command_executor,
        run_datacard_file,
        pre_start_command);

    if (!ok) {
      throw Exception("cannot start all TCPU processes");
    };

    ok = startProcesses(
        hms,
        c.command_generator,
        c.command_executor,
        run_datacard_file,
        pre_start_command);

    if (!ok) {
      throw Exception("cannot start all HM processes");
    };

    // set the target state
    target.run_number = run_number;
    using std::swap;
    swap(target.run_datacard, run_datacard);
    swap(target.run_datacard_file, run_datacard_file);

    return true;
  }
};

chrono::seconds time_since_tridas_epoch()
{
  static chrono::seconds const tridas_epoch(946684800); // 2000-01-01 00:00:00 UTC
  chrono::seconds const time_since_clock_epoch(
      chrono::floor<chrono::seconds>(
          chrono::system_clock::now().time_since_epoch()
      )
  );
  return time_since_clock_epoch - tridas_epoch;
}

struct start_transition
{
  template<class Fsm, class Evt, class Source, class Target>
  bool operator()(Evt const& ev, Fsm& fsm, Source& src, Target& target) const
  {
    chrono::seconds const start_time = time_since_tridas_epoch();
    TRIDAS_LOG(INFO) << "run start time: " << start_time.count();
    fsm.run_datacard.put("START_TIME", start_time.count());
    dump_on_file(fsm.run_datacard, fsm.run_datacard_file);

    NodeMap& node_map = *fsm.node_map_p;

    Context& c = ev.context();
    Nodes& em  = node_map[Node::EM];
    Nodes& tsv = node_map[Node::TSV];

    std::string const pre_start_command
        = fsm.run_datacard.get("TSC.PRE_START_COMMAND", std::string());

    bool ok = startProcesses(
        em,
        c.command_generator,
        c.command_executor,
        fsm.run_datacard_file,
        pre_start_command);

    if (!ok) {
      throw Exception("cannot start EM process");
    }

    ok = startProcesses(
        tsv,
        c.command_generator,
        c.command_executor,
        fsm.run_datacard_file,
        pre_start_command);

    if (!ok) {
      throw Exception("cannot start TSV process");
    }

    // set the target state
    target.run_start_time = chrono::system_clock::time_point(start_time);

    return true;
  }
};

struct BaseState
{
  virtual ~BaseState() {}
  typedef msm::back::args<void, StateMachineInfo&> accept_sig;
  void accept(StateMachineInfo&) const {}
  virtual std::string name() const { return "unknown"; }
};

// Note: all the states and the (sub-)state machines are constructed when the
// main state machine is started. This means that it's difficult/impossible to
// establish meaningful invariants in constructors. The following approach is
// used:
//
// 1. when applicable, the constructors of states and sub-state machines
// initialize the corresponding member variables to some basic/null state
//
// 2. the transition action/guard that makes the state machine enter a
// state/sub-state machine initializes those member variables so to establish
// the invariant. This, in principle, could also be done in on_entry if the
// operations cannot fail and the info needed to complete the action is
// available in the event or in the enclosing fsm. on_entry has to be used to
// initialize a state when the target of the transition is a sub-state machine
// and that state is the entry point
//
// 3. on_entry can be used to assert that invariant
//
// 4. on_exit is used to reset the member variables to their basic/null state
//
// The invariants are described in
// https://bitbucket.org/chiarusi/tridas/wiki/TSC%20State%20Machine

using msm::front::Row;
using msm::front::none;
using msm::front::Internal;

struct TridasSM_: msm::front::state_machine_def<TridasSM_, BaseState>
{
  // disable exception catching,
  // i.e. let exceptions exit the sm and reach the application logic
  typedef int no_exception_thrown;
  // no need for message queue; a message queue is needed if
  // entry/exit/transition actions generate themselves events
  typedef int no_message_queue;

  struct Idle : public msm::front::state<BaseState>
  {
    virtual void accept(StateMachineInfo& info) const
    {
      info.sm_state = name();
    }
    virtual std::string name() const { return "idle"; }
  };

  struct Initiated_: msm::front::state_machine_def<Initiated_, BaseState>
  {
    typedef int no_exception_thrown;
    typedef int no_message_queue;

    /// The run setup identifier used to retrieve the datacard for this run
    std::string run_setup;

    /// @brief The datacard for this run
    /// @note It's a template because some information will be filled in later stages
    Configuration template_datacard;

    /// @brief The map of nodes for this run
    NodeMap node_map;

    // for uniformity with other sub-state machines, see query_transition()
    NodeMap* node_map_p;

    Initiated_(): node_map_p(0) {}

    template<class Event, class FSM>
    void on_entry(Event const&, FSM&)
    {
      // class invariant
      assert(!run_setup.empty());
      assert(!template_datacard.empty());
      assert(!node_map.empty());

      node_map_p = &node_map;
    }

    template<class Event, class FSM>
    void on_exit(Event const&, FSM&)
    {
      run_setup.clear();
      template_datacard.clear();
      clear(node_map);
    }

    virtual void accept(StateMachineInfo& info) const
    {
      info.run_setup = run_setup;
      info.node_map = node_map;
    }

    struct Standby : public msm::front::state<BaseState>
    {
      virtual void accept(StateMachineInfo& info) const
      {
        info.sm_state = name();
      }
      virtual std::string name() const { return "standby"; }
    };

    struct Configured_: msm::front::state_machine_def<Configured_,BaseState>
    {
      typedef int no_exception_thrown;
      typedef int no_message_queue;

      int run_number;
      Configuration run_datacard;
      fs::path run_datacard_file;
      NodeMap* node_map_p;

      Configured_(): run_number(-1), node_map_p(0) {}

      template<class Event, class Fsm>
      void on_entry(Event const&, Fsm& fsm)
      {
        // pre-condition: TCPUs, HMs are running
        assert(run_number >= 0);
        assert(!run_datacard.empty());
        boost::system::error_code ec;
        assert(fs::is_regular_file(run_datacard_file, ec));
        node_map_p = &fsm.node_map;
      }

      template<class Event, class FSM>
      void on_exit(Event const& ev, FSM&)
      {
        NodeMap& node_map = *node_map_p;

        // stop HM, TCPUs, EM in this order

        Nodes& hms   = node_map[Node::HM];
        Nodes& tcpus = node_map[Node::TCPU];
        Nodes& em    = node_map[Node::EM];

        Context& c = ev.context();
        bool const ok_hms   = stopProcesses(hms, c.command_generator, c.command_executor);
        bool const ok_tcpus = stopProcesses(tcpus, c.command_generator, c.command_executor);
        bool const ok_em    = stopProcesses(em, c.command_generator, c.command_executor);

        if (!(ok_hms && ok_tcpus && ok_em)) {
          TRIDAS_LOG(WARNING) << "cannot stop"
                              << ((!ok_hms) ? " HMs" : "")
                              << ((!ok_tcpus) ? " TCPUs" : "")
                              << ((!ok_em) ? " EM" : "");
        }

        run_number = -1;
        run_datacard.clear();
        run_datacard_file.clear();
        node_map_p = 0;
      }

      virtual void accept(StateMachineInfo& info) const
      {
        info.run_number = run_number;
        info.run_datacard_file = run_datacard_file;
      }

      struct Ready : public msm::front::state<BaseState>
      {
        virtual void accept(StateMachineInfo& info) const
        {
          info.sm_state = name();
        }

        virtual std::string name() const { return "ready"; }
      };

      struct Running : public msm::front::state<BaseState>
      {
        chrono::system_clock::time_point run_start_time;

        template<class Event, class FSM>
        void on_entry(Event const&, FSM&)
        {
          // pre-condition: TSV and EM are running

          assert(run_start_time != chrono::system_clock::time_point());
        }

        template<class Event, class FSM>
        void on_exit(Event const& ev, FSM& fsm)
        {
          NodeMap& node_map = *fsm.node_map_p;

          // stop only the TSV; stop the EM in Configured_::on_exit

          Context& c = ev.context();
          Nodes& tsv = node_map[Node::TSV];
          bool const ok = stopProcesses(tsv, c.command_generator, c.command_executor);
          if (!ok) {
            TRIDAS_LOG(WARNING) << "cannot stop the TSV";
          }

          run_start_time = chrono::system_clock::time_point();
        }

        virtual void accept(StateMachineInfo& info) const
        {
          info.sm_state = name();
          info.run_start_time = run_start_time;
        }

        virtual std::string name() const { return "running"; }
      };

      typedef Ready initial_state;

      struct transition_table: mpl::vector<
        //    Start     Event        Target      Action       Guard
        //   +---------+------------+-----------+--------+--------------+
        Row  < Ready   , StartEvent , Running   , none   , start_transition >
        > {};

      virtual std::string name() const { return "Configured"; }

      template <class FSM, class Event>
      void no_transition(Event const& ev, FSM& fsm, int state)
      {
        throw Exception("no transition for event " + ev.name());
      }

    }; // Configured_

    typedef msm::back::state_machine<Configured_> Configured;

    typedef Standby initial_state;

    struct transition_table : mpl::vector<
      //    Start     Event        Target      Action       Guard
      //   +---------+------------+-----------+--------+--------------+
      Row < Standby    , ConfigureEvent , Configured  , none, configure_transition >,
      Row < Configured , StopEvent      , Standby     , none, none               >
      //  +------------+----------------+-------------+----------------------+
      > {};

    struct internal_transition_table : mpl::vector<
      //    Start     Event         Next      Action               Guard
      Internal < QueryEvent, none , query_transition>
      > {};

    virtual std::string name() const { return "Initiated"; }

    template <class FSM, class Event>
    void no_transition(Event const& ev, FSM& fsm, int state)
    {
      throw Exception("no transition for event " + ev.name());
    }

  }; // Initiated_

  typedef msm::back::state_machine<Initiated_> Initiated;

  typedef Idle initial_state;

  struct transition_table: mpl::vector<
    //    Start         Event               Next       Action    Guard
    //+--------------+---------------+-----------+------------+---------------+
    Row< Idle      , InitEvent     , Initiated , none, init_transition  >,
    Row< Initiated , ResetEvent    , Idle      , none, none >
    //+--------------+---------------+-----------+---------------------------+
    > {};

  virtual std::string name() const { return "TridasSM"; }

  template <class FSM, class Event>
  void no_transition(Event const& ev, FSM& fsm, int state)
  {
    throw Exception("no transition for event " + ev.name());
  }
};

typedef msm::back::state_machine<TridasSM_> TridasSM;

/// @cond
class StateMachine::Impl
{
 public:
  Impl(Context& context) : context(context) {};
  TridasSM sm;
  Context& context;
};
/// @endcond

StateMachine::StateMachine(Context& context) :
    impl_(new Impl(context))
{
  impl_->sm.start(InitialEvent(impl_->context));
}

StateMachine::~StateMachine()
{
  impl_->sm.stop(FinalEvent(impl_->context));
}

void StateMachine::init(std::string const& run_setup)
{
  bool const event_handled
      = impl_->sm.process_event(InitEvent(impl_->context, run_setup));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

void StateMachine::configure()
{
  bool const event_handled
      = impl_->sm.process_event(ConfigureEvent(impl_->context));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

void StateMachine::start()
{
  bool const event_handled
      = impl_->sm.process_event(StartEvent(impl_->context));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

void StateMachine::stop()
{
  bool const event_handled
      = impl_->sm.process_event(StopEvent(impl_->context));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

void StateMachine::reset()
{
  bool const event_handled
      = impl_->sm.process_event(ResetEvent(impl_->context));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

std::string StateMachine::currentState() const
{
  StateMachineInfo info;
  impl_->sm.visit_current_states(boost::ref(info));
  return info.sm_state;
}

StateMachineInfo StateMachine::info() const
{
  StateMachineInfo info;
  impl_->sm.visit_current_states(boost::ref(info));
  return info;
}

void StateMachine::query()
{
  bool const event_handled
      = impl_->sm.process_event(QueryEvent(impl_->context));
  assert(event_handled == msm::back::HANDLED_TRUE);
}

}}
