#include "state_machine_info.hpp"
#include <numeric>

namespace tridas {
namespace tsc {

typedef std::pair<int, int> Values;

struct Plus
{
  Values operator()(Values const& values, Node const& node)
  {
    return Values(
        values.first + std::max(node.n_running_instances(), 0)
      , values.second + std::max(node.n_instances(), 0)
    );
  }
};

struct NodeRoleNotEqual
{
  Node::Role role;
  NodeRoleNotEqual(Node::Role r): role(r) {}
  bool operator()(Node const& node) const
  {
    return node.role() != role;
  }
};

void role_summary(std::ostream& os, Node::Role role, Nodes const& nodes)
{
  os << ToString(role) << ':';
  if (!nodes.empty()) {
    Values v = std::accumulate(nodes.begin(), nodes.end(), Values(), Plus());
    assert(std::find_if(nodes.begin(), nodes.end(), NodeRoleNotEqual(role)) == nodes.end());
    os << v.first << '/' << v.second;
  }
}

std::string node_summary(NodeMap const& nm)
{
  // TCPU:7/8;HM:0/1
  std::ostringstream os;
             role_summary(os, Node::TSV , nm[Node::TSV]);
  os << ';'; role_summary(os, Node::HM  , nm[Node::HM]);
  os << ';'; role_summary(os, Node::TCPU, nm[Node::TCPU]);
  os << ';'; role_summary(os, Node::EM  , nm[Node::EM]);

  return os.str();
}

std::ostream& operator<<(std::ostream& os, StateMachineInfo const& info)
{
  os << info.sm_state << " ";
  if (info.run_setup != "NA") {
    os << "RUNSETUP:" << info.run_setup << " ";
  }
  if (!info.run_datacard_file.empty()) {
    os << "RUNDATACARD:" << info.run_datacard_file.native() << " ";
  }
  if (info.run_number != -1) {
    os << "RUNNUMBER:" << info.run_number << " ";
  }
  if (info.run_start_time != boost::chrono::system_clock::time_point()) {
    os << "RUNSTARTTIME:" << boost::chrono::duration_cast<boost::chrono::seconds>(info.run_start_time.time_since_epoch()).count() << " ";
  }
  os << "NODES:" << node_summary(info.node_map);

  return os;
}

}}
