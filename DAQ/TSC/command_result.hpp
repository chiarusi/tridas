#ifndef DAQ_TSC_COMMAND_RESULT_HPP
#define DAQ_TSC_COMMAND_RESULT_HPP

#include <string>
#include <vector>
#include <algorithm>
#include <cassert>

namespace tridas {
namespace tsc {

class CommandResult
{
  CommandResult(
      int exit_status_or_errno
    , std::string output_or_strerror
    , bool internal_error = false
  )
      : exit_status_or_errno_(exit_status_or_errno)
      , output_or_strerror_(output_or_strerror)
      , internal_error_(internal_error)
  {}
 public:
  int exit_status() const
  {
    assert(!internal_error_);
    return exit_status_or_errno_;
  }
  int sys_errno() const
  {
    assert(internal_error_);
    return exit_status_or_errno_;
  }
  std::string output() const
  {
    assert(!internal_error_);
    return output_or_strerror_;
  }
  std::string strerror() const
  {
    assert(internal_error_);
    return output_or_strerror_;
  }
  bool internal_error() const
  {
    return internal_error_;
  }
  static CommandResult make_internal_error_result(int e, std::string const& s)
  {
    return CommandResult(e, s, true);
  }
  static CommandResult make_successful_result(int e, std::string const& s)
  {
    return CommandResult(e, s, false);
  }

 private:
  int exit_status_or_errno_;
  std::string output_or_strerror_;
  bool internal_error_;

};

class CommandResults: private std::vector<CommandResult>
{
  typedef std::vector<CommandResult> V;
 public:
  using V::value_type;
  using V::iterator;
  using V::const_iterator;
  using V::reference;
  using V::const_reference;
  using V::begin;
  using V::end;
  using V::empty;
  using V::size;
  using V::push_back;
};

/// @brief Determine if the result of an executed command is to be considered
///        successful
///
/// @param result The result of an excecuted command
/// @retval true if there was no internal error during execution and the exit
/// status of the command denotes success
/// @retval false otherwise
///
inline
bool successful(CommandResult const& result)
{
  return !result.internal_error() && result.exit_status() == EXIT_SUCCESS;
}

/// @cond

inline
bool not_successful(CommandResult const& result)
{
  return !successful(result);
}

/// @endcond

/// @brief Determine if all the results of a set of executed command are
///        successful
///
/// @param  results The results of the execution of a set of commands

/// @retval true    if all the results are successful
/// @retval false   otherwise
///
inline
bool all_successful(CommandResults const& results) {
  return
      std::find_if(
          results.begin()
        , results.end()
        , not_successful
      ) == results.end();
}

}}

#endif
