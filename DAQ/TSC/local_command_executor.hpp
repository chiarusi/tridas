#ifndef DAQ_TSC_LOCALCOMMANDEXECUTOR_HPP
#define DAQ_TSC_LOCALCOMMANDEXECUTOR_HPP

#include "command_executor.hpp"

namespace tridas {
namespace tsc {

class LocalCommandExecutor: public CommandExecutor
{
  bool dry_run_;

 public:
  explicit LocalCommandExecutor(bool dry_run = false);
  CommandResult operator()(Command const& command, Node const& node) const;
  CommandResults operator()(Commands const& commands, Nodes const& nodes) const;

  // define the virtual destructor to silence -Wnon-virtual-dtor
  virtual ~LocalCommandExecutor() {}

};

}}

#endif
