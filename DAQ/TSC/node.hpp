#ifndef DAQ_TSC_NODE_HPP
#define DAQ_TSC_NODE_HPP

#include <vector>
#include <string>
#include <boost/array.hpp>
#include <algorithm>
#include "Configuration/Configuration.h"

namespace tridas {
namespace tsc {

class Node
{
 public:
  enum Role {INVALID_ROLE = -1, TSV = 0, HM, TCPU, EM, N_ROLES};
  enum { INVALID_INSTANCES = -1 };

 private:
  Role role_;
  std::string hostname_;
  int n_instances_;
  size_t starting_id_;
  int n_running_instances_;

 public:
  Node(Role role, std::string const& hostname, size_t n_instances, size_t starting_id)
      : role_(role)
      , hostname_(hostname)
      , n_instances_(n_instances)
      , starting_id_(starting_id)
      , n_running_instances_(0)
  {}

  Role role() const { return role_; }
  std::string hostname() const { return hostname_; }
  int n_instances() const { return n_instances_; }
  size_t starting_id() const { return starting_id_; }
  void n_running_instances(int n) { n_running_instances_ = n; }
  int n_running_instances() const { return n_running_instances_; }
};

typedef std::vector<Node> Nodes;
typedef boost::array<Nodes,Node::N_ROLES> NodeMap;

inline void clear_nodes(Nodes& nodes)
{
  nodes.clear();
}

inline void clear(NodeMap& nm)
{
  std::for_each(nm.begin(), nm.end(), clear_nodes);
}

//! @throws Exception if TCPU nodes information is not present or not correct
Nodes read_tcpu_nodes(Configuration const& configuration);
Nodes read_hm_nodes(Configuration const& configuration);
Nodes read_em_nodes(Configuration const& configuration);
Nodes read_ch_nodes(Configuration const& configuration);
Nodes read_tsv_nodes(Configuration const& configuration);

inline std::string ToString(Node::Role v) {
  static std::string const s[] = { "TSV", "HM", "TCPU", "EM" };
  assert((v >= Node::TSV && v < Node::N_ROLES) || v == Node::INVALID_ROLE);
  if (v >= Node::TSV && v < Node::N_ROLES) {
    return s[v];
  } else {
    return "Invalid";
  }
}

inline Node::Role FromString(std::string const& role_name) {
  if (role_name == "TSV") return Node::TSV;
  if (role_name == "HM") return Node::HM;
  if (role_name == "TCPU") return Node::TCPU;
  if (role_name == "EM") return Node::EM;
  return Node::INVALID_ROLE;
}

}}

#endif
