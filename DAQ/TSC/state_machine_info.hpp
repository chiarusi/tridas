#ifndef TRIDAS_TSC_STATEMACHINEINFO_HPP
#define TRIDAS_TSC_STATEMACHINEINFO_HPP

/// @file

#include <string>
#include <iosfwd>
#include <boost/filesystem.hpp>
#include <boost/chrono/system_clocks.hpp>
#include "node.hpp"

namespace tridas {
namespace tsc {

/// @class StateMachineInfo
/// @brief Collect information about the current state of the state machine
struct StateMachineInfo
{
  std::string sm_state;
  std::string run_setup;
  boost::filesystem::path run_datacard_file;
  int run_number;
  boost::chrono::system_clock::time_point run_start_time;
  NodeMap node_map;
  // TODO replace with something like boost::optional or proper types
  StateMachineInfo(): run_setup("NA"), run_number(-1) {}
};

std::ostream& operator<<(std::ostream& os, StateMachineInfo const& info);

}}

#endif
