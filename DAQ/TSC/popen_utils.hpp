#ifndef DAQ_TSC_POPENUTILS_HPP
#define DAQ_TSC_POPENUTILS_HPP

namespace tridas {
namespace tsc {

class Command;
class CommandResult;

CommandResult popen_execute(Command const& command);

}}

#endif
