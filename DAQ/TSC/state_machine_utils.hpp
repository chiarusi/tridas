#ifndef DAQ_TSC_STATEMACHINEUTILS_HPP
#define DAQ_TSC_STATEMACHINEUTILS_HPP

/// @file

#include <string>
#include <boost/filesystem.hpp>
#include "Configuration/Configuration.h"
#include "command_generator.hpp"
#include "context.hpp"
#include "node.hpp"

namespace tridas {
namespace tsc {

class CommandExecutor;

/// @brief For each role, read the list of nodes and corresponding attributes
/// from the configuration
///
/// @param configuration the configuration
/// @throw Exception if the information about one of the roles is not present,
///        is evidently incorrect or is incomplete
NodeMap readNodeMap(Configuration const& configuration);

/// @brief Start a TriDAS process on a set of nodes
///
/// @param nodes The set of nodes where a process should be started. After the
/// call each node will be updated based on its new status.
/// @param cg    The mechanism used to generate a command
/// @param executor The mechanism used to execute a command
/// @param configuration_file The path of the datacard to be passed to each
///                           starting process
/// @param pre_start_command String to be prepended to the command executed on
///                          the remote node (useful for performance evaluation
///                          or debugging)
/// @retval true  if all the processes have started correctly
/// @retval false otherwise
bool startProcesses(
    Nodes& nodes
  , CommandGenerator const& cg
  , CommandExecutor const& executor
  , boost::filesystem::path const& configuration_file
  , std::string const& pre_start_command
);

/// @brief Stop the TriDAS process on a set of nodes
///
/// @param nodes The set of nodes where the process should be stopped. After the
/// call each node will be updated based on its new status.
/// @param cg    The mechanism used to generate the stop command
/// @param executor The mechanism used to execute the command
/// @retval true  if all the processes have been stopped correctly
/// @retval false otherwise
bool stopProcesses(
    Nodes& nodes
  , CommandGenerator const& cg
  , CommandExecutor const& executor
);

/*
 * \brief Query the TriDAS process on a set of nodes
 *
 * \param nodes The set of nodes where the process to be queried runs. After the
 * call each node will be updated based on its retrieved status.
 * \param cg    The mechanism used to generate the stop command
 * \param executor The mechanism used to execute the command
 * \retval true  if all the processes have been queried correctly
 * \retval false otherwise
 */
bool queryProcesses(
    Nodes& nodes
  , CommandGenerator const& cg
  , CommandExecutor const& executor
);

/// @brief Retrieve the datacard corresponing to a run setup
///
/// @param run_setup The run setup identifier. For the moment it is assumed to
/// be a path on the local file system, such that it is:
/// - absolute
/// - a regular file
/// - without spaces
/// @return the datacard corresponding to the run setup
/// @throw Exception if the run setup doesn't satisfy the conditions above
/// @throw any exception raised by the Configuration package while reading
///        the file or manipulating its data structures
Configuration retrieve_configuration(std::string const& run_setup);

/// @brief Retrieve a run number
///
/// The run number is determined incrementing the previous run number,
/// if available, otherwise it is set to 1.
/// @param configuration The run datacard
/// @return a new run number
/// @note The datacard is needed to retrieve the information needed to
/// find where the previous run number is saved.
/// @see make_run_number_filename
/// @see create_run_number_file
/// @note At the moment the previous run number is determined looking for a
/// symbolic link called \c latest in the directory set with the
/// TSC.DATACARD_SHAREDDIR configuration option. \c latest points to a file
/// whose name encodes the run number. The encoding is simply the stringified
/// run number padded with 0 to eight characters. In order to force the
/// generation of a certain run number it is sufficient to create a filename
/// according to the above format in that directory and create a symbolic link
/// to it called \c latest.
int retrieve_run_number(Configuration const& configuration);

/// @brief computes a file path where to store a datacard for a run
///
/// The generated path is a combination of information retrieved from the
/// datacard itself, the run setup and the run number.
/// @param configuration The run datacard
/// @param run_setup The run setup identifier
/// @param run_number The run number
/// @see retrieve_configuration for assumptions on run_setup
/// @return a filesystem path
/// @throw Exception if the information required to be in the datacard is
/// missing, incomplete or incorrect
/// @pre run_setup satisfy the requirements expressed in retrieve_configuration
/// @pre run_number must be greater than zero
boost::filesystem::path make_configuration_filename(
    Configuration const& configuration
  , std::string const& run_setup
  , int run_number
);

/// @brief dump a datacard on file
///
/// The operation is atomic wrt the file system, i.e. the file is first created
/// in a temporary location and then moved to the final destination if the
/// creation is successful.
/// @param c datacard
/// @param p path
/// @throw Exception if the write to the temporary file fails
/// @throw any exception raised by filesystem operations
/// @see make_configuration_filename
void dump_on_file(Configuration const& c, boost::filesystem::path const& p);

/// @brief computes a file path to persist a run number
///
/// The filename is generated as described in the note of retrieve_run_number().
/// @param configuration The run datacard
/// @param run_number The run number
/// @return a filesystem path
/// @throw Exception if the information required to be in the datacard is
/// missing, incomplete or incorrect
/// @pre run_number must be greater than zero
boost::filesystem::path make_run_number_filename(
    Configuration const& configuration
  , int run_number
);

/// @brief create the run number file
///
/// Persist the run number file created with make_run_number_filename() and
/// create a \c latest link to it to ease retrieval. Moreover the file
/// corresponding to the previous run number is removed.
/// @param p the path of the file to be created
/// @throw Exception if the file cannot be created
/// @throw boost.filesystem exceptions if the filesystem operations to manage
/// symbolic links and previous files fail
/// @see make_run_number_filename
/// @pre p is an absolute filename and is not a directory
/// @post p is created and a \c latest symbolic link links to it
void create_run_number_file(boost::filesystem::path const& p);

/// @brief quick check that the set of nodes makes sense for a run
///
/// At the moment it checks that the set of TCPUs and HMs are not empty and that
/// there is exacly one TSV and exactly one EM
/// @param node_map The map of nodes that are included in a run
/// @retval true if the check is successful
/// @retval false otherwise
bool node_map_makes_sense(NodeMap const& node_map);

}}

#endif
