#ifndef DAQ_TSC_CONTEXT_HPP
#define DAQ_TSC_CONTEXT_HPP

/// @file

#include <string>

namespace boost {
namespace asio {
class io_service;
}}

namespace tridas {
namespace tsc {

class CommandGenerator;
class CommandExecutor;

/// @class Context
/// @brief A bag of objects that need to be passed around
///
/// The Context class is used to aggregate some objects that are instantiated in
/// the main function and then passed to other entities, notably the state
/// machine. This is mainly to avoid the use of global objects, which are
/// replaced by explicit instantiations in the main function.
/// @see StateMachine
struct Context
{
  Context(
      boost::asio::io_service& ios
    , CommandGenerator& command_generator
    , CommandExecutor& command_executor
  )
      : ios(ios)
      , command_generator(command_generator)
      , command_executor(command_executor)
  {
  }
  /// The global io_service
  boost::asio::io_service& ios;
  /// The generator of commands to be executed during a state machine transition
  CommandGenerator& command_generator;
  /// The executor of commands during a state machine transition
  CommandExecutor& command_executor;
};

}}

#endif
