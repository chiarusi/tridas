#ifndef DAQ_TSC_UPDATENODE_HPP
#define DAQ_TSC_UPDATENODE_HPP

/// @file

#include <string>
#include <algorithm>
#include "Log/log.hpp"
#include "node.hpp"
#include "node_state.hpp"
#include "command_result.hpp"

namespace tridas {
namespace tsc {

inline std::string error_string(CommandResult const& result)
{
  if (result.internal_error()) {
    return "internal error " + boost::lexical_cast<std::string>(result.sys_errno()) + " - " + result.strerror();
  } else {
    return "error " + boost::lexical_cast<std::string>(result.exit_status()) + " - " + result.output();
  }
}

/// @brief Update the number of running instances of the node.
///
/// The new value is taken from the result of a command executed on that
/// node. If the result denotes failure, the number of running instances is set
/// to invalid.
///
/// @param node           The node
/// @param command_result The command result
/// @retval true          if the result denotes failure, or the command
///                       output is incorrect, or the number or running instances
///                       is greater than the max number of running instances
/// @retval false         otherwise
/// @post  The number of running instances is updated to what contained in the
///        command result
inline
bool update_node(Node& node, CommandResult const& command_result)
{
  if (successful(command_result)) {
    try {
      NodeState const state = NodeState::parse(command_result.output());
      if (state.role == node.role()
          && state.n_running >= 0
          && state.n_running <= node.n_instances()) {
        node.n_running_instances(state.n_running);
        return true;
      } else {
        TRIDAS_LOG(ERROR)
            << node.hostname() << ": unexpected command output: expecting '"
            << (ToString(node.role()) + ":N")
            << "' (N >= 0 && N <= " << node.n_instances() << "), received '"
            // TODO the following string should probably be limited to K
            // characters; if longer add ellipsis. Moreove if would be useful to
            // escape control characters (e.g. a final newline) to avoid
            // strange characters or patterns in the log file
            << command_result.output() << '\'';
        node.n_running_instances(Node::INVALID_INSTANCES);
        return false;
      }
    } catch (NodeState::ParseError const&) {
      TRIDAS_LOG(ERROR)
          << node.hostname() << ": unexpected command output '"
          // see TODO above
          << command_result.output() << '\'';
      return false;
    }
  } else {
    TRIDAS_LOG(ERROR) << node.hostname() << ": command failed ("
                      << error_string(command_result) << ')';
    node.n_running_instances(Node::INVALID_INSTANCES);
    return false;
  }
}

/// @brief Update the number of running instances associated to each node.
///
/// The new value is taken from the result of a command executed on that
/// node. If the result for a node denotes failure, the number of running
/// instances is set to invalid.
///
/// @param nodes           The set of nodes
/// @param command_results The set of command results
/// @retval true           if one of the single updates fails
/// @retval false          otherwise
/// @pre   The number of nodes equals the number of results
/// @post  For each node, the number of running instances of the corresponding
///        TriDAS process is updated to what contained in the command result
/// @see update_node
inline
bool update_nodes(Nodes& nodes, CommandResults const& command_results)
{
  assert(command_results.size() == nodes.size());

  bool result = true;

  CommandResults::const_iterator result_it = command_results.begin();
  CommandResults::const_iterator const result_last = command_results.end();
  Nodes::iterator node_it = nodes.begin();

  for ( ; result_it != result_last; ++result_it, ++node_it) {
    if (!update_node(*node_it, *result_it)) {
      result = false;
    }
  }

  return result;
}

}}

#endif
