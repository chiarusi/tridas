#ifndef DAQ_TSC_STATEMACHINE_HPP
#define DAQ_TSC_STATEMACHINE_HPP

/// @file

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/chrono/system_clocks.hpp>
#include <boost/filesystem.hpp>
#include "node.hpp"
#include "exception.hpp"
#include "state_machine_info.hpp"

namespace tridas {
namespace tsc {

class Context;

/// @class StateMachine
/// @brief The state machine that controls the TriDAS system.
///
/// The StateMachine class implements the state machine that controls the TriDAS
/// system.
/// @startuml{StateMachine.png}
/// 
/// state StateMachine {
///   [*] --> Idle
///   Idle --> Initiated: init
///   state Initiated {
///     Initiated: run_setup
///     Initiated: template_datacard
///     Initiated: node_map
///     [*] --> Standby
///     Standby --> Configured: configure
///     state Configured {
///       Configured: run_number
///       Configured: run_datacard
///       Configured: run_datacard_file
///       [*] --> Ready
///       Ready -r-> Running: start
///       Running: run_start_time
///     }
///     Configured --> Standby: stop
///   }
///   Initiated --> Idle: reset
/// }
///
/// @enduml

class StateMachine
{
 public:
  /// @brief Construct and start the state machine
  /// @param context the Context
  explicit StateMachine(Context& context);

  /// @brief Run the init transition
  /// @param runsetup the run setup for this run
  /// @note Failure is always reported with an exception.
  void init(std::string const& runsetup);

  /// @brief Run the configure transition
  /// @note Failure is always reported with an exception.
  void configure();

  /// @brief Run the start transition
  /// @note Failure is always reported with an exception.
  void start();

  /// @brief Run the stop transition
  /// @note Failure is always reported with an exception.
  void stop();

  /// @brief Run the reset transition
  /// @note Failure is always reported with an exception.
  void reset();

  /// @brief Run the query (internal) transition
  /// 
  /// Retrieve the state from each node in the system. To access the
  /// newly-computed state, see info()
  void query();

  /// @brief Compute and retrieve salient information from the state machine
  StateMachineInfo info() const;

  /// @brief Return the name of the current state of the state machine
  std::string currentState() const;

  /// @brief Stop and destroy the state machine
  /// @note Stopping the state machine is not to be confused with executing the
  ///       @ref stop transition
  ~StateMachine();

 private:
  class Impl;
  boost::shared_ptr<Impl> impl_;
};

}}

#endif
