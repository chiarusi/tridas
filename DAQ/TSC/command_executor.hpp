#ifndef DAQ_TSC_COMMANDEXECUTOR_HPP
#define DAQ_TSC_COMMANDEXECUTOR_HPP

#include "command.hpp"
#include "command_result.hpp"
#include "node.hpp"

namespace tridas {
namespace tsc {

class CommandExecutor
{
 public:
  virtual CommandResult operator()(Command const& command, Node const& node) const = 0;
  virtual CommandResults operator()(Commands const& commands, Nodes const& nodes) const = 0;

 protected:
  // objects of subclasses are meant to be allocated on the stack and passes
  // around as references to the base class -> no need to call delete
  ~CommandExecutor() {}
};

}}

#endif
