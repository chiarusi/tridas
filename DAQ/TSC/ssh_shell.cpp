#include "ssh_shell.hpp"
#include <boost/thread/once.hpp>
#include <boost/filesystem.hpp>
#include <boost/array.hpp>

namespace fs = boost::filesystem;

namespace ssh {

struct libssh2_init_raii
{
  libssh2_init_raii()
  {
    int rc = libssh2_init(0);
    assert(rc == 0 && "libssh2_init");
  }
  ~libssh2_init_raii()
  {
    libssh2_exit();
  }
};

void initialize_libssh2()
{
  static libssh2_init_raii _;
}

ShellPtr Shell::create(
    boost::asio::io_service& ios
    , fs::path const& ssh_dir
    , std::string const& user
    , std::string const& host
    , std::string const& port
)
{
  return ShellPtr(new Shell(ios, ssh_dir, user, host, port));
}

Shell::Shell(
    boost::asio::io_service& ios
    , fs::path const& ssh_dir
    , std::string const& user
    , std::string const& host
    , std::string const& port
)
    : m_ssh_dir(ssh_dir)
    , m_user(user)
    , m_host(host)
    , m_port(port)
    , m_socket(ios)
    , m_timeout_timer(ios)
    , m_retry_timer(ios)
    , m_reply_delimiter('\0') // the initialization is not really needed
{
  initialize_libssh2();

  assert(!m_host.empty());
  if (ssh_dir.is_relative()) {
    throw std::runtime_error("ssh_dir is not an absolute path");
  }
  fs::file_status st = status(ssh_dir);
  if (!exists(st)) {
    throw std::runtime_error("ssh_dir doesn't exist");
  }
  if (!is_directory(st)) {
    throw std::runtime_error("ssh_dir is not a directory");
  }
}

void Shell::reset()
{
  // go back to the state at the end of the ctor

  // abort any outstanding operation
  boost::system::error_code ec;
  m_retry_timer.cancel(ec);
  m_timeout_timer.cancel(ec);
  m_socket.cancel(ec);
  m_buffer.clear();

  m_channel.reset();
  m_session.reset();

  m_socket.close();
}

void Shell::disconnect()
{
  // abort any outstanding operation
  boost::system::error_code ec;
  m_retry_timer.cancel(ec);
  m_timeout_timer.cancel(ec);
  m_socket.cancel(ec);
  m_buffer.clear();

  // - try to disconnect politely, ignoring possible errors
  // - reset the SSH structures and recreate them anew during a later
  //   reconnection
  //   - if reused I've seen errors such as
  //     "Bad packet length 3608842649. [preauth]"
  //     on the server side

  if (m_channel) {
    // ignore return values
    libssh2_channel_send_eof(m_channel.get());
    libssh2_channel_close(m_channel.get());
    m_channel.reset();
  }

  if (m_session) {
    // ignore return value
    libssh2_session_disconnect(m_session.get(), "ssh::Shell disconnect");
    m_session.reset();
  }

  m_socket.close(ec);
}

std::string Shell::host() const
{
  return m_host;
}

std::string Shell::port() const
{
  return m_port;
}

std::string Shell::release_reply()
{
  std::string result = m_buffer;
  m_buffer.clear();
  return result;
}

void Shell::log_verbosity(int v)
{
  assert(m_session);
  libssh2_trace(m_session.get(), v);
}

void Shell::async_connect(boost::asio::deadline_timer::duration_type const& timeout)
{
  async_connect();
  m_timeout_timer.expires_from_now(timeout);
  m_timeout_timer.async_wait(
      boost::bind(
          &Shell::on_timeout
          , this
          , boost::asio::placeholders::error
      )
  );
}

void Shell::async_connect()
{
  try_connect(boost::system::error_code());
}

void Shell::try_connect(boost::system::error_code const& error)
{
  if (!error) {
    using namespace boost::asio::ip;
    tcp::resolver resolver(m_socket.get_io_service());
    tcp::resolver::query query(m_host, m_port);
    boost::system::error_code ec;
    tcp::resolver::iterator iterator = resolver.resolve(query, ec);
    if (ec) {
      throw std::runtime_error("cannot resolve hostname " + m_host);
    } else {
      boost::asio::async_connect(
          m_socket,
          iterator,
          boost::bind(
              &Shell::handle_connect,
              this,
              boost::asio::placeholders::error
          )
      );
    }
  }
}

void Shell::handle_connect(boost::system::error_code const& error)
{
  if (error) {
    m_retry_timer.expires_from_now(
        boost::posix_time::milliseconds(10)
    );
    m_retry_timer.async_wait(
        boost::bind(
            &Shell::try_connect
            , this
            , boost::asio::placeholders::error
        )
    );
  } else {
    // initialize the SSH session after the TCP connection
    m_session.reset(libssh2_session_init(), libssh2_session_free);
    libssh2_session_set_blocking(m_session.get(), 0);
    try_ssh_handshake(boost::system::error_code());
  }
}

void Shell::try_ssh_handshake(boost::system::error_code const& error)
{
  if (!error) {
    int rc = libssh2_session_handshake(m_session.get(), m_socket.native_handle());
    switch (rc) {
      case LIBSSH2_ERROR_NONE:
        try_authenticate(boost::system::error_code());
        break;
      case LIBSSH2_ERROR_EAGAIN:
        retry_when_ready(
            boost::bind(
                &Shell::try_ssh_handshake
                , this
                , boost::asio::placeholders::error
            )
        );
        break;
      default:
        reset();
        break;
    }
  }
}

void Shell::try_create_channel(boost::system::error_code const& error)
{
  if (!error) {
    m_channel.reset(libssh2_channel_open_session(m_session.get()), libssh2_channel_free);
    if (m_channel) {
      try_create_shell(boost::system::error_code());
    } else {
      int rc = libssh2_session_last_error(m_session.get(), 0, 0, 0);
      if (rc == LIBSSH2_ERROR_EAGAIN) {
        retry_when_ready(
            boost::bind(
                &Shell::try_create_channel
                , this
                , boost::asio::placeholders::error
            )
        );
      } else {
        reset();
      }
    }
  }
}

void Shell::try_create_shell(boost::system::error_code const& error)
{
  if (!error) {
    int rc = libssh2_channel_shell(m_channel.get());
    switch (rc) {
      case LIBSSH2_ERROR_NONE:
        m_timeout_timer.cancel();
        break;
      case LIBSSH2_ERROR_EAGAIN:
        retry_when_ready(
            boost::bind(
                &Shell::try_create_shell
                , this
                , boost::asio::placeholders::error
            )
        );
        break;
      default:
        reset();
    }
  }
}

void Shell::on_timeout(boost::system::error_code const& error)
{
  if (!error) {
    reset();
  }
}

bool Shell::is_connected() const
{
  return m_socket.is_open();
}

std::string Shell::public_key_hash() const
{
  assert(is_connected());

  // pointer to an internal buffer
  char const* const fingerprint = libssh2_hostkey_hash(
      m_session.get()
      , LIBSSH2_HOSTKEY_HASH_SHA1
  );
  if (!fingerprint) {
    throw std::runtime_error("libssh2_hostkey_hash");
  }

  std::ostringstream os;
  for (int i = 0; i < 20; i++) {
    char c = fingerprint[i];
    os << std::hex << ((c >> 4) & 0xF) << (c & 0xF);
  }

  return os.str();
}

void Shell::try_authenticate(boost::system::error_code const& error)
{
  if (!error) {
    int rc = libssh2_userauth_publickey_fromfile(
        m_session.get()
        , m_user.c_str()
        , (m_ssh_dir/"id_rsa.pub").c_str()
        , (m_ssh_dir/"id_rsa").c_str()
        , 0 // no password
    );
    switch (rc) {
      case LIBSSH2_ERROR_NONE:
        try_create_channel(boost::system::error_code());
        break;
      case LIBSSH2_ERROR_EAGAIN:
        retry_when_ready(
            boost::bind(
                &Shell::try_authenticate
                , this
                , boost::asio::placeholders::error
            )
        );
        break;
      default:
        reset();
        break;
    }
  }
}

void Shell::async_exec(Command const& command, boost::asio::deadline_timer::duration_type const& timeout)
{
  async_exec(command);
  m_timeout_timer.expires_from_now(timeout);
  m_timeout_timer.async_wait(
      boost::bind(
          &Shell::on_timeout
          , this
          , boost::asio::placeholders::error
      )
  );
}
void Shell::async_exec(Command const& command)
{
  assert(m_buffer.empty() && "async_exec while there is a pending one");
  m_buffer = command.str();
  m_buffer_current = m_buffer.begin();
  m_reply_delimiter = command.reply_delimiter();

  m_socket.async_write_some(
      boost::asio::null_buffers()
      , boost::bind(
          &Shell::try_send_command
          , this
          , boost::asio::placeholders::error
      )
  );
}

void Shell::try_send_command(boost::system::error_code const& error)
{
  if (!error) {

    ssize_t n_written = libssh2_channel_write(
        m_channel.get()
        , &*m_buffer_current
        , std::distance(m_buffer_current, m_buffer.end())
    );
    bool eom = false;
    while (n_written >= 0 && !eom) {
      assert(n_written <= std::distance(m_buffer_current, m_buffer.end()));
      std::advance(m_buffer_current, n_written);
      eom = m_buffer_current == m_buffer.end();
      if (!eom) {
        n_written = libssh2_channel_write(
            m_channel.get()
            , &*m_buffer_current
            , std::distance(m_buffer_current, m_buffer.end())
        );
      }
    }

    assert(n_written < 0 || eom);

    if (eom) {
      m_buffer.clear();
      m_socket.async_receive(
          boost::asio::null_buffers(),
          boost::bind(
              &Shell::try_read_reply,
              this,
              boost::asio::placeholders::error
          )
      );
    } else if (n_written == LIBSSH2_ERROR_EAGAIN) {
      retry_when_ready(
          boost::bind(
              &Shell::try_send_command
              , this
              , boost::asio::placeholders::error
          )
      );
    } else { // error
      reset();
    }
  }
}

void Shell::try_read_reply(boost::system::error_code const& error)
{
  if (!error) {

    typedef boost::array<char, 4096> buffer_type;
    buffer_type data;
    ssize_t len = libssh2_channel_read(
        m_channel.get()
        , data.data()
        , data.size()
    );
    bool eom = false;
    while (len >= 0 && !eom) {
      assert(static_cast<buffer_type::size_type>(len) <= data.size());
      buffer_type::iterator data_begin = data.begin();
      buffer_type::iterator data_end = data.begin() + len;
      // assume that after the delimiter there won't be anything
      if (*(data_end - 1) == m_reply_delimiter) {
        eom = true;
        --data_end;
      }
      m_buffer.insert(m_buffer.end(), data_begin, data_end);
      assert(m_buffer[m_buffer.size() - 1] != m_reply_delimiter);
      if (!eom) {
        len = libssh2_channel_read(
            m_channel.get()
            , data.data()
            , data.size()
        );
      }
    }

    assert(len < 0 || eom);

    if (eom) {
      m_timeout_timer.cancel();
    } else if (len == LIBSSH2_ERROR_EAGAIN) {
      retry_when_ready(
          boost::bind(
              &Shell::try_read_reply
              , this
              , boost::asio::placeholders::error
          )
      );
    } else { // error
      reset();
    }
  }
}

}
