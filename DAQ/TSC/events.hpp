#ifndef DAQ_TSC_EVENTS_HPP
#define DAQ_TSC_EVENTS_HPP

#include <string>

namespace tridas {
namespace tsc {

class Context;

class BaseEvent {
 public:
  explicit BaseEvent(Context& context) : context_(context) { }
  Context& context() const { return context_; }
  virtual std::string name() const = 0;
 protected:
  virtual ~BaseEvent() {}
 private:
  Context& context_;
};

class InitEvent : public BaseEvent {
  std::string run_setup_;
 public:
  InitEvent(Context& context, std::string const& run_setup)
      : BaseEvent(context), run_setup_(run_setup) { };
  std::string name() const { return "init"; }
  std::string run_setup() const { return run_setup_; }
};

class ResetEvent : public BaseEvent {
 public:
  explicit ResetEvent(Context& context) : BaseEvent(context) { };
  std::string name() const {
    return "reset";
  }
};

class ConfigureEvent : public BaseEvent {
 public:
  explicit ConfigureEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "configure";  }
};

class StartEvent : public BaseEvent {
 public:
  explicit StartEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "start"; }
};

class StopEvent : public BaseEvent {
 public:
  explicit StopEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "stop"; }
};

class QueryEvent : public BaseEvent {
 public:
  explicit QueryEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "query"; }
};

// control which events are passed to the SM at start/stop
class InitialEvent : public BaseEvent
{
 public:
  explicit InitialEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "InitialEvent"; }
};

class FinalEvent : public BaseEvent
{
 public:
  explicit FinalEvent(Context& context) : BaseEvent(context) { };
  std::string name() const { return "FinalEvent"; }
};

}}

#endif
