#include "command_generator.hpp"
#include "command_executor.hpp"
#include "node.hpp"

using namespace tridas;
using namespace tridas::tsc;

Node make_node(std::string const& host, Node::Role role)
{
  return Node(role, host, 1, 0);
}

NodeMap makeNodeMap()
{
  NodeMap result(4);

  std::vector<Node> hm_nodes;
  hm_nodes.push_back(make_node("hm1.cnaf.infn.it", Node::HM));
  hm_nodes.push_back(make_node("hm2.cnaf.infn.it", Node::HM));
  result[Node::HM] = hm_nodes;

  std::vector<Node> tcpu_nodes;
  tcpu_nodes.push_back(make_node("tcpu1.cnaf.infn.it", Node::TCPU));
  tcpu_nodes.push_back(make_node("tcpu2.cnaf.infn.it", Node::TCPU));
  result[Node::TCPU] = tcpu_nodes;

  std::vector<Node> em_nodes;
  em_nodes.push_back(make_node("em.cnaf.infn.it", Node::EM));
  result[Node::EM] = em_nodes;

  std::vector<Node> tsv_nodes;
  tsv_nodes.push_back(make_node("tsv.cnaf.infn.it", Node::TSV));
  result[Node::TSV] = tsv_nodes;

  return result;
}

int main()
{
  Log::init("t_command", Log::DEBUG);
  NodeMap node_map = makeNodeMap();

  CommandGenerator cg("tridas-run");
  bool const dry_run = true;
  LocalCommandExecutor execute_(dry_run);
  CommandExecutor& execute = execute_;

  {
    Nodes const& nodes = node_map[Node::TCPU];
    Commands commands = cg.prepareStartCommands(nodes);
    CommandResults results = execute(commands, nodes);
  }
  /*
  {
    StartSystemCommands commands = cg.prepareStartCommands(node_map[Node::HM]);
    StartSystemCommandResults results = execute(commands);
  }
  {
    StartSystemCommands commands = cg.prepareStartCommands(node_map[Node::TSV]);
    StartSystemCommandResults results = execute(commands);
  }
  {
    StartSystemCommands commands = cg.prepareStartCommands(node_map[Node::EM]);
    StartSystemCommandResults results = execute(commands);
  }
  {
    StopSystemCommands commands = cg.prepareStopCommands(node_map[Node::TCPU]);
    StopSystemCommandResults results = execute(commands);
  }
  {
    StopSystemCommands commands = cg.prepareStopCommands(node_map[Node::HM]);
    StopSystemCommandResults results = execute(commands);
  }
  {
    StopSystemCommands commands = cg.prepareStopCommands(node_map[Node::TSV]);
    StopSystemCommandResults results = execute(commands);
  }
  {
    StopSystemCommands commands = cg.prepareStopCommands(node_map[Node::EM]);
    StopSystemCommandResults results = execute(commands);
  }
  {
    StateSystemCommands commands = cg.prepareStateCommands(node_map[Node::TCPU]);
    StateSystemCommandResults results = execute(commands);
  }
  {
    StateSystemCommands commands = cg.prepareStateCommands(node_map[Node::HM]);
    StateSystemCommandResults results = execute(commands);
  }
  {
    StateSystemCommands commands = cg.prepareStateCommands(node_map[Node::TSV]);
    StateSystemCommandResults results = execute(commands);
  }
  {
    StateSystemCommands commands = cg.prepareStateCommands(node_map[Node::EM]);
    StateSystemCommandResults results = execute(commands);
  }
  */
}
