#include "node.hpp"
#include "exception.hpp"

namespace tridas {
namespace tsc {

typedef boost::property_tree::ptree_error ConfigurationError;

Nodes read_tcpu_nodes(Configuration const& configuration)
{
  try {
    Nodes nodes;

    size_t starting_id = 0;
    Configuration const& hosts = configuration.get_child("TCPU.HOSTS");

    for (Configuration::const_iterator it = hosts.begin(), e = hosts.end();
         it != e; ++it) {

      std::string const hostname = it->second.get<std::string>("CTRL_HOST");
      size_t const n_instances = it->second.get<size_t>("N_INSTANCES");
      nodes.push_back(Node(Node::TCPU, hostname, n_instances, starting_id));

      starting_id += n_instances;
    }

    return nodes;
  } catch (ConfigurationError const& e) {
    throw Exception(std::string("configuration error (") + e.what() + ')');
  }
}

Nodes read_hm_nodes(Configuration const& configuration)
{
  try {
    Nodes nodes;

    size_t starting_id = 0;
    Configuration const& hosts = configuration.get_child("HM.HOSTS");

    for (Configuration::const_iterator it = hosts.begin(), e = hosts.end();
        it != e; ++it) {

      std::string const hostname = it->second.get<std::string>("CTRL_HOST");
      size_t const n_instances = it->second.get<size_t>("N_INSTANCES");

      nodes.push_back(Node(Node::HM, hostname, n_instances, starting_id));

      starting_id += n_instances;
    }

    return nodes;
  } catch (ConfigurationError const& e) {
    throw Exception(std::string("configuration error (") + e.what() + ')');
  }
}

Nodes read_em_nodes(Configuration const& configuration)
{
  try {
    Nodes nodes;

    std::string const hostname = configuration.get<std::string>("EM.CTRL_HOST");
    size_t const n_instances = 1;
    size_t const starting_id = 0;
    nodes.push_back(Node(Node::EM, hostname, n_instances, starting_id));

    return nodes;
  } catch (ConfigurationError const& e) {
    throw Exception(std::string("configuration error (") + e.what() + ')');
  }
}

Nodes read_tsv_nodes(Configuration const& configuration)
{
  try {
    Nodes nodes;

    std::string const hostname = configuration.get<std::string>("TSV.CTRL_HOST");
    size_t const n_instances = 1;
    size_t const starting_id = 0;
    nodes.push_back(Node(Node::TSV, hostname, n_instances, starting_id));

    return nodes;
  } catch (ConfigurationError const& e) {
    throw Exception(std::string("configuration error (") + e.what() + ')');
  }
}

}}
