#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "Log/log.hpp"
#include "context.hpp"
#include "state_machine.hpp"
#include "command_generator.hpp"
#include "ssh_command_executor.hpp"
#include "sshshell_command_executor.hpp"
#include "request.hpp"
#include "version.hpp"

/// @file

/// @namespace tridas::tsc
/// @brief All TSC entities are in the tsc namespace

using namespace tridas::tsc;
using boost::asio::local::stream_protocol;

namespace po = boost::program_options;
namespace fs = boost::filesystem;

void remove_endpoint(stream_protocol::endpoint const& endpoint)
{
  int const r = std::remove(endpoint.path().c_str());
  assert((r == 0 || (r == -1 && errno == ENOENT)) && "std::remove failed");
}

int main(int argc, char* argv[])
{
  std::ofstream dev_null("/dev/null");
  tridas::Log::init("RunTSC", tridas::Log::DEBUG/*, dev_null*/);
  tridas::Log::UseSyslog(/*0, LOG_LOCAL0*/);

  fs::path const home(std::getenv("HOME"));
  fs::path const default_ssh_dir(home/".ssh");
  fs::path ssh_dir;
  std::string const default_user(std::getenv("USER"));
  std::string user;

  po::options_description usage("usage:");
  usage.add_options()
      ("help,h", "show usage")
      ("version,v", "print TriDAS version")
      ("dry-run,n", "dry run")
      ("ssh-dir,s"
       , po::value<fs::path>(&ssh_dir)->default_value(default_ssh_dir)
       , "use an alternative ssh directory (must be an absolute path)"
      )
      ("user,u"
       , po::value<std::string>(&user)->default_value(default_user)
       , "use an alternative remote user"
      )
      ;

  try {
    po::variables_map options;
    po::store(po::parse_command_line(argc, argv, usage), options);
    po::notify(options);

    if (options.count("help")) {
      std::cout << usage << '\n';
      return EXIT_SUCCESS;
    }

    if (options.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }

    bool const dry_run = options.count("dry-run");

    boost::asio::io_service ios;

    CommandGenerator command_generator("tridas-run");
    SshShellCommandExecutor command_executor(ios, user, ssh_dir, dry_run);
    Context context(ios, command_generator, command_executor);
    StateMachine sm(context);

    RequestDispatcher dispatcher(sm);
    ResponseMaker response_maker;

    // TODO: the endpoint should go in /var/run, but this requires that some
    // init script creates a subdirectory in /var/run with the appropriate
    // permissions
    std::string const endpoint_name("/tmp/tsc.sock");
    stream_protocol::endpoint const endpoint(endpoint_name);
    remove_endpoint(endpoint);

    stream_protocol::acceptor acceptor(ios, endpoint);

    bool quit = false;
    while (!quit) {

      stream_protocol::iostream stream;
      acceptor.accept(*stream.rdbuf());  // a streambuffer *is-a* socket

      // show the current state machine state to a client that connects
      stream << make_response(response_maker, StateResponse(sm.info())) << '\n';

      std::string request_str;
      while (!quit && getline(stream, request_str)) {

        try {

          Request const request = parse_request(request_str);
          Response const response = dispatch_request(dispatcher, request);
          std::string const r = make_response(response_maker, response);
          stream << r << '\n';
          TRIDAS_LOG(INFO) << r;

        } catch (QuitRequest const& r) {
          quit = true;
          TRIDAS_LOG(INFO) << "quitting";
        } catch (Exception const& e) {
          std::string const r = make_response(e, sm.currentState());
          stream << r << '\n';
          TRIDAS_LOG(WARNING) << r;
        }
      }
    }

    remove_endpoint(endpoint);
  } catch (boost::program_options::error const& e) {
    std::cerr << e.what() << '\n' << usage << '\n';
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    TRIDAS_LOG(ERROR) << "exception: " << e.what();
    return EXIT_FAILURE;
  } catch (...) {
    TRIDAS_LOG(ERROR) << "Unknown exception. Quitting...";
    return EXIT_FAILURE;
  }
}
