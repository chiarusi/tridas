#include <fstream>
#include "update_node.hpp"
#include "Log/log.hpp"
#include <boost/detail/lightweight_test.hpp>

using namespace tridas::tsc;

void test_NodeState()
{
  BOOST_TEST_EQ(NodeState::parse("HM:1"), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse(" HM:1"), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse("HM:1 "), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse(" HM:1 "), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse("\nHM:1"), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse("HM:1\n"), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse(" HM:1\n"), NodeState(Node::HM, 1));
  BOOST_TEST_EQ(NodeState::parse("HM:-1"), NodeState(Node::HM, Node::INVALID_INSTANCES));
  BOOST_TEST_EQ(NodeState::parse("EM:"), NodeState(Node::EM, Node::INVALID_INSTANCES));
  BOOST_TEST_THROWS(NodeState::parse("PP:4"), NodeState::ParseError);
  BOOST_TEST_THROWS(NodeState::parse("TCPU:4/5"), NodeState::ParseError);
  BOOST_TEST_THROWS(NodeState::parse("HM:1/"), NodeState::ParseError);
  BOOST_TEST_THROWS(NodeState::parse("HM"), NodeState::ParseError);
  BOOST_TEST_THROWS(NodeState::parse("PP:4/5"), NodeState::ParseError);
}

Node make_node(Node::Role r, int n_instances)
{
  return Node(r, "host.example.com", n_instances, 0);
}

void test_update_nodes()
{
  {
    CommandResult cr(CommandResult::make_successful_result(0, "TCPU:4"));
    Node node(make_node(Node::TCPU, 5));
    BOOST_TEST(update_node(node, cr));
  }
  {
    CommandResult cr(CommandResult::make_successful_result(0, "TCPU:4"));
    Node node(make_node(Node::TCPU, 3));
    BOOST_TEST(!update_node(node, cr));
  }
  {
    CommandResults cr;
    Nodes nodes;
    BOOST_TEST(update_nodes(nodes, cr));
  }
  {
    CommandResults cr;
    cr.push_back(CommandResult::make_successful_result(0, "TCPU:4"));
    Nodes nodes;
    nodes.push_back(make_node(Node::TCPU, 5));
    BOOST_TEST(update_nodes(nodes, cr));
    BOOST_TEST_EQ(nodes.front().n_running_instances(), 4);
  }    
  {
    CommandResults cr;
    cr.push_back(CommandResult::make_successful_result(0, "EM:2"));
    cr.push_back(CommandResult::make_successful_result(0, "TCPU:4"));
    cr.push_back(CommandResult::make_internal_error_result(11, "blah blah"));
    Nodes nodes;
    nodes.push_back(make_node(Node::EM, 3));
    nodes.push_back(make_node(Node::TCPU, 3));
    nodes.push_back(make_node(Node::TCPU, 4));
    BOOST_TEST(!update_nodes(nodes, cr));
    BOOST_TEST_EQ(nodes[0].n_running_instances(), 2);
    BOOST_TEST_EQ(nodes[1].n_running_instances(), Node::INVALID_INSTANCES);
    BOOST_TEST_EQ(nodes[2].n_running_instances(), Node::INVALID_INSTANCES);
  }    

}

int main()
{
  std::ofstream dev_null("/dev/null");
  tridas::Log::init("tsc_tests", tridas::Log::DEBUG, dev_null);

  test_NodeState();
  test_update_nodes();

  return boost::report_errors();
}
