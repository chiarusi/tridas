
include_directories(
  inc
  ${PACKAGES_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${CMAKE_BINARY_DIR}
)

add_executable(
  RunTCPU
  src/RunTCPU.cpp
  src/STSReceiver.cpp
  src/TriggerInterface.cpp
  src/TTSAssembler.cpp
  src/TTSManager.cpp
  src/TTSBuild.cpp
  src/PMTHit.cpp
  src/Event_Classes.cpp
  src/Messages.cpp)

target_link_libraries(
  RunTCPU
  Packages
  ${CMAKE_DL_LIBS}
  ${Boost_LIBRARIES}
  ${ZMQ_LIBRARIES}
)

add_dependencies(RunTCPU version)

install(TARGETS RunTCPU RUNTIME DESTINATION bin)

add_subdirectory(trigger_plugins)
