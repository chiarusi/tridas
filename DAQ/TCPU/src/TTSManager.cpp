#include "TTSManager.h"

#include <boost/chrono/duration.hpp>
#include <boost/lexical_cast.hpp>

#include "TriggerInterface.h"
#include "log.hpp"

using namespace std;

namespace tridas {
namespace tcpu {

long const tsv_timeout_ms = 1;
long const infinite_timeout = -1;

TTSManager::TTSManager(
  SharedQueue<EventMessage>& MQ_TTS2EM,
  SharedQueue<EventCollector*>& MQ_TEFree,
  SharedQueue<TowerTimeSlice*>& MQ_TTSDone,
  Endpoint const& em_endpoint,
  MonitorParameters const& monitor_parameters)
    :
      MQ_TTS2EM_(MQ_TTS2EM),
      MQ_TEFree_(MQ_TEFree),
      MQ_TTSDone_(MQ_TTSDone),
      socket_(context_),
      mon_streamer_(monitor_parameters.hostname, monitor_parameters.port) {
  Plugrate_.reserve(MAX_PLUGINS_NUMBER);
  for (unsigned int i = 0; i < MAX_PLUGINS_NUMBER; ++i) {
    Plugrate_.push_back(
      trm::RunningAvgObservable(
        "Plugin_" + boost::lexical_cast < std::string > (i),
        "Hz",
        "Trigger rates for plugin #" + boost::lexical_cast < std::string > (i),
        10));
  }
  socket_.tcpConnect(em_endpoint.hostname, em_endpoint.port);
}

void TTSManager::operator()(
  TcpuId tcpu_id,
  std::string const& tsc_port,
  size_t tokens) {

  network::Context context;
  network::Producer socket(context);

  TRIDAS_LOG(DEBUG)
    << "Opening socket on port "
    << tsc_port
    << " for TSV communications";
  socket.tcpBind(tsc_port);

  std::vector<TSMessage> tokens_to_tsv;

  TSMessage ts_msg;
  ts_msg.timeslice_id = 0;
  ts_msg.tcpu_id = tcpu_id;
  for (size_t i = 0; i < tokens; ++i) {
    tokens_to_tsv.push_back(ts_msg);
  }

  EventMessage event_msg(0, TTSMessage(0));

  boost::chrono::system_clock::time_point start_time =
    boost::chrono::system_clock::now();

  size_t throughput = 0;

  while (true) {

    while (!tokens_to_tsv.empty()) {
      ts_msg = tokens_to_tsv.back();
      if (socket.timedSend(&ts_msg, sizeof(ts_msg), tsv_timeout_ms)) {
        TRIDAS_LOG(DEBUG)
          << "Done TS "
          << ts_msg.timeslice_id
          << " by TCPU "
          << ts_msg.tcpu_id
          << " - Sending token to TSV";
        tokens_to_tsv.pop_back();
      }
    }

    // Send event
    MQ_TTS2EM_.get(event_msg);

    TTSMessage const& c_tts_msg = event_msg.tts_msg;

    event_msg.tts_msg.startSend();
    // Only if there are triggered events by plugins, send them to the EM
    if (!event_msg.evc_pointer->plugin_events().empty()) {
      throughput += SendEvent(
        *event_msg.evc_pointer,
        *c_tts_msg.towerTimeSlice());
    }
    event_msg.tts_msg.stopSend();

    // Release event
    event_msg.evc_pointer->reset();
    MQ_TEFree_.put(event_msg.evc_pointer);

    // Release TTS
    MQ_TTSDone_.put(c_tts_msg.towerTimeSlice());

    // Add new token
    ts_msg.timeslice_id = c_tts_msg.timesliceId();
    tokens_to_tsv.push_back(ts_msg);

    // To report the unit of the following durations, a trick is used:
    // the count() member function is called on all but last of them.
    TRIDAS_LOG(INFO)
    << "TTS " << c_tts_msg.timesliceId()
    << " times " << c_tts_msg.l1ComputationTime().count()  //M1
    << ' ' << c_tts_msg.l2ComputationTime().count()  //M2
    << ' ' << c_tts_msg.computationTime().count()  //M3
    << ' ' << c_tts_msg.sendTime().count()  //M4
    << ' ' << c_tts_msg.lifeTime().count()  //M5
    << ' ' << c_tts_msg.buildingTime();  //M6

    boost::chrono::duration<double> duration_time =
      boost::chrono::system_clock::now() - start_time;

    if (duration_time.count() >= 2) {
      TRIDAS_LOG(INFO) << "TCPU to EM expected throughput: " << throughput / duration_time
        .count() / 131072.0 << " Mb/s";
      throughput = 0;
      start_time = boost::chrono::system_clock::now();
    }
  }

}

static std::size_t triggeredTimeSliceSize(std::vector<TriggeredEvent const*> trig_events) {
  std::size_t total = 0;
  for (size_t i = 0; i < trig_events.size(); ++i) {
    total += trig_events[i]->getEventSize();
  }
  return total;
}

static
void fillTeh(TriggeredEvent const& tev, TEHeaderInfo& TEH) {
  TEH.EventTag = 12081972;
  TEH.EventID = tev.EventID_;
  TEH.EventL = sizeof(TEH) + tev.getEventSize();
  TEH.nHit = tev.nHit_;
  TEH.StartTime5ns = reinterpret_cast<PMTHit*>(tev.L1_first_seed_)->getT5ns().count();
  std::copy(tev.nseeds_, tev.nseeds_ + MAX_TRIGGERS_NUMBER, TEH.nseeds);
  std::copy(tev.plugin_nseeds_, tev.plugin_nseeds_ + MAX_TRIGGERS_NUMBER, TEH.plugin_nseeds);
}

size_t TTSManager::SendEvent(EventCollector const& evc, TowerTimeSlice const& tts) {

  /*
   - create a TS header for the EM (where the TSID and the n. of Triggered Events are written)
   - loop over all TriggereEvents and
   - create TriggeredEvent header
   - copy the TriggeredEvent data

   TTSH : TTS_ID, WAGON_ID, NTE

   { TTSH [(TEH,TED) (TEH,TED) (TEH,TED) (TEH,TED) ...] }
   { TTSH [(TEH,TED) (TEH,TED) (TEH,TED) (TEH,TED) ...] }
   { TTSH [(TEH,TED) (TEH,TED) (TEH,TED) (TEH,TED) ...] }

   */

  std::vector<TriggeredEvent const*> plugin_events = evc.plugin_events();

  if (plugin_events.empty()) {
    TRIDAS_LOG(WARNING)
      << "TTSManager::SendEvent - Trying to send an empty event collector";
    return 0;
  }

  TRIDAS_LOG(DEBUG)
    << "TTSManager::SendEvent - Events/TS: "
    << plugin_events.size();

  size_t sent_bytes = 0;

  TimeSliceHeader WHI;
  WHI.TS_ID = tts.GetTSID();
  WHI.NEvents = plugin_events.size();
  WHI.TS_size = sizeof(WHI) + triggeredTimeSliceSize(plugin_events) + WHI
    .NEvents * sizeof(TEHeaderInfo);

  size_t bytes = socket_.timedSendMore(&WHI, sizeof(WHI), infinite_timeout);
  if (bytes != sizeof(WHI)) {
    TRIDAS_LOG(WARNING)
      << "Sent "
      << bytes
      << " instead of "
      << sizeof(WHI)
      << " bytes to EM";
  }
  sent_bytes += bytes;

  for (unsigned int i = 0; i < plugin_events.size(); i++) {
    TriggeredEvent const& tev = *plugin_events[i];

    TEHeaderInfo TEH;
    TEH.TSCompleted = tts.completed();
    fillTeh(tev, TEH);
    bytes = socket_.timedSendMore(
      static_cast<char const*>(static_cast<void const*>(&TEH)),
      sizeof(TEH),
      infinite_timeout);

    if (bytes != sizeof(TEH)) {
      TRIDAS_LOG(WARNING)
        << "Sent "
        << bytes
        << " instead of "
        << sizeof(TEH)
        << " bytes to EM";
    }
    sent_bytes += bytes;

    // The following operation can be improved a lot by modifying the
    // public interface of TriggeredEvent

    std::size_t const data_size = TEH.EventL - sizeof(TEH);
    std::vector<char> ev_data(data_size, 0);

    tev.CopyToBuffer(&ev_data.front(), data_size);
    if (i != plugin_events.size() - 1) {
      bytes = socket_.timedSendMore(
        &ev_data.front(),
        data_size,
        infinite_timeout);
    } else {
      bytes = socket_.timedSend(&ev_data.front(), data_size, infinite_timeout);
    }
    if (bytes != data_size) {
      TRIDAS_LOG(WARNING)
        << "Sent "
        << bytes
        << " instead of "
        << data_size
        << " bytes to EM";
    }
    sent_bytes += bytes;
  }

  return sent_bytes;
}

// Unused method!
void TTSManager::SendPluginRateToMonitor(EventCollector const* const evc) {
  double const TTSduration_sec = evc->tts_duration();
  for (std::size_t i = 0; i < MAX_PLUGINS_NUMBER; ++i) {
    Plugrate_[i].put(evc->stats_for_plugin(i) / TTSduration_sec);
    if (!Plugrate_[i].count() % 10) {
      mon_streamer_ << Plugrate_[i];
    }
  }
}

}
}
