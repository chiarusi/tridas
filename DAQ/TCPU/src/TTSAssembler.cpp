#include "TTSAssembler.h"

#include <map>

#include <cassert>

#include <boost/bind.hpp>

#include "log.hpp"

using namespace std;

namespace tridas {
namespace tcpu {

TTSAssembler::TTSAssembler(
  size_t n_sectors,
  boost::chrono::seconds const& tts_ready_timeout)
  :
  n_sectors_(n_sectors),
  tts_ready_timeout_(tts_ready_timeout),
  last_completed_ts_(0)
{
}

bool TTSAssembler::addSts(STSMessage const& sts_msg)
{

  TowerTimeSlice* tts;
  std::map<TS_t, TTSMessage>::iterator const tts_map_it = tts_map_.find(
      sts_msg.timeslice_id);

  // Check if the ts isn't already in the map
  if (tts_map_it == tts_map_.end()) {
    assert(sts_msg.timeslice_id != 0 && "TS id can not be zero!");

    // Check if the TS id is too old
    if (sts_msg.timeslice_id <= last_completed_ts_) {
      TRIDAS_LOG(WARNING)
        << "Discarding sector "
        << sts_msg.pointer->GetSectorID()
        << " of TS "
        << sts_msg.timeslice_id
        << " : it's too old!";
      return false;
    }

    // Get free tts pointer or allocate memory
    if (mq_tts_free_.empty()) {
      tts = new TowerTimeSlice(n_sectors_);
    } else {
      tts = mq_tts_free_.back();
      mq_tts_free_.pop_back();
    }

    // Add the tts to the map
    tts_map_.insert(tts_map_it, std::make_pair(sts_msg.timeslice_id, TTSMessage(tts)));
    tts->SetTSID(sts_msg.timeslice_id);
    ts_map_[boost::chrono::system_clock::now()] = sts_msg.timeslice_id;
  } else {

    // The ts is already in the map
    tts = tts_map_it->second.towerTimeSlice();
  }

  // Update the tts
  tts->AddSTS(sts_msg.pointer);

  // If the ts tts is completed remove it from the map
  if (tts->completed()) {
    TsMap::iterator it = std::find_if(
      ts_map_.begin(),
      ts_map_.end(),
      boost::bind(&TsMap::value_type::second, _1) == tts->GetTSID());
    assert(it != ts_map_.end() && "TS id not found");

    std::map<TS_t, TTSMessage>::iterator const completed_tts =
        tts_map_.find(it->second);

    assert(completed_tts != tts_map_.end() && "TS id not found.");

    completed_tts->second.stopReceive();

    ready_tts_.push_back(completed_tts->second);

    // Update last_completed_ts_ (if younger)
    if (it->second > last_completed_ts_) {
      last_completed_ts_ = it->second;
    }
    //free the map entry
    tts_map_.erase(it->second);
    ts_map_.erase(it);
  }

  return true;
}

void TTSAssembler::addEmptyTts(TowerTimeSlice* tts)
{
  assert(tts);
  mq_tts_free_.push_back(tts);
}

std::vector<TTSMessage> TTSAssembler::getReadyTts()
{

  boost::chrono::system_clock::time_point
  const ready_time(
    boost::chrono::system_clock::now() - boost::chrono::system_clock::duration(
      tts_ready_timeout_));

  // Get already completed tts from ready_tts_ and clear it
  std::vector<TTSMessage> ready_tts;
  ready_tts_.swap(ready_tts);

  // Add uncompleted and timed out timeslices
  TsMap::iterator const it = ts_map_.lower_bound(ready_time);
  int const tts_to_remove = std::distance(ts_map_.begin(), it);
  for (int i = 0; i != tts_to_remove; ++i) {
    TS_t const timeslice_id = ts_map_.begin()->second;
    std::map<TS_t, TTSMessage>::iterator const item = tts_map_.find(
        timeslice_id);

    item->second.stopReceive();
    ready_tts.push_back(item->second);
    tts_map_.erase(item);

    ts_map_.erase(ts_map_.begin());
    if (timeslice_id > last_completed_ts_) {
      last_completed_ts_ = timeslice_id;
    }
  }

  return ready_tts;
}

}
}
