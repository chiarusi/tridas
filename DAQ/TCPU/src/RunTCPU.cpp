#include <string>
#include <vector>
#include <csignal>
#include <cstdlib>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "log.hpp"
#include "Configurator.h"
#include "Configuration.h"
#include "Endpoint.h"
#include "HMParameters.h"
#include "TCPUParameters.h"
#include "MonitorParameters.h"
#include "PluginParameters.h"
#include "TriggerInterface.h"
#include "STSReceiver.h"
#include "TTSManager.h"
#include "SharedQueue.h"

#include "monitoring.hpp"
#include "version.hpp"

using namespace tridas;

namespace trm = tridas::monitoring;
using namespace tridas::tcpu;

int main(int argc, char* argv[]) {
  tridas::Log::init("RunTCPU");

  boost::filesystem::path cardfile;
  size_t tcpu_id;

  boost::program_options::options_description desc("Options");

  desc.add_options()("help,h", "Print help messages.")(
    "version,v",
    "print TriDAS version")(
    "id,i",
    boost::program_options::value<size_t>(&tcpu_id)->required(),
    "TCPU id.")(
    "datacard,d",
    boost::program_options::value<boost::filesystem::path>(&cardfile)->required(),
    "Datacard file.");

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv).options(desc).run(),
      vm);
    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    if (vm.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }
    boost::program_options::notify(vm);


    if (!boost::filesystem::exists(cardfile)) {
      std::cerr << cardfile.string() << " does not exist\n";
      return EXIT_FAILURE;
    } else if (!boost::filesystem::is_regular_file(cardfile)) {
      std::cerr << cardfile.string() << " is not a regular file\n";
      return EXIT_FAILURE;
    }

    Configuration const configuration = read_configuration(cardfile);
    Geometry const geometry = get_geometry_parameters(configuration);
    HMParameters const hm_parameters = get_hm_parameters(configuration);
    TCPUParameters const tcpu_parameters = get_tcpu_parameters(configuration);
    StructParameters const struct_parameters = get_struct_parameters(
      configuration);
    TriggerParameters const trigger_parameters = get_trigger_parameters(
      configuration);
    MonitorParameters const monitor_parameters = get_monitor_parameters(
      configuration);

    // Configure log
    tridas::Log::init(
        "RunTCPU",
        tridas::Log::FromString(
            configuration.get<std::string>("TCPU.LOG_LEVEL")));
    if (configuration.get<bool>("TCPU.LOG_TO_SYSLOG")) {
      tridas::Log::UseSyslog();
    }

    // Check plugins_directory parameter
    if (!boost::filesystem::exists(tcpu_parameters.plugins_directory)) {
      TRIDAS_LOG(ERROR)
        << "Wrong plugins directory: "
        << tcpu_parameters.plugins_directory
        << " does not exist";
      return EXIT_FAILURE;
    } else if (!boost::filesystem::is_directory(
        tcpu_parameters.plugins_directory)) {
      TRIDAS_LOG(ERROR)
        << "Wrong plugins directory: "
        << tcpu_parameters.plugins_directory
        << " is not a directory";
      return EXIT_FAILURE;
    }

    size_t const n_total_floors = geometry.floors_per_tower * geometry.towers;
    size_t const floors_per_sector = n_total_floors / hm_parameters.count;
    size_t const pmts_per_sector = floors_per_sector * geometry.pmts_per_floor;

    // Check if number of sectors are correct
    if (n_total_floors % hm_parameters.count) {
      TRIDAS_LOG(ERROR)
        << "Wrong total number of floors: "
        << n_total_floors
        << " is not a multiple of "
        << hm_parameters.count;
      return EXIT_FAILURE;
    }

    std::string const tcpu_tag(
      "TCPU_" + boost::lexical_cast<std::string>(tcpu_id));
    SharedQueue<TTSMessage> MQ_TTSReady;
    SharedQueue<EventMessage> MQ_TTS2EM;
    SharedQueue<EventCollector*> MQ_TEFree;
    SharedQueue<TowerTimeSlice*> MQ_TTSDone;

    sigset_t set;
    sigfillset(&set);  // mask all signals
    pthread_sigmask(SIG_SETMASK, &set, NULL);  // set mask

    /******************************/

    TRIDAS_LOG(DEBUG) << "RunTCPU::main - Starting NET_th thread";
    STSReceiver STSrx(
      MQ_TTSDone,
      MQ_TTSReady,
      hm_parameters.count,
      pmts_per_sector,
      struct_parameters.pmt_buffer_size,
      boost::chrono::seconds(struct_parameters.tts_ready_timeout));
    std::vector<Endpoint> const tcpu_endpoints = get_tcpu_data_endpoints(
      configuration);

    // Check if tcpu id is correct
    if (tcpu_id >= tcpu_endpoints.size()) {
      TRIDAS_LOG(ERROR)
        << "TCPU ID out of range: "
        << tcpu_id
        << " [0, "
        << tcpu_endpoints.size()
        << ")";
      return EXIT_FAILURE;
    }

    std::string const data_port = tcpu_endpoints[tcpu_id].port;
    boost::thread NET_th_id(STSrx, data_port);

    /******************************/

    TRIDAS_LOG(DEBUG) << "RunTCPU::main - Starting TRIGGER_th thread";

    TriggerInterface TrigUI(
      MQ_TTSReady,
      MQ_TTS2EM,
      MQ_TEFree,
      geometry,
      trigger_parameters,
      tcpu_parameters.plugins_directory,
      get_plugins_parameters(configuration),
      monitor_parameters);

    boost::thread_group TRIGGER_th_id;
    for (size_t i = 0; i < tcpu_parameters.parallel_tts; ++i) {
      TRIGGER_th_id.add_thread(new boost::thread(TrigUI));
    }

    /******************************/

    // tts_ready_timeout is in seconds while delta_timeslice is in milliseconds
    size_t const tokens =
      struct_parameters.tts_ready_timeout * 1000 / struct_parameters
        .delta_timeslice.count();
    TRIDAS_LOG(INFO) << "Using " << tokens << " tokens";

    Endpoint const em_endpoint(
      configuration.get<std::string>("EM.DATA_HOST"),
      configuration.get<std::string>("EM.DATA_PORT"));

    TRIDAS_LOG(DEBUG) << "RunTCPU::main - Starting EM_th thread";
    TTSManager TTSM(
      MQ_TTS2EM,
      MQ_TEFree,
      MQ_TTSDone,
      em_endpoint,
      monitor_parameters);
    boost::thread EM_th_id(
      boost::ref(TTSM),
      tcpu_id,
      get_tcpu_control_endpoints(configuration)[tcpu_id].port,
      tokens
      );

    /******************************/

    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGTERM);
    //sigdelset(&set, SIGCHLD);
    int sig_caught;
    sigwait(&set, &sig_caught);

    TRIDAS_LOG(WARNING) << "RunTCPU: Received signal " << sig_caught;

    // Send interrupt to all threads...
    NET_th_id.interrupt();
    TRIGGER_th_id.interrupt_all();
    EM_th_id.interrupt();

    // Join all threads...

    NET_th_id.detach();

    EM_th_id.detach();
  } catch (const boost::program_options::error& e) {
    std::cerr << e.what() << '\n' << desc << std::endl;
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    TRIDAS_LOG(ERROR) << e.what();
    return EXIT_FAILURE;
  } catch (...) {
    TRIDAS_LOG(ERROR) << "Unknown exception. Quitting...";
    return EXIT_FAILURE;
  }
}
