//
// Created by favaro on 11/06/15.
//

#include "Messages.h"

#include <boost/chrono.hpp>

namespace tridas {
namespace tcpu {

const static boost::chrono::system_clock::time_point zero_timestamp;

TTSMessage::TTSMessage(TowerTimeSlice* tts)
  :
  tts_(tts),
  m_construction(boost::chrono::system_clock::now())
{
}

void TTSMessage::stopReceive()
{
  assert(m_complete_receive == zero_timestamp && "stopReceive called twice");
  m_complete_receive = boost::chrono::system_clock::now();
}

void TTSMessage::startCompute()
{
  assert(m_start_computation == zero_timestamp && "startCompute called twice");
  assert(m_complete_receive != zero_timestamp && "stopReceive not called yet");
  m_start_computation = boost::chrono::system_clock::now();
}

void TTSMessage::startL2Compute()
{
  assert(m_start_l2_computation == zero_timestamp && "startL2Compute called twice");
  assert(m_start_computation != zero_timestamp && "startCompute not called yet");
  m_start_l2_computation = boost::chrono::system_clock::now();
}

void TTSMessage::stopCompute()
{
  assert(m_stop_computation == zero_timestamp && "stopCompute called twice");
  assert(m_start_l2_computation != zero_timestamp && "startL2Compute not called yet");
  m_stop_computation = boost::chrono::system_clock::now();
}

void TTSMessage::startSend()
{
  assert(m_start_send == zero_timestamp && "startSend called twice");
  assert(m_stop_computation != zero_timestamp && "stopCompute not called yet");
  m_start_send = boost::chrono::system_clock::now();
}

void TTSMessage::stopSend()
{
  assert(m_complete == zero_timestamp && "stopSend called twice");
  assert(m_start_send != zero_timestamp && "startSend not called yet");

  m_complete = boost::chrono::system_clock::now();
}

TTSMessage::duration TTSMessage::l1ComputationTime() const
{
  assert(m_start_l2_computation != zero_timestamp && "startL2Compute never called");
  return m_start_l2_computation - m_start_computation;
}

TTSMessage::duration TTSMessage::l2ComputationTime() const
{
  assert(m_stop_computation != zero_timestamp && "stopCompute never called");
  return m_stop_computation - m_start_l2_computation;
}

TTSMessage::duration TTSMessage::computationTime() const
{
  assert(m_stop_computation != zero_timestamp && "stopCompute never called");
  return m_stop_computation - m_start_computation;
}

TTSMessage::duration TTSMessage::sendTime() const
{
  assert(m_complete != zero_timestamp && "stopSend never called");
  return m_complete - m_start_send;
}

TTSMessage::duration TTSMessage::lifeTime() const
{
  assert(m_complete != zero_timestamp && "stopSend never called");
  return m_complete - m_construction;
}

TTSMessage::duration TTSMessage::buildingTime() const
{
  assert(m_complete_receive != zero_timestamp && "stopReceive never called");
  return m_complete_receive - m_construction;
}

} // ns tcpu
} // ns tridas
