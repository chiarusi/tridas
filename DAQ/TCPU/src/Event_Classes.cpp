#include <cstring>

#include "Event_Classes.h"
#include "log.hpp"

namespace tridas {
namespace tcpu {

TriggeredEvent::TriggeredEvent(unsigned int nTotPMT)
    :
      nTotPMT_(nTotPMT) {

  Init();
  Reset();

}

TriggeredEvent::~TriggeredEvent() {
  delete[] p_start_;
  delete[] p_end_;
}

void TriggeredEvent::Init() {
  p_start_ = new char*[nTotPMT_];
  p_end_ = new char*[nTotPMT_];
}

bool TriggeredEvent::CheckReset() {
  for (size_t i = 0; i < nTotPMT_; ++i) {
    if (p_start_[i] || p_end_[i])
      return false;
  }
  return true;
}

void TriggeredEvent::Reset() {
  for (size_t i = 0; i < nTotPMT_; ++i) {
    p_start_[i] = 0;
    p_end_[i] = 0;
  }

  plugin_ok_ = false;

  for (size_t i = 0; i < MAX_TRIGGERS_NUMBER; ++i) {
    nseeds_[i] = 0;
  }

  for (size_t i = 0; i < MAX_PLUGINS_NUMBER; ++i) {
    plugin_nseeds_[i] = 0;
  }

  nHit_ = 0;
  L1_first_seed_ = 0;
  sw_hit_ = 0;
  ew_hit_ = 0;

  event_size_ = 0;
  EventID_ = 0;
}

size_t TriggeredEvent::ComputeDataSize() {
  event_size_ = 0;
  for (size_t i = 0; i < nTotPMT_; ++i) {
    if (p_start_[i]) {
      event_size_ += static_cast<size_t>(p_end_[i] - p_start_[i]);
    }
  }
  return event_size_;
}

unsigned int TriggeredEvent::CheckPMTHitConsistency() {
  /*
   Re-parse all the hits and compare the found hit numbers to nHit_.

   Return value: 0 if Consistency; the counted n. hits if not

   */

  unsigned int total_counted_nhit = 0;

  for (size_t i = 0; i < nTotPMT_; ++i) {
    if (p_start_[i]) {
      total_counted_nhit += CountHitInPMT(i);
    }
  }
  if (total_counted_nhit == nHit_) {
    return 0;
  }

  return total_counted_nhit;
}

unsigned int TriggeredEvent::CountHitInPMT(int i) {

  unsigned int nhit = 0;

  char* pblock = p_start_[i];
  const size_t loadsize = (size_t) p_end_[i] - (size_t) p_start_[i];
  size_t readsize = 0;

  while (readsize < loadsize) {
    DataFrameHeader* dfh = dataframeheader_cast(pblock);
    if (!dfh->FragFlag) {
      nhit++;
    }
    const int dataframesize = sizeof(DataFrameHeader) + getDFHPayloadSize(*dfh);
    readsize += dataframesize;
    pblock += dataframesize;
  }
  return nhit;
}

size_t TriggeredEvent::CopyToBuffer(char* dest, std::size_t maxsize) const {

  if (event_size_ > maxsize) {
    TRIDAS_LOG(WARNING)
      << "TriggeredEvent::CopyToBuffer - Warning event_size_ "
      << event_size_
      << " exceeded the max allowed size of "
      << maxsize
      << " EVENT NOT COPIED";
    return 0;
  } else {
    for (size_t i = 0; i < nTotPMT_; i++) {
      if (p_start_[i]) {
        const size_t data_size = ((size_t) p_end_[i] - (size_t) p_start_[i]);
        memcpy(dest, p_start_[i], data_size);
        dest += data_size;
      }
    }
  }
  return event_size_;
}

EventCollector::EventCollector(unsigned int tot_pmts)
    :
      ts_id_(0),
      tts_duration_sec_(0),
      filled_trig_events_(0),
      plug_events_(MAX_PLUGINS_NUMBER, 0),
      tot_pmts_(tot_pmts) {
}

EventCollector::~EventCollector() {
  for (size_t i = 0; i < trig_events_.size(); ++i) {
    delete trig_events_[i];
  }
}

TriggeredEvent* EventCollector::alloc_trig_event() {
  assert(static_cast<size_t>(filled_trig_events_) <= trig_events_.size());
  if(static_cast<size_t>(filled_trig_events_) == trig_events_.size()){
    trig_events_.push_back(new TriggeredEvent(tot_pmts_));
  }
  return trig_events_[filled_trig_events_++];
}

void EventCollector::reset() {
  ts_id_ = 0;
  tts_duration_sec_ = 0;
  for (size_t i = 0; i < trig_events_.size(); ++i) {
    trig_events_[i]->Reset();
  }
  for (size_t i = 0; i < plug_events_.size(); ++i) {
    plug_events_[i] = 0;
  }
  filled_trig_events_ = 0;
}

std::vector<TriggeredEvent const*> EventCollector::plugin_events() const{
  std::vector<TriggeredEvent const*> plugin_events;
  for (int i = 0; i < filled_trig_events_; ++i) {
    TriggeredEvent const* tev = trig_events_[i];
    if (tev->plugin_ok_) {
      plugin_events.push_back(tev);
    }
  }
  return plugin_events;
}

}
}
