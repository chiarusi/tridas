#include "STSReceiver.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include "tridas_dataformat.hpp"
#include "log.hpp"


using namespace std;

namespace tridas {
namespace tcpu {

static long const timeout_ms = 100;

STSReceiver::STSReceiver(
  SharedQueue<TowerTimeSlice*>& mq_tts_done,
  SharedQueue<TTSMessage>& mq_tts_ready,
  size_t n_sectors,
  size_t pmts_per_sector,
  size_t pmt_ts_size,
  boost::chrono::seconds const& tts_ready_timeout)
    :
      mq_tts_done_(mq_tts_done),
      mq_tts_ready_(mq_tts_ready),
      pmts_per_sector_(pmts_per_sector),
      pmt_ts_size_(pmt_ts_size),
      tts_assembler_(n_sectors, tts_ready_timeout) {
}

bool STSReceiver::GetSTS(SectorTimeSlice* sts, network::ConsumerPtr socket) {

  s_HeaderInfoSTS HeaderInfoSTS;
  size_t received = socket->timedRecv(
    reinterpret_cast<char*>(&HeaderInfoSTS),
    sizeof(HeaderInfoSTS),
    timeout_ms);

  if (!received) {
    // timed out
    return false;
  }

  if (received != sizeof(HeaderInfoSTS)) {
    TRIDAS_LOG(ERROR)
      << "Received "
      << received
      << " bytes instead of "
      << sizeof(HeaderInfoSTS);
    return false;
  }

  sts->SetHeader(HeaderInfoSTS);

  // Receive buffers
  for (size_t i = 0; i < HeaderInfoSTS.pmtin_; ++i) {
    if (HeaderInfoSTS.pmtlist_[i]) {
      size_t const size = HeaderInfoSTS.BufSize_[i];
      SimpleBuffer* db = sts->GetBuffer(i);
      char* data = db->GetStartWrite();
      received = socket->timedRecv(data, size, timeout_ms);
      if (received != size) {
        TRIDAS_LOG(ERROR)
          << "Received "
          << received
          << " bytes instead of "
          << size;
        return false;
      }
      db->PutStartWrite(data + size);
    }
  }
  return true;
}

void STSReceiver::operator()(std::string const& port) {
  network::Context context;
  network::ConsumerPtr socket(new network::Consumer(context));
  TRIDAS_LOG(DEBUG) << "Open socket on port " << port << " for HM communications";
  socket->tcpBind(port);

  while (true) {

    SectorTimeSlice* sts;
    if (mq_sts_free_.empty()) {
      sts = new SectorTimeSlice(pmts_per_sector_, pmt_ts_size_);
    } else {
      sts = mq_sts_free_.back();
      mq_sts_free_.pop_back();
    }

    if (GetSTS(sts, socket)) {
      STSMessage msg;
      msg.pointer = sts;
      msg.timeslice_id = sts->GetTSID();
      if (!tts_assembler_.addSts(msg)) {
        sts->Reset();
        mq_sts_free_.push_back(sts);
      }
    } else {
      sts->Reset();
      mq_sts_free_.push_back(sts);
    }

    std::vector<TTSMessage> ready_tts = tts_assembler_.getReadyTts();
    while (!ready_tts.empty()) {
      // Add tts to the tts ready queue
      mq_tts_ready_.put(ready_tts.back());
      ready_tts.pop_back();
    }

    TowerTimeSlice* tts;
    while (mq_tts_done_.get_no_wait(tts)) {
      size_t const sts_in = tts->GetNSTSin();
      for (size_t i = 0; i < sts_in; ++i) {
        SectorTimeSlice* const sts = tts->GetSTS(i);
        if (sts) {
          sts->Reset();
          mq_sts_free_.push_back(sts);
        }
      }
      tts->Reset();
      tts_assembler_.addEmptyTts(tts);
    }

    //TRIDAS_LOG(DEBUG) << "Pending TTS: " << tts_assembler_.pendingTts();
  }
}

}
}
