#include <utility>
#include <cassert>
#include <iostream>

#include <boost/lexical_cast.hpp>

#include "PMTHit.h"
#include "exception.hpp"

using namespace std;

namespace tridas {
namespace tcpu {

const size_t floor_offset = 1;

std::pair<size_t, size_t> parseSamples(
    char const* start_hit,
    std::size_t max_size) {
  std::size_t length = 0;
  std::size_t samples_count = 0;

  while (length < max_size) {
    DataFrameHeader const* const dfh = dataframeheader_cast(start_hit + length);

    if (dfh->FragFlag == 0 && length) {
      break;
    }

    length += sizeof(*dfh) + getDFHPayloadSize(*dfh);
    samples_count += getDFHNSamples(*dfh);
  }

  return std::make_pair(length, samples_count);
}

PMTHit::PMTHit(char* start, std::size_t max_size, Geometry const& geometry)
    : pmt_id_(0),
      start_hit_(start),
      seed_map_(0),
      length_(0),
      previous_(0),
      next_(0) {

  DataFrameHeader const& dfh = *dataframeheader_cast(start_hit_);

  // Check geometry read from dataframe header
  if (dfh.PMTID >= geometry.pmts_per_floor) {
    throw Exception(
        "Wrong PMTID " + boost::lexical_cast<std::string>(dfh.PMTID)
            + " (there are "
            + boost::lexical_cast<std::string>(geometry.pmts_per_floor)
            + " pmts per floor)");
  }
  if (dfh.EFCMID < floor_offset) {
    throw Exception(
        "Wrong EFCMID " + boost::lexical_cast<std::string>(dfh.EFCMID)
            + " (floor offset is "
            + boost::lexical_cast<std::string>(floor_offset) + ")");
  }
  if (dfh.EFCMID - floor_offset >= geometry.floors_per_tower) {
    throw Exception(
        "Wrong EFCMID " + boost::lexical_cast<std::string>(dfh.EFCMID)
            + " (there are "
            + boost::lexical_cast<std::string>(geometry.floors_per_tower)
            + " floors per tower)");
  }
  if (dfh.TowerID >= geometry.towers) {
    throw Exception(
        "Wrong TowerID " + boost::lexical_cast<std::string>(dfh.TowerID)
            + " (there are " + boost::lexical_cast<std::string>(geometry.towers)
            + " towers)");
  }

  // sum pmts of completed towers
  pmt_id_ += static_cast<size_t>(dfh.TowerID) * geometry.pmts_per_floor
      * geometry.floors_per_tower;
  // sum pmts of completed floors
  pmt_id_ += static_cast<size_t>(dfh.EFCMID - floor_offset)
      * geometry.pmts_per_floor;
  // sum remaining pmts
  pmt_id_ += static_cast<size_t>(dfh.PMTID);

  // Compute time and do the time calibration
  T5ns_ = getDFHFullTime(dfh)
      + fine_time(geometry.calibrations[pmt_id_].time_offset);

  // Do the charge calibration
  std::pair<size_t, size_t> p = parseSamples(start_hit_, max_size);
  length_ = p.first;
  size_t validRawCharge = getDFHCharge(dfh);
  CaliCharge_ = (float) validRawCharge
      - geometry.calibrations[pmt_id_].pedestal
          * ((p.second > 10) ? 10 : p.second);
  has_minimum_bias_ = CaliCharge_ > geometry.calibrations[pmt_id_].threshold;
}

size_t PMTHit::GetFrames() const {
  size_t parsed_size = 0;
  size_t n_frames = 0;
  DataFrameHeader* dfh = dataframeheader_cast(start_hit_);
  do {
    parsed_size += sizeof(DataFrameHeader) + getDFHPayloadSize(*dfh);
    dfh = dataframeheader_cast(start_hit_ + parsed_size);
    ++n_frames;
  } while (dfh->FragFlag);
  return n_frames;
}

fine_time PMTHit::TimeDifference5ns(PMTHit* hit2) {
  return (hit2->getT5ns() - T5ns_);
}

ostream& operator<<(ostream& os, PMTHit const& pmt_hit) {
  os
    << "PMTHit info:"
    << "\nThis:\t"
    << &pmt_hit
    << "\nStartHit:\t"
    << static_cast<void const*>(pmt_hit.getRawDataStart())
    << "\nEndHit:\t"
    << static_cast<void const*>(pmt_hit.getRawDataEnd())
    << "\nPrevious:\t"
    << pmt_hit.previous()
    << "\nNext:\t"
    << pmt_hit.next()
    << "\n"
    << *dataframeheader_cast(pmt_hit.getRawDataStart());
  return os;
}

}
}
