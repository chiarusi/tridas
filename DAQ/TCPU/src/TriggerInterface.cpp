#include <dlfcn.h>
#include <cassert>

#include <boost/chrono/duration.hpp>

#include "TriggerInterface.h"
#include "log.hpp"

namespace tridas {
namespace tcpu {

TriggerInterface::TriggerInterface(
  SharedQueue<TTSMessage>& MQ_TTSReady,
  SharedQueue<EventMessage>& MQ_TTS2EM,
  SharedQueue<EventCollector*>& MQ_TEFree,
  Geometry const& geometry,
  TriggerParameters const& trigger_parameters,
  boost::filesystem::path const& plugins_dir,
  std::vector<PluginParameters> const& plugins,
  MonitorParameters const& monitor_parameters)
    :
      MQ_TTSReady_(MQ_TTSReady),
      MQ_TTS2EM_(MQ_TTS2EM),
      MQ_TEFree_(MQ_TEFree),
      geometry_(geometry),
      trigger_parameters_(trigger_parameters),
      monitoring_server_(
        trm::make_destination(
          monitor_parameters.hostname,
          monitor_parameters.port)),
      monitor_time_interval_(monitor_parameters.time_interval) {

  // For each plugin
  for (size_t i = 0; i < plugins.size(); ++i) {
    PluginParameters const& p = plugins[i];
    boost::filesystem::path plugin_lib("lib" + p.name + ".so");
    boost::filesystem::path plugin_path(
        plugins_dir.string() + "/" + plugin_lib.string());

    // Check plugin file
    if (!boost::filesystem::exists(plugin_path)) {
      TRIDAS_LOG(ERROR) << "Plugin " << plugin_path << " does not exist";
      continue;
    } else if (!boost::filesystem::is_regular_file(plugin_path)) {
      TRIDAS_LOG(ERROR) << "Plugin " << plugin_path << " is not a regular file";
      continue;
    }

    // Open lib
    boost::shared_ptr<void> module(
      dlopen(plugin_path.c_str(), RTLD_LAZY),
      dlclose);
    if (!module.get()) {
      TRIDAS_LOG(ERROR)
        << "Error opening library "
        << plugin_path
        << " with error: "
        << dlerror();
      continue;
    }
    modules_vector_.push_back(module);

    // Load module
    TriggerPlugin tp = reinterpret_cast<TriggerPlugin>(dlsym(
      module.get(),
      p.name.c_str()));
    if (!tp) {
      TRIDAS_LOG(ERROR)
        << "Error dlsym-ing function "
        << p.name
        << " from "
        << plugin_path
        << " with error: "
        << dlerror();
      continue;
    }

    plugins_vector_.push_back(std::make_pair(tp, p));
    TRIDAS_LOG(INFO) << "Plugin " << p.label << " with id " << p.id;
  }
}

void TriggerInterface::operator()() {

  TTSBuild tts_build(geometry_, trigger_parameters_);

  std::vector<trm::SimpleObservable> l1_events;

  // Number of events
  l1_events.push_back(
    trm::SimpleObservable("L1EVNo", "double", "Number of events"));

  // Events rate
  l1_events.push_back(
    trm::SimpleObservable("L1EVrate", "double", "Events rate"));

  std::vector<trm::SimpleObservable> l1_rates;

  // L1TOTAL_ID
  l1_rates.push_back(
    trm::SimpleObservable("L1TrigRate", "double", "Rate of all triggers"));

  // L1Q_ID
  l1_rates.push_back(
    trm::SimpleObservable("L1QRate", "double", "Rate of charge trigger"));

  // L1SC_ID
  l1_rates.push_back(
    trm::SimpleObservable("L1SCRate", "double", "Rate of simple coincidences"));

  // L1FC_ID
  l1_rates.push_back(
    trm::SimpleObservable("L1FCRate", "double", "Rate of floor coincidences"));

  // L1RT_ID
  l1_rates.push_back(
    trm::SimpleObservable("L1RTRate", "double", "Rate of random trigger"));

  std::vector<double> v_events(l1_events.size(), 0.0);
  std::vector<double> v_rates(l1_rates.size(), 0.0);

  boost::chrono::system_clock::time_point start_time =
    boost::chrono::system_clock::now();

  double tts_counter = 0;

  size_t pmts_total =
    geometry_.pmts_per_floor * geometry_.floors_per_tower * geometry_.towers;
  TTSMessage msg_tts(0);

  while (true) {

    MQ_TTSReady_.get(msg_tts);

    tts_build.SetTTS(msg_tts.towerTimeSlice());

    EventCollector* evc = 0;
    if (!MQ_TEFree_.get_no_wait(evc)) {
      evc = new EventCollector(pmts_total);
    }

    msg_tts.startCompute();

    // Run L1 triggers
    tts_build.HitBuildL1(evc);
    msg_tts.startL2Compute();

    // Run plugins
    PluginArgs plugin_args;
    plugin_args.evc = evc;
    plugin_args.geom = &geometry_;
    for (size_t i = 0; i < plugins_vector_.size(); ++i) {
      plugin_args.id = plugins_vector_[i].second.id;
      plugin_args.params = &(plugins_vector_[i].second.parameters);
      plugins_vector_[i].first(plugin_args);
    }
    msg_tts.stopCompute();

    // send the TTS to the EM
    MQ_TTS2EM_.put(EventMessage(evc, msg_tts));

    // Update and send monitoring informations about triggers

    std::vector<double> events_temp = tts_build.GetL1Event();
    std::transform(
      v_events.begin(),
      v_events.end(),
      events_temp.begin(),
      v_events.begin(),
      std::plus<double>());

    std::vector<double> l1_rate_temp = tts_build.GetL1Rate();
    std::transform(
      v_rates.begin(),
      v_rates.end(),
      l1_rate_temp.begin(),
      v_rates.begin(),
      std::plus<double>());

    ++tts_counter;

    boost::chrono::duration<double> const duration_time =
      boost::chrono::system_clock::now() - start_time;

    if (duration_time >= monitor_time_interval_) {
      for (size_t i = 0; i < v_events.size(); ++i) {
        l1_events[i].put(v_events[i] / tts_counter);
        trm::sendToMonitoring(monitoring_server_, l1_events[i]);
      }
      v_events.assign(v_events.size(), 0.0);
      for (size_t i = 0; i < v_rates.size(); ++i) {
        l1_rates[i].put(v_rates[i] / tts_counter);
        trm::sendToMonitoring(monitoring_server_, l1_rates[i]);
      }
      v_rates.assign(v_rates.size(), 0.0);
      start_time = boost::chrono::system_clock::now();
      tts_counter = 0;
    }
  }
}

}
}
