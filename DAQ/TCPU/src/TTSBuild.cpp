#include <numeric>
#include <algorithm>
#include <functional>

#include <assert.h>

#include "tridas_dataformat.hpp"
#include "TTSBuild.h"
#include "log.hpp"

using namespace std;

namespace tridas {
namespace tcpu {

namespace {
std::vector<PMTHit*>::const_iterator find_oldest_hit(
    std::vector<PMTHit*> const& ptr_pmt) {
  // Find first valid iterator
  std::vector<PMTHit*>::const_iterator it = ptr_pmt.begin();
  for (; it != ptr_pmt.end() && !*it; ++it);
  // Find the oldest one
  std::vector<PMTHit*>::const_iterator oldest_it = it;
  for (; it != ptr_pmt.end(); ++it) {
    if (*it && (*it)->IsOlderThan(**oldest_it)) {
      oldest_it = it;
    }
  }
  return oldest_it;
}
}

TTSBuild::TTSBuild(
    Geometry const& geometry,
    TriggerParameters const& trigger_parameters)
    : geometry_(geometry),
      trig_params_(trigger_parameters),
      end_event_window_(0) {
  // this is the actual constructor called by the TriggerInterface
  ConfigureVectors();
}

void TTSBuild::ConfigureVectors()
{
  size_t const pmts_total = geometry_.pmts_per_floor
      * geometry_.floors_per_tower * geometry_.towers;
  parsed_nhit_.resize(pmts_total); // array with amount of parsed hit per PMT
  all_PMTHit_.resize(pmts_total); // matrix of all the hits

  for (unsigned int i = 0; i < pmts_total; ++i) {
    friendSC_.push_back((i % 2) ? i - 1 : i + 1); // Simple coincidences
    std::vector<unsigned int> temp;
    unsigned int const pmt1 = i % geometry_.pmts_per_floor;
    unsigned int const pmt2 = (pmt1 % 2) ? pmt1 - 1 : pmt1 + 1;
    for (unsigned int j = 0; j < geometry_.pmts_per_floor; ++j) {
      if ((j != pmt1) && (j != pmt2))
        temp.push_back(i - pmt1 + j);  // Floor Coincidences
    }
    friendsFC_.push_back(temp);
  }

  // resize to pmts_floor - 2 (as dimension of temp in the previous for)
  friendsHitFC_.resize(geometry_.pmts_per_floor - 2);

  evt_nseeds_.resize(MAX_TRIGGERS_NUMBER);
  tts_nseeds_.resize(MAX_TRIGGERS_NUMBER);

}

void TTSBuild::SetTTS(TowerTimeSlice* TTS)
{
  TTS_ = TTS;

  // Reset parameters

  L1_nHit_ = 0;
  EventNumber_ = 0;
  L1win_is_open_ = false;
  TrigEv_ = 0;
  ptr_pmt_.clear();
  tts_nseeds_.assign(tts_nseeds_.size(), 0);
  parsed_nhit_.assign(parsed_nhit_.size(), 0);
  for (size_t i = 0; i < all_PMTHit_.size(); ++i) {
    all_PMTHit_[i].clear();
  }
  CurrentHit_ = 0;
  friendHitSC_ = 0;
  OldestHit_ = 0;

}

unsigned int TTSBuild::GetHitsInTriggeredEvent()
{

  PMTHit* cursor = SWHit_;
  unsigned int ntotalhit = 0;

  while (cursor != EWHit_) {
    ++ntotalhit;
    cursor = cursor->next();
  }

  return ntotalhit;

}

void TTSBuild::HitBuildL1(EventCollector* evc)
{
  EVC_ = evc;
  EVC_->set_ts_id(TTS_->GetTSID());

  // Configure all the array for the PMTHits
  ConfigureHitBuffers();

  // Find the oldest hit
  std::vector<PMTHit*>::const_iterator const it = find_oldest_hit(ptr_pmt_);

  //Check if the TTS is empty
  if (it == ptr_pmt_.end()) {
    TRIDAS_LOG(WARNING)
      << "TTSBuild - Found empty TTS with TS ID "
      << TTS_->GetTSID();
    return;
  }

  // reset hit handlers to the oldest hit
  CurrentHit_ = *it;
  OldestHit_ = *it;
  EWHit_ = *it;

  // verify if we need  to activate as first event the RandomTrigger
  CheckRandomTrigger();

  // find all the l1 triggers
  FindL1();
}

bool TTSBuild::CheckRandomTrigger()
{
  // yes:
  if (trig_params_.l1_flag_rt) {
    //    cout<<"DEBUG - TTSBuild::CheckRandomTrigger - RandomTrigger is ACTIVE"<<endl<<flush;
    if (!(TTS_->GetTSID() % trig_params_.l1_frequency_rt)) {

      ++evt_nseeds_[L1RT_ID];
      CurrentHit_->addSeed(L1_RT_SEED);

      // @ opening window in Random
      end_event_window_ = CurrentHit_->getT5ns()
          + fine_time(trig_params_.l1_delta_time_rt);

      L1win_is_open_ = true;
      TrigEv_ = EVC_->alloc_trig_event();

      TrigEv_->L1_first_seed_ = reinterpret_cast<char*>(CurrentHit_);

      unsigned int const pmtid = CurrentHit_->getAbsPMTID();
      TrigEv_->p_start_[pmtid] = CurrentHit_->getRawDataStart();
      TrigEv_->p_end_[pmtid] = CurrentHit_->getRawDataEnd();
      SWHit_ = CurrentHit_;
    }
    return true;
  }
  return false;
}

void TTSBuild::ConfigureHitBuffers()
{
  size_t const sts_in = TTS_->GetNSTSin();
  for (size_t i = 0; i < sts_in; ++i) { // loop over the various STS in a TTS
    SectorTimeSlice* sts = TTS_->GetSTS(i);
    if (sts && sts->GetHeader().pmtin_) { // if the i-th sts exists go...
      std::vector<SimpleBuffer*> const& sbuf = sts->GetBuffers();
      s_HeaderInfoSTS const& STS_h_ = sts->GetHeader();
      for (size_t ipmt = 0; ipmt < sbuf.size(); ++ipmt) { // loop over the various pmts
        if (STS_h_.pmtlist_[ipmt]) { // if the i-th pmt exists go...
          size_t const load = STS_h_.BufSize_[ipmt];
          size_t parsed = 0;
          char* pointer = sbuf[ipmt]->GetStartBuffer();
          while (parsed < load) {
            PMTHit hit(pointer, load - parsed, geometry_);
            all_PMTHit_[hit.getAbsPMTID()].push_back(hit);
            pointer += hit.length();
            parsed += hit.length();
          }
        }
      }
    }
  }

  for (size_t i = 0; i < all_PMTHit_.size(); ++i) {
    if(!all_PMTHit_[i].empty()) {
      ptr_pmt_.push_back(&all_PMTHit_[i][0]);
      parsed_nhit_[i] = 1;
    }
    else {
      ptr_pmt_.push_back(0);
      parsed_nhit_[i] = 0;
    }
  }
}

bool TTSBuild::InsertNewHit()
{
  size_t oldest_id = CurrentHit_->getAbsPMTID();

  if (parsed_nhit_[oldest_id] < all_PMTHit_[oldest_id].size()) {
    ptr_pmt_[oldest_id] = &all_PMTHit_[oldest_id][parsed_nhit_[oldest_id]];
    ++parsed_nhit_[oldest_id];
  } else {
    ptr_pmt_[oldest_id] = 0;
  }

  // Update oldest value
  std::vector<PMTHit*>::const_iterator const oldest_it = find_oldest_hit(
      ptr_pmt_);
  if (oldest_it == ptr_pmt_.end()) {
    return false;
  }

  PMTHit* PreviousHit = CurrentHit_;
  CurrentHit_ = *oldest_it;
  PreviousHit->set_next(CurrentHit_);
  CurrentHit_->set_previous(PreviousHit);

  return true;
}

void TTSBuild::PrintList()
{
  PMTHit* TestHit = OldestHit_;
  while (TestHit) {
    std::cout << *TestHit << std::endl;
    TestHit = TestHit->next();
  }
}

// ------------------------- L1 METHODS

std::vector<double> TTSBuild::GetL1Event()
{
  std::vector<double> events;
  events.push_back(EventNumber_);
  events.push_back(EventNumber_ / TTSduration_sec_);
  return events;
}

std::vector<double> TTSBuild::GetL1Rate()
{
  std::vector<double> rates;
  for (std::vector<unsigned int>::iterator it = tts_nseeds_.begin();
      it != tts_nseeds_.end(); ++it) {
    rates.push_back(*it / TTSduration_sec_);
  }
  return rates;
}

bool TTSBuild::TestL1()
{

  bool testl1 = false;

  if (CurrentHit_->hasMinimumBias()) {
    if (CurrentHit_->isOverThreshold(trig_params_.l1_charge_threshold)) {
      CurrentHit_->addSeed(L1_QTH_SEED);
      ++evt_nseeds_[L1Q_ID];
      testl1 = true;
    }
    if (friendHitSC_
        && CurrentHit_->TimeDifference5ns(friendHitSC_) <=
            trig_params_.l1_delta_time_sc) {
      CurrentHit_->addSeed(L1_SC_SEED);
      friendHitSC_->addSeed(L1_SC_SEED);
      ++evt_nseeds_[L1SC_ID];
      testl1 = true;
    }

    size_t const dim = friendsHitFC_.size();
    for (size_t i = 0; i < dim; ++i) {
      if (friendsHitFC_[i]
          && CurrentHit_->TimeDifference5ns(
              friendsHitFC_[i]) <= trig_params_.l1_delta_time_fc) {
        CurrentHit_->addSeed(L1_FC_SEED);
        friendsHitFC_[i]->addSeed(L1_FC_SEED);
        ++evt_nseeds_[L1FC_ID];
        testl1 = true;
      }
    }

  } // if charge is enough

  return testl1;

}

void TTSBuild::FindL1()
{

  unsigned int which_friend_pmt = 0;
  unsigned int which_friend_hit = 0;

  do {
    unsigned int const which_pmt = CurrentHit_->getAbsPMTID(); //let's take the first PMTID of the ordered list

    // SC
    which_friend_pmt = friendSC_[which_pmt];
    friendHitSC_ = 0;
    if (ptr_pmt_[which_friend_pmt]) {
      which_friend_hit = parsed_nhit_[which_friend_pmt] - 1; // we want the friend hit in the current list, so the last to be parsed
      friendHitSC_ = &all_PMTHit_[which_friend_pmt][which_friend_hit];
    }

    // FC
    size_t const dim = friendsFC_[which_pmt].size();
    for (size_t i = 0; i < dim; ++i) {
      which_friend_pmt = friendsFC_[which_pmt][i];
      friendsHitFC_[i] = 0;
      if (ptr_pmt_[which_friend_pmt]) {
        which_friend_hit = parsed_nhit_[which_friend_pmt] - 1; // we want the friend hit in the current list, so the last to be parsed
        friendsHitFC_[i] = &all_PMTHit_[which_friend_pmt][which_friend_hit];
      }
    }

    // If it is time to close the window, close it
    if (L1win_is_open_ && CurrentHit_->getT5ns() > end_event_window_) {
      FinalizeTriggeredEvent();
    }

    // If the window is still open, update the info
    if (L1win_is_open_) {
      ++L1_nHit_; // counting the "right" hits
      if (TrigEv_->p_start_[which_pmt] == 0) {
        TrigEv_->p_start_[which_pmt] = CurrentHit_->getRawDataStart();
      }
      TrigEv_->p_end_[which_pmt] = CurrentHit_->getRawDataEnd();
    }

    if (TestL1()) {
      fine_time const temp_end_event_window = CurrentHit_->getT5ns()
          + fine_time(trig_params_.l1_event_window_half_size);
      if (!L1win_is_open_) {
        L1win_is_open_ = true;
        TrigEv_ = EVC_->alloc_trig_event();
        end_event_window_ = temp_end_event_window;
        TrigEv_->L1_first_seed_ = reinterpret_cast<char*>(CurrentHit_);
        SetStartL1WindowHit();
      } else if (temp_end_event_window > end_event_window_) {
        end_event_window_ = temp_end_event_window;
      }
    }

  } while (InsertNewHit());

  if (L1win_is_open_) {
    FinalizeTriggeredEvent();
  }

  TTSduration_sec_ = (OldestHit_->TimeDifference5ns(CurrentHit_)).count()
      / 200000000.;  // from 5ns to 1s

  EVC_->set_ts_id(TTS_->GetTSID());
  EVC_->set_tts_duration(TTSduration_sec_);
}

void TTSBuild::FinalizeTriggeredEvent()
{

  // Few asserts before finalize the triggered event
  assert(CurrentHit_ && "Invalid current hit address");
  assert(SWHit_ && "Invalid start hit address");
  assert(L1win_is_open_ && "L1 window already closed");

  EWHit_ = CurrentHit_;
  L1win_is_open_ = false;

  TrigEv_->EventID_ = EventNumber_;

  // Sum all L1 triggers in evt_nseeds_[L1TOTAL_ID]
  evt_nseeds_[L1TOTAL_ID] = std::accumulate(evt_nseeds_.begin() + 1,
      evt_nseeds_.end(), 0);

  // Sum L1 triggers of this event in tts_nseeds_ vector
  std::transform(tts_nseeds_.begin(), tts_nseeds_.end(), evt_nseeds_.begin(),
      tts_nseeds_.begin(), std::plus<unsigned int>());

  // Copy evt_nseeds_ in TrigEv_->nseeds
  for (size_t i = 0; i < evt_nseeds_.size(); ++i){
    TrigEv_->nseeds_[i] = evt_nseeds_[i];
  }

  // Set to 0 all elements in evt_nseeds_
  evt_nseeds_.assign(evt_nseeds_.size(), 0);

  TrigEv_->sw_hit_ = reinterpret_cast<char*>(SWHit_);
  TrigEv_->ew_hit_ = reinterpret_cast<char*>(EWHit_);

  //plus what is still needed (nTotPMT_, etc.)
  TrigEv_->ComputeDataSize();

  TrigEv_->nHit_ = L1_nHit_;
  L1_nHit_ = 0;
  ++EventNumber_;

}

void TTSBuild::SetStartL1WindowHit()
{
  PMTHit* TestHit = CurrentHit_;
  // while(not too left, not to far from the trigger, not overlap )
  while (
      TestHit
   && TestHit->TimeDifference5ns(CurrentHit_) < trig_params_.l1_event_window_half_size
   && TestHit->next() != EWHit_) {

    unsigned int const pmtid = TestHit->getAbsPMTID();

    ++L1_nHit_; // counting the "left" hits + the trigger one
    TrigEv_->p_start_[pmtid] = TestHit->getRawDataStart();

    // temporary set the end of the window hits
    if (!TrigEv_->p_end_[pmtid]) {
      TrigEv_->p_end_[pmtid] = TestHit->getRawDataEnd();
    }

    SWHit_ = TestHit;

    TestHit = TestHit->previous();
  }
}

}
}

