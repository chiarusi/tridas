
#ifndef DAQ_TCPU_INC_TRIGGER_PLUGINS_TRIGSKY_ROTATION3D_H
#define DAQ_TCPU_INC_TRIGGER_PLUGINS_TRIGSKY_ROTATION3D_H

namespace tridas {
namespace tcpu {

class Rotation3D
{
    
private:
    float matrix_[3][3];
    
public:
    Rotation3D(float, float);
    float getMatrixElement(unsigned int, unsigned int);
    void setNewCoordinates(float&, float&, float&) const;
    
};

}
}

#endif
