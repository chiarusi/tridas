#ifndef DAQ_TCPU_TRIGGER_PLUGINS_TRIGSKY_TRIGSKY_H
#define DAQ_TCPU_TRIGGER_PLUGINS_TRIGSKY_TRIGSKY_H

#include <vector>

#include "Rotation3D.h"
#include "TriggerInterface.h"

namespace tridas {
namespace tcpu {

extern "C" void TrigSky(PluginArgs const& args);

class TrigSkyAlgo {

  std::vector<Rotation3D> directions_;
  int id_;
  EventCollector& evc_;
  Geometry const& geom_;

 private:
  bool checkCorrelation(int d, PMTHit* hit1, PMTHit* hit2);

 public:
  TrigSkyAlgo(int id, EventCollector& evc, Geometry const& geom);
  void run();

};

}
}

#endif
