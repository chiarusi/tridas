#include "TrigSky.h"

#include <cmath>

#include <boost/chrono.hpp>

#include "Event_Classes.h"
#include "Geometry.h"
#include "log.hpp"

namespace tridas {
namespace tcpu {

float const pi = 3.14159265358979;
float const c = 299.793;
float const tanc = tan(42 * pi / 180.0);
float const step = pi / 10;

void TrigSky(PluginArgs const& args) {
  TrigSkyAlgo trigger_sky(args.id, *args.evc, *args.geom);
  trigger_sky.run();
}

TrigSkyAlgo::TrigSkyAlgo(int id, EventCollector& evc, Geometry const& geom)
    :
      id_(id),
      evc_(evc),
      geom_(geom) {

  // Init directions vector
  for (float theta = step / 2; theta <= pi; theta += step) {
    for (float phi = step / 2; phi <= 2 * pi; phi += step) {
      directions_.push_back(Rotation3D(theta, phi));
    }
  }
}

bool TrigSkyAlgo::checkCorrelation(int d, PMTHit* hit1, PMTHit* hit2) {
  unsigned int pmt1 = hit1->getAbsPMTID();
  unsigned int pmt2 = hit2->getAbsPMTID();
  float newX1 = geom_.positions[pmt1].x;
  float newY1 = geom_.positions[pmt1].y;
  float newZ1 = geom_.positions[pmt1].z;
  directions_[d].setNewCoordinates(newX1, newY1, newZ1);
  float newX2 = geom_.positions[pmt2].x;
  float newY2 = geom_.positions[pmt2].y;
  float newZ2 = geom_.positions[pmt2].z;
  directions_[d].setNewCoordinates(newX2, newY2, newZ2);
  float expr1 = abs(
    hit1->TimeDifference5ns(hit2).count() * 5 * c - (newZ1 - newZ2));
  float expr2 = sqrt(pow(newX1 - newX2, 2) + pow(newY1 - newY2, 2)) * tanc;
  return expr1 <= expr2;
}

void TrigSkyAlgo::run() {

  unsigned int plug_events = 0;

  // for each event
  for (int i = 0; i < evc_.used_trig_events(); ++i) {
    TriggeredEvent& tev = *(evc_.trig_event(i));

    assert(tev.nseeds_[L1TOTAL_ID] && "Triggered event with no seeds");

    PMTHit* current_hit = reinterpret_cast<PMTHit*>(tev.L1_first_seed_);
    PMTHit* end_hit = reinterpret_cast<PMTHit*>(tev.ew_hit_);

    assert(current_hit && "invalid current hit address");
    assert(end_hit && "invalid end hit address");

    // Find seeds for this event
    std::vector<PMTHit*> seeds;
    long hits_counter = 0;
    do {
      if (current_hit->isSeed()) {
        seeds.push_back(current_hit);
      }
      current_hit = current_hit->next();
      ++hits_counter;
      assert(hits_counter <= tev.nHit_ && hits_counter >= 0);
    } while (current_hit && current_hit != end_hit);

    assert(seeds.size() && "Seeds not found");

    // for each direction
    for (size_t d = 0; d < directions_.size(); ++d) {

      // for each seed
      for (size_t j = 0; j < seeds.size() - 1; j++) {
        PMTHit* hit1 = seeds[j];
        for (unsigned int k = j + 1; k < seeds.size(); k++) {
          PMTHit* hit2 = seeds[k];

          // Check correlation
          if (checkCorrelation(d, hit1, hit2)) {
            // increment seeds counter of triggered event
            tev.plugin_nseeds_[id_]++;
            tev.plugin_ok_ = true;
          }
        }
      }
    }
    if (tev.plugin_ok_) {
      ++plug_events;
    }
  }

  evc_.set_stats_for_plugin(id_, plug_events);

  TRIDAS_LOG(DEBUG)
    << "TrigSkyAlgo triggered "
    << evc_.stats_for_plugin(id_)
    << " of "
    << evc_.used_trig_events()
    << " events in TTS "
    << evc_.ts_id();
}

}
}

