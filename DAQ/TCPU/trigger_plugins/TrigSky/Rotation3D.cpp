
#include "Rotation3D.h"
#include <iostream>

#include <cmath>

using namespace std;

namespace tridas {
namespace tcpu {

Rotation3D::Rotation3D(float t, float p)
{
    // find direction vector
    float sinT = sin(t);
    float x = cos(p) * sinT;
    float y = sin(p) * sinT;
    float z = cos(t);

    // find matrix of rotation in z
    float rz[3][3];
    float det = sqrt(pow((float)x, 2) + pow((float)y, 2));
    float cz = y / det;
    float sz = x / det;
    // [row][column]
    rz[0][0] = cz;
    rz[0][1] = sz * -1;
    rz[0][2] = 0;
    rz[1][0] = sz;
    rz[1][1] = cz;
    rz[1][2] = 0;
    rz[2][0] = 0;
    rz[2][1] = 0;
    rz[2][2] = 1;
 
    // find matrix of rotation in x
    float rx[3][3];
    float cx = z;
    float sx = sqrt(1 - pow((float)z, 2));
    rx[0][0] = 1;
    rx[0][1] = 0;
    rx[0][2] = 0;
    rx[1][0] = 0;
    rx[1][1] = cx;
    rx[1][2] = sx * -1;
    rx[2][0] = 0;
    rx[2][1] = sx;
    rx[2][2] = cx;
    
    // find matrix of the overall rotation
    // rtot = rx * rz
 	for (unsigned int i = 0; i < 3; i++)
 	{
 		for (unsigned int j = 0; j < 3; j++)
 		{
 			matrix_[i][j] = 0.;
 			for (unsigned int k = 0; k < 3; k++)
                matrix_[i][j] += rx[i][k] * rz[k][j];
        }
    }

}

float Rotation3D::getMatrixElement(unsigned int row, unsigned int column)
{
    return matrix_[row][column];
}

void Rotation3D::setNewCoordinates(float& x, float& y, float& z) const
{
    const float tempx = x;
    const float tempy = y;
    const float tempz = z;
    x = matrix_[0][0] * tempx + matrix_[0][1] * tempy + matrix_[0][2] * tempz;
    y = matrix_[1][0] * tempx + matrix_[1][1] * tempy + matrix_[1][2] * tempz;
    z = matrix_[2][0] * tempx + matrix_[2][1] * tempy + matrix_[2][2] * tempz;
}

}
}
