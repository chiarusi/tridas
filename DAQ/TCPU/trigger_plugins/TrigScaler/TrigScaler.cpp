#include "TrigScaler.h"
#include "Event_Classes.h"
#include "log.hpp"
#include "Configuration.h"
#include "PluginParameters.h"
namespace tridas {
namespace tcpu {

void TrigScaler(PluginArgs const& args) {

  int const id = args.id;
  int const scale_factor = args.params->get<int>("SCALE_FACTOR");
  assert(scale_factor > 0 && "Scale factor must be greater than 0");
  EventCollector& evc = *args.evc;

  int counter = scale_factor;
  unsigned plug_events = 0;

  // for each event
  for (int i = 0; i < evc.used_trig_events(); ++i) {
    TriggeredEvent& tev = *(evc.trig_event(i));
    assert(tev.nseeds_[L1TOTAL_ID] && "Triggered event with no seeds");
    if (counter % scale_factor == 0) {
      // increment seeds counter of triggered event
      tev.plugin_nseeds_[id]++;
      tev.plugin_ok_ = true;
      ++plug_events;
      counter = 1;
    } else {
      ++counter;
    }
  }

  evc.set_stats_for_plugin(id, plug_events);

  TRIDAS_LOG(DEBUG)
    << "TrigScalerAlgo triggered "
    << evc.stats_for_plugin(id)
    << " of "
    << evc.used_trig_events()
    << " events in TTS "
    << evc.ts_id();
}

}
}
