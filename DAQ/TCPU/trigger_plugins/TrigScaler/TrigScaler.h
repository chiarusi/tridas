#ifndef DAQ_TCPU_TRIGGER_PLUGINS_TRIGSCALER_TRIGSCALER_H
#define DAQ_TCPU_TRIGGER_PLUGINS_TRIGSCALER_TRIGSCALER_H

#include "TriggerInterface.h"

namespace tridas {
namespace tcpu {

extern "C" void TrigScaler(PluginArgs const& args);

}
}
#endif
