/* TrigTemplate.h -- demonstrate library use */

// TEMPLATE CODE: SUBSTITUTE THE "Template" string with the right trigger identifier
#ifndef DAQ_TCPU_TRIGGER_PLUGINS_TRIGTEMPLATE_TRIGTEMPLATE_H
#define DAQ_TCPU_TRIGGER_PLUGINS_TRIGTEMPLATE_TRIGTEMPLATE_H

#include "TriggerInterface.h"

namespace tridas {
namespace tcpu {

extern "C" void TrigTemplate(PluginArgs const& args);

}
}
#endif
