#ifndef DAQ_TCPU_TRIGGER_PLUGINS_TRIGRANDOM_TRIGRANDOM_H
#define DAQ_TCPU_TRIGGER_PLUGINS_TRIGRANDOM_TRIGRANDOM_H

#include "TriggerInterface.h"
namespace tridas {
namespace tcpu {

extern "C" void TrigRandom(PluginArgs const& args);

}
}
#endif
