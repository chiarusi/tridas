#include "TrigRandom.h"
#include "Event_Classes.h"
#include "log.hpp"
namespace tridas {
namespace tcpu {

void TrigRandom(PluginArgs const& args) {
  int const id = args.id;
  EventCollector& evc = *args.evc;
  int plug_events = 0;

  // for each event
  for (int i = 0; i < evc.used_trig_events(); ++i) {
    TriggeredEvent& tev = *(evc.trig_event(i));
    assert(tev.nseeds_[L1TOTAL_ID] && "Triggered event with no seeds");
    if (tev.nseeds_[L1RT_ID]) {
      // increment seeds counter of triggered event
      tev.plugin_nseeds_[id]++;
      tev.plugin_ok_ = true;
      ++plug_events;
    }
  }

  evc.set_stats_for_plugin(id, plug_events);

  TRIDAS_LOG(DEBUG)
    << "TrigRandom triggered "
    << evc.stats_for_plugin(id)
    << " of "
    << evc.used_trig_events()
    << " events in TTS "
    << evc.ts_id();
}

}
}
