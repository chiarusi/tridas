#ifndef DAQ_TCPU_INC_S_THREAD_H
#define DAQ_TCPU_INC_S_THREAD_H

#include "tridas_dataformat.hpp"
#include "TowerTimeSlice.h"
#include "Event_Classes.h"
#include "Messages.h"

namespace tridas {
namespace tcpu {

struct TSMessage
{
  TS_t timeslice_id;
  TcpuId tcpu_id;
};


struct STSMessage
{
  TS_t timeslice_id; // init id=0, then DAQ start from 1
  SectorTimeSlice* pointer;
};


struct EventMessage
{
  EventCollector* evc_pointer;
  TTSMessage tts_msg;

  EventMessage& operator=(const EventMessage& copy)
  {
    if (this != &copy) {
      evc_pointer = copy.evc_pointer;
      tts_msg = copy.tts_msg;
    }
    return *this;
  }

  EventMessage(EventCollector* evc_pointer,
               TTSMessage const& tts_msg) :
    evc_pointer(evc_pointer),
    tts_msg(tts_msg)
  {}
};

}
}

#endif
