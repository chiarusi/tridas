//
// Created by favaro on 11/06/15.
//

#ifndef TRIDAS_DAQ_TCPU_INC_MESSAGES_H
#define TRIDAS_DAQ_TCPU_INC_MESSAGES_H

#include "tridas_dataformat.hpp"
#include "TowerTimeSlice.h"

namespace tridas {
namespace tcpu {

class TTSMessage
{
 private:
  TowerTimeSlice* tts_;
  // time of TTS creation
  boost::chrono::system_clock::time_point m_construction;

  // time of completion of receiving
  boost::chrono::system_clock::time_point m_complete_receive;

  // time of start of L1 trigger (i.e. start of triggering)
  boost::chrono::system_clock::time_point m_start_computation;

  // time of start of L2 trigger (i.e. stop of L1)
  boost::chrono::system_clock::time_point m_start_l2_computation;

  // time of stop of L2 trigger (i.e. stop of all triggering facilities)
  boost::chrono::system_clock::time_point m_stop_computation;

  // time of start of sending data to EM
  boost::chrono::system_clock::time_point m_start_send;

  // time of end of life of the TTS in the TCPU (i.e. end sending to EM).
  boost::chrono::system_clock::time_point m_complete;

 public:

  typedef boost::chrono::system_clock::time_point::duration duration;

  explicit TTSMessage(TowerTimeSlice* tts);

  TS_t timesliceId() const
  {
    return tts_->GetTSID();
  }

  TowerTimeSlice* towerTimeSlice() const
  {
    return tts_;
  }

  void stopReceive();

  void startCompute();

  void startL2Compute();

  void stopCompute();

  void startSend();

  void stopSend();

  duration l1ComputationTime() const;

  duration l2ComputationTime() const;

  duration computationTime() const;

  duration sendTime() const;

  duration lifeTime() const;

  duration buildingTime() const;


};

} // ns tcpu
} // ns tridas

#endif
