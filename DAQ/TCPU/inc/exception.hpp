#ifndef DAQ_TCPU_EXCEPTION_HPP
#define DAQ_TCPU_EXCEPTION_HPP

#include <string>
#include <stdexcept>

namespace tridas {
namespace tcpu {

class Exception : public std::runtime_error
{
 public:
  explicit Exception(std::string const& error) : std::runtime_error(error) {}
};

}}

#endif
