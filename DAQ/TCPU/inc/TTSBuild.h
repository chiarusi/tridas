#ifndef DAQ_TCPU_INC_TTSBUILD_H
#define DAQ_TCPU_INC_TTSBUILD_H


#include <iostream>
#include <vector>
#include <algorithm>

#include "TowerTimeSlice.h"
#include "PMTHit.h"
#include "Event_Classes.h"
#include "Geometry.h"
#include "TriggerParameters.h"

#include "monitoring.hpp"

namespace trm = tridas::monitoring;

namespace tridas {
namespace tcpu {

class TTSBuild
{

private:

  bool L1win_is_open_;

  // Settings for L1 selection
  unsigned int EventNumber_;

  std::vector<unsigned int> friendSC_; // array of partners for Simple Coincidences
  std::vector<std::vector<unsigned int> > friendsFC_; // array of partners for Floor Coincidences

  Geometry const geometry_;
  TriggerParameters const trig_params_;

  size_t L1_nHit_;

  std::vector<unsigned int> evt_nseeds_;
  std::vector<unsigned int> tts_nseeds_;

  PMTHit* friendHitSC_; // pointer to match with First for Simple Coincidences
  // friendHitSC_ is a single element because 1 pmt has only 1 friend in simple coincidence
  std::vector<PMTHit*> friendsHitFC_; // pointer to match with First for Floor Coincidences
  // friendsHitFC_ is a vector because 1 pmt has (NPMTFLOOR - 2) friends in floor coincidences

  double TTSduration_sec_;

  EventCollector* EVC_;
  TowerTimeSlice* TTS_;

  std::vector<PMTHit*> ptr_pmt_; // array if pointers to the next-hit start of each PMT.
  std::vector<unsigned int> parsed_nhit_;

  PMTHit* CurrentHit_;
  PMTHit* OldestHit_; // the oldest hit in the list, ever (i.e. the first of the firsts)

  PMTHit* SWHit_;
  PMTHit* EWHit_;

  fine_time end_event_window_;

  TriggeredEvent* TrigEv_; // temporary triggered event

  std::vector< std::vector<PMTHit> > all_PMTHit_;

private:

  void ConfigureVectors();

  bool CheckRandomTrigger();

  void FindL1();
  bool TestL1();
  void CheckL1Window();

  void SetStartL1WindowHit();

  void FinalizeTriggeredEvent();

  void ConfigureHitBuffers();

  void ParseLastPMT(const int);

  bool InsertNewHit();

  unsigned int GetHitsInTriggeredEvent();

public:

  TTSBuild(Geometry const& geometry, TriggerParameters const&
      trigger_parameters);

  void SetTTS(TowerTimeSlice*);
  void HitBuildL1(EventCollector* evc);

  std::vector<double> GetL1Event();
  std::vector<double> GetL1Rate();

  void PrintList();

};

}
}

#endif
