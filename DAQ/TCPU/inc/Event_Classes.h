#ifndef DAQ_TCPU_INC_EVENT_CLASSES_H
#define DAQ_TCPU_INC_EVENT_CLASSES_H

#include <vector>

#include "tridas_dataformat.hpp"
namespace tridas {
namespace tcpu {

class TriggeredEvent {
  unsigned int nTotPMT_;
  std::size_t event_size_;

  void Init();
  unsigned int CountHitInPMT(int);

 public:

  // reset in Reset()

  bool plugin_ok_;
  unsigned int EventID_;
  unsigned int nseeds_[MAX_TRIGGERS_NUMBER];
  unsigned int plugin_nseeds_[MAX_PLUGINS_NUMBER];

  unsigned int nHit_;  //hits in the events
  char* L1_first_seed_;
  // sw_hit_ and ew_hit_ are the first and last PMTHit in the ordered list of this event
  char* sw_hit_;  // for plugins, it's a list of PMTHits
  char* ew_hit_;  // for plugins, it's a list of PMTHits

  //---------

  // Local memory allocation of the char buffers
  char** p_start_;  // it's an array of array because there is an array foreach pmt
  char** p_end_;  // it's an array of array because there is an array foreach pmt

 public:
  TriggeredEvent(unsigned int);
  ~TriggeredEvent();

  bool CheckReset();
  void Reset();

  inline std::size_t getEventSize() const {
    return event_size_;
  }

  // TO BE CONTINUED
  std::size_t ComputeDataSize();

  std::size_t CopyToBuffer(char* dest, std::size_t maxsize) const;

  unsigned int CheckPMTHitConsistency();  // this reparse all the hits and compare the found hit numbers to nHit_.

};

class EventCollector {
  TS_t ts_id_;
  double tts_duration_sec_;
  std::vector<TriggeredEvent*> trig_events_;
  int filled_trig_events_;
  std::vector<int> plug_events_;
  unsigned int tot_pmts_;

 public:
  EventCollector(unsigned int tot_pmts);
  ~EventCollector();
  void reset();

  // ts_id_ setter and getter
  void set_ts_id(TS_t ts_id) {
    ts_id_ = ts_id;
  }
  TS_t ts_id() const {
    return ts_id_;
  }

  // tts_duration_sec_ setter and getter
  void set_tts_duration(float tts_duration_sec) {
    tts_duration_sec_ = tts_duration_sec;
  }
  float tts_duration() const {
    return tts_duration_sec_;
  }

  // trig_events_ setter and getter
  TriggeredEvent* alloc_trig_event();
  TriggeredEvent* trig_event(int id) const {
    assert(id >= 0 && id < filled_trig_events_);
    return trig_events_[id];
  }
  int used_trig_events() const {
    return filled_trig_events_;
  }

  // plug_events_ setter and getter
  void set_stats_for_plugin(int plugin_id, int events) {
    assert(
        plugin_id >= 0 && static_cast<size_t>(plugin_id) < plug_events_.size());
    assert(plug_events_[plugin_id] == 0);
    assert(events >= 0);
    plug_events_[plugin_id] = events;
  }
  int stats_for_plugin(int plugin_id) const {
    assert(
        plugin_id >= 0 && static_cast<size_t>(plugin_id) < plug_events_.size());
    return plug_events_[plugin_id];
  }

  std::vector<TriggeredEvent const*> plugin_events() const;

};

}
}

#endif
