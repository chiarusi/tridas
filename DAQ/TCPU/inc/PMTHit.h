#ifndef DAQ_TCPU_INC_PMTHIT_H
#define DAQ_TCPU_INC_PMTHIT_H

#include <iostream>

#include "Geometry.h"
#include "tridas_dataformat.hpp"

#define L1_RT_SEED 0x01
#define L1_QTH_SEED 0x02
#define L1_SC_SEED 0x04
#define L1_FC_SEED 0x08

// This define is just to check two seeds at the same time
#define L2_FC_AND_SC 0x0C

namespace tridas {
namespace tcpu {

class PMTHit {

 private:
  fine_time T5ns_;
  float CaliCharge_;  //  charge corrected by ADC pedestals
  size_t pmt_id_;
  char* start_hit_;
  unsigned int seed_map_;
  size_t length_;
  bool has_minimum_bias_;
  PMTHit* previous_;
  PMTHit* next_;  // time-ordered topological list 1 hit/PMT

 public:
  PMTHit(char*, std::size_t max_size, Geometry const& geometry);
  fine_time TimeDifference5ns(PMTHit*);  // expressed 5ns units
  bool IsOlderThan(PMTHit const& hit2) const {
    return hit2.getT5ns() > T5ns_;
  }
  bool isOverThreshold(float qth) const {
    return CaliCharge_ >= qth;
  }
  bool hasMinimumBias() const {
    return has_minimum_bias_;
  }
  size_t length() const {
    return length_;
  }
  fine_time getT5ns() const {
    return T5ns_;
  }
  float getCaliCharge() const {
    return CaliCharge_;
  }
  size_t getAbsPMTID() const {
    return pmt_id_;
  }
  void addSeed(unsigned int seed) {
    seed_map_ |= seed;
  }
  bool isSeed() const {
    return seed_map_;
  }
  bool isSeed(unsigned int seed) const {
    return seed_map_ & seed;
  }
  unsigned int getSeedMap() const {
    return seed_map_;
  }
  char* getRawDataStart() {
    return start_hit_;
  }
  char const* getRawDataStart() const {
    return start_hit_;
  }
  char* getRawDataEnd() {
    return start_hit_ + length_;
  }
  char const* getRawDataEnd() const {
    return start_hit_ + length_;
  }
  friend std::ostream& operator<<(std::ostream& os, PMTHit const& pmt_hit);

  PMTHit* previous() {
    return previous_;
  }
  PMTHit const* previous() const {
    return previous_;
  }
  PMTHit* next() {
    return next_;
  }
  PMTHit const* next() const {
    return next_;
  }
  void set_previous(PMTHit* hit) {
    previous_ = hit;
  }
  void set_next(PMTHit* hit) {
    next_ = hit;
  }
  // required by tools of analysis
  size_t GetFrames() const;

};

}
}

#endif
