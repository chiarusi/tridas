#ifndef DAQ_TCPU_INC_TTSMANAGER_H
#define DAQ_TCPU_INC_TTSMANAGER_H

#include <boost/lexical_cast.hpp>

#include "s_thread.h"
#include "SharedQueue.h"
#include "MonitorParameters.h"
#include "monitoring.hpp"
#include "tridas_dataformat.hpp"
#include "Endpoint.h"

#include "inter_objects_communication/producer_consumer.hpp"

namespace trm = tridas::monitoring;

namespace tridas {
namespace tcpu {

class TTSManager
{
  std::vector<trm::RunningAvgObservable> Plugrate_;

  SharedQueue<EventMessage>& MQ_TTS2EM_;
  SharedQueue<EventCollector*>& MQ_TEFree_;
  SharedQueue<TowerTimeSlice*>& MQ_TTSDone_;

  network::Context context_;
  network::Producer socket_;
  trm::MonStreamer mon_streamer_;

  size_t SendEvent(EventCollector const& evc, TowerTimeSlice const& tts);
  void SendPluginRateToMonitor(EventCollector const*const evc);

 public:

  TTSManager(
    SharedQueue<EventMessage>& MQ_TTS2EM,
    SharedQueue<EventCollector*>& MQ_TEFree,
    SharedQueue<TowerTimeSlice*>& MQ_TTSDone,
    Endpoint const& em_endpoint,
    MonitorParameters const& monitor_parameters);

  void operator()(TcpuId tcpu_id, std::string const& tsc_port, std::size_t tokens);
};

}
}

#endif
