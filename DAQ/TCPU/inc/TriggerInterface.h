#ifndef DAQ_TCPU_INC_TRIGGERINTERFACE_H
#define DAQ_TCPU_INC_TRIGGERINTERFACE_H

#include <vector>

#include <dirent.h>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#include "s_thread.h"
#include "SharedQueue.h"
#include "TTSBuild.h"
#include "tridas_dataformat.hpp"
#include "Geometry.h"
#include "StructParameters.h"
#include "TriggerParameters.h"
#include "PluginParameters.h"
#include "MonitorParameters.h"
#include "monitoring.hpp"
#include "Configuration.h"

namespace trm = tridas::monitoring;

namespace tridas {
namespace tcpu {

struct PluginArgs {
  int id;
  EventCollector* evc;
  Geometry const* geom;
  Configuration const* params;
};

typedef void (*TriggerPlugin)(PluginArgs const& args);

class TriggerInterface {

  // Shared queues
  SharedQueue<TTSMessage>& MQ_TTSReady_;
  SharedQueue<EventMessage>& MQ_TTS2EM_;
  SharedQueue<EventCollector*>& MQ_TEFree_;

  // Const fields
  Geometry const& geometry_;
  TriggerParameters const& trigger_parameters_;
  tridas::monitoring::Destination const monitoring_server_;
  boost::chrono::duration<double> const monitor_time_interval_;

  std::vector<boost::shared_ptr<void> > modules_vector_;
  std::vector<std::pair<TriggerPlugin, PluginParameters> > plugins_vector_;

 public:
  TriggerInterface(SharedQueue<TTSMessage>& MQ_TTSReady,
                   SharedQueue<EventMessage>& MQ_TTS2EM,
                   SharedQueue<EventCollector*>& MQ_TEFree,
                   Geometry const& geometry,
                   TriggerParameters const& trigger_parameters,
                   boost::filesystem::path const& plugins_dir,
                   std::vector<PluginParameters> const& plugins,
                   MonitorParameters const& monitor_parameters);
  void operator()();
};

}
}

#endif
