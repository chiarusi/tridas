#ifndef DAQ_TCPU_INC_TTSASSEMBLER_H
#define DAQ_TCPU_INC_TTSASSEMBLER_H

#include <map>
#include <vector>

#include <boost/chrono.hpp>

#include "s_thread.h"
#include "TowerTimeSlice.h"
#include "tridas_dataformat.hpp"

namespace tridas {
namespace tcpu {

class TTSAssembler {
  typedef std::map<boost::chrono::system_clock::time_point, TS_t> TsMap;

  std::vector<TowerTimeSlice*> mq_tts_free_;
  size_t n_sectors_;
  boost::chrono::seconds tts_ready_timeout_;
  TsMap ts_map_;
  std::map<TS_t, TTSMessage> tts_map_;
  std::vector<TTSMessage> ready_tts_;
  TS_t last_completed_ts_;

 public:
  TTSAssembler(
      size_t n_sectors,
      boost::chrono::seconds const& tts_ready_timeout);
  bool addSts(STSMessage const& sts_msg);
  void addEmptyTts(TowerTimeSlice* tts);
  std::vector<TTSMessage> getReadyTts();
  size_t pendingTts() {
    return tts_map_.size();
  }
};

}
}

#endif
