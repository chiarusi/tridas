#ifndef DAQ_TCPU_INC_STSRECEIVER_H
#define DAQ_TCPU_INC_STSRECEIVER_H

#include <iostream>
#include <fstream>

#include "s_thread.h"
#include "SharedQueue.h"
#include "TowerTimeSlice.h"
#include "SectorTimeSlice.h"
#include "TTSAssembler.h"

#include "inter_objects_communication/producer_consumer.hpp"
namespace tridas {
namespace tcpu {

class STSReceiver {

 private:
  SharedQueue<TowerTimeSlice*>& mq_tts_done_;
  SharedQueue<TTSMessage>& mq_tts_ready_;
  std::vector<SectorTimeSlice*> mq_sts_free_;
  size_t pmts_per_sector_;
  size_t pmt_ts_size_;
  TTSAssembler tts_assembler_;

 public:
  STSReceiver(
    SharedQueue<TowerTimeSlice*>& mq_tts_done,
    SharedQueue<TTSMessage>& mq_tts_ready,
    size_t n_sectors,
    size_t pmts_per_sector,
    size_t pmt_ts_size,
    boost::chrono::seconds const& tts_ready_timeout);
  void operator()(std::string const& port);

 private:
  bool GetSTS(SectorTimeSlice*, network::ConsumerPtr socket);
};

}
}

#endif
