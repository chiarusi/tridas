#ifndef DAQ_FCM_DATACARD_READER_HPP
#define DAQ_FCM_DATACARD_READER_HPP

#include <string>

#include "Configuration.h"

namespace tridas {
namespace fcm {

Configuration getDataCardConfiguration(const std::string& conf_str);


}
}
#endif
