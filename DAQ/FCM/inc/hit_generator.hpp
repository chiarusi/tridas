#ifndef DAQ_FCM_HITGENERATOR_HPP
#define DAQ_FCM_HITGENERATOR_HPP

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#include "f_dataformat_p3.hpp"
#include "TimeOfHit.h"

namespace tridas {
namespace fcm {

class HitGenerator
{
  std::vector<char> m_data;
  std::vector<size_t> m_dfhs;
  std::vector<T5nsec> m_end_times;
  std::vector<T5nsec> m_begin_times;

  tridas::phase3::DataFrameHeader& at(size_t n);
  const tridas::phase3::DataFrameHeader& at(size_t n) const;
  void loadFromFile(boost::filesystem::path const& filename, size_t nhits);

 public:

  HitGenerator(boost::filesystem::path const& filename, size_t number_of_hits_per_PMT, unsigned int NPMT, unsigned int TowerID, unsigned int EFCMID, const boost::chrono::seconds& secs);
  void updateTimes(std::vector<boost::shared_ptr<TimeOfHit> >& generators);
  size_t size() const;
  const char* getHits() const;
  T5nsec getDeltaTime() const;
  void resetTimes(boost::chrono::nanoseconds const& offset);
};

}
}

#endif
