#ifndef DAQ_FCM_INC_TIMEOFHIT_H
#define DAQ_FCM_INC_TIMEOFHIT_H

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/exponential_distribution.hpp>

namespace tridas {
namespace fcm {

class TimeOfHit
{
  double max_delay_;
  double single_rate_;
  boost::random::mt19937 engine_;
  boost::random::exponential_distribution<> dist_;
  size_t overflow_;

  // not copyable
  TimeOfHit(TimeOfHit const&);
  TimeOfHit& operator=(TimeOfHit const&);

public:
  TimeOfHit(double max_delay, double single_rate, unsigned int seed);
  double GetDeltaTime();
  size_t overflow() const
  {
    return overflow_;
  }
};

}
}
#endif
