#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>

#include <boost/program_options.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono/duration.hpp>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include "datacard_reader.hpp"
#include "Configuration.h"
#include "monitoring.hpp"
#include "log.hpp"

#include "hit_generator.hpp"
#include "TimeOfHit.h"

#include "version.hpp"

namespace this_thread = boost::this_thread;
namespace po = boost::program_options;
namespace chrono = boost::chrono;

using namespace tridas::fcm;

static inline
std::string short_host_name()
{
  std::string const boost_host_name = boost::asio::ip::host_name();
  size_t const pos = boost_host_name.find('.');
  return boost_host_name.substr(0, pos);
}

int main(int argc, char* argv[])
{
  boost::filesystem::path dcfilename;
  std::string session;
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages.")
    ("version,v", "print TriDAS version")
    ("datacard,d", po::value<boost::filesystem::path>(&dcfilename)->required(), "DataCard file.")
    ("session,s", po::value<std::string>(&session)->required(), "Session ID.");

  try {
    po::variables_map vm;

    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    if (vm.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "FCMsimu: Error: " << e.what() << '\n';
    std::cerr << desc << std::endl;
    return EXIT_FAILURE;
  }

  if (!boost::filesystem::exists(dcfilename)) {
    std::cerr << dcfilename.string() << " does not exist\n";
    return EXIT_FAILURE;
  } else if (!boost::filesystem::is_regular_file(dcfilename)) {
    std::cerr << dcfilename.string() << " is not a regular file\n";
    return EXIT_FAILURE;
  }

  Configuration const configuration = read_ini_configuration(dcfilename);

  Configuration const datacard = getDataCardConfiguration(
      configuration.get<std::string>(session + "." + short_host_name()));

  tridas::Log::init(
      "FCMSimu." + session,
      tridas::Log::FromString(configuration.get<std::string>("GENERAL.LOG_LEVEL")));

  TRIDAS_LOG(DEBUG) << "Configuration:\n" << datacard;

  bool use_monitoring = false;

  tridas::monitoring::Destination mon_server_endpoint;

  try {
    mon_server_endpoint = tridas::monitoring::make_destination(
        configuration.get<std::string>("GENERAL.MONITORING_HOST"),
        9999);
    use_monitoring = true;
  } catch (boost::property_tree::ptree_error const& e) {
    std::cout << e.what()  << '\n';
  } catch (boost::system::system_error& e) {
    std::cout << e.what() << '\n';
  }

  tridas::monitoring::MonHandler mon_handler;
  tridas::monitoring::SimpleObservable throughput(
      "FCMsimu_throughput_" + short_host_name() + "." + session,
      "MB/s",
      "Throughput produced by the FCMsimu");

  tridas::monitoring::SimpleObservable ts_id(
      "FCMsimu_ts_id_" + short_host_name() + "." + session,
      "TS_ID",
      "Current TimeSlice Identifier.");

  const unsigned int NPMT = configuration.get<unsigned int>(
      "GENERAL.NPMTFLOOR");

  std::vector<boost::shared_ptr<TimeOfHit> > time_of_hit;

  double const single_rate = datacard.get<double>("single_rate");
  double const max_delay   = 10. / single_rate;

  for (unsigned int i = 0; i < NPMT; ++i) {
    double const seed = i * 1e6 + datacard.get<unsigned int>("EFCMID")
        + datacard.get<unsigned int>("TowerID") * 50;
    time_of_hit.push_back(
        boost::shared_ptr<TimeOfHit>(
            new TimeOfHit(max_delay, single_rate, seed)));
  }

  boost::filesystem::path filename(
      datacard.get<std::string>("filename").insert(
          0,
          dcfilename.string().substr(0, dcfilename.string().rfind("/") + 1)));

  if (!boost::filesystem::exists(filename)) {
    TRIDAS_LOG(ERROR) <<  filename.string() << " does not exist";
    return EXIT_FAILURE;
  } else if (!boost::filesystem::is_regular_file(filename)) {
    TRIDAS_LOG(ERROR) <<  filename.string() << " is not a regular file";
    return EXIT_FAILURE;
  }

  chrono::nanoseconds const program_begin = chrono::system_clock::now().time_since_epoch();

  chrono::seconds start_time = chrono::duration_cast<chrono::seconds>(
      program_begin
    - chrono::seconds(946684800)
  );

  if (datacard.count("start_time")) {
    start_time = chrono::seconds(
        datacard.get<unsigned int>("start_time")
    );
  } else {
    TRIDAS_LOG(INFO) << "Using automatic start time: " << start_time;
  }

  HitGenerator hit_generator(filename,
      datacard.get<unsigned int>("hits_per_pmt"),
      NPMT,
      datacard.get<unsigned int>("TowerID"),
      datacard.get<unsigned int>("EFCMID"),
      start_time
  );

  unsigned short const port = datacard.get<unsigned short>("port");

  boost::asio::io_service service;

  boost::asio::ip::tcp::acceptor server(
      service,
      boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port));

  TRIDAS_LOG(INFO) << "Opened port: " << port;

  while (true) {

    TRIDAS_LOG(INFO) << "Waiting for a client...";

    boost::asio::ip::tcp::socket client(service);

    server.accept(client);

    TRIDAS_LOG(INFO) << "Client " << client.remote_endpoint().address().to_string()
                     << " accepted from port: "
                     << port;

    chrono::nanoseconds const client_time =
        chrono::system_clock::now().time_since_epoch();

    hit_generator.resetTimes(
        client_time
        - program_begin
        + start_time);

    chrono::nanoseconds start_time_bw(client_time);

    size_t sent_size = 0;

    do {
      chrono::nanoseconds start_time =
          chrono::system_clock::now().time_since_epoch();

      hit_generator.updateTimes(time_of_hit);

      boost::system::error_code ec;

      size_t const write_ret_value = boost::asio::write(
          client,
          boost::asio::buffer(hit_generator.getHits(), hit_generator.size()),
          ec);

      if (!write_ret_value || ec) {
        TRIDAS_LOG(ERROR) << "Connection to the HitManager lost.";
        break; // go ahead and accept another connection
      }

      chrono::nanoseconds sleep_time = hit_generator.getDeltaTime()
          - (start_time - chrono::system_clock::now().time_since_epoch());

      if (sleep_time.count() > 0) {
        this_thread::sleep_for(sleep_time);
      } else {
        TRIDAS_LOG(ERROR) << "Process too slow!";
      }

      sent_size += static_cast<size_t>(write_ret_value);
      chrono::nanoseconds const interval =
          chrono::system_clock::now().time_since_epoch() - start_time_bw;

      if (interval >= chrono::seconds(1)) {
        double const throughput_now = (1e9 * sent_size / (1024 * 1024)) / interval.count();

        if (use_monitoring) {
          mon_handler.send(
              mon_server_endpoint,
              throughput.put(throughput_now));
          mon_handler.send(
              mon_server_endpoint,
              ts_id.put(
                  boost::chrono::duration_cast<boost::chrono::milliseconds>(
                      tridas::phase3::getDFHFullTime(
                          *tridas::phase3::dataframeheader_cast(
                              hit_generator.getHits()))).count() / 200));
        } else {
          TRIDAS_LOG(INFO) << "Sent " << throughput_now << " MB/s";
        }
        start_time_bw = chrono::system_clock::now().time_since_epoch();
        sent_size = 0;
      }

    } while (true);
  }
}
