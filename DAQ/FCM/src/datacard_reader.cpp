#include "datacard_reader.hpp"
#include <sstream>

namespace tridas {
namespace fcm {


Configuration getDataCardConfiguration(const std::string& conf_str)
{
  std::stringstream ss(conf_str);

  std::string temp_string;
  unsigned int temp_uint;
  double temp_double;

  Configuration conf;

  // The parameters are written in plain text, separated by spaces in a
  // precise order. Here the parameter list is showed:

  // 1-RawDataFileName
  // 2-Number of hits to load per PMT
  // 3-Rate (in Hz)
  // 4-Port
  // 5-TowerID
  // 6-EFCMID
  // 7-Start time (seconds since the beginning of the 2000) [Optional]

  // Parameter 1
  ss >> temp_string;
  conf.put("filename", temp_string);

  // Parameter 2
  ss >> temp_uint;
  conf.put("hits_per_pmt", temp_uint);

  // Parameter 3
  ss >> temp_double;
  conf.put("single_rate", temp_double);

  // Parameter 4
  ss >> temp_string;
  conf.put("port", temp_string);

  // Parameter 5
  ss >> temp_uint;
  conf.put("TowerID", temp_uint);

  // Parameter 6
  ss >> temp_uint;
  conf.put("EFCMID", temp_uint);

  // Parameter 7
  temp_uint = -1;
  ss >> temp_uint;
  if (temp_uint != static_cast<unsigned>(-1)) {
    conf.put("start_time", temp_uint);
  }

  return conf;
}

}
}
