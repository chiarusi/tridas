#include "TimeOfHit.h"
#include <boost/random/random_device.hpp>

namespace tridas {
namespace fcm {


TimeOfHit::TimeOfHit(double max_delay, double single_rate, unsigned int seed)
  : max_delay_(max_delay),
    // get the seed from a Non-deterministic Uniform Random Number Generator
    engine_(seed),
    dist_(single_rate),
    overflow_(0)
{}

double TimeOfHit::GetDeltaTime()
{
  double const v = dist_(engine_);
  if (v > max_delay_) {
    ++overflow_;
    return max_delay_;
  } else {
    return v;
  }
}

}
}
