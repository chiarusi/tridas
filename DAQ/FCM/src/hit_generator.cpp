#include "hit_generator.hpp"
#include <fstream>
#include <stdexcept>
#include <algorithm>

namespace chrono = boost::chrono;
namespace tridas {
namespace fcm {

HitGenerator::HitGenerator(boost::filesystem::path const& filename,
                           size_t number_of_hits_per_PMT,
                           unsigned int NPMT,
                           unsigned int TowerID,
                           unsigned int EFCMID,
                           const chrono::seconds& secs)
  :
  m_end_times(NPMT, secs),
  m_begin_times(NPMT, secs)
{
  // Loads raw data from file
  loadFromFile(filename, NPMT * number_of_hits_per_PMT);

  size_t i = 0;
  int PMTID = -1;

  // This loop iterates on all the frames (nb, not hits)
  while (i < m_data.size()) {

    tridas::phase3::DataFrameHeader& header = *tridas::phase3::dataframeheader_cast(
        &m_data.front() + i);

    if (!header.FragFlag) {
      PMTID = (PMTID + 1) % NPMT;
      setDFHFullTime(secs, header);
    }

    header.PMTID = PMTID;
    header.EFCMID = EFCMID;
    header.TowerID = TowerID;

    i += sizeof(tridas::phase3::DataFrameHeader) + getDFHPayloadSize(header);
  }
}

tridas::phase3::DataFrameHeader& HitGenerator::at(size_t n)
{
  return *tridas::phase3::dataframeheader_cast(&m_data.front() + m_dfhs[n]);
}

const tridas::phase3::DataFrameHeader& HitGenerator::at(size_t n) const
{
  return *tridas::phase3::dataframeheader_cast(&m_data.front() + m_dfhs[n]);
}

void HitGenerator::loadFromFile(boost::filesystem::path const& filename, size_t nhits)
{
  // Loads part of the file into memory
  std::ifstream input(filename.c_str(), std::ios::binary);

  if (!input) {
    throw std::runtime_error("Error opening file " + filename.string());
  }

  while ((m_dfhs.size() <= nhits) && (!input.eof())) {
    const int pos = input.tellg();

    tridas::phase3::DataFrameHeader header;

    input.read(static_cast<char*>(static_cast<void*>(&header)),
        sizeof(tridas::phase3::DataFrameHeader));

    // If it is a frame at the beginning of an hit or it is the first one
    if ((!header.FragFlag) || (pos == 0)) {
      m_dfhs.push_back(pos);
    }

    // Pass to the next dfh
    input.seekg(getDFHPayloadSize(header), input.cur);
  }

  m_data.resize(m_dfhs.back());
  m_dfhs.pop_back();

  input.seekg(0, input.end);
  size_t filesize = input.tellg();
  input.seekg(0, input.beg);

  if (filesize < m_data.size()) {
    throw std::runtime_error("Input file too small");
  }

  input.read(static_cast<char*>(static_cast<void*>(&m_data.front())),
      m_data.size());

  // ensures that the first frame is a starting frame
  at(0).FragFlag = 0;
}

static
bool day_comparator(T5nsec first, T5nsec second)
{
  tridas::phase3::DataFrameHeader h;

  setDFHFullTime(first, h);
  unsigned const day_first = getDFHDays(h);

  setDFHFullTime(second, h);
  unsigned const day_second = getDFHDays(h);

  return day_second != day_first;
}

void HitGenerator::updateTimes(std::vector<boost::shared_ptr<TimeOfHit> >& generators)
{
  T5nsec const min_begin
      = *std::min_element(m_begin_times.begin(), m_begin_times.end());

  m_begin_times = m_end_times;


  // With C++11 it will be possible to substitute the following 2 lines
  // with a single std::minmax_element.
  T5nsec const max = *std::max_element(m_end_times.begin(), m_end_times.end());
  T5nsec const min = *std::min_element(m_end_times.begin(), m_end_times.end());

  // 10 ms has been chosen as a very good magic number.
  if (max - min >= boost::chrono::milliseconds(10)) {
    resetTimes(max);
  }

  std::vector<T5nsec> day_times(m_end_times.size(), T5nsec(0));

  for (std::size_t i = 0, e = m_end_times.size(); i < e; ++i) {
    tridas::phase3::DataFrameHeader h;
    setDFHFullTime(m_end_times[i], h);
    day_times[i] = getDFHDayTime(h);
  }

  bool const date_change = day_comparator(min_begin, max);

  for (std::size_t i = 0, end = m_dfhs.size(); i < end; ++i) {
    tridas::phase3::DataFrameHeader& header = at(i);
    T5nsec const delta = T5nsec(static_cast<uint64_t>((
        generators[header.PMTID].get()->GetDeltaTime()) * 2e8));

    m_end_times[header.PMTID] += delta;
    day_times[header.PMTID]   += delta;

    if (date_change || day_times[header.PMTID] >= chrono::seconds(86400)) {
      setDFHFullTime(m_end_times[header.PMTID], header);
    } else {
      setDFHDayTime(day_times[header.PMTID], header);
    }
  }
}

void HitGenerator::resetTimes(chrono::nanoseconds const& offset)
{
  for (size_t i = 0, e = m_end_times.size(); i < e; ++i) {
    m_end_times[i] = chrono::duration_cast<T5nsec>(offset);
  }
}

size_t HitGenerator::size() const
{
  return m_data.size();
}

const char* HitGenerator::getHits() const
{
  return &m_data.front();
}

T5nsec HitGenerator::getDeltaTime() const
{
  T5nsec biggest_interval = m_end_times[0] - m_begin_times[0];

  for (size_t i = 1, end = m_end_times.size(); i < end; ++i) {
    T5nsec const interval = m_end_times[i] - m_begin_times[i];
    if (interval > biggest_interval) {
      biggest_interval = interval;
    }
  }

  return biggest_interval;
}

}
}
