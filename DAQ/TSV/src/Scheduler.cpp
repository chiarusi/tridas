#include "Scheduler.h"


namespace tridas {
namespace tsv {


Scheduler::Scheduler(TS_t first_ts_id)
  :
  next_ts_id_(first_ts_id)
{
}

void Scheduler::removeDoneTS(TS_t done_ts_id)
{
  std::map<TS_t, TcpuId>::iterator ts_map_it = ts_map_.find(done_ts_id);
  assert(ts_map_it != ts_map_.end());
  std::map<TcpuId, ssize_t>::iterator tcpu_map_it = tcpu_map_.find(
    ts_map_it->second);
  assert(tcpu_map_it != tcpu_map_.end());
  --tcpu_map_it->second;
  ts_map_.erase(ts_map_it);
}

TS_t Scheduler::getNextTS(TcpuId tcpu_id)
{
  TS_t current_ts_id = next_ts_id_;
  ++next_ts_id_;
  ts_map_[current_ts_id] = tcpu_id;
  std::map<TcpuId, ssize_t>::iterator tcpu_map_it = tcpu_map_.find(tcpu_id);
  if (tcpu_map_it != tcpu_map_.end()) {
    ++tcpu_map_it->second;
  } else {
    tcpu_map_[tcpu_id] = 1;
  }
  return current_ts_id;
}

size_t Scheduler::size()
{
  return ts_map_.size();
}

std::map <TcpuId, ssize_t> Scheduler::getTcpuMap()
{
  return tcpu_map_;
}

}
}
