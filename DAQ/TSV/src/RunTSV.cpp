#include <iostream>
#include <stdexcept>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "Configuration.h"
#include "Configurator.h"
#include "TSVParameters.h"
#include "log.hpp"
#include "Scheduler.h"
#include "inter_objects_communication/producer_consumer.hpp"

#include "version.hpp"

using namespace tridas;
using namespace tridas::tsv;

static long const tcpus_timeout_ms = 1;
static long const hm_timeout_ms = 1;

struct DoneTSMessage {
  TS_t timeslice_id;
  TcpuId tcpu_id;
};

struct NextTSMessage {
  TS_t timeslice_id;
  TcpuId tcpu_id;
};

int main(int argc, char* argv[]) {
  boost::program_options::options_description desc("Options");

  try {

    boost::filesystem::path cardfile;

    desc.add_options()("help,h", "Print help messages.")(
      "version,v",
      "print TriDAS version")(
      "datacard,d",
      boost::program_options::value<boost::filesystem::path>(&cardfile)->required(),
      "Datacard file.");

    boost::program_options::variables_map vm;
    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv).options(desc).run(),
      vm);

    if (vm.count("help")) {
      // what if we should run in background?
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    if (vm.count("version")) {
      std::cout << tridas::version() << '\n';
      return EXIT_SUCCESS;
    }
    boost::program_options::notify(vm);

    if (!boost::filesystem::exists(cardfile)) {
      std::cerr << cardfile.string() << " does not exist\n";
      return EXIT_FAILURE;
    } else if (!boost::filesystem::is_regular_file(cardfile)) {
      std::cerr << cardfile.string() << " is not a regular file\n";
      return EXIT_FAILURE;
    }

    Configuration configuration = read_configuration(cardfile);
    TSVParameters tsv_parameters = get_tsv_parameters(configuration);

    // Configure log
    tridas::Log::init(
        "RunTSV",
        tridas::Log::FromString(
            configuration.get<std::string>("TSV.LOG_LEVEL")));
    if (configuration.get<bool>("TSV.LOG_TO_SYSLOG")) {
      tridas::Log::UseSyslog();
    }

    TRIDAS_LOG(INFO) << "Starting supervisor with pid " << getpid();

    std::vector<network::ProducerPtr> hm_sockets;
    network::Context context;
    network::Consumer tcpu_socket(context);
    Scheduler scheduler(tsv_parameters.start_timeslice_id);

    TRIDAS_LOG(INFO) << "Start TS ID: " << tsv_parameters.start_timeslice_id;

    // Connect to HMs
    for (size_t i = 0; i < tsv_parameters.hms.size(); ++i) {
      network::ProducerPtr p(new network::Producer(context));
      hm_sockets.push_back(p);
      TRIDAS_LOG(DEBUG)
        << "Connecting to HM "
        << tsv_parameters.hms[i].hostname
        << " on port "
        << tsv_parameters.hms[i].port;
      p->tcpConnect(tsv_parameters.hms[i].hostname, tsv_parameters.hms[i].port);
    }

    // Connect to TCPUs
    for (size_t i = 0; i < tsv_parameters.tcpus.size(); ++i) {
      TRIDAS_LOG(DEBUG)
        << "Connecting to TCPU "
        << tsv_parameters.tcpus[i].hostname
        << " on port "
        << tsv_parameters.tcpus[i].port;
      tcpu_socket.tcpConnect(
        tsv_parameters.tcpus[i].hostname,
        tsv_parameters.tcpus[i].port);
    }

    while (true) {
      DoneTSMessage tcpu_msg;

      if (tcpu_socket.timedRecv(
        &tcpu_msg,
        sizeof(tcpu_msg),
        tcpus_timeout_ms)) {

        TRIDAS_LOG(DEBUG)
          << "Received token from TCPU "
          << tcpu_msg.tcpu_id
          << " related to TS "
          << tcpu_msg.timeslice_id;

        // Update map of timeslices (if the TS is not zero)
        if (tcpu_msg.timeslice_id) {
          scheduler.removeDoneTS(tcpu_msg.timeslice_id);
        }
        // Get next timeslice and fill the message for the HMs
        NextTSMessage hm_msg;
        hm_msg.tcpu_id = tcpu_msg.tcpu_id;
        hm_msg.timeslice_id = scheduler.getNextTS(hm_msg.tcpu_id);

        TRIDAS_LOG(DEBUG)
          << "Sending message to HMs: TS "
          << hm_msg.timeslice_id
          << " to TCPU "
          << hm_msg.tcpu_id;

        // Send to all HMs
        for (size_t i = 0; i < hm_sockets.size(); ++i) {
          if (!hm_sockets[i]->timedSend(
            &hm_msg,
            sizeof(hm_msg),
            hm_timeout_ms)) {
            TRIDAS_LOG(WARNING) << "TSV failed sending message to HM " << i;
          }
        }
      }
    }

  } catch (const boost::program_options::error& e) {
    std::cerr << e.what() << std::endl << desc << std::endl;
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  } catch (...) {
    TRIDAS_LOG(ERROR) << "Caught unknown exception";
    return EXIT_FAILURE;
  }
}
