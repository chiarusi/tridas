#include <boost/thread.hpp>  // just to avoid linking errors (it's a mistery!)
#include <boost/detail/lightweight_test.hpp>

#include "Scheduler.h"

#include "tridas_dataformat.hpp"

using namespace tridas::tsv;


int main()
{
  TS_t next_ts_id = 100;

  Scheduler scheduler(next_ts_id);

  {  // Check size, it should be zero
    size_t const expected = 0;
    BOOST_TEST_EQ(scheduler.size(), expected);
  }

  {// Check getNextTS, it should return 100
    TS_t const expected = 100;
    TcpuId const tcpu_id = 0;
    BOOST_TEST_EQ(scheduler.getNextTS(tcpu_id), expected);
  }
  {  // Check size, it should be 1
    size_t const expected = 1;
    BOOST_TEST_EQ(scheduler.size(), expected);
  }

  scheduler.removeDoneTS(100);

  {  // Check size, it should be zero
    size_t const expected = 0;
    BOOST_TEST_EQ(scheduler.size(), expected);
  }

  boost::report_errors();
}
