#ifndef DAQ_TSC_INC_SCHEDULER_H
#define DAQ_TSC_INC_SCHEDULER_H

#include <map>

#include "tridas_dataformat.hpp"

namespace tridas {
namespace tsv {

class Scheduler {

 private:
  std::map <TS_t, TcpuId> ts_map_;
  std::map <TcpuId, ssize_t> tcpu_map_;
  TS_t next_ts_id_;

 public:
  Scheduler(TS_t first_ts_id);
  void removeDoneTS(TS_t done_ts_id);
  TS_t getNextTS(TcpuId tcpu_id);
  std::map <TcpuId, ssize_t> getTcpuMap();
  size_t size();
};

}
}
#endif

