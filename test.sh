#!/bin/sh

OPTS=$(getopt -o dc:f: -n 'test.sh' -- "$@")
[ $? ] || exit 1
eval set -- "${OPTS}"

while true; do
  case "$1" in
    -d) DRYRUN="echo Would run:"; shift ;;
    -c) configuration_file="$2"; shift 2 ;;
    -f) fcm_configuration_file="$2"; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

# paths of TriDAS executables

if [ -n "${TRIDAS_ROOT}" ]; then

  # this is probably an installed environment

  RunTSV_exe="${TRIDAS_ROOT}/bin/RunTSV"
  RunTCPU_exe="${TRIDAS_ROOT}/bin/RunTCPU"
  RunHM_exe="${TRIDAS_ROOT}/bin/RunHM"
  FCMsimu_exe="${TRIDAS_ROOT}/bin/FCMsimu"

else

  # this is probably a development environment

  GUESSED_TRIDAS_ROOT=$(dirname $(readlink -f $0))

  RunTSV_exe=$(find ${GUESSED_TRIDAS_ROOT} -wholename \*TSV/RunTSV | head -1)
  RunTCPU_exe=$(find ${GUESSED_TRIDAS_ROOT} -wholename \*TCPU/RunTCPU | head -1)
  RunHM_exe=$(find ${GUESSED_TRIDAS_ROOT} -wholename \*HM/RunHM | head -1)
  FCMsimu_exe=$(find ${GUESSED_TRIDAS_ROOT} -wholename \*FCM/FCMsimu | head -1)

fi

cat <<EOF
Using executables:
${RunTSV_exe}
${RunTCPU_exe}
${RunHM_exe}
${FCMsimu_exe}
EOF

# do the executables exist?
if [ ! \( -x ${RunTSV_exe} -a -x ${RunTCPU_exe} -a -x ${RunHM_exe} -a -x ${FCMsimu_exe} \) ]
then
  echo "One or more TriDAS executables do not exist or are not executables."
  ls -l ${RunTSV_exe} ${RunTCPU_exe} ${RunHM_exe} ${FCMsimu_exe}
  exit 1
fi

if [ -n "${TRIDAS_ROOT}" ]; then
  export LD_LIBRARY_PATH=${TRIDAS_ROOT}/lib:${LD_LIBRARY_PATH}
fi

if [ -n "${BOOST_ROOT}" ]; then
  export LD_LIBRARY_PATH=${BOOST_ROOT}/lib:${LD_LIBRARY_PATH}
fi

if [ -n "${ZMQ_ROOT}" ]; then
  export LD_LIBRARY_PATH=${ZMQ_ROOT}/lib:${LD_LIBRARY_PATH}
fi

# are the executables ok?
if
  ldd ${RunTSV_exe} | grep -q "not found" ||
  ldd ${RunTCPU_exe} | grep -q "not found" ||
  ldd ${RunHM_exe} | grep -q "not found" ||
  ldd ${FCMsimu_exe} | grep -q "not found"
then
  echo "The TriDAS executables are not valid."
  if [ -z "${TRIDAS_ROOT}" -o -z "${BOOST_ROOT}" -o -z "${ZMQ_ROOT}" ]; then
    echo "Try setting TRIDAS_ROOT and/or BOOST_ROOT and/or ZMQ_ROOT"
  fi
  exit 1
fi

# at this point if GUESSED_TRIDAS_ROOT is set we can assume the guess
# is correct
# moreover create the symlink etc -> datacards because some
# paths in the configuration file use etc
if [ -n "${GUESSED_TRIDAS_ROOT}" ]; then
  TRIDAS_ROOT=${GUESSED_TRIDAS_ROOT}
  ln -s datacards ${GUESSED_TRIDAS_ROOT}/etc 2>/dev/null
fi

# move to the root
cd ${TRIDAS_ROOT}

if [ -z "${configuration_file}" ]; then

  if [ -r ${TRIDAS_ROOT}/etc/configuration_test.json ]; then
    configuration_file=${TRIDAS_ROOT}/etc/configuration_test.json
  else
    echo "Cannot find the configuration file. Try setting it with the -c option."
    exit 1
  fi

fi

if [ -z "${fcm_configuration_file}" ]; then

  if [ -r ${TRIDAS_ROOT}/etc/fcm.ini ]; then
    fcm_configuration_file=${TRIDAS_ROOT}/etc/fcm.ini
  else
    echo "Cannot find the FCM configuration file. Try setting it with the -f option."
    exit 1
  fi

fi

cat <<EOF
Using configuration files:
${configuration_file}
${fcm_configuration_file}
EOF

# if existing processes are running kill them
${DRYRUN} killall -q -9 RunTSV RunTCPU RunHM FCMsimu

# Run TCPU
${DRYRUN} ${RunTCPU_exe} -d ${configuration_file} -i 0 &

sleep 3

# Run HM
${DRYRUN} ${RunHM_exe} -d  ${configuration_file} -i 0 &

sleep 3

# Fix hostname in the fcm configuration file
${DRYRUN} sed -e "s/TESTHOSTNAME/$(hostname -s)/" -i- ${fcm_configuration_file}

# Run FCM
${DRYRUN} ${FCMsimu_exe} -d ${fcm_configuration_file} -s 0 &
${DRYRUN} ${FCMsimu_exe} -d ${fcm_configuration_file} -s 1 &

sleep 5

# Run TSV
${DRYRUN} ${RunTSV_exe} -d ${configuration_file} &

# Wait some time in order to receive data
sleep 45

# Kill processes
${DRYRUN} killall RunTSV RunTCPU RunHM FCMsimu

# give some time to processes to exit
sleep 3

# if some are still alive, kill hard

[ ps -C RunTSV > /dev/null 2>&1 ] && ${DRYRUN} killall -9 RunTSV
[ ps -C RunTCPU > /dev/null 2>&1 ] && ${DRYRUN} killall -9 RunTCPU
[ ps -C RunHM > /dev/null 2>&1 ] && ${DRYRUN} killall -9 RunHM
[ ps -C FCMsimu > /dev/null 2>&1 ] && ${DRYRUN} killall -9 FCMsimu
