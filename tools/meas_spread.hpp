#ifndef TRIDAS_TOOLS_MEAS_SPREAD_HPP
#define TRIDAS_TOOLS_MEAS_SPREAD_HPP
#include <istream>
#include <deque>

void measSpread(std::istream& input,
               int number_of_frames,
               unsigned int offset,
               unsigned int const npmts,
               std::deque<double>& spread);

#endif // TRIDAS_TOOLS_MEAS_SPREAD_HPP
