#include "meas_spread.hpp"
#include "f_dataformat_p3.hpp"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/min.hpp>
namespace baccu = boost::accumulators;

void measSpread(std::istream& input,
               int number_of_frames,
               unsigned int offset,
               unsigned int const npmts,
               std::deque<double>& spread)
{
  int counter = 0;

  while (input && counter != number_of_frames) {
    baccu::accumulator_set<double, baccu::stats<baccu::tag::max, baccu::tag::min > > acc;

    unsigned int i = 0;

    while (i < npmts) {
      tridas::phase3::DataFrameHeader dfh;
      input.read(
          static_cast<char*>(static_cast<void*>(&dfh)),
          sizeof(dfh));

      input.ignore(getDFHPayloadSize(dfh));

      if (dfh.FragFlag) {
        continue;
      }

      if (!offset) {
        acc(getDFHFullTime(dfh).count());

        if (!i) {
          ++counter;
        }
      } else {
        if (!i) {
          --offset;
        }
      }
      ++i;
    }

    if (!offset) {
      spread.push_back(baccu::max(acc) - baccu::min(acc));
    }
  }

}
