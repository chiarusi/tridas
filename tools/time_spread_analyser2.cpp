#include "meas_spread.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <numeric>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

inline
static
void analyse(std::istream& input,
             int number_of_frames,
             unsigned int offset,
             unsigned int const npmts)
{
  std::deque<double> spread;

  int counter = 0;

  while (input) {
    spread.clear();
    measSpread(input, number_of_frames, offset, npmts, spread);
    double const c = std::accumulate(spread.begin(), spread.end(), 0.) / spread.size();
    std::cout << counter++ << ' ' << c * 5e-9 << '\n';
  }
}

int main(int argc, char* argv[]) {
  std::string filename;

  int number_of_frames = 100;

  unsigned int offset = 0;

  po::options_description desc("Options");
  desc.add_options()
      ("help,h", "Print help messages.")
      (
          "filename,f",
          po::value<std::string>(&filename),
          "Raw data file name. If omitted read from stdin.")
      (
          "offset,o" ,
          po::value<unsigned int>(&offset),
          "Number of frames to skip.")
      (
          "number,n",
          po::value<int>(&number_of_frames),
          "Number of frames to parse per bunch.");

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "TimeSpreadAnalyser2: Error: " << e.what() << '\n';
    std::cerr << desc << std::endl;
    return EXIT_FAILURE;
  }

  unsigned int const npmts = 6;

  if (vm.count("filename")) {
    std::ifstream input(filename.c_str(), std::ios::binary);

    analyse(input, number_of_frames, offset, npmts);
  } else {
    analyse(std::cin, number_of_frames, offset, npmts);
  }
}
