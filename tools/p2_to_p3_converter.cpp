#include "f_dataformat_p3.hpp"
#include "f_dataformat_p2.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include <boost/program_options.hpp>

namespace po = boost::program_options;


int main(int argc, char* argv[]) {
  string input_filename;
  string output_filename;

  int nhits = -1;

  po::options_description desc("Options");
  desc.add_options()
      ("help,h",   "Print help messages.")
      ("input,i",  po::value<string>(&input_filename)->required(), "Input data file.")
      ("output,o", po::value<string>(&output_filename)->required(), "Output data file.")
      ("nhits,n",  po::value<int>(&nhits), "Number of hits to collect (default: all hits).");

  try {
    po::variables_map vm;

    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }

    po::notify(vm);
  }
  catch (const po::error& e) {
    cerr << "P2toP3 DataConverter: Error: " << e.what() << endl;
    cerr << desc << endl;
    return -1;
  }


  ifstream input_file (input_filename.c_str() , ios::binary);

  ofstream output_file(output_filename.c_str(), ios::binary);

  int hits_counter = 0;

  while (input_file.good() && hits_counter != nhits) {
    tridas::phase3::DataFrameHeader out_header;
    phase2::DataFrameHeader  in_header;

    char hit_buffer[4096];

    input_file.read(static_cast<char*>(static_cast<void*>(&in_header)), sizeof(in_header));

    // assign out_header from in_header

    // First DWORD
    out_header.SyncBytes = 21930;
    out_header.TowerID   = in_header.TowerID;
    out_header.EFCMID    = in_header.EFCMID;
    out_header.PMTID     = in_header.PMTID;

    // Second DWORD
    out_header.Years        = 20; // 2014
    out_header.Days         = 0;  // 1-gen

    setDFHDayTime(getDFHDayTime(in_header), out_header);

    out_header.ZipFlag      = in_header.ZipFlag;
    out_header.FragFlag     = in_header.FragFlag;
    out_header.Charge       = in_header.Charge;
    out_header.FifoFull     = in_header.FifoFull;
    out_header.NDataSamples = in_header.NDataSample;

    input_file.read(hit_buffer, getDFHPayloadSize(out_header));
    int read_bytes = input_file.gcount();

    output_file.write(static_cast<const char*>(static_cast<const void*>(&out_header)), sizeof(out_header));
    output_file.write(hit_buffer, read_bytes);

    ++hits_counter;
  }

  if (nhits != -1 && nhits != hits_counter) {
    cerr << "Collected " << hits_counter << " hits instead of " << nhits << " because of end-of-file reached!\n";
  }

}
