#include "ptfile_reader.hpp"

#include <iostream>
#include <fstream>
#include <cassert>
#include <boost/program_options.hpp>

using namespace tridas::post_trigger;

int main(int argc, char* argv[])
{
  std::string cardfile;
  std::string ptfile;
  bool write_dc = false;

  boost::program_options::options_description desc("Options");

  desc.add_options()
      ("help,h", "Print this help and exit.")
      ("extract-dc,d", boost::program_options::value<std::string>(&cardfile), "Extract the datacard file.")
      ("ptfile,p", boost::program_options::value<std::string>(&ptfile)->required(), "PostTrigger file to analyse.");

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return EXIT_SUCCESS;
    }

    write_dc = vm.count("extract-dc");

    boost::program_options::notify(vm);
  } catch (boost::program_options::error const& e) {
    std::cerr << e.what() << '\n' << desc << '\n';
    return EXIT_FAILURE;
  }

  PtFileReader<sample::uncompressed> pt_reader(ptfile);

  if (write_dc) {
    std::ofstream datacard(cardfile.c_str());
    assert(datacard && "Error opening datacard file for writing.");

    datacard << pt_reader.datacard();
  }

  std::size_t nevent = 0;

  int count = 0;

  for (PtFileReader<sample::uncompressed>::iterator it = pt_reader.begin(), et = pt_reader.end(); it != et; ++it) {
    TimeSlice<sample::uncompressed> const ts = *it;
    nevent += ts.nEvents();
    std::cout << "TS " << ++count << "/" << pt_reader.nTS() << '\n'
              << "TS ID: " << ts.id() << '\n'
              << "Number of events: " << ts.nEvents() << '\n';
  }

  assert(pt_reader.nEvents() == nevent);
}
