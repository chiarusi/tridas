#include "ptfile_reader.hpp"
#include <cassert>
#include <vector>
#include <algorithm>
#include <boost/detail/lightweight_test.hpp>

int main(int argc, char* argv[])
{
  assert(argc == 2 && "No pt file specified");
  BOOST_TEST_THROWS(
      tridas::post_trigger::PtFileReader<tridas::post_trigger::sample::none>(
          "/non_existent_pt_file"),
      std::runtime_error);


  {
    using namespace tridas::post_trigger;
    typedef sample::uncompressed Sample;
    PtFileReader<Sample> const r(argv[1]);
    std::vector< TimeSlice<Sample> > v;

    for (PtFileReader<Sample>::const_iterator it = r.begin(), et = r.end(); it != et; ++it) {
      v.push_back(*it);
    }

    BOOST_TEST_EQ(v.size(), r.nTS());
  }

  boost::report_errors();
}
