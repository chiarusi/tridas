#include "f_dataformat_p3.hpp"

#include <fstream>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
namespace po = boost::program_options;
using namespace tridas;

void parseData(std::istream& input,
               int number_of_frames,
               unsigned int offset)
{
  int counter = 0;

  while (input && counter != number_of_frames) {
    phase3::DataFrameHeader dfh;
    input.read(
        static_cast<char*>(static_cast<void*>(&dfh)),
        sizeof(dfh));

    if (input.gcount() != sizeof(dfh)) {
      break;
    }

    assert(testDFHSync(dfh));

    input.ignore(getDFHPayloadSize(dfh));

    if (!offset) {
      std::cout << "-------------------------------------------------\n"
                << "Counter: " << ++counter << '\n'
                << dfh << '\n';

    } else {
      --offset;
    }
  }
}

int main(int argc, char* argv[])
{
  std::string filename;

  int number_of_frames = -1;

  unsigned int offset = 0;

  po::options_description desc("Options");
  desc.add_options()
      ("help,h",     "Print help messages.")
      (
          "filename,f",
          po::value<std::string>(&filename),
          "Raw data file name. If omitted read from stdin.")
      (
          "offset,o" ,
          po::value<unsigned int>(&offset),
          "Number of frames to skip.")
      (
          "number,n",
          po::value<int>(&number_of_frames),
          "Number of frames to parse.");

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return EXIT_SUCCESS;
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "Phase 3 FrameParser: Error: " << e.what() << '\n';
    std::cerr << desc << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("filename")) {
    std::ifstream input(filename.c_str(), std::ios::binary);

    parseData(input, number_of_frames, offset);
  } else {
    parseData(std::cin, number_of_frames, offset);
  }
}
