#include <iostream>
#include <boost/program_options.hpp>
#include <boost/chrono.hpp>
#include <boost/asio.hpp>

#include "inter_objects_communication/stream_receiver.hpp" // for make_endpoint
#include "f_dataformat_p3.hpp"

int main(int argc, char* argv[])
{
  int run_duration = -1;
  int nhit_for_print = 10000;
  std::string address("localhost");
  std::string port;

  boost::program_options::options_description desc("Options");

  desc.add_options()
      ("help,h", "Print this help and exit.")
      (
          "duration,d",
          boost::program_options::value<int>(&run_duration)->default_value(run_duration),
          "Number of seconds to wait before closing the program.\
Set to -1 for infinite, which is also the default.")
      (
          ",n",
          boost::program_options::value<int>(&nhit_for_print)->default_value(nhit_for_print),
          "Number of hits to collect before printing one line.")
      (
          "host,a",
          boost::program_options::value<std::string>(&address)->default_value(
              address),
          "IP address or hostname of the data producer.")
      (
          "port,p",
          boost::program_options::value<std::string>(&port)->required(),
          "TCP port of the data producer.");

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(
            argc,
            argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return EXIT_SUCCESS;
    }

    boost::program_options::notify(vm);
  } catch (boost::program_options::error const& e) {
    std::cerr << e.what() << '\n' << desc << '\n';
    return EXIT_FAILURE;
  }

  boost::asio::io_service service;
  boost::asio::ip::tcp::socket sock(service);

  sock.connect(network::make_endpoint(service, address, port));

  std::vector<char> dummy_buffer;

  std::cout << "# <count> <Machine time [5 ns]> <Data time [5 ns]>\n";

  boost::chrono::system_clock::time_point const stop =
      boost::chrono::system_clock::now() +
      boost::chrono::seconds(
          run_duration < 0
             ? std::numeric_limits<unsigned int>::max()
             : run_duration);

  unsigned int count = 0;

  while (stop > boost::chrono::system_clock::now()) {
    tridas::phase3::DataFrameHeader header;

    {
      char *const data = static_cast<char*>(static_cast<void*>(&header));
      boost::asio::read(sock, boost::asio::buffer(data, sizeof(header)));
    }

    boost::chrono::system_clock::time_point const now = boost::chrono::system_clock::now();

    if (count % nhit_for_print == 0) {
      std::cout
          << count << ' '
          << now.time_since_epoch().count() / 5 << ' '
          << tridas::phase3::getDFHFullTime(header).count() << std::endl;
    }

    if (!tridas::phase3::subsequent(header)) {
      ++count;
    }

    std::size_t const payload_size = tridas::phase3::getDFHPayloadSize(header);

    dummy_buffer.resize(payload_size);

    boost::asio::read(
        sock,
        boost::asio::buffer(&dummy_buffer.front(),
        payload_size));
  }
  sock.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
  sock.close();
}
