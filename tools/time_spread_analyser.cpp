#include "meas_spread.hpp"

#include <fstream>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main(int argc, char* argv[]) {
  std::string filename;

  int number_of_frames = -1;

  unsigned int offset = 0;

  po::options_description desc("Options");
  desc.add_options()
      ("help,h", "Print help messages.")
      (
          "filename,f",
          po::value<std::string>(&filename),
          "Raw data file name. If omitted read from stdin.")
      (
          "offset,o" ,
          po::value<unsigned int>(&offset),
          "Number of frames to skip.")
      (
          "number,n",
          po::value<int>(&number_of_frames),
          "Number of frames to parse.");

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "TimeSpreadAnalyser: Error: " << e.what() << '\n';
    std::cerr << desc << std::endl;
    return EXIT_FAILURE;
  }

  unsigned int const npmts = 6;

  std::deque<double> spread;

  if (vm.count("filename")) {
    std::ifstream input(filename.c_str(), std::ios::binary);

    measSpread(input, number_of_frames, offset, npmts, spread);
  } else {
    measSpread(std::cin, number_of_frames, offset, npmts, spread);
  }

  for (unsigned int i = 0, e = spread.size(); i != e; ++i) {
    std::cout << i << ' ' <<  spread[i] << '\n';
  }
}
