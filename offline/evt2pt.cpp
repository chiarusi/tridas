/*
 * evt2pt   This program allows the conversion from the FEMSim .evt ascii
 *          format to the NEMO Phase 3 post-trigger file format.
 */

#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <string>
#include <fstream>

#include "femsim_parser.hpp"
#include "pmt_numbering.hpp"

namespace femsim = tridas::femsim;
namespace strict_numbering = tridas::strict_numbering;

int main(int argc, char* argv[])
{
  std::string femfilename;
  std::string ptfilename;
  int npmts_per_floor = 6;
  int nfloors_per_tower = 14;

  boost::program_options::options_description desc("Options");

  desc.add_options()
      ("help,h", "Prints this help and exit.")
      (
          "fem,i",
          boost::program_options::value<std::string>(&femfilename)->required(),
          "Input FEMSim evt file name.")
      (
          "pt,o",
          boost::program_options::value<std::string>(&ptfilename)->required(),
          "Output pt-file name.")
      (
          "pmts",
          boost::program_options::value<int>(&npmts_per_floor)->default_value(npmts_per_floor),
          "Set the number of PMTs per floor.")
      (
          "floors",
          boost::program_options::value<int>(&nfloors_per_tower)->default_value(nfloors_per_tower),
          "Set the number of floors per tower.");

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv).options(desc).run(),
        vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return EXIT_SUCCESS;
    }

    boost::program_options::notify(vm);
  } catch (boost::program_options::error const& e) {
    std::cerr << e.what() << '\n' << desc << '\n';
    return EXIT_FAILURE;
  }

  {
    boost::system::error_code ec;
    bool const exists = boost::filesystem::exists(femfilename, ec);

    if (ec) {
      std::cerr << "Error opening " << femfilename << ": " << ec.message() << '\n';
      return EXIT_FAILURE;
    }

    if (!exists) {
      std::cerr << "Error opening " << femfilename << ": No such file or directory\n";
      return EXIT_FAILURE;
    }
  }

  std::ifstream input(femfilename.c_str());
  assert(input);

  boost::property_tree::ptree const fem_header = femsim::read_sim_header(input);

  std::ifstream::pos_type const start_events = input.tellg();

  boost::property_tree::ptree const meta = femsim::read_metadata(input);

  input.seekg(start_events);

  boost::property_tree::ptree datacard;

  datacard.put_child("header", fem_header);
  datacard.put_child("events", meta);


  tridas::phase3::PTHeader pt_header = {
    datacard.get<uint32_t>("header.start_run"),
    0,
    0,
    0,
    0,
    0,
    0
  };

  std::ofstream output(ptfilename.c_str());

  assert(output);

  write(output, &pt_header);

  std::ofstream::pos_type const start_datacard = output.tellp();
  write_json(output, datacard);
  int const datacard_size = output.tellp() - start_datacard;

  strict_numbering::PmtNumbers geo(npmts_per_floor, nfloors_per_tower);

  int ts_count = 0;

  while (input && input.peek() != EOF) {
    femsim::convert_event(input, output, geo);
    ++ts_count;
  }

  pt_header.datacard_size = datacard_size;
  pt_header.number_of_timeslices = ts_count;
  pt_header.number_of_events = ts_count;
  pt_header.effective_file_size = output.tellp();

  output.seekp(0);
  write(output, &pt_header);
}
