#include <iostream>
#include <tools/ptfile_reader.hpp>
#include <boost/thread.hpp>
#include <boost/program_options.hpp>
#include "log.hpp"
#include "trig_sim_details.hpp"
#include "pmt_numbering.hpp"
#include "TowerTimeSlice.h"

// For EM capabilities
#include <DAQ/EM/inc/FileWriter.h>

// For TSV capabilities
#include "inter_objects_communication/producer_consumer.hpp"

// For HM configuration
#include "HMParameters.h"

#include "subprocess.hpp"

typedef tridas::post_trigger::sample::uncompressed Sample;

// Copied from RunTSV.cpp
struct DoneTSMessage {
  TS_t timeslice_id;
  TcpuId tcpu_id;
};

namespace tpt = tridas::post_trigger;
namespace po = boost::program_options;
namespace em = tridas::em;

namespace strict_numbering = tridas::strict_numbering;

using namespace tridas::femsim;

int main(int argc, char* argv[])
{
  int n_worker_threads = 2;
  int n_sectors = 1;
  std::vector<std::string> trigger_parameters;
  std::string inptfile;
  std::string outptfile;
  std::string detfile;
  std::string tcpu_path;
  std::string plugin_path;
  std::string cardfile("datacard.json");

  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages.")
    (
        "tcpu",
        po::value<std::string>(&tcpu_path)
            ->required()
            ->value_name("filename"),
        "Path of the RunTCPU executable.")
    (
        "plugin-dir,p",
        po::value<std::string>(&plugin_path)
            ->required()
            ->value_name("dirname"),
        "Path of the TCPU plugins.")
    (
        "datacard",
        po::value<std::string>(&cardfile)
            ->default_value(cardfile)
            ->value_name("filename"),
        "Path of the datacard (it is output, not input!).")
    (
        "workers,w",
        po::value<int>(&n_worker_threads)
            ->default_value(n_worker_threads)
            ->value_name("n"),
        "Set the number of TCPU worker threads.")
    (
        "sectors,n",
        po::value<int>(&n_sectors)
            ->default_value(n_sectors)
            ->value_name("n"),
        "Set the number of sectors == number of HM to simulate.")
    (
        "input,i",
        po::value<std::string>(&inptfile)
            ->required()
            ->value_name("filename"),
        "Input Post Trigger file.")
    (
        "detectorfile,d",
        po::value<std::string>(&detfile)
            ->required()
            ->value_name("filename"),
        "Detector file used to produce the simulated data.")
    (
        "output,o",
        po::value<std::string>(&outptfile)
            ->required()
            ->value_name("filename"),
        "Output Post Trigger file.")
    (
        "trig,t",
        po::value< std::vector<std::string> >(&trigger_parameters)
            ->required()
            ->multitoken()
            ->value_name("params"),
        "Set the triggers to be activated and their parameters. See the reference.");

  try {
    po::variables_map vm;

    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

    po::notify(vm);
  } catch (const po::error& e) {
    std::cerr << "TrigSim: Error: " << e.what() << '\n';
    std::cerr << desc << std::endl;
    return EXIT_FAILURE;
  } catch (const std::exception& e) {
    std::cerr << "TrigSim: Error: " << e.what() << '\n';
    return EXIT_FAILURE;
  }

  // Initialise the logger
  tridas::Log::init("TrigSim", tridas::Log::INFO);

  // Prepare the input data
  strict_numbering::PmtId pmt_id(6, 14);

  // Create the datacard
  Configuration const conf = make_offline_datacard(
      n_sectors,
      n_worker_threads,
      detfile,
      plugin_path);

  // Write the datacard to file
  write_json(cardfile, conf);

  // Launch the TCPU
  std::string const tcpu_cmd_line =
      tcpu_path
      + " -i 0 -d "
      + cardfile
      + " 2>stderr.txt 1>stdout.txt";

  Subprocess tcpu_process(tcpu_cmd_line);

  network::Context context;
  network::Consumer tsv2tcpu(context);
  std::vector<network::ProducerPtr> hm2tcpu;
  network::Consumer tcpu2em(context);

  tsv2tcpu.tcpConnect(
      "localhost",
      conf.get<std::string>("TCPU.BASE_CTRL_PORT"));

  for (int i = 0; i < n_sectors; ++i) {
    hm2tcpu.push_back(network::ProducerPtr(new network::Producer(context)));
    hm2tcpu[i]->tcpConnect(
        "localhost",
        conf.get<std::string>("TCPU.BASE_DATA_PORT"));
  }

  tcpu2em.tcpBind(conf.get<std::string>("EM.DATA_PORT"));

  em::FileWriter filewriter(
      tcpu2em,
      conf.get<int>("INTERNAL_SW_PARAMETERS.DELTA_TS"),
      "localhost",
      9999,
      1,
      time(0),
      std::numeric_limits<std::size_t>::max(),
      cardfile,
      outptfile);

  boost::thread em_th(boost::ref(filewriter));

  tpt::PtFileReader<Sample> const reader(inptfile);

  int const nts = reader.nTS();
  int const step = nts < 10 ? 1 : nts / 10;

  int ts_count = 0;

  int const npmt_sect = 8 * 14 * 6 / n_sectors;

  TRIDAS_LOG(INFO) << "Starting!\n";

  for (
      tpt::PtFileReader<Sample>::const_iterator it = reader.begin(), et = reader.end();
      it != et;
      ++it) {

    tpt::TimeSlice<Sample> const ts = *it;

    // convert TS to a number of STS
    std::map<int, std::vector<unsigned char> > const tts = convert(ts, pmt_id);

    TRIDAS_LOG(INFO) << "Number of PMTs in next TS: " << tts.size() << '.';

    // The following is just to block until a token has been received.
    {
      DoneTSMessage tsmessage;
      tsv2tcpu.timedRecv(&tsmessage, sizeof(tsmessage), -1);
    }

    TRIDAS_LOG(INFO) << "Sending TS " << ts.id() << '.';

    for (int i = 0; i < n_sectors; ++i) {
      std::map<int, std::vector<unsigned char> >::const_iterator const lower
          = tts.lower_bound(i * npmt_sect);

      std::map<int, std::vector<unsigned char> >::const_iterator const upper
          = tts.upper_bound((i + 1) * npmt_sect);

      std::size_t sent_size =
          send(lower, upper, ts.id(), i, npmt_sect, *hm2tcpu[i]);

      TRIDAS_LOG(INFO) << "Sent " << sent_size << " Bytes";
    }

    TRIDAS_LOG(INFO) << "TS " << ts.id() << " sent.";

    ++ts_count;
    if (ts_count % step == 0) {
      double const percentage = (100. * ts_count) / nts;
      TRIDAS_LOG(INFO) << "Work status: " << std::setprecision(2) << percentage;
    }
  }

  // The following is just to block until a token has been received.
  for (int i = 0; i < 4; ++i) {
    DoneTSMessage tsmessage;
    tsv2tcpu.timedRecv(&tsmessage, sizeof(tsmessage), -1);
  }

  tcpu_process.kill(2);

  filewriter.stop();
  em_th.join();
}
