#include "femsim_parser.hpp"
#include <cassert>
#include <fstream>
#include <boost/detail/lightweight_test.hpp>



void dummy_geometry(int id, tridas::phase3::DataFrameHeader& header)
{
  assert(id < 16);
  header.PMTID = id;
  header.EFCMID = id;
  header.TowerID = id;
}

namespace femsim = tridas::femsim;

int main(int argc, char* argv[])
{
  // Test write()
  {
    std::ofstream not_opened;
    int const string_size = 4;
    char const str[string_size] = {'c', 'i', 'a', 'o'};
    BOOST_TEST_THROWS(write(not_opened, str, string_size), std::runtime_error);
  }
  {
    std::ostringstream oss;
    int const string_size = 4;
    char const str[string_size] = {'c', 'i', 'a', 'o'};
    std::size_t const size = write(oss, str, string_size);
    BOOST_TEST_EQ(size, string_size);
    BOOST_TEST_EQ(std::string(str, string_size), oss.str());
  }

  // Test make_dfh()
  {
    int const charge = 8;
    int const nsamples = 10;
    tridas::phase3::fine_time const time(94176000000000000);
    tridas::phase3::DataFrameHeader const dfh = femsim::make_dfh(
        6,
        charge,
        nsamples,
        time,
        dummy_geometry);

    BOOST_TEST_EQ(tridas::phase3::getDFHCharge(dfh), charge);
    BOOST_TEST_EQ(tridas::phase3::getDFHNSamples(dfh), nsamples);
    BOOST_TEST_EQ(tridas::phase3::getDFHFullTime(dfh), time);
  }

  // Test read_item()
  {
    std::string const first = "first ";
    std::string const second = " second ";
    std::istringstream iss(first + ":" + second);

    std::pair<std::string, std::string> const item = femsim::read_item(iss);

    BOOST_TEST_EQ(item.first, first);
    BOOST_TEST_EQ(item.second, second);
  }

  boost::report_errors();
}

