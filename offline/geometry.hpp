#ifndef TRIDAS_OFFLINE_GEOMETRY_HPP
#define TRIDAS_OFFLINE_GEOMETRY_HPP

#include "detector.hpp"
#include <map>
#include <string>
namespace tridas {
namespace femsim {


inline
std::map<int, tridas::datacard::element::Tower> parseGeometry(
    std::istream& input,
    int npmt_floor,
    int nfloor_tower)
{

  using tridas::datacard::element::Tower;
  using tridas::datacard::element::Floor;
  using tridas::datacard::element::PMT;

  std::map<int, Tower> towers;

  // 9 campi per riga
  std::string line;
  while (getline(input, line)) {
    std::istringstream iss(line);

    int abs_id = 0;
    float x = 0, y = 0, z = 0;
    float cx = 0, cy = 0, cz = 0;
    float dummy;
    int pos_tower_id;

    iss >> abs_id >> x >> y >> z >> cx >> cy >> cz >> dummy >> pos_tower_id;

    if (abs_id == 0) {
      break;
    }

    --abs_id;

    // This should be calculated in the same way as in trigsim
    int const tower_id = abs_id / (nfloor_tower * npmt_floor);
    int const floor_id = abs_id / npmt_floor + 1 - nfloor_tower * tower_id;
    int const pmt_id = abs_id - npmt_floor * (floor_id - 1 + tower_id * nfloor_tower);

    assert(pos_tower_id - 1 == tower_id);

    std::map<int, Tower>::iterator const it
        = towers.insert(std::make_pair(tower_id, Tower(tower_id))).first;

    Tower::iterator fit = it->second.begin();
    while (fit != it->second.end()) {
      if (fit->second.id() == floor_id) {
        break;
      } else {
        ++fit;
      }
    }

    if (fit == it->second.end()) {
      fit = it->second.add_floor(Floor(floor_id)).first;
    }

    fit->second.add_pmt(PMT(pmt_id, x, y, z, cx, cy, cz, 5, 5, 25));
  }

  return towers;
}

}
}

#endif // TRIDAS_OFFLINE_GEOMETRY_HPP
