#include "femsim_parser.hpp"

namespace tridas {
namespace femsim {

boost::property_tree::ptree read_sim_header(std::istream &input) {
  boost::property_tree::ptree tree;

  while (true) {
    std::pair<std::string, std::string> const item = read_item(input);

    if (item.first == "end_event") {
      break;
    }

    // This ptree "remap" is needed for some reason that I don't understand,
    // which is consequence of the C++11 std::pair.
    // in C++03 it was sufficient to do
    // tree.push_back(item);

    boost::property_tree::ptree temp;
    temp.put("", item.second);

    tree.push_back(std::make_pair(item.first, temp));
  }

  return tree;
}

boost::property_tree::ptree read_metadata(std::istream &input) {
  boost::property_tree::ptree tree;

  while (input && input.peek() != EOF) {

    std::pair<std::string, std::string> item = read_item(input);
    assert(item.first == "start_event");

    std::string const event_id = item.second;

    boost::property_tree::ptree event;

    while (item.first != "end_event") {
      if (item.first != "hit" && item.first != "hit_fem") {
        // This ptree "remap" is needed for some reason that I don't understand,
        // which is consequence of the C++11 std::pair.
        // in C++03 it was sufficient to do
        // event.push_back(item);
        boost::property_tree::ptree temp;
        temp.put("", item.second);

        event.push_back(std::make_pair(item.first, temp));
      }
      item = read_item(input);
    }

    tree.put_child(event_id, event);
  }

  return tree;
}

} // ns fem
}