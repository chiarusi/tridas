#include <boost/detail/lightweight_test.hpp>
#include "pmt_numbering.hpp"

int main()
{
  {
    using namespace tridas::strict_numbering;

    int const npmt_per_floor = 6;
    int const nfloor_per_tower = 14;
    int const ntowers = 8;

    PmtId pmtid(npmt_per_floor, nfloor_per_tower);

    PmtNumbers pmtnum(npmt_per_floor, nfloor_per_tower);

    for (int i = 0; i < npmt_per_floor * nfloor_per_tower * ntowers; ++i) {
      DataFrameHeader dfh;

      pmtnum(i, dfh);

      int const id = pmtid(dfh);

      BOOST_TEST_EQ((dfh.PMTID < npmt_per_floor), true);
      BOOST_TEST_EQ((dfh.EFCMID <= nfloor_per_tower), true);
      BOOST_TEST_EQ((dfh.TowerID <= ntowers), true);

      BOOST_TEST_EQ(i, id);
    }
  }

  boost::report_errors();
}
