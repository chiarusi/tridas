#include "trig_sim_details.hpp"
#include "datacard.hpp"
#include "SectorTimeSlice.h"
#include "geometry.hpp"
#include <fstream>

namespace tridas {
namespace femsim {

Configuration make_offline_datacard(
    int n_sectors,
    int n_worker_threads,
    boost::filesystem::path const& detfile,
    boost::filesystem::path const& plugin_path)
{
  Configuration conf;

  tridas::datacard::element::DetectorGeometry const dg(6, 14, 8);

  tridas::datacard::element::InternalSwParameters const isp(
      200,                        // Delta TS
      1000000,                    // PMT buffer size
      0,                          // STS ready time-out
      1,                          // TTS ready in memory
      0);                         // STS in memory

  tridas::datacard::element::HmEndpoints hme;

  hme.add_endpoint("localhost", n_sectors);

  tridas::datacard::element::HM const hm("DEBUG", 3000, 0, "", 0, hme);

  tridas::datacard::element::TcpuEndpoints tcpue;

  tcpue.add_endpoint("localhost", "localhost", 1);

  tridas::datacard::element::TriggerParameters const trigger_parameters(
      600,
      4,
      20,
      500,
      1,
      5,
      20000);

  Configuration scale_factor;
  scale_factor.put("SCALE_FACTOR", "1");

  tridas::datacard::element::Plugin const validate_l1(
      "ALLGOOD",
      "TrigScaler",
      0,
      scale_factor);


  tridas::datacard::element::Plugins plugins;

  plugins.add(validate_l1);

  tridas::datacard::element::TCPU const tcpu(
      "DEBUG",
      3100,
      3200,
      plugin_path.string(),
      n_worker_threads,
      tcpue,
      trigger_parameters,
      plugins);

  tridas::datacard::element::EM const em(
      "localhost",
      "localhost",
      3300,
      1,
      "DEBUG",
      false,
      0,
      "",
      "");

  tridas::datacard::element::Monitor monitor("localhost", 9999, 2);

  std::ifstream geomfile(detfile.string().c_str());

  std::map<int, tridas::datacard::element::Tower> const detector
      = parseGeometry(geomfile, 6, 14);

  conf & dg & isp & hm & tcpu & em & monitor;

  // PLEASE: Let's allow us to use range-for!
  typedef std::map<int, tridas::datacard::element::Tower>::const_iterator iter;

  for (iter it = detector.begin(), et = detector.end(); it != et; ++it) {
    conf & it->second;
  }

  return conf;
}

static
std::pair<int, int> parse_buffer(std::vector<unsigned char> const& buffer)
{
  int nframes = 0;
  int nhits = 0;

  unsigned int pos = 0;
  while (pos < buffer.size()) {
    DataFrameHeader const * const head
        = dataframeheader_cast(&buffer.front() + pos);

    ++nframes;

    if (!subsequent(*head)) {
      ++nhits;
    }

    pos += sizeof(*head) + getDFHPayloadSize(*head);
  }

  return std::make_pair(nhits, nframes);
}

static
s_HeaderInfoSTS make_sts_header(
    std::size_t ts_id,
    std::size_t sector_id,
    int npmt_sect,
    std::map<int, std::vector<unsigned char> >::const_iterator begin,
    std::map<int, std::vector<unsigned char> >::const_iterator end)
{
  s_HeaderInfoSTS header;

  header.id_ = ts_id;
  header.sector_ = sector_id;
  header.pmtin_ = npmt_sect;

  int const offset = npmt_sect * sector_id;

  for (unsigned int i = 0; i < NMAXPMT; ++i) {
    header.pmtlist_[i] = false;
    header.BufSize_[i] = 0;
    header.nhit_[i] = 0;
    header.nframe_[i] = 0;
  }

  for (; begin != end; ++begin) {
    int const index = begin->first - offset;
    header.pmtlist_[index] = true;
    header.BufSize_[index] = begin->second.size();

    std::pair<int, int> const numbers = parse_buffer(begin->second);

    header.nhit_[index] = numbers.first;
    header.nframe_[index] = numbers.second;
  }

  assert(header.pmtin_ <= NMAXPMT);

  return header;
}

std::size_t send(
    std::map<int, std::vector<unsigned char> >::const_iterator begin,
    std::map<int, std::vector<unsigned char> >::const_iterator end,
    std::size_t ts_id,
    std::size_t sector_id,
    int npmt_sect,
    network::Producer& socket)
{
  s_HeaderInfoSTS const header = make_sts_header(
      ts_id,
      sector_id,
      npmt_sect,
      begin,
      end);

  std::size_t sent = 0;

  sent += socket.timedSendMore(
      static_cast<char const*>(static_cast<void const*>(&header)),
      sizeof(header),
      -1);

  std::map<int, std::vector<unsigned char> >::const_iterator last = end;

  --last;

  for (; begin != last; ++begin) {
    unsigned char const*const buffer = &begin->second.front();
    std::size_t const buffer_size = begin->second.size();

    sent += socket.timedSendMore(
        buffer,
        buffer_size,
        -1);
  }

  unsigned char const*const buffer = &last->second.front();
  std::size_t const buffer_size = last->second.size();

  sent += socket.timedSend(
            buffer,
            buffer_size,
            -1);

  {
    std::size_t size_of_sts = 0;

    for (unsigned int i = 0; i < NMAXPMT; ++i) {
      size_of_sts += header.BufSize_[i];
    }
    assert(sent == size_of_sts + sizeof(header));
  }

  return sent;
}

}
}
