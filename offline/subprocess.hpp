#ifndef TRIDAS_OFFLINE_SUBPROCESS_HPP
#define TRIDAS_OFFLINE_SUBPROCESS_HPP

// Very basic subprocess class
// It helps in launching a program in a child process
// acting very like the system (man 3 system) function, but running
// concurrently and with kill capability.

#include <unistd.h>
#include <signal.h>
#include <string>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <sys/wait.h>

namespace tridas {
namespace femsim {


class Subprocess
{
  pid_t m_pid;

 public:

  explicit
  Subprocess(std::string const& cmd_line)
  {
    m_pid = fork();

    if (m_pid == -1) {
      throw std::runtime_error(
        std::string("fork() error: ") + strerror(errno));
    }

    if (m_pid == 0) {
      // see man 3 system
      execl("/bin/sh", "sh", "-c", cmd_line.c_str(), (char*) 0);
    }
  }

  void kill(int signum = SIGTERM)
  {
    if (m_pid) {
      ::kill(m_pid, signum);
    } else {
      throw std::runtime_error(
        "Subprocess cannot be killed from within the subprocess itself.");
    }
  }

  bool isAlive() const
  {
    if (m_pid) {
      int const result = ::kill(m_pid, 0);

      bool pstatus = false;

      if (result == -1) {
        if (errno != ESRCH) {
          throw std::runtime_error(
            std::string("Subprocess error: ") + strerror(errno));
        } else {
          pstatus = false;
        }
      } else {
        pstatus = true;
      }

      return pstatus;
    } else {
      // You ARE running.
      return true;
    }
  }

  ~Subprocess()
  {
    if (m_pid) {
      waitpid(m_pid, 0, 0);
    }
  }
};

}
}
#endif // TRIDAS_OFFLINE_SUBPROCESS_HPP
