#include "subprocess.hpp"
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/detail/lightweight_test.hpp>

int main()
{
  // Test the command is actually executed
  using namespace tridas::femsim;
  {
    boost::chrono::seconds const sleep_time(10);
    boost::chrono::system_clock::time_point const start =
        boost::chrono::system_clock::now();

    // in this way it should work just like the "system" (man 3 system)
    // function.

    {
      Subprocess const sleep_p("sleep 10");
    }

    boost::chrono::system_clock::time_point const stop =
        boost::chrono::system_clock::now();

    boost::chrono::seconds const duration
        = boost::chrono::duration_cast<boost::chrono::seconds>(stop - start);

    BOOST_TEST_EQ(sleep_time, duration);
  }

  // Test the kill command
  {
    boost::chrono::seconds const sleep_time(2);
    boost::chrono::system_clock::time_point const start =
        boost::chrono::system_clock::now();

    {
      Subprocess sleep_p("sleep 10");

      // This duality of duration types in boost really sucks.
      boost::posix_time::seconds const posix_sleep_time(sleep_time.count());

      boost::this_thread::sleep(posix_sleep_time);

      sleep_p.kill();
    }

    boost::chrono::system_clock::time_point const stop =
        boost::chrono::system_clock::now();

    boost::chrono::seconds const duration
        = boost::chrono::duration_cast<boost::chrono::seconds>(stop - start);

    BOOST_TEST_EQ(sleep_time, duration);
  }

  // Test the isAlive command
  {
    Subprocess const sleep_p("sleep 1");
    bool const is_alive = sleep_p.isAlive();

    BOOST_TEST_EQ(is_alive, true);
  }

  boost::report_errors();
}
