#ifndef TRIDAS_OFFLINE_TRIGSIM_DETAILS_HPP
#define TRIDAS_OFFLINE_TRIGSIM_DETAILS_HPP

#include "Configuration.h"
#include <string>
#include <boost/filesystem.hpp>

#include "tools/ptfile_reader.hpp"
#include "inter_objects_communication/producer_consumer.hpp"

namespace tridas {
namespace femsim {

template<typename Ider>
std::map<int, std::vector<unsigned char> > convert(
    tridas::post_trigger::TimeSlice<tridas::post_trigger::sample::uncompressed> const& ts,
    Ider pmtid)
{
  using namespace tridas::post_trigger;
  using namespace tridas::phase3;

  std::map<int, std::vector<unsigned char> > pmt_buffer_map;

  for (
      TimeSlice<sample::uncompressed>::const_iterator tit = ts.begin(), tet = ts.end();
      tit != tet;
      ++tit) {
    for (
        Event<sample::uncompressed>::const_iterator eit = tit->begin(), eet = tit->end();
        eit != eet;
        ++eit) {
      Hit<sample::uncompressed> const& hit = *eit;

      //assert(!tridas::phase3::compressed(hit.frameHeader(0)) && "");

      int const id = pmtid(hit.frameHeader(0));

      std::vector<unsigned char>& buffer = pmt_buffer_map[id];

      int nsamples_in_previous_frame = 0;

      for (unsigned int i = 0; i < hit.nFrames(); ++i) {

        buffer.insert(
            buffer.end(),
            static_cast<char const*>(
                static_cast<void const*>(
                    &hit.frameHeader(i))),
            static_cast<char const*>(
                static_cast<void const*>(
                    &hit.frameHeader(i) + 1)));

        buffer.insert(
            buffer.end(),
            static_cast<char const*>(
                static_cast<void const*>(
                    &hit[nsamples_in_previous_frame])),
            static_cast<char const*>(
                static_cast<void const*>(
                    &hit[nsamples_in_previous_frame] + getDFHNSamples(hit.frameHeader(i)))));

        nsamples_in_previous_frame += getDFHNSamples(hit.frameHeader(i));
      }
    }
  }
  return pmt_buffer_map;
}

std::size_t send(
    std::map<int, std::vector<unsigned char> >::const_iterator begin,
    std::map<int, std::vector<unsigned char> >::const_iterator end,
    std::size_t ts_id,
    std::size_t sector_id,
    int npmt_sect,
    network::Producer& socket);

Configuration make_offline_datacard(
    int n_sectors,
    int n_worker_threads,
    boost::filesystem::path const& detfile,
    boost::filesystem::path const& plugin_path);

}
}

#endif // TRIDAS_OFFLINE_TRIGSIM_DETAILS_HPP
