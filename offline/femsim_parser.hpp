#ifndef TRIDAS_OFFLINE_FEMSIM_PARSER_HPP
#define TRIDAS_OFFLINE_FEMSIM_PARSER_HPP

#include "f_dataformat_p3.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <string>
#include <istream>
#include <ostream>
#include <boost/chrono.hpp>
#include <stdexcept>

template<typename RawData>
std::size_t write(std::ostream& out, RawData const* data, std::size_t n = 1)
{
  std::size_t const size = sizeof(RawData) * n;

  std::ostream::pos_type const initialp = out.tellp();

  out.write(
      static_cast<char const*>(
          static_cast<void const*>(data)),
      size);

  std::ostream::pos_type const finalp = out.tellp();

  if (static_cast<std::size_t>(finalp - initialp) != size) {
    throw std::runtime_error("Impossible to write the correct size");
  }

  return size;
}

namespace tridas {
namespace femsim {

template<typename Geometrician>
tridas::phase3::DataFrameHeader make_dfh(
    int pmt_id,
    int charge,
    int nsamples,
    tridas::phase3::fine_time time,
    Geometrician& geo)
{
  tridas::phase3::DataFrameHeader dfh;

  geo(pmt_id - 1, dfh);

  dfh.SyncBytes = 21930;

  tridas::phase3::setDFHFullTime(time, dfh);

  dfh.NDataSamples = nsamples;
  dfh.FifoFull = 0;
  dfh.Charge = charge;
  dfh.FragFlag = 0; // Not fragmented
  dfh.ZipFlag  = 0; // Uncompressed
  return dfh;
}

inline
std::pair<std::string, std::string> read_item(std::istream& input)
{
  std::string line;
  getline(input, line);

  std::string::size_type const p = line.find(":");

  // It would be good to check whether a colon is present or not,
  // but due to the evt file format each line must have one and only one.
  //if (p == std::string::npos) {
  //  return std::make_pair(line, "");
  //}

  assert(p != std::string::npos);

  return std::make_pair(line.substr(0, p), line.substr(p + 1));
}

template<typename Geometrician>
std::size_t str2frame(std::string const& str, std::ostream& out, int nsec, Geometrician& geo)
{
  std::istringstream iss(str);

  // Content of a hit_fem:
  //  - Photon ID
  //  - PMT ID, in range [1 - NPMTDetector[
  //  - time in 10ns unit
  //  - time in 500us unit
  //  - parity, -> t += p * 5ns
  //  - total charge in ADC counts
  //  - number of samples (N)
  //  - N x sample in ADC counts

  int photon_id = 0;
  int pmt_id = 0;
  int64_t t10ns = 0;
  int64_t t500us = 0;
  int parity = 0;
  int charge = 0;
  int nsamples = 0;

  iss >> photon_id
      >> pmt_id
      >> t10ns
      >> t500us
      >> parity
      >> charge
      >> nsamples;

  assert(parity < 2 && parity >= 0);


  typedef boost::chrono::duration<int64_t, boost::ratio<1, 2000> > T500usec;

  tridas::phase3::fine_time const t =
      T5nsec(2 * t10ns + parity)
      + T500usec(t500us)
      + boost::chrono::seconds(nsec);

  tridas::phase3::DataFrameHeader const dfh = make_dfh(pmt_id, charge, nsamples, t, geo);

  std::vector<int16_t> samples(nsamples);
  for (int i = 0; i < nsamples; ++i) {
    iss >> samples[i];
  }

  write(out, &dfh);
  write(out, &samples.front(), nsamples);

  return sizeof(dfh) + nsamples * sizeof(int16_t);
}

boost::property_tree::ptree read_sim_header(std::istream& input);

template<typename Geometrician>
void convert_event(std::istream& input, std::ostream& output, Geometrician& geo)
{
  std::pair<std::string, std::string> const head = read_item(input);

  assert(
      head.first == "start_event"
      && "Position of the evt file not pointing to an event");

  std::istringstream iss(head.second);

  tridas::phase3::TS_t ts_id;

  iss >> ts_id;

  std::vector<std::string> hits;

  while (true) {
    std::pair<std::string, std::string> const item = read_item(input);

    if (item.first == "hit_fem") {
      hits.push_back(item.second);
    } else if (item.first == "end_event") {
      break;
    }
  }

  tridas::phase3::TimeSliceHeader ts_header = {ts_id, 1, 0};
  tridas::phase3::TEHeaderInfo event_header;

  // Set event_header
  event_header.EventTag = 12081972;
  event_header.EventID = 0;
  event_header.EventL = 0;
  event_header.nHit = hits.size();
  event_header.StartTime5ns = ts_id * 1000000000;
  event_header.TSCompleted = 1;
  memset(event_header.nseeds, 0, tridas::phase3::MAX_TRIGGERS_NUMBER);
  memset(event_header.plugin_trigtype, 0, tridas::phase3::MAX_PLUGINS_NUMBER);
  memset(event_header.plugin_nseeds, 0, tridas::phase3::MAX_PLUGINS_NUMBER);

  std::ostream::pos_type const begin_ts = output.tellp();

  write(output, &ts_header);
  write(output, &event_header);

  std::size_t size = 0;

  for (unsigned int i = 0; i < hits.size(); ++i) {
    size += str2frame(hits[i], output, ts_id, geo);
  }

  std::ostream::pos_type const end_ts = output.tellp();

  ts_header.TS_size = sizeof(ts_header) + sizeof(event_header) + size;
  event_header.EventL = sizeof(event_header) + size;

  output.seekp(begin_ts);

  write(output, &ts_header);
  write(output, &event_header);

  output.seekp(end_ts);
}

boost::property_tree::ptree read_metadata(std::istream& input);

} // ns fem
} // ns tridas

#endif // TRIDAS_OFFLINE_FEMSIM_PARSER_HPP
