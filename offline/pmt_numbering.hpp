#ifndef TRIDAS_OFFLINE_PMT_NUMBERING_HPP
#define TRIDAS_OFFLINE_PMT_NUMBERING_HPP

#include <cassert>
#include <tridas_dataformat.hpp>

namespace tridas {
namespace strict_numbering {

class PmtId
{
  int m_pmt_per_floor;
  int m_floor_per_tower;

 public:

  PmtId(int pmt_per_floor, int floor_per_tower)
    :
    m_pmt_per_floor(pmt_per_floor),
    m_floor_per_tower(floor_per_tower)
  {}

  int operator ()(DataFrameHeader const& header) const
  {
    assert(header.EFCMID > 0);
    return header.PMTID
        + (header.TowerID * m_floor_per_tower + header.EFCMID - 1) * m_pmt_per_floor;
  }
};

class PmtNumbers
{
  int m_n_pmt_floor;
  int m_n_floor_tower;

 public:

  PmtNumbers(int n_pmt_floor, int n_floor_tower)
    :
    m_n_pmt_floor(n_pmt_floor),
    m_n_floor_tower(n_floor_tower)
  {}

  void operator ()(int pmt_id, DataFrameHeader& dfh) const
  {
    // WARNING the absolute PMT id numbering in simulations starts from 1,
    // while in electronics it starts from 0. Lower it by one before
    // passing it to this function when converting from .evt file.

    dfh.TowerID = pmt_id / (m_n_floor_tower * m_n_pmt_floor);

    // FCM ID ranges in [1, 14]
    dfh.EFCMID = pmt_id / m_n_pmt_floor + 1 - m_n_floor_tower * dfh.TowerID;

    dfh.PMTID = pmt_id - m_n_pmt_floor * (dfh.EFCMID -1 + dfh.TowerID * m_n_floor_tower);
  }
};

} // ns strict_numbering
}
#endif // TRIDAS_OFFLINE_PMT_NUMBERING_HPP
