### README

* `cd tridas/ci/boost158/`
* `docker build -t boost158/${label} -f Dockerfile.${label} .`
* `docker run --rm -i -v ${WORKSPACE}:/mnt boost158/${label} sh /mnt/tridas/ci/boost158/build.sh`