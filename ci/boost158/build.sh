#!/bin/sh

usage()
{
cat << EOF
usage: $0 options

OPTIONS:
   -h      Show this message
   -d      Set the output directory (default is /mnt)
EOF
}

OUTPUT_DIR=/mnt

# check arguments
while getopts "hd:" OPTION
do
     case ${OPTION} in
         h)
             usage
             exit
             ;;
         d)
             OUTPUT_DIR=${OPTARG}
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

# Wget boost
wget http://sourceforge.net/projects/boost/files/boost/1.58.0/boost_1_58_0.tar.bz2/download

# Extract and enter
tar --bzip2 -xf download
cd boost_1_58_0

mkdir /boost-1.58

# Bootstrap and install
./bootstrap.sh --without-libraries=python,iostreams
./b2 address-model=64 architecture=x86 --prefix=/boost-1.58 --build-type=minimal --build-dir=build link=shared install

cd ..

# prepare a dev tarball for distribution that includes everything
tar czf ${OUTPUT_DIR}/boost-1.58-dev.tgz /boost-1.58

# prepare a runtime tarball for distribution that includes only shared libraries
find /boost-1.58/lib -name \*.so\* -print0 | tar czf ${OUTPUT_DIR}/boost-1.58.tgz --null -T -