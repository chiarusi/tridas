### README

This Dockerfile allows to run royweb inside a docker container.

1. Build and tag the image with "tridas/royweb":
    * `docker build -t tridas/royweb .`
2. Run the container as a daemon:
    * `docker run -d -t --net=host tridas/royweb royweb --ip=<IP> --port=<PORT>`