### README

* `cd tridas/ci/tridas/`
* `docker build -t tridas/${label} -f Dockerfile.${label} .`
* `docker run --rm -i -e GIT_COMMIT=${GIT_COMMIT} -v ${WORKSPACE}:/mnt tridas/${label} sh /mnt/tridas/ci/tridas/build.sh`