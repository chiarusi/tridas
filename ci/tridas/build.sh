#!/bin/sh -x

assert () {
        [ "$2" = "$3" ] && return
        >&2 echo "$1: assert failed: $2 == $3"
        exit 1
}

usage() {
cat << EOF
usage: $0 options

OPTIONS:
   -h      Show this message
   -w      Set the workspace directory (default is /mnt)
   -d      Compile in Debug mode (default is Release)
   -c      Compile using C++11 support
EOF
}

BUILD_TYPE=Release
USE_CPP11=0
WORKSPACE=/mnt

# check arguments
while getopts "hdcw:" OPTION
do
     case ${OPTION} in
         h)
             usage
             exit
             ;;
         d)
             BUILD_TYPE=Debug
             ;;         
         c)
             USE_CPP11=1
             ;;
         w)
             WORKSPACE=${OPTARG}
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

cd ${WORKSPACE}

# check artifacts
assert "check boost artifacts" "$(ls boost-*-dev*.tgz 2>/dev/null | wc -l)" "1"
assert "check zmq artifacts" "$(ls zeromq-*-dev*.tgz 2>/dev/null | wc -l)" "1"
assert "check libssh2 artifacts" "$(ls libssh2-*-Linux*.tar.gz 2>/dev/null | wc -l)" "1"

# tar env vars
boost_tar=${PWD}/$(ls boost-*-dev*.tgz 2>/dev/null)
zmq_tar=${PWD}/$(ls zeromq-*-dev*.tgz 2>/dev/null)
libssh2_tar=${PWD}/$(ls libssh2-*-Linux*.tar.gz 2>/dev/null)

# set root vars
boost_root=${PWD}/boost
zmq_root=${PWD}/zmq
libssh2_root=${PWD}/libssh2

# create root dirctories
mkdir ${boost_root}
mkdir ${zmq_root}
mkdir ${libssh2_root}

# untar artifacts
tar xvf ${boost_tar} -C ${boost_root} --strip-components 1
tar xvf ${zmq_tar} -C ${zmq_root} --strip-components 1
tar xvf ${libssh2_tar} -C ${libssh2_root} --strip-components 1

cd tridas
mkdir -p build
cd build

version=$(date +%Y%m%dT%H%M%S)-$(echo ${GIT_COMMIT} | cut -c 1-6)
cmake -DBOOST_ROOT=${boost_root} -DZMQ_ROOT=${zmq_root} -DLibssh2_DIR=${libssh2_root}/lib/cmake/libssh2 -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DTRIDAS_USE_CPP11=${USE_CPP11} -DCPACK_PACKAGE_VERSION=${version} ..
make VERBOSE=1
make test
make package

mv TriDAS-${version}-Linux.tar.gz ${WORKSPACE}/
