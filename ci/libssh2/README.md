### README

* `cd tridas/ci/libssh2/`
* `docker build -t libssh2/${label} -f Dockerfile.${label} .`
* `docker run --rm -i -v ${WORKSPACE}:/mnt libssh2/${label} sh /mnt/tridas/ci/libssh2/build.sh`