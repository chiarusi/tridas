#!/bin/sh

usage()
{
cat << EOF
usage: $0 options

OPTIONS:
   -h      Show this message
   -d      Set the output directory (default is /mnt)
EOF
}

OUTPUT_DIR=/mnt

# check arguments
while getopts "hd:" OPTION
do
     case ${OPTION} in
         h)
             usage
             exit
             ;;
         d)
             OUTPUT_DIR=${OPTARG}
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

# Wget libssh2
wget https://github.com/libssh2/libssh2/archive/libssh2-1.7.0.tar.gz

# Extract and enter
tar zxfv libssh2-1.7.0.tar.gz
cd libssh2-libssh2-1.7.0
mkdir build && cd build

# Configure and install
cmake \
  -DBUILD_SHARED_LIBS=ON \
  -DBUILD_EXAMPLES=OFF \
  -DBUILD_TESTING=OFF \
  ..
make package

mv libssh2*.tar.gz ${OUTPUT_DIR}/