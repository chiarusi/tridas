### README

* `cd tridas/ci/zeromq411/`
* `docker build -t zeromq411/${label} -f Dockerfile.${label} .`
* `docker run --rm -i -v ${WORKSPACE}:/mnt zeromq411/${label} sh /mnt/tridas/ci/zeromq411/build.sh`