#!/bin/sh

usage()
{
cat << EOF
usage: $0 options

OPTIONS:
   -h      Show this message
   -d      Set the output directory (default is /mnt)
EOF
}

OUTPUT_DIR=/mnt

# check arguments
while getopts "hd:" OPTION
do
     case ${OPTION} in
         h)
             usage
             exit
             ;;
         d)
             OUTPUT_DIR=${OPTARG}
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

# Wget zeromq
wget http://download.zeromq.org/zeromq-4.1.1.tar.gz

# Extract and enter
tar zxfv zeromq-4.1.1.tar.gz && mv zeromq-4.1.1 zeromq-4.1.1-src
BASE_DIR=${PWD}
cd zeromq-4.1.1-src

# Configure and install
mkdir ${BASE_DIR}/zeromq-4.1.1
./configure --without-libsodium --prefix=${BASE_DIR}/zeromq-4.1.1
make install

# Get the C++ wrapper (last working version tested)
# wget https://raw.githubusercontent.com/zeromq/cppzmq/master/zmq.hpp
wget https://raw.githubusercontent.com/zeromq/cppzmq/e7c20935819fa501c73c91236d4dcae44aadb29f/zmq.hpp
mv zmq.hpp ${BASE_DIR}/zeromq-4.1.1/include/

# prepare a dev tarball for distribution that includes everything
tar czvf ${OUTPUT_DIR}/zeromq-4.1.1-dev.tgz ${BASE_DIR}/zeromq-4.1.1